<?php

use App\Models\TextBlock;
use App\Services\SettingsService;
use App\Models\SiteSetting;
use App\Helpers\ViewHelper;


if (!function_exists('setting')) {
    function setting($code): SiteSetting
    {
        $settings = app()->make(SettingsService::class);
        return  $settings->get($code);
    }
}

if (!function_exists('textBlock')) {
    function textBlock($code): TextBlock
    {
        $blocks = app()->make(\App\Services\TextBlocksService::class);
        return  $blocks->get($code);
    }
}

if (!function_exists('settingSection')) {
    function settingSection($code): \Illuminate\Support\Collection
    {
        $settings = app()->make(SettingsService::class);
        return  $settings->section($code);
    }
}

if (!function_exists('getSymbol')) {
    function getSymbol($code, string $additionalClass = ''): string
    {
        return ViewHelper::getSymbol($code, $additionalClass);
    }
}

if (!function_exists('vueTag')) {
    function vueTag(string $class = 'vue-', array $data = [], string $tag = 'div', string $content = ''): string
    {
        return ViewHelper::vueTag($class, $data, $tag, $content);
    }
}

if (!function_exists('clearPhone')) {
    function clearPhone(?string $phone, string $prefix = '+7'): string
    {
        return ViewHelper::clearPhone($phone, $prefix);
    }
}

if (!function_exists('getImage')) {
    function getImage(string $path): string
    {
        return ViewHelper::getImage($path);
    }
}
