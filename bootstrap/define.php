<?php

define('P_DR', realpath(__DIR__ . '/../'));
define("P_RESOURCE", "/resources/");
define("P_PUBLIC", "/public/");
define("P_FONTS", "/fonts/");
define("P_PUBLIC_PATH", P_DR . P_PUBLIC);
define('P_IMAGES', '/images/');
define('P_PICTURES', '/storage/public/');
define('P_BUILD', '/build/');
define('SYMBOLS_SVG', P_BUILD . 'symbols.svg');
define('P_BLANK_IMG', '/images/empty.svg');

define('CACHE_TIME', 3600 * 24 * 30);
define('EXT_ALLOWED_FILES', 'jpg,jpeg,png,pdf');
