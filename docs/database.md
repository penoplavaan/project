# Схема БД. Смотреть через сервис https://dbdiagram.io/d

```
Table users as U {
  id int [pk, increment]
  role_id int
  email varchar
}

Table attachments as A {
  id int [pk, increment]
  name varchar
  original_name varchar
  path varchar
  mime varchar
  size int
}

Table cook_data as CD {
  id int [pk, increment]
  user_id int [ref: > U.id]
  speciality_id int [ref: > CS.id]
  name varchar
  active tinyint
  verified tinyint
  passport_verified tinyint
  sanitary_verified tinyint
  admin_comment text
}

Table cook_to_categories as C2C {
  id int [pk, increment]
  cook_id int [ref: > CD.id]
  category_id int [ref: > DC.id]
}

Table cook_speciality as CS {
  id int [pk, increment]
  title varchar
}

Table cities {
  id int [pk, increment]
  name varchar
}

Table dishes as D {
  id int [pk, increment]
  cook_id int [ref: > CD.id]
  category_id int [ref: > DC.id]
  name varchar
  price int
}

Table dish_categories as DC {
  id int [pk, increment]
  name varchar
  parent_id int [ref: > DC.id]
}

Table text_pages {
  id int [pk, increment]
  url varchar
  title varchar
  text varchar
}

Table text_blocks {
  id int [pk, increment]
  code varchar
  title varchar
  text varchar
}

Table site_settings {
  id int [pk, increment]
  name varchar
  code varchar
  value varchar
  text text
}

Table main_promo_block_slides {
  id int [pk, increment]
  sort int
  name varchar
  code varchar
  value varchar
  dish_id int [ref: > D.id]
}

Table main_our_cooks {
  id int [pk, increment]
  cook_id int [ref: > CD.id]
  sort int
}

```
