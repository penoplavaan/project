<?php

namespace App\Orchid;

class OrchidDetector 
{
    protected static $isOrchid = false;

    public static function setIsOrchid() 
    {
        static::$isOrchid = static::isOrchidRoute();
    }

    public static function isOrchid(): bool 
    {
        return static::$isOrchid;
    }

    public static function isOrchidRoute(): bool 
    {
        $url = request()->url();

        $adminRoot = config('platform.prefix');

        return strpos($url, $adminRoot) !== false;
    }
}
