<?php

namespace App\Orchid\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteSettingsRequest extends FormRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'site-settings.code'         => ['nullable', 'string'],
            'site-settings.active'       => ['nullable', 'boolean'],
            'site-settings.text'         => ['nullable', 'string', 'max:255'],
            'site-settings.sort'         => ['nullable', 'integer'],
            'site-settings.big_text'     => ['nullable', 'string'],
            'site-settings.color'        => ['nullable', 'string', 'max:255'],
            'site-settings.section_id'   => ['nullable', 'numeric'],
            'site-settings.attachment'   => ['nullable', 'array'],
            'site-settings.attachment.*' => ['numeric'],
        ];
    }
}
