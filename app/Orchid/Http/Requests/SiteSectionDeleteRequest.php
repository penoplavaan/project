<?php

namespace App\Orchid\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteSectionDeleteRequest extends FormRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'site-sections.id' => ['required', 'string', 'exists:site_settings_sections,id'],
        ];
    }
}
