<?php

namespace App\Orchid\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiteSectionsRequest extends FormRequest
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'site-sections.code'                => ['required', 'string'],
            'site-sections.name'                => ['required', 'string'],
            'site-sections.parent_section_id' => ['nullable', 'string'],
        ];
    }
}
