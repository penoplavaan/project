<?php

namespace App\Orchid\Http\Controllers;


use App\Services\TextComponentService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TextController extends Controller
{

    public function componentList(Request $request, TextComponentService $textComponentService): JsonResponse
    {
        $componentName = $request->query('componentName', "");

        $componentList = $textComponentService->getComponentList();

        $componentListForSelect = array_map(function($component) {
            return ['text' => $component['title'], 'value' => $component['name']];
        }, $componentList);
        $componentListForSelect = array_values($componentListForSelect);

        if (array_key_exists($componentName, $componentList)) {
            $selectedComponent = $componentList[$componentName];
        } else {
            $componentName = "";
            $selectedComponent = reset($componentList);
        }

        $componentParams = [];

        if ($selectedComponent) {
            $componentName = $selectedComponent['name'];
            $componentParams = $textComponentService->getComponentParams($componentName) ?: [];
        }

        return response()->json([
            'componentList' => $componentListForSelect,
            'selectedComponent' => $componentName,
            'selectedComponentParams' => $componentParams,
        ]);
    }

    public function templateList(): JsonResponse
    {
        $templateList = [
            [
                'title' => 'Картинка с подписью',
                'description' => 'Картинка с подписью',
                'view' => 'orchid::text-templates.image',
            ],
            [
                'title' => 'Таблица с заголовком',
                'description' => 'Таблица с заголовком',
                'view' => 'orchid::text-templates.table',
            ],
            [
                'title' => 'Цитата',
                'description' => 'Цитата с заполненными типовыми данными',
                'view' => 'orchid::text-templates.quote',
            ],
            [
                'title' => 'Пример заполненной текстовой страницы',
                'description' => 'Тестовая страница с примером всех типов блоков',
                'view' => 'orchid::text-templates.text-template',
            ],
        ];

        foreach ($templateList as $key => $template) {
            if (!array_key_exists('view', $template)) continue;

            try {
                $content = view($template['view'])->toHtml();
            } /** @noinspection PhpUnusedLocalVariableInspection */ catch(Exception $e) {
                $content = "";
            }

            $templateList[$key]['content'] = $content;
            unset($templateList[$key]['view']);
        }

        $templateList = array_filter($templateList, function($template) {
            return is_string($template['content']) && mb_strlen($template['content']);
        });
        return response()->json($templateList);
    }
}
