<?php

declare(strict_types=1);

namespace App\Orchid;

use App\Models\SiteSettingsSection;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    public function register()
    {
        parent::register();

        OrchidDetector::setIsOrchid();
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        $items = array_merge(
            [
                Menu::make('На сайт')
                    ->icon('monitor')
                    ->target('_blank')
                    ->url('/'),

                Menu::make(__('Users'))
                    ->icon('user')
                    ->route('platform.systems.users')
                    ->permission('platform.systems.users')
                    ->title(__('Access rights')),

                Menu::make(__('Roles'))
                    ->icon('lock')
                    ->route('platform.systems.roles')
                    ->permission('platform.systems.roles'),

                Auth::user()->hasAccess('content.settings')
                    ? Menu::make('Настройки сайта')
                        ->icon('settings')
                        ->route('platform.site-settings.index')
                        ->list($this->getSettingsSubMenu())
                    : null,

                Menu::make('Меню сайта')
                    ->icon('menu')
                    ->route('platform.menu')
                    ->permission('platform.systems.roles'),

                Auth::user()->hasAccess('content.main')
                    ? Menu::make('Блоки на главной странице')
                        ->icon('map')
                        ->route('platform.resource.list', ['resource' => 'main-second-promo-resources'])
                        ->list($this->getMainPageMenu())
                        ->title('Таблицы и сущности')
                    : null,

                Auth::user()->hasAccess('content.for-cooks')
                    ? Menu::make('Блоки на "Поварам"')
                        ->icon('map')
                        ->route('platform.resource.list', ['resource' => 'for-cook-faq-resources'])
                        ->list($this->getForCooksPageMenu())
                    : null,

                Auth::user()->hasAccess('content.refs')
                    ? Menu::make('Заказы')
                    ->route('platform.resource.list', ['resource' => 'order-resources'])
                    : null,

                Auth::user()->hasAccess('content.refs')
                    ? Menu::make('Справочники')
                        ->route('platform.resource.list', ['resource' => 'dish-category-resources'])
                        ->list($this->getReferenceMenu())
                    : null,

                Auth::user()->hasAccess('content.refs')
                    ? Menu::make('Обратная связь')
                    ->route('platform.resource.list', ['resource' => 'feedback-resources'])
                    ->icon('friends')
                    : null,
            ],
        );

        return array_filter($items);
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    public function getExampleMenu(): array {
        return [
            Menu::make('Example screen')
                ->sort(5000)
                ->icon('monitor')
                ->route('platform.example')
                ->title('Navigation')
                ->badge(function () {
                    return 6;
                }),

            Menu::make('Dropdown menu')
                ->sort(5000)
                ->icon('code')
                ->list([
                    Menu::make('Sub element item 1')->icon('bag'),
                    Menu::make('Sub element item 2')->icon('heart'),
                ]),

            Menu::make('Basic Elements')
                ->sort(5000)
                ->title('Form controls')
                ->icon('note')
                ->route('platform.example.fields'),

            Menu::make('Advanced Elements')
                ->sort(5000)
                ->icon('briefcase')
                ->route('platform.example.advanced'),

            Menu::make('Text Editors')
                ->sort(5000)
                ->icon('list')
                ->route('platform.example.editors'),

            Menu::make('Overview layouts')
                ->sort(5000)
                ->title('Layouts')
                ->icon('layers')
                ->route('platform.example.layouts'),

            Menu::make('Chart tools')
                ->sort(5000)
                ->icon('bar-chart')
                ->route('platform.example.charts'),

            Menu::make('Cards')
                ->sort(5000)
                ->icon('grid')
                ->route('platform.example.cards')
                ->divider(),

            Menu::make('Documentation')
                ->sort(5000)
                ->title('Docs')
                ->icon('docs')
                ->url('https://orchid.software/en/docs'),

            Menu::make('Changelog')
                ->sort(5000)
                ->icon('shuffle')
                ->url('https://github.com/orchidsoftware/platform/blob/master/CHANGELOG.md')
                ->target('_blank')
                ->badge(function () {
                    return Dashboard::version();
                }, Color::DARK()),
        ];
    }

    /**
     * @return Menu[]
     */
    public function getSettingsSubMenu(): array
    {
        $siteSections = SiteSettingsSection::all();

        $items = [];

        foreach ($siteSections as $section) {
            $items[$section->parent_section_id ?? 0][] = $section->toArray();
        }

        return $this->buildTreeSubMenu($items);
    }

    /**
     * @return Menu[]
     */
    public function getMainPageMenu(): array
    {
        return [
            Menu::make('Главная: Отзывы')
                ->route('platform.resource.list', ['resource' => 'main-review-resources'])
                ->icon('map'),

            /*Menu::make('Главная: Слайды') // Старые промо-слайды - отключены
                ->route('platform.resource.list', ['resource' => 'main-promo-slide-resources'])
                ->icon('map'),*/

            Menu::make('Главная: Слайды')
                ->route('platform.resource.list', ['resource' => 'main-second-promo-resources'])
                ->icon('map'),

            Menu::make('Главная: Подборки')
                ->route('platform.resource.list', ['resource' => 'main-dish-compilation-resources'])
                ->icon('map'),

            Menu::make('Главная: Список категорий')
                ->route('platform.resource.list', ['resource' => 'main-promo-category-compilation-resources'])
                ->icon('map'),
        ];
    }

    /**
     * @return Menu[]
     */
    public function getForCooksPageMenu(): array
    {
        return [
            Menu::make('Поварам: вопросы')
                ->route('platform.resource.list', ['resource' => 'for-cook-faq-resources'])
                ->icon('map'),

            Menu::make('Поварам: как работает')
                ->route('platform.resource.list', ['resource' => 'for-cook-how-work-resources'])
                ->icon('map'),

            Menu::make('Поварам: отзывы')
                ->route('platform.resource.list', ['resource' => 'for-cook-review-resources'])
                ->icon('map'),

            Menu::make('Поварам: поддержка')
                ->route('platform.resource.list', ['resource' => 'for-cook-support-resources'])
                ->icon('map'),

            Menu::make('Поварам: фишки')
                ->route('platform.resource.list', ['resource' => 'for-cook-feature-resources'])
                ->icon('map'),
        ];
    }

    /**
     * @return Menu[]
     */
    public function getReferenceMenu(): array
    {
        return [
            Menu::make('Разделы меню')
                ->route('platform.resource.list', ['resource' => 'dish-category-resources']),
            Menu::make('Специализации поваров')
                ->route('platform.resource.list', ['resource' => 'cook-speciality-resources']),
            Menu::make('Города')
                ->route('platform.resource.list', ['resource' => 'city-resources']),
            Menu::make('Категории обратной связи')
                ->route('platform.resource.list', ['resource' => 'feedback-category-resources']),
            Menu::make('Теги блюд')
                ->route('platform.resource.list', ['resource' => 'tags-resources']),
            Menu::make('Лейблы')
                ->route('platform.resource.list', ['resource' => 'label-resources']),
        ];
    }

    /**
     * @param $elements
     * @param int $parentId
     * @return Menu[]
     */
    public function buildTreeSubMenu($elements, int $parentId = 0): array
    {
        $result = [];

        if (is_array($elements) && isset($elements[$parentId])) {
            foreach ($elements[$parentId] as $element) {
                $result[] = Menu::make($element['name'])
                    ->route(
                        'platform.site-settings.index',
                        ['section' => $element['id']]
                    )
                    ->icon('folder')
                    ->list($this->buildTreeSubMenu($elements, $element['id']) ?? null)
                    ->active('platform.site-settings*');
            }
        }

        return $result;
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }
}
