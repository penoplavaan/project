<?php

namespace App\Orchid\Resources;

use App\Models\FeedbackCategory;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\TD;

class FeedbackCategoryResource extends BaseResource {

    public static $model = FeedbackCategory::class;

    public static function permission(): ?string {
        return 'content.refs';
    }

    public static function displayInNavigation(): bool {
        return false;
    }

    public function fields(): array {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('name')->title('Название')->required()->maxlength(250),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),
        ];
    }

    public function columns(): array {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))->sort()->defaultHidden()->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))->sort()->defaultHidden()->component(UpdatedAt::class),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn(FeedbackCategory $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('sort', __('admin.sort'))->sort(),
            TD::make('name', 'Название')->sort(),
        ];
    }

    public function rules(Model $model): array {
        return [
            'active' => ['boolean'],
            'sort'   => ['required', 'integer'],
            'name'   => ['required', 'string', 'max:250'],
        ];
    }

    public function onSave(ResourceRequest $request, FeedbackCategory $model) {
        $fields = $request->validated('model');

        // todo forceFill заменить на fill и проверить, что всё работает
        $model->forceFill($fields)->save();
    }
}
