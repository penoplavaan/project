<?php

namespace App\Orchid\Resources;


use App\Models\Callback;
use App\Models\PromoCard;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\TD;

class CallbackResource extends BaseResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Callback::class;

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            Input::make('created_at')
                ->title(__('admin.created_at'))
                ->disabled(true),

            Input::make('phone')->title('Телефон')->required()->maxlength(250),
            Relation::make('promo_card_id')
                ->fromModel(PromoCard::class, 'name')
                ->title(__('admin.promo-card-title-1')),

        ];
    }

    public static function icon(): string
    {
        return 'call-out';
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->render(function ($model) {
                    return $model->created_at->toDateTimeString();
                }),

            TD::make('updated_at', __('admin.updated_at'))
                ->render(function ($model) {
                    return $model->updated_at->toDateTimeString();
                })
                ->defaultHidden(),

            TD::make('phone', 'Телефон'),

            TD::make('categories', __('admin.promo-card-title-1'))
                ->render(fn (Callback $model) => $model->promoCard?->name)
        ];
    }

}
