<?php

namespace App\Orchid\Resources;

use App\Helpers\BaseHelper;
use App\Models\DeliveryType;
use App\Models\Order;
use App\Models\PaySystem;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\OrderDishes;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\TD;

class OrderResource extends BaseResource
{

    public static $model = Order::class;

    public static function permission(): ?string
    {
        return 'orders-list.read';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            OrderDishes::make('id'),
            Input::make('client_name')->title('ФИО покупателя')->required(),
            Input::make('client_phone')->title('Телефон покупателя')->required(),
            Input::make('recipient_name')->title('Получатель: ФИО'),
            Input::make('recipient_phone')->title('Получатель: телефон'),

            Relation::make('delivery_type_id')
                ->title('Способ доставки')
                ->fromModel(DeliveryType::class, 'name')
                ->required(),

            Input::make('delivery_date')->title('Дата доставки'),
            Input::make('delivery_time')->title('Время доставки'),

            Relation::make('pay_system_id')
                ->title('Способ оплаты')
                ->fromModel(PaySystem::class, 'name')
                ->required(),

            TextArea::make('comment')->title('Комментарий покупателя')
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id')->sort(),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden(false)
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('user_id', 'Покупатель')
                ->render(function (Order $model) {
                    if (empty($model->user)) {
                        return empty($model->user_id) ? '<i>Неавторизованный</i>' : '???';
                    }
                    $url = route('platform.systems.users.edit', ['user' => $model->user_id]);
                    $name = $model->user->getFullName();
                    return BaseHelper::truncateString($name, 50) . ' <a href="' . $url . '">[' . $model->user_id . ']</a>';
                })
                ->sort()
                ->filter(),

            TD::make('client_phone', 'Телефон')
                ->sort()
                ->filter(),

            TD::make('cook_id', 'Повар')
                ->render(function (Order $model) {
                    if (empty($model->cook)) {
                        return '';
                    }
                    $url = route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $model->cook_id]);
                    $name = $model->cook->user->getFullName();
                    return BaseHelper::truncateString($name, 50) . ' <a href="' . $url . '">[' . $model->cook_id . ']</a>';
                })
                ->sort()
                ->filter(),

            TD::make('id', 'Количество блюд')
                ->render(function (Order $model) {
                    return $model->orderToDishes->count();
                }),

            TD::make('payed', 'Оплачен')
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(function (Order $model) {
                    return $model->payed ? 'Да' : 'Нет';
                }),

            TD::make('sum', 'Сумма')
                ->sort()
                ->filter('numberRange'),
        ];
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function rules(Model $model): array
    {
        return [
            'client_name'      => ['required', 'string', 'max:250'],
            'client_phone'     => ['required', 'string', 'max:20'],
            'recipient_name'   => ['nullable', 'string', 'max:250'],
            'recipient_phone'  => ['nullable', 'string', 'max:20'],
            'delivery_type_id' => ['required'],
            'delivery_date'    => ['nullable'],
            'delivery_time'    => ['nullable'],
            'pay_system_id'    => ['required'],
            'comment'          => ['nullable', 'string', 'max:2000'],
        ];
    }

    public function onSave(ResourceRequest $request, Order $model)
    {
        // Ничего не сохраняем - редактирование заказов через админку недоступно (на текущем этапе развития проекта)
    }
}
