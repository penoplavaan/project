<?php

namespace App\Orchid\Resources;

use App\Models\Dish;
use App\Models\MainDishCompilation;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\TD;

class MainDishCompilationResource extends BaseResource
{

    public static $model = MainDishCompilation::class;

    public static function permission(): ?string
    {
        return 'content.main';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(true),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),

            Input::make('name')->title('Название')->required()->maxlength(250),

            Input::make('url')->title('Ссылка')->maxlength(250),

            Relation::make('dishes.')
                ->fromModel(Dish::class, 'name')
                ->applyScope('active')
                ->displayAppend('orchidRelCookName')
                ->multiple()
                ->title('Блюдо'),
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (MainDishCompilation $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('name', __('admin.name')),
        ];
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function rules(Model $model): array
    {
        return [
            'active'     => ['boolean'],
            'sort'       => ['required', 'integer'],
            'name'       => ['required', 'string', 'max:250'],
            'url'        => ['nullable', 'string', 'max:250'],
            'dishes'     => ['required', 'array'],
        ];
    }

    public function onSave(ResourceRequest $request, MainDishCompilation $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->dishes()->sync(
            $request->input('dishes', [])
        );
    }
}
