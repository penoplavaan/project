<?php

namespace App\Orchid\Resources;

use App\Models\ForCookFeature;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\Tinymce;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class ForCookFeatureResource extends BaseResource
{
    public static $model = ForCookFeature::class;

    public static function permission(): ?string
    {
        return 'content.for-cooks';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),
            Input::make('title')->title(__('admin.title'))->required()->maxlength(250),
            Tinymce::make('text')->title(__('admin.text'))->value('')->set('data-height', '400'),

            Upload::make('attachment')
                ->title('Иконка SVG')
                ->maxFiles(1)
                ->acceptedFiles('image/svg+xml')
                ->required()
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (ForCookFeature $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('title', __('admin.title')),

            TD::make('id', __('admin.preview'))
                ->render(function (ForCookFeature $model) {
                    /** @var \App\Helpers\Image $img */
                    if ($model->attachment && $model->attachment->count()) {
                        $img = $model->attachment[0]->getImage();
                        $src = $img->getUrl();
                        return '<img src="' . $src . '" alt="">';
                    }

                    return '';
                }),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'active' => ['boolean'],
            'sort'   => ['required', 'integer'],
            'title'  => ['required', 'string', 'max:250'],
            'text'   => ['required', 'string', 'max:50000'],

            'attachment' => ['required']
        ];
    }

    public function onSave(ResourceRequest $request, ForCookFeature $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
