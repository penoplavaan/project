<?php

namespace App\Orchid\Resources;

use App\Models\ForCookHowWork;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\Tinymce;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\TD;

class ForCookHowWorkResource extends BaseResource
{
    public static $model = ForCookHowWork::class;

    public static function permission(): ?string
    {
        return 'content.for-cooks';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),
            Input::make('title')->title(__('admin.title'))->required()->maxlength(250),
            Tinymce::make('text')->title(__('admin.text'))->value('')->set('data-height', '400'),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (ForCookHowWork $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('title', __('admin.title')),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'active' => ['boolean'],
            'sort'   => ['required', 'integer'],
            'title'  => ['required', 'string', 'max:250'],
            'text'   => ['required', 'string', 'max:50000'],
        ];
    }

    public function onSave(ResourceRequest $request, ForCookHowWork $model)
    {
        $fields = $request->validated('model');

        $fields['text'] ??= '';

        // todo forceFill заменить на fill и проверить, что всё работает
        $model->forceFill($fields)->save();
    }
}
