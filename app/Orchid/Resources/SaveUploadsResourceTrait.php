<?php

namespace App\Orchid\Resources;

use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Support\Facades\Alert;

trait SaveUploadsResourceTrait
{

    /**
     * Множественное сохранение файла в ресурсе
     *
     * @param $request
     * @param $element
     */
    public function onSave(ResourceRequest $request, Model $element)
    {
        $data = $request->validated();
        $element->fill($data['model'])->save();

        $element->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );

        Alert::info('Успешно сохранён');
    }
}
