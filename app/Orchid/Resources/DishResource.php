<?php

namespace App\Orchid\Resources;

use App\Models\Cook;
use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\Label;
use App\Models\Tag;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\DishCook;
use Illuminate\Support\Facades\Auth;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class DishResource extends BaseResource
{

    public static $model = Dish::class;

    public const SORT_NAMES = [
        1 => 'Первая Волна',
        2 => 'Вторая Волна',
        3 => 'По умолчанию',
        4 => 'Андердоги',
    ];

    public static function permission(): ?string
    {
        return 'content-dishes.read';
    }

    public function actions(): array
    {
        if (Auth::user()->hasAccess('content-dishes.write')) {
            return parent::actions();
        } else {
            return [];
        }
    }

    public static function icon(): string
    {
        return 'book-open';
    }

    public function fields(): array
    {
        abort_if(!Auth::user()->hasAccess('content-dishes.write'), 403);

        return [
            Group::make([
                DishCook::make('cook_id'),
            ]),

            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),

            Input::make('name')->title(__('admin.dish.name'))->required()->maxlength(250),

            Select::make('named_sort')
                ->title('Группа сортировки')
                ->options(static::SORT_NAMES),

            Relation::make('cook_id')
                ->fromModel(Cook::class, 'id')
                ->displayAppend('orchidRelUserName')
                ->applyScope('withUserModel')
                ->searchColumns('users.name')
                ->title(__('admin.dish.cook')),

            Relation::make('category_id')
                ->fromModel(DishCategory::class, 'title')
                ->title(__('admin.dish.category')),

            Relation::make('tags')->fromModel(Tag::class, 'name')->title('Тэги')->multiple(),

            Relation::make('additionalCategories')->fromModel(DishCategory::class, 'title')->title('Дополнительные категории')->multiple(),

            Relation::make('labels')->fromModel(Label::class, 'name')->title('Лейблы')->multiple(),

            Input::make('quantity')->title('Количество порций')->required()->maxlength(20),
            Input::make('weight')->title('Вес')->required()->maxlength(20),
            Input::make('storage_conditions')->title('Условия хранения')->maxlength(200),
            Input::make('delivery')->title('Условия доставки')->maxlength(200),
            Input::make('calories')->title('Калорийность')->maxlength(20),
            Input::make('protein')->title('Белки')->maxlength(20),
            Input::make('fats')->title('Жиры')->maxlength(20),
            Input::make('carbohydrates')->title('Углеводы')->maxlength(20),
            Input::make('cooking_time')->title('Время приготовления')->maxlength(20),
            Input::make('package')->title('Упаковка')->maxlength(20),


            Input::make('price')->title(__('admin.dish.price'))->required()->maxlength(12),

            Input::make('preview_text')->title(__('admin.dish.preview_text'))->required()->maxlength(999),

            Upload::make('attachment')
                ->title('Фотографии')
                ->maxFiles(10)
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (Dish $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('name', __('admin.dish.name'))
                ->filter()
                ->sort(),

            TD::make('cook_id', __('admin.dish.cook'))
                ->render(function (Dish $model) {
                    $url = route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $model->cook_id]);
                    if (empty($model->cook->user)) {
                        return '';
                    }

                    return $model->cook->user->getFullName() . ' <a href="' . $url . '">[' . $model->cook_id . ']</a>';
                })
            ->filter(),

            TD::make('category_id', __('admin.dish.category'))
                ->render(function (Dish $model) {
                    return ($model->category?->getTitle() ?? '') . ' [' . $model->category_id . ']';
                })
                ->filter()
//                ->filterOptions(DishCategory::all()->map(fn($el) => $el->title)->toArray())
            ,

            TD::make('id', __('admin.preview'))
                ->render(function (Dish $model) {
                    /** @var \App\Helpers\Image $img */
                    if ($model->attachment && $model->attachment->count()) {
                        $img = $model->attachment[0]->getImage();
                        $src = $img->resize('admin.preview')->getUrl();
                        return '<img src="' . $src . '" alt="">';
                    }

                    return '';
                }),


            TD::make('price', __('admin.dish.price'))
                ->sort()
                ->filter('numberRange'),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'name'                 => ['required', 'string', 'max:250'],
            'active'               => ['boolean'],
            'cook_id'              => ['required', 'integer'],
            'category_id'          => ['required', 'integer'],
            'named_sort'           => ['required', 'integer'],
            'tags'                 => ['nullable'],
            'additionalCategories' => ['nullable'],
            'labels'               => ['nullable'],
            'quantity'             => ['required', 'string'],
            'calories'             => ['nullable', 'string'],
            'weight'               => ['nullable', 'string'],
            'storage_conditions'   => ['nullable', 'string'],
            'delivery'             => ['nullable', 'string'],
            'protein'              => ['nullable', 'string'],
            'fats'                 => ['nullable', 'string'],
            'carbohydrates'        => ['nullable', 'string'],
            'cooking_time'         => ['nullable', 'string'],
            'package'              => ['nullable', 'string'],
            'price'                => ['required', 'integer', 'max:999999'],
            'preview_text'         => ['required','string', 'max:999999'],
            'attachment'           => ['required'],
        ];
    }

    public function onSave(ResourceRequest $request, Dish $model)
    {
        abort_if(!Auth::user()->hasAccess('content-dishes.write'), 403);
        $fields = $request->validated('model');
        unset($fields['attachment']);
        $model->fill($fields)->save();
        $model->tags()->sync($fields['tags'] ?? []);
        $model->labels()->sync($fields['labels'] ?? []);
        $model->additionalCategories()->sync($fields['additionalCategories'] ?? []);

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
