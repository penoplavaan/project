<?php

namespace App\Orchid\Resources;

use App\Helpers\Image;
use App\Models\Dish;
use App\Models\MainPromoSlide;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class MainPromoSlideResource extends BaseResource
{

    public static $model = MainPromoSlide::class;

    public static function permission(): ?string
    {
        return 'content.main';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(true),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),

            Relation::make('dish_id')
                ->fromModel(Dish::class, 'id')
                ->displayAppend('name')
                ->title(__('admin.dish.name')),

            Upload::make('attachment')
                ->title('Заменить фотографию блюда')
                ->maxFiles(1)
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (MainPromoSlide $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('dish_id', __('admin.dish.name'))
                ->render(function (MainPromoSlide $model) {
                    // todo ссылка на страницу блюда
                    return $model->dish->name;
                }),

            TD::make('dish_id', __('admin.dish.cook'))
                ->render(function (MainPromoSlide $model) {
                    // todo ссылка на страницу повара
                    return $model->dish->cook->user->getFullName();
                }),

            TD::make('id', __('admin.preview'))
                ->render(function (MainPromoSlide $model) {
                    /** @var \App\Helpers\Image $img */
                    if ($model->attachment && $model->attachment->count()) {
                        $img = $model->attachment[0]->getImage();
                    } else {
                        $img = $model->dish->getImage();
                    }

                    if (!$img) {
                        $src = Image::make('/images/empty-dish.svg')->resize('admin.preview')->getUrl();
                    } else {
                        $src = $img->resize('admin.preview')->getUrl();
                    }
                    return '<img src="' . $src . '" alt="">';
                }),
        ];
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function rules(Model $model): array
    {
        return [
            'sort'        => ['required', 'integer'],
            'dish_id'     => ['required', 'integer'],
            'attachment'  => [],
        ];
    }

    public function onSave(ResourceRequest $request, MainPromoSlide $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
