<?php

namespace App\Orchid\Resources;

use App\Models\TextPage;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use App\Orchid\Fields\Tinymce;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\TD;

class TextPageResource extends BaseResource
{

    public static $model = TextPage::class;

    public static function permission(): ?string
    {
        return 'content.text';
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('title')->title(__('admin.title'))->required()->maxlength(250),
            Input::make('url')->title(__('admin.url'))->required()->maxlength(250),
            Tinymce::make('text')->title(__('admin.text'))->value('')->set('data-height', '800'),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('title', __('admin.title'))->sort()->filter(),
            TD::make('url', __('admin.url'))->sort()->filter(),
            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (TextPage $model) => $model->active ? 'Да' : 'Нет'),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'active' => ['boolean'],
            'title'  => ['required', 'string', 'max:250'],
            'url'    => ['nullable', 'string', 'max:250'],
            'text'   => ['nullable', 'string', 'max:50000'],
        ];
    }

    public function onSave(ResourceRequest $request, TextPage $model)
    {
        $fields = $request->validated('model');

        if (isset($fields['upload'])) {
            unset($fields['upload']);
        }

        $fields['text'] ??= '';

        // todo forceFill заменить на fill и проверить, что всё работает
        $model->forceFill($fields)->save();
    }
}
