<?php

namespace App\Orchid\Resources;

use App\Models\MainReview;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class MainReviewResource extends BaseResource
{
    public static $model = MainReview::class;

    public static function permission(): ?string
    {
        return 'content.main';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(true),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),

            Input::make('name')->title(__('admin.main-review.name'))->required()->maxlength(250),
            Input::make('position')->title(__('admin.main-review.position'))->required()->maxlength(250),

            TextArea::make('text')->title(__('admin.main-review.text'))->required()
                ->set('rows', '10'),

            Upload::make('attachment')
                ->title('Фотография')
                ->maxFiles(1)
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (MainReview $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('name', __('admin.main-review.name')),
            TD::make('position', __('admin.main-review.position')),

            TD::make('id', __('admin.preview'))
                ->render(function (MainReview $model) {
                    /** @var \App\Helpers\Image $img */
                    if ($model->attachment && $model->attachment->count()) {
                        $img = $model->attachment[0]->getImage();
                        $src = $img->resize('admin.preview')->getUrl();
                        return '<img src="' . $src . '" alt="">';
                    }

                    return '';
                }),
        ];
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function rules(Model $model): array
    {
        return [
            'active'     => ['boolean'],
            'sort'       => ['required', 'integer'],
            'name'       => ['required', 'string', 'max:250'],
            'position'   => ['required', 'string', 'max:250'],
            'text'       => ['required', 'string', 'max:50000'],
            'attachment' => [],
        ];
    }

    public function onSave(ResourceRequest $request, MainReview $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
