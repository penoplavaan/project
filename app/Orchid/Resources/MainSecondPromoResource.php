<?php

namespace App\Orchid\Resources;

use App\Helpers\Image;
use App\Models\Cook;
use App\Models\Dish;
use App\Models\MainSecondPromo;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class MainSecondPromoResource extends BaseResource
{

    public static $model = MainSecondPromo::class;

    public static function permission(): ?string
    {
        return 'content.main';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(true),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),

            Relation::make('cook_id')
                ->fromModel(Cook::class, 'id')
                ->displayAppend('orchidRelUserName')
                ->applyScope('withUserModel')
                ->searchColumns('users.name')
                ->title(__('admin.dish.cook')),

            Upload::make('attachment')
                ->title('Картинка для слайда')
                ->maxFiles(1),

            Input::make('url')->title(__('admin.url'))->maxlength(250),
            Input::make('price')->title('Цена')->maxlength(20),
            Input::make('name')->title(__('admin.name'))->maxlength(250),
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (MainSecondPromo $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('cook_id', __('admin.dish.cook'))
                ->render(function (MainSecondPromo $model) {
                    return $model->cook->user?->getFullName() ?? $model->cook_id;
                }),

            TD::make('url', __('admin.url')),
            TD::make('price', 'Цена блюда'),
            TD::make('name', __('admin.name')),

            TD::make('id', __('admin.preview'))
                ->render(function (MainSecondPromo $model) {
                    /** @var \App\Helpers\Image $img */
                    if ($model->attachment && $model->attachment->count()) {
                        $img = $model->attachment[0]->getImage();
                    }

                    if (!isset($img) || empty($img)) {
                        $src = Image::make('/images/empty-dish.svg')->resize('admin.preview')->getUrl();
                    } else {
                        $src = $img->resize('admin.preview')->getUrl();
                    }
                    return '<img src="' . $src . '" alt="">';
                }),
        ];
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function rules(Model $model): array
    {
        return [
            'active'      => ['boolean'],
            'sort'        => ['required', 'integer'],
            'cook_id'     => ['required', 'integer'],
            'url'         => ['nullable', 'string', 'max:250'],
            'name'        => ['nullable', 'string', 'max:250'],
            'price'       => ['nullable', 'numeric', 'max:1000000'],
            'attachment'  => [],
        ];
    }

    public function onSave(ResourceRequest $request, MainSecondPromo $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
