<?php

namespace App\Orchid\Resources;

use App\Models\TextBlock;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\Tinymce;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\TD;

class TextBlockResource extends BaseResource
{

    public static $model = TextBlock::class;

    public static function permission(): ?string
    {
        return 'content.text';
    }

    public static function icon(): string
    {
        return 'text-left';
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('code')->title(__('admin.code'))->required()->maxlength(250),
            Input::make('title')->title(__('admin.title'))->required()->maxlength(250),
            Tinymce::make('text1')->title(__('admin.text'))->value('')->set('data-height', '400'),
            Tinymce::make('text2')->title(__('admin.text2'))->value('')->set('data-height', '400'),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('code', __('admin.code'))->filter(),
            TD::make('title', __('admin.title')),
            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (TextBlock $model) => $model->active ? 'Да' : 'Нет'),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'active' => ['boolean'],
            'code'   => ['required', 'string', 'max:250'],
            'title'  => ['required', 'string', 'max:250'],
            'text1'  => ['nullable', 'string', 'max:50000'],
            'text2'  => ['nullable', 'string', 'max:50000'],
        ];
    }

    public function onSave(ResourceRequest $request, TextBlock $model)
    {
        $fields = $request->validated('model');

        $fields['text1'] ??= '';
        $fields['text2'] ??= '';

        // todo forceFill заменить на fill и проверить, что всё работает
        $model->forceFill($fields)->save();
    }
}
