<?php

namespace App\Orchid\Resources;

use App\Models\Gallery;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class GalleryResource extends BaseResource
{
    use SaveUploadsResourceTrait;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = Gallery::class;

    public static function permission(): ?string
    {
        return 'content.text';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            Input::make('name')->title(__('admin.name')),
            Upload::make('attachment')->title(__('admin.uploads')),
            //CheckBox::make('show_on_main')->sendTrueOrFalse()->title(__('admin.show_on_main')),
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id')->sort()->filter(),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('name', __('admin.name'))->sort()->filter(),
            TD::make('attachment', __('admin.images'))
                ->render(fn(Gallery $model) => view('orchid::components.preview-list', ['attachments' => $model->attachment])),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'name'         => ['required', 'string'],
            'attachment'   => ['array'],
            'attachment.*' => ['numeric'],
            //'show_on_main' => ['nullable', 'boolean'],
        ];
    }
}
