<?php

namespace App\Orchid\Resources;

use App\Models\City;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\TD;

class CityResource extends BaseResource
{

    public static $model = City::class;

    public static function permission(): ?string
    {
        return 'content.refs';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('is_top')
                ->title('Выводить в попапе')
                ->sendTrueOrFalse()
                ->value(0),

            Input::make('sort')
                ->title(__('admin.sort'))
                ->required()
                ->value(500)
                ->maxlength(10),

            Input::make('name')
                ->title('Город')
                ->maxlength(250)
                ->required(),

            Input::make('region')
                ->title('Регион')
                ->maxlength(250),

            Input::make('district')
                ->title('Округ')
                ->maxlength(250),

            Input::make('lat')
                ->title('Широта')
                ->maxlength(20),

            Input::make('lng')
                ->title('Долгота')
                ->maxlength(20),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('is_top', 'Выводить в попапе')
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn(City $model) => $model->is_top ? 'Да' : 'Нет'),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('name', 'Город')->filter(),
            TD::make('region', 'Регион'),
            TD::make('district', 'Округ'),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'is_top'   => ['boolean'],
            'sort'     => ['required', 'integer'],
            'name'     => ['required', 'string', 'max:250'],
            'region'   => ['nullable', 'string', 'max:250'],
            'district' => ['nullable', 'string', 'max:250'],
            'lat'      => ['nullable', 'numeric'],
            'lng'      => ['nullable', 'numeric'],
        ];
    }

    public function onSave(ResourceRequest $request, City $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();
    }
}
