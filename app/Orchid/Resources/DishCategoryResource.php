<?php

namespace App\Orchid\Resources;

use App\Models\DishCategory;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\TD;

class DishCategoryResource extends BaseResource
{

    public static $model = DishCategory::class;

    public static function permission(): ?string
    {
        return 'content.refs';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('title')->title('Название')->required()->maxlength(250),
            Input::make('menu_title')->title('Текст пункта меню')->maxlength(250),
            Input::make('h1')->title('Тег H1 для раздела')->maxlength(250),
            Input::make('description')->title('Тег meta description')->maxlength(250),
            Input::make('meta_title')->title('Тег title')->maxlength(250),
            Input::make('slug')->title(__('admin.code'))->required()->maxlength(250),
            Input::make('sort')->title(__('admin.sort'))->required()->maxlength(10)->value(500),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (DishCategory $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('sort', __('admin.sort'))->sort(),
            TD::make('title', 'Название')->sort(),

            TD::make('slug', __('admin.code')),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'active'      => ['boolean'],
            'sort'        => ['required', 'integer'],
            'title'       => ['required', 'string', 'max:250'],
            'h1'          => ['string', 'max:250'],
            'menu_title'  => ['string', 'max:250'],
            'description' => ['string', 'max:250'],
            'meta_title'  => ['string', 'max:250'],
            'slug'        => ['required', 'string', 'max:250'],
        ];
    }

    public function onSave(ResourceRequest $request, DishCategory $model)
    {
        $fields = $request->validated('model');

        // todo forceFill заменить на fill и проверить, что всё работает
        $model->forceFill($fields)->save();
    }
}
