<?php

namespace App\Orchid\Resources;

use App\Models\MainPromoCategoryCompilation;
use App\Models\PromoCard;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\TD;

class MainPromoCategoryCompilationResource extends BaseResource
{

    public static $model = MainPromoCategoryCompilation::class;

    public static function permission(): ?string
    {
        return 'content.main';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(true),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),
            Input::make('name')->title('Название')->required()->maxlength(250),

            Relation::make('promocards.')
                ->fromModel(PromoCard::class, 'name')
                ->multiple()
                ->title('Карточки'),
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (MainPromoCategoryCompilation $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('name', __('admin.name')),
        ];
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function rules(Model $model): array
    {
        return [
            'active'     => ['boolean'],
            'sort'       => ['required', 'integer'],
            'name'       => ['required', 'string', 'max:250'],
            'promocards' => ['required', 'array'],
        ];
    }

    public function onSave(ResourceRequest $request, MainPromoCategoryCompilation $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->promoCards()->sync(
            $request->input('promocards', [])
        );
    }
}
