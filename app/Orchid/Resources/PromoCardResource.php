<?php

namespace App\Orchid\Resources;

use App\Models\DishCategory;
use App\Models\PromoCard;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class PromoCardResource extends BaseResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\PromoCard::class;


    //todo обьеденить с сортирокой в блюдах
    public const SORT_NAMES = [
        1 => 'Первая Волна',
        2 => 'Вторая Волна',
        3 => 'По умолчанию',
        4 => 'Андердоги',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('name')->title('Название')->required()->maxlength(250),
            Relation::make('categories')
                ->fromModel(DishCategory::class, 'title')
                ->title('Категории')
                ->multiple(),
            CheckBox::make('show_on_main_category')
                ->title('Показывать в главной категории')
                ->sendTrueOrFalse()
                ->value(1),
            Select::make('named_sort')
                ->title('Группа сортировки')
                ->options(static::SORT_NAMES),
            TextArea::make('preview_text')
                ->title('Описание')
                ->maxlength(999),
            Input::make('price')->title('Описание цены')
                ->help('Например: от 500 ₽')
                ->maxlength(20),
            Input::make('button_text')->title('Текст кнопки')->maxlength(20),
            Input::make('link')->title('Ссылка')->maxlength(100),
            Input::make('sort')->title(__('admin.sort'))->value(500)->maxlength(10),
            Upload::make('attachment')
                ->title('Фотографии')
                ->maxFiles(1),
        ];
    }

    public static function icon(): string
    {
        return 'badge';
    }

    public function with(): array
    {
        return array_merge(parent::with(), [
            'categories'
        ]);
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('updated_at', 'Update date')
                ->render(function ($model) {
                    return $model->updated_at->toDateTimeString();
                }),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (PromoCard $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('name', 'Название')->sort(),

            TD::make('categories', __('admin.dish.category'))
                ->render(function (PromoCard $model) {
                    return $model->categories
                        ->map(fn (DishCategory $c) => $c->getTitle() . '['. $c->id .']')
                        ->join(', ');
                })
                ->filter()
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'active'                => ['boolean'],
            'name'                  => ['required', 'string', 'max:250'],
            'categories'            => ['nullable'],
            'show_on_main_category' => ['boolean'],
            'named_sort'            => ['required', 'integer'],
            'preview_text'          => ['required','string', 'max:999999'],
            'button_text'           => ['nullable', 'string'],
            'price'                 => ['nullable', 'string'],
            'link'                  => ['nullable', 'string'],
            'sort'                  => ['nullable', 'integer'],
            'attachment'            => ['nullable'],
        ];
    }

    public function legend(): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters(): array
    {
        return [];
    }

    public function onSave(ResourceRequest $request, PromoCard $model)
    {
        abort_if(!Auth::user()->hasAccess('content-dishes.write'), 403);
        $fields = $request->validated('model');
        $model->fill($fields)->save();
        $model->categories()->sync($fields['categories'] ?? []);

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
