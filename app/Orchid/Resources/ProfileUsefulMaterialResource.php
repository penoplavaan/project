<?php

namespace App\Orchid\Resources;

use App\Models\Dish;
use App\Models\ProfileUsefulMaterial;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\Tinymce;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class ProfileUsefulMaterialResource extends BaseResource
{

    public static $model = ProfileUsefulMaterial::class;

    public static function permission(): ?string
    {
        return 'content.useful';
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(true),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),

            Input::make('title')->title("Заголовок")->maxlength(255),

            Input::make('anchor')->title("Якорь")->maxlength(20),

            Tinymce::make('text')->title(__('admin.text'))->value('')->set('data-height', '400'),

            Upload::make('attachment')
                ->title('Прикреплённые файлы')
        ];
    }

    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (ProfileUsefulMaterial $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('title', __('admin.title')),
        ];
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function rules(Model $model): array
    {
        return [
            'sort'        => ['required', 'integer'],
            'title'       => [],
            'text'        => [],
            'anchor'      => ['nullable', 'string', 'max:20'],
            'attachment'  => [],
        ];
    }

    public function onSave(ResourceRequest $request, ProfileUsefulMaterial $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
