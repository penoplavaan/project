<?php

namespace App\Orchid\Resources;

use App\Models\Feedback;
use App\Models\FeedbackCategory;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Orchid\Crud\ResourceRequest;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\TD;

class FeedbackResource extends BaseResource
{

    public static $model = Feedback::class;

    public static function permission(): ?string
    {
        return 'content.refs';
    }

    public static function icon(): string
    {
        return 'map';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            Input::make('name')
                ->title('Имя')
                ->maxlength(10),

            Input::make('sort')
                ->title(__('admin.sort'))
                ->required()
                ->value(500)
                ->maxlength(10),

            Input::make('phone')
                ->title('Телефон')
                ->maxlength(250),

            Input::make('email')
                ->title('Email')
                ->maxlength(250),

            Group::make([
                Relation::make('topic_id')
                    ->fromModel(FeedbackCategory::class, 'name')
                    ->title(__('admin.dish.category')),
            ]),

            Input::make('question')
                ->title('Текст вопроса')
                ->maxlength(200),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('name', 'Имя пользователя')->filter(),
            TD::make('phone', 'Телефон'),
            TD::make('email', 'Почта'),
            TD::make('topic', 'Тема вопроса')
                ->render(function (Feedback $model) {
                    return $model->topic?->name ?? '';
                })->filter(),
            TD::make('question', 'Текст вопроса')->width('200px'),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'name'     => ['required', 'string', 'max:250'],
            'sort'     => ['required', 'integer'],
            'phone'    => ['required', 'string', 'max:250'],
            'email'    => ['required', 'email:rfc,dns', 'max:255'],
            'topic_id' => ['required', 'string', 'max:250'],
            'text'     => ['required', 'string', 'max:2000'],
        ];
    }

    public function onSave(ResourceRequest $request, Feedback $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();
    }
}
