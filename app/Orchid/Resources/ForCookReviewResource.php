<?php

namespace App\Orchid\Resources;

use App\Models\ForCookReview;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\Tinymce;
use Illuminate\Database\Eloquent\Model;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class ForCookReviewResource extends BaseResource
{
    public static $model = ForCookReview::class;

    public static function permission(): ?string
    {
        return 'content.for-cooks';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),
            Input::make('name')->title(__('admin.name'))->required()->maxlength(250),
            Input::make('position')->title(__('admin.position'))->required()->maxlength(250),
            Tinymce::make('text')->title(__('admin.text'))->value('')->set('data-height', '400'),

            Upload::make('attachment')
                ->title('Фотография (392*680)')
                ->maxFiles(1)
                ->acceptedFiles('image/*')
                ->required()
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (ForCookReview $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('title', __('admin.title')),
            TD::make('position', __('admin.position')),

            TD::make('id', __('admin.preview'))
                ->render(function (ForCookReview $model) {
                    /** @var \App\Helpers\Image $img */
                    if ($model->attachment && $model->attachment->count()) {
                        $img = $model->attachment[0]->getImage();
                        $src = $img->resize('admin.preview')->getUrl();
                        return '<img src="' . $src . '" alt="">';
                    }

                    return '';
                }),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'active'     => ['boolean'],
            'sort'       => ['required', 'integer'],
            'name'       => ['required', 'string', 'max:250'],
            'position'   => ['required', 'string', 'max:250'],
            'text'       => ['required', 'string', 'max:50000'],

            'attachment' => ['required'],
        ];
    }

    public function onSave(ResourceRequest $request, ForCookReview $model)
    {
        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->attachment()->syncWithoutDetaching(
            $request->input('attachment', [])
        );
    }
}
