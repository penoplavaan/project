<?php

namespace App\Orchid\Resources;

use App\Models\Label;
use App\Orchid\Actions\ActivateAction;
use App\Orchid\Actions\DeactivateAction;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\TD;

class LabelResource extends BaseResource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Label::class;

    public static function permission(): ?string
    {
        return 'content.refs';
    }

    public static function displayInNavigation(): bool
    {
        return false;
    }

    public function actions(): array
    {
        return array_merge([ActivateAction::class, DeactivateAction::class]);
    }

    public function delete(Model $model): void
    {
        $model->setAttribute('active', false);
        $model->save();
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @return array
     */
    public function fields(): array
    {
        return [
            CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
            Input::make('name')->title('Название')->required()->maxlength(250),
            Input::make('sort')->title(__('admin.sort'))->required()->value(500)->maxlength(10),
            Input::make('color')->title('Цвет текста')->type('color')->maxlength(10),
            Input::make('background')->title('Цвет фона')->type('color')->maxlength(10),

        ];
    }



    /**
     * Get the columns displayed by the resource.
     *
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', 'Date of creation')
                ->render(function ($model) {
                    return $model->created_at->toDateTimeString();
                }),

            TD::make('updated_at', 'Update date')
                ->render(function ($model) {
                    return $model->updated_at->toDateTimeString();
                }),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (Label $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('sort', __('admin.sort'))->sort(),
            TD::make('name', 'Название')->sort(),

        ];
    }

    /**
     * Get the sights displayed by the resource.
     *
     * @return Sight[]
     */
    public function legend(): array
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @return array
     */
    public function filters(): array
    {
        return [];
    }
}
