<?php

namespace App\Orchid\Resources;

use App\Helpers\BaseHelper;
use App\Models\City;
use App\Models\Cook;
use App\Models\CookSpeciality;
use App\Models\DishCategory;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use App\Orchid\Fields\CookUser;
use Illuminate\Support\Facades\Auth;
use Orchid\Crud\ResourceRequest;
use Orchid\Platform\Models\User;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\TD;

class CookResource extends BaseResource
{

    public static $model = Cook::class;

    public static function permission(): ?string
    {
        return 'content-cooks.read';
    }

    public static function icon(): string
    {
        return 'people';
    }

    public function fields(): array
    {
        abort_if(!Auth::user()->hasAccess('content-cooks.write'), 403);

        return [
            CookUser::make('user_id'),

            Group::make([
                CheckBox::make('active')->title(__('admin.active'))->sendTrueOrFalse()->value(1),
                CheckBox::make('verified')->title(__('admin.verified'))->sendTrueOrFalse()->value(1),
                CheckBox::make('passport_verified')->title(__('admin.passport_verified'))->sendTrueOrFalse()->value(1),
                CheckBox::make('sanitary_verified')->title(__('admin.sanitary_verified'))->sendTrueOrFalse()->value(1),
            ]),

            Group::make([
                Select::make('user_id')
                    ->fromModel(User::class, 'name')
                    ->title(__('admin.cook.user'))->required(),
                Select::make('speciality_id')
                    ->fromModel(CookSpeciality::class, 'title')
                    ->title(__('admin.cook.speciality'))->required(),
                Relation::make('category.')
                    ->fromModel(DishCategory::class, 'title')
                    ->multiple()
                    ->title(__('admin.dish.category')),
            ]),

            Group::make([
                Relation::make('city_id')
                    ->title('Город')
                    ->fromModel(City::class, 'name')
                    ->maxlength(250),

                Input::make('address')
                    ->title('Адрес')
                    ->maxlength(250)
                    ->required(),

                Input::make('coords')
                    ->title('Координаты')
                    ->maxlength(50),
            ]),


            TextArea::make('about')
                ->title('О себе')
                ->value('')
                ->set('rows', '10'),

            Group::make([
                CheckBox::make('show_on_main')->title(__('admin.show_on_main'))->sendTrueOrFalse()->value(1),
                Input::make('delivery')->title(__('admin.delivery'))->maxlength(50),
                Input::make('sort')->title(__('admin.sort'))->value(500)->maxlength(10),
            ]),

            Group::make([
                Upload::make('passport_id')
                    ->title('Паспорт')
                    ->acceptedFiles('image/*')
                    ->maxFiles(1)
                    ->groups('passport'),

                Upload::make('sanitary_id')
                    ->title('Санитарная книжка')
                    ->acceptedFiles('image/*')
                    ->maxFiles(1)
                    ->groups('sanitary'),

                Upload::make('document_id')
                    ->title('Дополнительные документы')
                    ->acceptedFiles('image/*')
                    ->maxFiles(10)
                    ->groups('document'),
            ]),

            Upload::make('certificate_id')
                ->title('Сертификаты')
                ->acceptedFiles('image/*')
                ->maxFiles(10)
                ->groups('certificate'),

            Upload::make('kitchen_id')
                ->title('Фотографии кухни')
                ->acceptedFiles('image/*')
                ->maxFiles(10)
                ->groups('kitchen'),

            Group::make([
                TextArea::make('admin_comment')
                    ->title('Комментарии от администратора')
                    ->value('')
                    ->set('rows', '10'),

                Input::make('manual_rating')
                    ->type('number')
                    ->step(0.1)
                    ->max(5)
                    ->min(0)
                    ->title('Ручная оценка')
                    ->help('от 0 до 5'),
            ]),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id')
                ->filter()
                ->sort(),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('active', __('admin.active'))
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn(Cook $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('sort', __('admin.sort'))
                ->sort(),

            TD::make('user_id', __('admin.cook.user'))
                ->filter(
                    Relation::make('user_id')
                        ->title('Повар')
                        ->fromModel(User::class, 'name')
                        ->searchColumns('name', 'id')
                        ->maxlength(250)
                )
                ->render(function (Cook $model) {
                    if (empty($model->user)) {
                        return '';
                    }
                    $url = route('platform.systems.users.edit', ['user' => $model->user_id]);
                    $name = $model->user->getFullName();
                    return BaseHelper::truncateString($name, 50) . ' <a href="' . $url . '">[' . $model->user_id . ']</a>';
                }),

            TD::make('user.email', 'Email')
                ->defaultHidden()
                ->render(function (Cook $model) {
                    return $model->user?->email ?? '';
                }),

            TD::make('user.phone', 'Телефон')
                ->defaultHidden()
                ->render(function (Cook $model) {
                    return $model->user?->phone ?? '';
                }),

            TD::make('speciality_id', __('admin.cook.speciality'))
                ->render(function (Cook $model) {
                    return $model->speciality?->title ?? '';
                })->filter(),

            TD::make('verified', __('admin.verified'))
                ->sort()
                ->render(function (Cook $model) {
                    return $model->verified ? 'Да' : 'Нет';
                }),

            TD::make('address', 'Адрес')
                ->filter()
                ->defaultHidden(),

            TD::make('show_on_main', 'На главной')
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn(Cook $model) => $model->show_on_main ? 'Да' : 'Нет'),

            TD::make('delivery', 'Условия доставки')
                ->filter()
                ->defaultHidden(),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            //main
            'user_id'           => ['required', 'integer'],
            'speciality_id'     => ['required'],
            'category'          => ['nullable'],
            'city_id'           => ['required'],
            'address'           => ['required', 'string', 'max:250'],
            'coords'            => ['nullable', 'string', 'max:50'],
            'delivery'          => ['nullable', 'string', 'max:50'],

            // checkboxes
            'active'            => ['boolean'],
            'sort'              => ['required', 'integer'],
            'verified'          => ['boolean'],
            'show_on_main'      => ['boolean'],
            'passport_verified' => ['boolean'],
            'sanitary_verified' => ['boolean'],

            // files
            'passport_id'       => [],
            'sanitary_id'       => [],
            'certificate_id'    => [],
            'kitchen_id'        => [],
            'document_id'       => [],

            // other
            'admin_comment'     => [],
            'about'             => [],
            'manual_rating'     => ['nullable', 'max:5', 'min:0'],
        ];
    }

    public function actions(): array
    {
        if (Auth::user()->hasAccess('content-cooks.write')) {
            return parent::actions();
        } else {
            return [];
        }
    }

    public function onSave(ResourceRequest $request, Cook $model)
    {
        abort_if(!Auth::user()->hasAccess('content-cooks.write'), 403);


        $fields = $request->validated('model');

        $model->fill($fields)->save();

        $model->category()->sync($fields['category'] ?? []);

        // files
        $passportId = $fields['passport_id'] ?? [];
        $sanitaryId = $fields['sanitary_id'] ?? [];
        $certificateId = $fields['certificate_id'] ?? [];
        $kitchenId = $fields['kitchen_id'] ?? [];
        $documentId = $fields['document_id'] ?? [];
        $attachments = array_merge($passportId, $sanitaryId, $certificateId, $kitchenId, $documentId);

        $model->attachment()->syncWithoutDetaching($attachments);
    }
}
