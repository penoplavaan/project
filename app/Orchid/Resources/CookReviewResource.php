<?php

namespace App\Orchid\Resources;

use App\Helpers\BaseHelper;
use App\Models\Cook;
use App\Models\CookReview;
use App\Models\User;
use App\Orchid\Components\CreatedAt;
use App\Orchid\Components\UpdatedAt;
use Orchid\Crud\ResourceRequest;
use Orchid\Screen\Fields\CheckBox;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\RadioButtons;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\TD;

class CookReviewResource extends BaseResource
{

    public static $model = CookReview::class;

    public static function permission(): ?string
    {
        return 'content.cook-reviews';
    }

    public static function icon(): string
    {
        return 'bar-chart';
    }

    public function fields(): array
    {
        return [
            CheckBox::make('active')
                ->title('Промодерирован и выводится на сайте')
                ->sendTrueOrFalse()
                ->value(1),

            Relation::make('cook_id')
                ->required()
                ->fromModel(Cook::class, 'id')
                ->displayAppend('orchidRelUserName')
                ->applyScope('withUserModel')
                ->searchColumns('users.name')
                ->title('Повар'),

            Relation::make('user_id')
                ->required()
                ->fromModel(User::class, 'id')
                ->displayAppend('name')
                ->searchColumns('name')
                ->title('Пользователь'),

            RadioButtons::make('rating')
                ->title('Рейтинг')
                ->required()
                ->options([
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                ])
                ->value(5),

            TextArea::make('text')
                ->title(__('admin.main-review.text'))
                ->required()
                ->maxlength(400)
                ->set('rows', '10'),
        ];
    }

    public function columns(): array
    {
        return [
            TD::make('id'),

            TD::make('created_at', __('admin.created_at'))
                ->sort()
                ->defaultHidden()
                ->component(CreatedAt::class),

            TD::make('updated_at', __('admin.updated_at'))
                ->sort()
                ->defaultHidden()
                ->component(UpdatedAt::class),

            TD::make('active', 'Промодерирован')
                ->sort()
                ->filter('select')
                ->filterOptions(['Нет', 'Да'])
                ->render(fn (CookReview $model) => $model->active ? 'Да' : 'Нет'),

            TD::make('cook_id', 'Повар')
                ->render(function (CookReview $model) {
                    $url = route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $model->cook_id]);
                    if (empty($model->cook->user)) {
                        return '';
                    }

                    return BaseHelper::truncateString($model->cook->user->getFullName(), 40) . ' <a href="' . $url . '">[' . $model->cook_id . ']</a>';
                })
            ->filter(),

            TD::make('cook_id', 'Пользователь')
                ->render(function (CookReview $model) {
                    $url = route('platform.systems.users.edit', ['user' => $model->user_id]);
                    return BaseHelper::truncateString($model->user->getFullName(), 40) . ' <a href="' . $url . '">[' . $model->user_id . ']</a>';
                })
            ->filter(),

            TD::make('rating', 'Рейтинг')
                ->sort()
                ->filter(),
        ];
    }

    public function rules(Model $model): array
    {
        return [
            'cook_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'active'  => ['boolean'],
            'text'    => ['required', 'string', 'max:400'],
            'rating'  => ['required', 'integer', 'min:1', 'max:5'],
        ];
    }

    public function onSave(ResourceRequest $request, CookReview $model)
    {
        $fields = $request->validated('model');
        $model->fill($fields)->save();
    }
}
