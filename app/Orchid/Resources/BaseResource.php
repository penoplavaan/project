<?php

namespace App\Orchid\Resources;

use App\Models\BaseModel;
use App\Orchid\Actions\ActivateAction;
use App\Orchid\Actions\DeactivateAction;
use App\Orchid\Actions\DeleteAction;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Orchid\Crud\Resource;

abstract class BaseResource extends Resource
{

    public static function sort(): string
    {
        return 10;
    }

    public static function label(): string
    {
        $name = static::nameWithoutResource();

        return __('admin.' . Str::kebab($name) . '-title');
    }

    public static function singularLabel(): string
    {
        $name = static::nameWithoutResource();

        return __('admin.' . Str::kebab($name) . '-title-1');
    }

    /**
     * Если это страница добавления - true
     *
     * @return bool
     */
    public function isAdd(): bool
    {
        $name = Route::getCurrentRoute()->getName();

        return $name === 'platform.resource.create';
    }

    public function legend(): array
    {
        return [];
    }

    public function filters(): array
    {
        return [];
    }

    public function actions(): array
    {
        $actionList = [DeleteAction::class];

        /** @var BaseModel $model */
        $model = $this->getModel();

        if ($model->hasActiveFlag()) {
            $actionList = array_merge([ActivateAction::class, DeactivateAction::class], $actionList);
        }

        return $actionList;
    }
}
