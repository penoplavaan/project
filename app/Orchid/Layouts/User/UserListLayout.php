<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use App\Helpers\BaseHelper;
use App\Helpers\ViewHelper;
use Orchid\Platform\Models\User;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class UserListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'users';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('id', 'ID')
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (User $user) {
                    return Link::make('' . $user->id)->route('platform.systems.users.edit', ['user' => $user->id]);
                }),

            TD::make('name', __('Name'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (User $user) {
                    if (isset($user->name) && !empty($user->name)) {
                        return BaseHelper::truncateString($user->name, 40);
                    }
                    return BaseHelper::truncateString($user->email, 50);
                }),

            TD::make('email', __('Email'))
                ->sort()
                ->cantHide()
                ->filter(Input::make())
                ->render(function (User $user) {
                    return BaseHelper::truncateString($user->email, 50);
                }),

            TD::make('phone', 'Телефон')
                ->sort()
                ->filter(Input::make())
                ->render(function (User $user) {
                    return ViewHelper::clearPhone($user->phone);
                }),

            TD::make('created_at', 'Дата регистрации на сервисе')
                ->sort()
                ->render(function (User $user) {
                    return $user->created_at->toDateTimeString();
                }),

            TD::make('updated_at', __('Last edit'))
                ->sort()
                ->render(function (User $user) {
                    return $user->updated_at->toDateTimeString();
                }),

            TD::make('email_verified_at', 'Емейл верифицирован')
                ->defaultHidden()
                ->sort()
                ->render(function (User $user) {
                    return !empty($user->email_verified_at) ? 'Да' : 'Нет';
                }),

            TD::make('phone_verified', 'Телефон верифицирован')
                ->defaultHidden()
                ->sort()
                ->render(function (User $user) {
                    return !empty($user->phone_verified) ? 'Да' : 'Нет';
                }),

            TD::make(__('Actions'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->render(function (User $user) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([

                            Link::make(__('Edit'))
                                ->route('platform.systems.users.edit', $user->id)
                                ->icon('pencil'),

//                            Button::make(__('Delete'))
//                                ->icon('trash')
//                                ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
//                                ->method('remove', [
//                                    'id' => $user->id,
//                                ]),
                        ]);
                }),
        ];
    }
}
