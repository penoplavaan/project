<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use App\Orchid\Fields\UserCook;
use Orchid\Platform\Models\User;
use Orchid\Screen\Field;
use Orchid\Screen\Layouts\Rows;

class UserCookLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        /** @var User $user */
        $user = $this->query->get('user');

        return [
            UserCook::make('user.userId')->value($user ? $user->id : null),
        ];
    }
}
