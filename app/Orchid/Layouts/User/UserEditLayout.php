<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class UserEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {

        return [
            Input::make('user.name')
                ->type('text')
                ->max(255)
                ->title('Фамилия и имя')
                ->value('')
                ->placeholder('Иванов Иван'),

            Input::make('user.email')
                ->type('email')
                ->required()
                ->title(__('Email'))
                ->placeholder(__('Email')),

            CheckBox::make('user.email_verified')
                ->placeholder()
                ->title('Электронная почта подтверждена')
                ->placeholder('Да, подтверждена'),

            CheckBox::make('user.advertisement')
                ->placeholder()
                ->title('Пользователь подписан на рекламу')
                ->placeholder('Да, подписан'),

            Input::make('user.phone')
                ->type('phone')
                ->required()
                ->title('Телефон')
                ->placeholder(''),

            CheckBox::make('user.phone_verified')
                ->placeholder()
                ->title('Номер телефона подтвержден')
                ->placeholder('Да, подтвержден'),

            Upload::make('user.avatar_id')
                ->title('Аватар')
                ->acceptedFiles('image/*')
                ->maxFiles(1)
        ];
    }
}
