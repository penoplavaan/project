<?php



namespace App\Orchid\Layouts\SiteSettings;

use App\Models\SiteSettingsSection;
use App\Orchid\Fields\Tinymce;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class SiteSettingsEditLayout extends Rows
{

    /**
     * ReusableEditLayout constructor.
     *
     * @param int $section
     */
    public function __construct(protected int $section = 0)
    {
        abort_if(!Auth::user()->hasAccess('content.settings'), 403);
    }

    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        if (request()->get('textarea')) {
            $bigText = TextArea::make('site-settings.big_text')
                ->rows(10)
                ->title('Подробный текст');
        } else {
            $bigText = Tinymce::make('site-settings.big_text')
                ->title('Подробный текст');
        }

        return [
            Group::make(
                [
                    CheckBox::make('site-settings.active')
                        ->sendTrueOrFalse()
                        ->value(1)
                        ->title(__('admin.active')),
                    Input::make('site-settings.sort')
                        ->type('number')
                        ->value(500)
                        ->title(__('admin.sort')),

                    Input::make('site-settings.color')
                        ->type('color')
                        ->title('Цвет'),
                ]
            ),
            Group::make(
                [
                    Select::make('site-settings.section_id')
                        ->fromModel(SiteSettingsSection::class, 'name', 'id')
                        ->value($this->section)
                        ->empty('Корневой элемент')
                        ->title('Родительский раздел'),
                    Input::make('site-settings.code')
                        ->type('text')
                        ->max(255)
                        ->title('Код элемента'),
                ]
            ),
            Group::make(
                [
                    Input::make('site-settings.text')
                        ->type('text')
                        ->max(255)
                        ->title(__('admin.value')),
                    Upload::make('site-settings.attachment')
                        ->title('Файлы'),
                ]
            ),

            $bigText,
        ];
    }
}
