<?php



namespace App\Orchid\Layouts\SiteSettings;

use App\Models\SiteSettingsSection;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;


class SiteSectionsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'site-sections';

    public $title = 'Разделы';

    public function __construct(public ?SiteSettingsSection $previousSection)
    {
        abort_if(!Auth::user()->hasAccess('content.settings'), 403);
    }

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('name', 'Название раздела')->render(
                function (SiteSettingsSection $siteSections) {
                    $name = $siteSections->id && $siteSections->id !== $this->previousSection?->id
                        ? '[' . $siteSections->id . '] ' . $siteSections->name
                        : '..';

                    return Link::make($name)
                        ->icon('folder')
                        ->route('platform.site-settings.index', ['section' => $siteSections->id]);
                }
            ),
            TD::make('code', 'Код элемента'),
        ];
    }

    /**
     * @return string
     */
    protected function textNotFound(): string
    {
        return 'Подразделы в данном разделе не найдены';
    }
}
