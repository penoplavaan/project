<?php


namespace App\Orchid\Layouts\SiteSettings;

use App\Models\SiteSetting;
use Illuminate\Support\Facades\Auth;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;


class SiteSettingsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'site-settings';

    public $title = 'Настройки сайта';

    public function __construct()
    {
        abort_if(!Auth::user()->hasAccess('content.settings'), 403);
    }

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('code', __('admin.code'))
                ->sort()
                ->render(function (SiteSetting $siteSettings) {
                    return Link::make(sprintf('[%s] %s', $siteSettings->id, $siteSettings->code))
                        ->route('platform.site-settings.edit', $siteSettings->id);
                }),
            TD::make('active', __('admin.active'))
                ->sort()
                ->filter()
                ->filterOptions(['Да', 'Нет'])
                ->render(function (SiteSetting $model) {
                    return $model->active ? 'Да' : 'Нет';
                }),
            TD::make('sort', __('admin.sort'))
                ->sort()
                ->filter(),
            TD::make('text', __('admin.value'))
                ->width('300px')
                ->sort()
                ->filter(),
            TD::make('id', __('admin.menu-site-edit'))
                ->render(function (SiteSetting $siteSettings) {
                    return Link::make()
                        ->icon('pencil')
                        ->route('platform.site-settings.edit', $siteSettings->id);
                }),
        ];
    }

    /**
     * @return string
     */
    protected function textNotFound(): string
    {
        return 'Настройки в данном разделе не найдены';
    }
}
