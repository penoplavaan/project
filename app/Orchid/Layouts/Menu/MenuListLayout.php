<?php

namespace App\Orchid\Layouts\Menu;

use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use App\Models\MenuItem;
use Orchid\Screen\Fields\Group;

class MenuListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'menu_items';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('title', __('admin.title'))
                ->render(function (MenuItem $model): string {
                    $w = ($model->level * 20) . "px";
                    return "<span style='width: $w; display: inline-block'></span>" . $model->title;
                })
                ->cantHide(),
            TD::make('active', __('admin.active'))
                ->cantHide()
                ->render(function (MenuItem $model) {
                    return $model->active ? 'Да' : 'Нет';
                }),
            TD::make('code', __('admin.code'))->cantHide(),
            TD::make('url', __('admin.url'))->cantHide(),
            TD::make('sort', __('admin.sort'))->cantHide(),

            TD::make(__('Actions'))
                ->render(function (MenuItem $model) {
                    return Group::make([
                            ModalToggle::make()
                                ->style('width: auto;')
                                ->modal('editModal')
                                ->modalTitle(__('admin.menu-site-edit'))
                                ->method('editModalApply')
                                ->icon('pencil')
                                ->asyncParameters([$model->id]),
                            ModalToggle::make()
                                ->style('width: auto;')
                                ->modal('addModal')
                                ->modalTitle(__('admin.menu-site-create'))
                                ->method('addModalApply')
                                ->icon('plus')
                                ->asyncParameters([$model->id]),
                            Button::make()
                                ->style('width: auto;')
                                ->icon('trash')
                                ->confirm(__('admin.menu-site-delete-confirm', ['e' => $model->title]))
                                ->method('removeItem', [$model->id]),
                        ])
                        ->autoWidth()
                        ->alignEnd();
                })
            ->cantHide(),
        ];
    }

    protected function total(): array
    {
        return [

        ];
    }
}
