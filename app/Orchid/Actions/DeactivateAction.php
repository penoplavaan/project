<?php

namespace App\Orchid\Actions;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Orchid\Crud\Action;
use Orchid\Screen\Actions\Button;

class DeactivateAction extends Action
{
    /**
     * The button of the action.
     *
     * @return Button
     */
    public function button(): Button
    {
        return Button::make(__('admin.Deactivate'))->icon('fire');
    }

    /**
     * Perform the action on the given models.
     *
     * @param \Illuminate\Support\Collection $models
     */
    public function handle(Collection $models)
    {
        $models->each(function (Model $item) {
            if ($item instanceof BaseModel) {
                if (!$item->hasActiveFlag()) {
                    return;
                }
            }

            $item->active = false;
            $item->save();
        });
    }
}
