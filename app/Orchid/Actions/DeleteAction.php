<?php

namespace App\Orchid\Actions;

use Illuminate\Support\Collection;
use Orchid\Crud\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Toast;

class DeleteAction extends Action
{
    /**
     * The button of the action.
     *
     * @return Button
     */
    public function button(): Button
    {
        return Button::make('Remove')
            ->confirm(__('admin.Are you sure you want to delete this resources?'))
            ->icon('trash');
    }

    /**
     * Perform the action on the given models.
     *
     * @param \Illuminate\Support\Collection $models
     */
    public function handle(Collection $models)
    {
        foreach ($models as $item) {
            $item->delete();
        }

        Toast::message(__('admin.Successful removed!'));
    }
}
