<?php

declare(strict_types=1);

namespace App\Orchid\Presenters;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use Illuminate\Support\Str;
use Laravel\Scout\Builder;
use Orchid\Screen\Contracts\Personable;
use Orchid\Screen\Contracts\Searchable;
use Orchid\Support\Presenter;

class UserPresenter extends Presenter implements Searchable, Personable
{
    /**
     * @return string
     */
    public function label(): string
    {
        return 'Users';
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->entity->name;
    }

    /**
     * @return string
     */
    public function subTitle(): string
    {
        $roles = $this->entity->roles->pluck('name')->implode(' / ');
        $isCook = $this->entity->isCook();

        return (string) Str::of($roles)
            ->limit(20)
            ->whenEmpty(fn () => $isCook ? 'Повар' : 'Пользователь');
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return route('platform.systems.users.edit', $this->entity);
    }

    /**
     * @return string
     */
    public function image(): ?string
    {
        /** @var Image|null $image */
        $image = $this->entity->getAvatar();

        return $image ? $image->resize('admin.preview')->getUrl() : ViewHelper::getEmptyAvatar($this->entity->isCook());
    }

    /**
     * The number of models to return for show compact search result.
     *
     * @return int
     */
    public function perSearchShow(): int
    {
        return 3;
    }

    /**
     * @param string|null $query
     *
     * @return Builder
     */
    public function searchQuery(string $query = null): Builder
    {
        return $this->entity->search($query);
    }
}
