<?php

namespace App\Orchid\Fields;

use App\Models\User;
use Orchid\Screen\Field;

class CookUser extends Field
{

    protected $view = 'orchid::fields.cook-user';

    protected static array $userCache = [];

    /**
     * @param mixed $value
     *
     * @return static
     */
    public function value($value): self
    {
        return $this->set('value', $value);
    }

    public function getAttributes(): array
    {
        $attrs = parent::getAttributes();

        if (isset($attrs['value'])) {
            $userId = $attrs['value'];

            if (!isset(static::$userCache[$userId])) {
                static::$userCache[$userId] = User::firstWhere('id', $userId);
            }

            $attrs['user'] = static::$userCache[$userId];
        }

        return $attrs;
    }
}
