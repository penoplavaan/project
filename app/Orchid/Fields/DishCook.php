<?php

namespace App\Orchid\Fields;

use App\Models\Cook;
use Orchid\Screen\Field;

class DishCook extends Field
{
    protected $view = 'orchid::fields.dish-cook';

    protected static $cookCache = [];

    /**
     * @param mixed $value
     *
     * @return static
     */
    public function value($value): self
    {
        return $this->set('value', $value);
    }

    public function getAttributes(): array
    {
        $attrs = parent::getAttributes();

        if (isset($attrs['value'])) {
            $cookId = $attrs['value'];

            if (!isset(static::$cookCache[$cookId])) {
                static::$cookCache[$cookId] = Cook::firstWhere('id', $cookId);
            }

            $attrs['cook'] = static::$cookCache[$cookId];
        }

        return $attrs;
    }
}
