<?php



namespace App\Orchid\Fields;

use Orchid\Screen\Field;

/**
 * Class Input.
 *
 * @method Tinymce accept($value = true)
 * @method Tinymce accesskey($value = true)
 * @method Tinymce autocomplete($value = true)
 * @method Tinymce autofocus($value = true)
 * @method Tinymce checked($value = true)
 * @method Tinymce disabled($value = true)
 * @method Tinymce form($value = true)
 * @method Tinymce formaction($value = true)
 * @method Tinymce formenctype($value = true)
 * @method Tinymce formmethod($value = true)
 * @method Tinymce formnovalidate($value = true)
 * @method Tinymce formtarget($value = true)
 * @method Tinymce name(string $value = null)
 * @method Tinymce placeholder(string $value = null)
 * @method Tinymce required(bool $value = true)
 * @method Tinymce value($value = true)
 * @method Tinymce help(string $value = null)
 * @method Tinymce title(string $value = null)
 */
class Tinymce extends Field
{

    /**
     * @var string
     */
    protected $view = 'orchid::fields.tinymce';

    /**
     * Default attributes value.
     *
     * @var array
     */
    protected $attributes = [
        'data-height' => '480'
    ];

    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    protected $inlineAttributes = [
        'accept',
        'accesskey',
        'autocomplete',
        'autofocus',
        'disabled',
        'form',
        'formaction',
        'formenctype',
        'formmethod',
        'formnovalidate',
        'formtarget',
        'name',
        'placeholder',
        'required',
        'value',
        'data-height'
    ];

}
