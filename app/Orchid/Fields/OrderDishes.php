<?php

namespace App\Orchid\Fields;

use App\Models\Order;
use Orchid\Screen\Field;

class OrderDishes extends Field
{
    protected $view = 'orchid::fields.order-dishes';

    /**
     * @param mixed $value
     *
     * @return static
     */
    public function value($value): self
    {
        return $this->set('value', $value);
    }

    public function getAttributes(): array
    {
        $attrs = parent::getAttributes();

        if (isset($attrs['value'])) {
            $orderId = $attrs['value'];

            $order = Order::whereId($orderId)->first();

            $attrs['order'] = $order;
        }

        return $attrs;
    }
}
