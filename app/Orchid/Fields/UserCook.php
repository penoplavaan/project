<?php

namespace App\Orchid\Fields;

use App\Models\Cook;
use Orchid\Screen\Field;

class UserCook extends Field
{

    protected $view = 'orchid::fields.user-cook';

    protected static array $cookCache = [];

    /**
     * @param mixed $value
     *
     * @return static
     */
    public function value($value): self
    {
        return $this->set('value', $value);
    }

    public function getAttributes(): array
    {
        $attrs = parent::getAttributes();

        if (isset($attrs['value'])) {
            $userId = $attrs['value'];

            if (!isset(static::$cookCache[$userId])) {
                static::$cookCache[$userId] = Cook::firstWhere('user_id', $userId);
            }

            $attrs['cook'] = static::$cookCache[$userId];
        }

        return $attrs;
    }
}
