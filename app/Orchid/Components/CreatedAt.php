<?php

namespace App\Orchid\Components;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\View\Component;

class CreatedAt extends Component
{

    public function __construct(protected Model $model)
    {
    }

    public function render()
    {
        /** @var Carbon $createdAt */
        $createdAt = $this->model->created_at;
        
        return $createdAt ? $createdAt->format('d.m.y H:i:s') : '';
    }
}
