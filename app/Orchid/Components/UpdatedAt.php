<?php

namespace App\Orchid\Components;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\View\Component;

class UpdatedAt extends Component
{

    public function __construct(protected Model $model)
    {
    }

    public function render()
    {
        /** @var Carbon $createdAt */
        $updatedAt = $this->model->updated_at;
        
        return $updatedAt ? $updatedAt->format('d.m.y H:i:s') : '';
    }
}
