<?php

namespace App\Orchid\Components;

use Illuminate\View\Component;
use Orchid\Attachment\Models\Attachment;

class Preview extends Component
{

    public function __construct(protected Attachment $attachment)
    {
    }

    public function render()
    {
        return view('orchid::components.preview', ['attachment' => $this->attachment]);
    }
}
