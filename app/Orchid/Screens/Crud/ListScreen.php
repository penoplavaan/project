<?php

namespace App\Orchid\Screens\Crud;

use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;

class ListScreen extends \Orchid\Crud\Screens\ListScreen
{

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        $grid = collect($this->resource->columns());

        $grid->prepend(TD::make()
           ->width(50)
           ->cantHide()
           ->canSee($this->availableActions()->isNotEmpty())
           ->render(function (Model $model) {
               return CheckBox::make('_models[]')
                   ->value($model->getKey())
                   ->checked(false);
           }));

        $grid->push(TD::make(__('Actions'))
            ->cantHide()
            ->width(50)
            ->render(function (Model $model) {
                return $this->getTableActions($model)
                    ->alignCenter()
                    ->autoWidth()
                    ->render();
            }));

        return [
            Layout::selection($this->resource->filters()),
            Layout::table('model', $grid->toArray()),
        ];
    }

    /**
     * @param Model $model
     *
     * @return Group
     */
    protected function getTableActions(Model $model): Group
    {
        return Group::make([

            Link::make('')
                ->icon('pencil')
                ->canSee($this->can('update'))
                ->route('platform.resource.edit', [
                    $this->resource::uriKey(),
                    $model->getAttribute($model->getKeyName()),
                ]),
        ]);
    }
}
