<?php

namespace App\Orchid\Screens\Menu;

use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Layouts\Modal;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Screen;
use App\Models\MenuItem;
use App\Orchid\Layouts\Menu\MenuListLayout;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Illuminate\Http\Request;

class MenuScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'menu_items' => MenuItem::leftMargin(),
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Меню сайта';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            ModalToggle::make('Create')
                ->modal('editModal')
                ->modalTitle(__('admin.menu-site-create'))
                ->method('editModalApply')
                ->icon('plus'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            $this->editModal(),
            $this->addModal(),
            MenuListLayout::class,
        ];
    }

    public function getModal($key): Modal
    {
        return Layout::modal(
            $key,
            [
                Layout::rows(
                    [
                        CheckBox::make('menuItem.active')
                            ->value(true)
                            ->title(__('admin.active'))
                            ->sendTrueOrFalse(),

                        Input::make('menuItem.title')
                            ->required()
                            ->title(__('admin.title')),

                        Relation::make('menuItem.parent_id')
                            ->title(__('admin.parent'))
                            ->fromModel(MenuItem::class, 'title'),

                        Input::make('menuItem.code')
                            ->title(__('admin.code')),

                        Input::make('menuItem.url')
                            ->title(__('admin.url')),

                        Input::make('menuItem.sort')
                            ->type('number')
                            ->value(500)
                            ->title(__('admin.sort')),

                        Picture::make('menuItem.icon_id')
                            ->title(__('admin.icon'))
                            ->acceptedFiles('.svg')
                            ->targetId(),
                    ]
                ),
            ]
        );
    }

    public function editModal(): Modal
    {
        return $this->getModal('editModal')->async('asyncGetData');
    }

    public function asyncGetData(MenuItem $menuItem): array
    {
        return [
            'menuItem' => $menuItem,
        ];
    }

    public function addModal()
    {
        return $this->getModal('addModal')->async('asyncAddData');
    }

    public function asyncAddData(MenuItem $parent): array
    {
        $model = MenuItem::make();
        $model->parent_id = $parent->id;

        return [
            'menuItem' => $model,
        ];
    }


    public function editModalApply(MenuItem $menuItem, Request $request): void
    {
        $request->validate($this->rules());

        $menuItem->fill($request->get('menuItem'))->save();
        Toast::info("Элемент [$menuItem->id: $menuItem->title] изменён");
    }

    public function addModalApply(MenuItem $parentItem, Request $request): void
    {
        $request->validate($this->rules());

        $menuItem = MenuItem::make();
        $menuItem->fill($request->get('menuItem'))->save();
        Toast::info("Элемент [$menuItem->id: $menuItem->title] добавден");
    }

    public function removeItem(MenuItem $menuItem)
    {
        $menuItem->deleteWithChildren();
        Toast::info("Элемент [$menuItem->id: $menuItem->title] удалён.");
    }

    public function rules(): array
    {
        return [
            'menuItem.title' => 'required|max:255',
            'menuItem.icon_id' => ['nullable', 'numeric'],
        ];
    }
}
