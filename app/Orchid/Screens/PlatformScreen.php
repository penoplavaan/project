<?php

declare(strict_types=1);

namespace App\Orchid\Screens;

use App\Models\Cook;
use App\Models\Dish;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class PlatformScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Административная панель';
    }

    /**
     * Display header description.
     *
     * @return string|null
     */
    public function description(): ?string
    {
        return 'Сайт использует администранивную панель Orchid';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Orchid')
                ->href('http://orchid.software')
                ->icon('globe-alt'),

            Link::make('Документация')
                ->href('https://orchid.software/en/docs')
                ->icon('docs'),

            Link::make('GitHub')
                ->href('https://github.com/orchidsoftware/platform')
                ->icon('social-github'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $welcomeData = [
            'users' => [
                'total' => User::count(),
                'email' => User::whereNot('email_verified_at', null)->count(),
                'phone' => User::whereNot('phone_verified', 0)->count(),
            ],
            'cooks' => [
                'total' => Cook::count(),
                'verified' => Cook::where('verified', 1)->count(),
            ],
            'dishes' => [
                'total' => Dish::count(),
            ],
            'orders' => [
                'all' => [
                    'count' => Order::count(),
                    'sum'   => (int)Order::query()->select(\DB::raw("SUM(sum) as sum"))->first()->sum
                ],
                'month' => [
                    'count' => Order::where('created_at', '>', Carbon::now()->subMonth())->count(),
                    'sum'   => (int)Order::query()
                        ->where('created_at', '>', Carbon::now()->subMonth())
                        ->select(\DB::raw("SUM(sum) as sum"))->first()->sum
                ],
                'week' => [
                    'count' => Order::where('created_at', '>', Carbon::now()->subWeek())->count(),
                    'sum'   => (int)Order::query()
                        ->where('created_at', '>', Carbon::now()->subWeek())
                        ->select(\DB::raw("SUM(sum) as sum"))->first()->sum
                ],
                'day' => [
                    'count' => Order::where('created_at', '>', Carbon::now()->subDay())->count(),
                    'sum'   => (int)Order::query()
                        ->where('created_at', '>', Carbon::now()->subDay())
                        ->select(\DB::raw("SUM(sum) as sum"))->first()->sum
                ],
            ]
        ];

        return [
            Layout::view('orchid::welcome', ['count' => $welcomeData]),
        ];
    }
}
