<?php

namespace App\Orchid\Screens\SiteSettings;

use App\Orchid\Http\Requests\SiteSectionDeleteRequest;
use App\Orchid\Http\Requests\SiteSectionsRequest;
use App\Models\SiteSettingsSection;
use App\Models\SiteSetting;
use App\Orchid\Layouts\SiteSettings\SiteSectionsListLayout;
use App\Orchid\Layouts\SiteSettings\SiteSettingsListLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class SiteSettingsScreen extends Screen
{
    /**
     * @var string
     */
    public string $name;
    public ?SiteSettingsSection $section;
    public ?SiteSettingsSection $previousSection;

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return $this->name;
    }

    /**
     * Query data.
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request): iterable
    {
        $sectionId = $request->get('section');
        $filterSettings = ['section_id' => null];
        $filterSections = ['parent_section_id' => null];

        $sectionElem = SiteSettingsSection::find($sectionId);
        $previosSection = null;

        if ($sectionElem) {
            $filterSettings['section_id'] = $sectionId;
            $filterSections['parent_section_id'] = $sectionId;
            $previosSection = $sectionElem->parent ?: new SiteSettingsSection();
        }

        $sectionPaginator = SiteSettingsSection::where($filterSections)->paginate(30);

        if ($sectionElem) {
            $newCollection = $sectionPaginator->getCollection()->prepend($previosSection);
            $sectionPaginator->setCollection($newCollection);
        }

        return [
            'site-settings'   => SiteSetting::filters()->where($filterSettings)->paginate(30),
            'site-sections'   => $sectionPaginator,
            'name'            => $sectionElem ? 'Раздел настроек сайта: ' . $sectionElem->name : 'Настройки сайта',
            'section'         => $sectionElem,
            'previousSection' => $previosSection,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Создать настройку')
                ->icon('plus')
                ->href(route('platform.site-settings.create', ['section' => $this->section?->id ?: 0])),

            ModalToggle::make('Создать раздел')
                ->modal('createSection')
                ->method('createSection')
                ->icon('folder')
                ->modalTitle('Создать раздел'),

            ModalToggle::make('Удалить раздел')
                ->modal('deleteSection')
                ->method('deleteSection')
                ->icon('trash')
                ->modalTitle('Удалить раздел'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::wrapper('orchid::layouts.vertical-columns', [
                'columnLeft'  => new SiteSectionsListLayout($this->previousSection),
                'columnRight' => SiteSettingsListLayout::class,
            ]),

            Layout::modal(
                'createSection',
                [
                    Layout::rows([
                        Input::make('site-sections.name')
                            ->title('Название')
                            ->required(),
                        Input::make('site-sections.code')
                            ->title('Код элемента')
                            ->required(),
                        Select::make('site-sections.parent_section_id')
                            ->fromModel(SiteSettingsSection::class, 'name', 'id')
                            ->empty('Корневой элемент')
                            ->value($this->section?->id ?: 0)
                            ->title('Родительский раздел'),
                    ]),
                ]
            )->applyButton('Создать'),

            Layout::modal(
                'deleteSection',
                [
                    Layout::rows([
                        Select::make('site-sections.id')
                            ->fromModel(SiteSettingsSection::class, 'name', 'id')
                            ->value($this->section?->id ?: 0)
                            ->title('Раздел'),
                    ]),
                ]
            )->applyButton('Удалить'),
        ];
    }

    /**
     * @param SiteSectionsRequest $request
     * @param SiteSettingsSection $siteSections
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createSection(SiteSectionsRequest $request, SiteSettingsSection $siteSections)
    {
        $data = $request->validated()['site-sections'];

        $siteSections->fill($data);

        $siteSections->save();

        Toast::info('Раздел успешно создан');

        return redirect()->route('platform.site-settings.index', ['section' => $siteSections->id]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|null
     */
    public function deleteSection(SiteSectionDeleteRequest $request): ?\Illuminate\Http\RedirectResponse
    {
        $validated = $request->validated();

        $sectionId = $validated['site-sections']['id'];

        /** @var SiteSettingsSection $section */
        $section = SiteSettingsSection::find($sectionId);
        $subSectionsCount = SiteSettingsSection::whereParentSectionId($sectionId)->count();

        if ($subSectionsCount > 0) {
            Toast::error('Раздел содержит дочерние разделы. Удалите их перед удалением раздела');
            return null;
        }

        $settingsCount = SiteSetting::whereSectionId($sectionId)->count();

        if ($settingsCount > 0) {
            Toast::error('Раздел содержит элементы. Удалите их перед удалением раздела');
            return null;
        }

        $section->delete();

        Toast::info('Раздел успешно Удалён');

        return redirect()->route('platform.site-settings.index');
    }
}
