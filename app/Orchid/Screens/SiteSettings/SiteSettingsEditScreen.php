<?php


namespace App\Orchid\Screens\SiteSettings;

use App\Orchid\Http\Requests\SiteSettingsRequest;
use App\Models\SiteSetting;
use App\Orchid\Layouts\SiteSettings\SiteSettingsEditLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class SiteSettingsEditScreen extends Screen
{
    /**
     * @var SiteSetting
     */
    public     $siteSettings;
    public int $section;

    /**
     * Query data.
     *
     * @param Request $request
     * @param SiteSetting $siteSettings
     * @return array
     */
    public function query(Request $request, SiteSetting $siteSettings): iterable
    {
        $section = (int)$request->get('section');

        return [
            'site-settings' => $siteSettings,
            'section'       => $section
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return request()->route()->getName() === 'platform.site-settings.edit'
            ? 'Редактирование настройки сайта'
            : 'Создание настройки сайта';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(request()->get('textarea') ? 'tinymce' : 'textarea')
                ->icon('reload')
                ->method('showTextarea', ['textarea' => request()->get('textarea')]),

            Button::make(__('admin.Accept'))
                ->icon('check')
                ->method('saveAndAccept'),

            Button::make(__('Save'))
                ->icon('check')
                ->method('saveAndReturn'),

            Button::make(__('admin.Save and return'))
                ->icon('check')
                ->method('saveAndAdd'),

            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove'),

            Button::make(__('Cancel'))
                ->icon('ban')
                ->method('cancel'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            Layout::blank([
                new SiteSettingsEditLayout($this->section),
            ]),
        ];
    }

    /**
     * @param Request $request
     * @param SiteSetting $siteSettings
     */
    public function save(SiteSettingsRequest $request, SiteSetting $siteSettings)
    {
        $data = $request->validated()['site-settings'];

        $siteSettings->fill($data)->save();

        $siteSettings->attachment()->syncWithoutDetaching(
            $data['attachment'] ?? []
        );

        Toast::info('Настройка успешно создана');
    }

    /**
     * @param SiteSettingsRequest $request
     * @param SiteSetting $siteSettings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveAndReturn(SiteSettingsRequest $request, SiteSetting $siteSettings)
    {
        $this->save($request, $siteSettings);

        return redirect()->route('platform.site-settings.index', ['section' => $siteSettings->section_id]);
    }

    /**
     * @param SiteSettingsRequest $request
     * @param SiteSetting $siteSettings
     * @return void
     */
    public function saveAndAccept(SiteSettingsRequest $request, SiteSetting $siteSettings): void
    {
        $this->save($request, $siteSettings);

        Toast::success(__('admin.Successful saved!'));
    }

    /**
     * @param SiteSettingsRequest $request
     * @param SiteSetting $siteSettings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveAndEdit(SiteSettingsRequest $request, SiteSetting $siteSettings)
    {
        $this->save($request, $siteSettings);

        return redirect()->route('platform.site-settings.edit', ['section' => $siteSettings->section_id]);
    }

    /**
     * @param SiteSettingsRequest $request
     * @param SiteSetting $siteSettings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveAndAdd(SiteSettingsRequest $request, SiteSetting $siteSettings)
    {
        $this->save($request, $siteSettings);

        return redirect()->route('platform.site-settings.create', ['section' => $siteSettings->section_id]);
    }

    /**
     * @param SiteSetting $siteSettings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(SiteSetting $siteSettings)
    {
        $siteSettings->delete();

        Toast::info('Настройка удалена');

        return redirect()->route('platform.site-settings.index', ['section' => $siteSettings->section_id]);
    }

    /**
     * @param SiteSetting $siteSettings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancel(SiteSetting $siteSettings)
    {
        return redirect()->route('platform.site-settings.index', ['section' => $siteSettings->section_id]);
    }

    /**
     * @param SiteSettingsRequest $request
     * @param SiteSetting $siteSettings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function showTextarea(SiteSettingsRequest $request, SiteSetting $siteSettings)
    {
        $params = ['siteSetting' => $siteSettings->id, 'textarea' => 1];

        if (request()->get('textarea')) {
            unset($params['textarea']);
        }

        return redirect()->route('platform.site-settings.edit', $params);
    }
}
