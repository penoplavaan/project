<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class IsCook {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        /** @var User $user */
        $user = $request->user();

        if (!$user) {
            abort(403);
        }

        if (!$user->isCook()) {
            abort(403);
        }

        return $next($request);
    }
}

