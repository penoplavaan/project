<?php

namespace App\Http\Requests;

use App\Helpers\BaseHelper;
use App\Models\DishCategory;
use App\Models\Tag;
use App\Rules\ActiveDishId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileAddDishFormRequest extends FormRequest
{

    public function rules()
    {
        $fileSize = ceil(BaseHelper::getMaxFileSize() / 1024); // to KB
        $categoriesIds = DishCategory::query()->select(['id'])->get()->pluck('id')->toArray();
        $tagIds = Tag::query()->select(['id'])->get()->pluck('id')->toArray();

        return [
            'id'                 => ['nullable', new ActiveDishId],
            'category_id'        => ['required', Rule::in($categoriesIds)],
            'name'               => ['required', 'max:255'],
            'price'              => ['required', 'numeric', 'max:1000000'],
            'preview_text'       => ['required', 'max:2000'],
            'storage_conditions' => ['nullable', 'max:2000'],
            'calories'           => ['nullable', 'max:20'],
            'protein'            => ['nullable', 'max:20'],
            'fats'               => ['nullable', 'max:20'],
            'carbohydrates'      => ['nullable', 'max:20'],
            'quantity'           => ['required', 'max:40'],
            'weight'             => ['required', 'max:40'],
            'package'            => ['nullable', 'max:40'],
            'cooking_time'       => ['nullable', 'max:100'],

            'tags'               => ['nullable', 'array', Rule::in($tagIds)],
            'images'             => ['array', "min:0", "max:" . (int)setting('max.file.count')->text ?? 10],
            'images.*'           => ['image', "max:$fileSize"],
            // Разобраться как сделать так чтобы images + attachment Ids были меньше 10-ти
            'attachmentsIds'     => ['array', "min:0", "max:" . (int)setting('max.file.count')->text ?? 10],
        ];
    }
}
