<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DishRemoveRequest extends FormRequest
{

    public function rules()
    {
        return [
            'id'   => ['required', 'integer'],
        ];
    }
}
