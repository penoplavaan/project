<?php

namespace App\Http\Requests;

use App\Helpers\BaseHelper;
use App\Helpers\ViewHelper;
use Illuminate\Foundation\Http\FormRequest;

class ProfileEditFormRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        parent::prepareForValidation();
        $this->merge([
            'phone' => ViewHelper::clearPhone($this->phone),
        ]);
    }

    public function rules()
    {
        $fileSize = ceil(BaseHelper::getMaxFileSize() / 1024); // to KB

        return [
            'name'           => ['required', 'max:255'],
            'email'          => ['nullable', 'email:rfc,dns', 'max:255'],
            'phone'          => ['required', 'phone'],
            'about'          => ['nullable', 'max:2000'],
            'address'        => ['nullable', 'max:255'],

            'avatar'        => ['array', "min:0", "max:1"],
            'avatar.*'      => ['image', "max:$fileSize"],
        ];
    }
}
