<?php

namespace App\Http\Requests;

use App\Forms\FormDescriptionInterface;
use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest implements FormDescriptionInterface
{

    public function rules()
    {
        return [
            'password'    => ['required'],
            'newPassword' => ['required', 'min:6'],
        ];
    }

    public function formDescription(): array
    {
        return [
            "attributes" => [
                "method" => "POST",
                "name"   => "account-auth",
                "action" => route('profile.change-password')
            ],

            "fields" => [
                "password" => [
                    "type"     => "password",
                    "name"     => "password",
                    "label"    => "Текущий пароль",
                    "required" => true,
                ],
                "newPassword" => [
                    "type"     => "password",
                    "name"     => "newPassword",
                    "label"    => "Новый пароль",
                    "required" => true,
                ]
            ],

            "validation" => $this->rules(),
            "validationType" => 'laravel',
        ];
    }
}
