<?php

namespace App\Http\Requests;

use App\Models\City;
use App\Models\CookSpeciality;
use App\Models\DishCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileCookFormRequest extends FormRequest
{

    public function rules()
    {
        $specialityIds = CookSpeciality::query()->select(['id'])->get()->pluck('id')->toArray();
        $categoryIds = DishCategory::query()->select(['id'])->get()->pluck('id')->toArray();

        $cityIds = City::query()->select(['id'])->get()->pluck('id')->toArray();

        return [
            'about'          => ['max:2000'],
            'experience'     => ['integer'],

            'city_id'        => ['required', Rule::in($cityIds)],
            'address'        => ['required', 'max:255'],

            'speciality_id'  => ['required', Rule::in($specialityIds)],
            'categories'     => ['required', 'array', Rule::in($categoryIds)],
        ];
    }
}
