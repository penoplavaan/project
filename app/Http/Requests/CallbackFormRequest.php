<?php

namespace App\Http\Requests;

use App\Helpers\ViewHelper;
use Illuminate\Foundation\Http\FormRequest;

class CallbackFormRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        parent::prepareForValidation();
        $this->merge([
            'phone' => ViewHelper::clearPhone($this->phone),
        ]);
    }

    public function rules()
    {
        $recaptchaActive = (int)setting('recaptcha.active')->text;

        return [
            'phone'         => ['required', 'phone'],
            'promo_card_id' => ['nullable'],
            'recaptcha'     => $recaptchaActive ? ['required'] : ['nullable'],
        ];
    }
}
