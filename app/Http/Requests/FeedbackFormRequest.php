<?php

namespace App\Http\Requests;

use App\Helpers\ViewHelper;
use App\Models\FeedbackCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeedbackFormRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        parent::prepareForValidation();
        $this->merge([
            'phone' => ViewHelper::clearPhone($this->phone),
        ]);
    }

    public function rules()
    {
        $categoryIds = FeedbackCategory::query()->select(['id'])->get()->pluck('id')->toArray();

        $recaptchaActive = (int)setting('recaptcha.active')->text;

        return [
            'name'     => ['required', 'max:255'],
            'phone'    => ['required', 'phone'],

            'email'    => ['required', 'email:rfc,dns', 'max:255'],
            'question' => ['required', 'max:2000'],

            'topic_id'  => ['required', Rule::in($categoryIds)],
            'recaptcha' => $recaptchaActive ? ['required'] : ['nullable'],
        ];
    }
}
