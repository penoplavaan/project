<?php

namespace App\Http\Requests;

use App\Helpers\BaseHelper;
use App\Helpers\ViewHelper;
use Illuminate\Foundation\Http\FormRequest;

class BecomeCookFormRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        parent::prepareForValidation();
        $this->merge([
            'phone' => ViewHelper::clearPhone($this->phone),
        ]);
    }

    public function rules()
    {
        $fileSize = ceil(BaseHelper::getMaxFileSize() / 1024); // to KB
        $fileCount = BaseHelper::getMaxFileCount();

        $profileRequest = new ProfileCookFormRequest();
        $profileRules = $profileRequest->rules();

        return array_merge($profileRules, [
            'name'           => ['required', 'max:255'],
            'email'          => ['required', 'email:rfc,dns', 'max:255'],

            'avatar'        => ['array', "min:0", "max:1"],
            'avatar.*'      => ['image', "max:$fileSize"],

            'kitchen'       => ['array', "min:0", "max:$fileCount"],
            'kitchen.*'     => ['image', "max:$fileSize"],

            'passport'      => ['array', "max:1"],
            'passport.*'    => ['mimes:' . EXT_ALLOWED_FILES, "max:$fileSize"],

            'sanitary'      => ['array', "min:0", "max:$fileCount"],
            'sanitary.*'    => ['mimes:' . EXT_ALLOWED_FILES, "max:$fileSize"],

            'certificate'   => ['array', "min:0", "max:$fileCount"],
            'certificate.*' => ['mimes:' . EXT_ALLOWED_FILES, "max:$fileSize"],
        ]);
    }
}
