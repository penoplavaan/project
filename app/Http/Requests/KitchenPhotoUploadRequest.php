<?php

namespace App\Http\Requests;

use App\Helpers\BaseHelper;
use Illuminate\Foundation\Http\FormRequest;

class KitchenPhotoUploadRequest extends FormRequest
{

    public function rules()
    {
        $fileSize = ceil(BaseHelper::getMaxFileSize() / 1024); // to KB

        return [
            'kitchenPhoto' => ['required', 'image', "max:$fileSize"],
        ];
    }
}
