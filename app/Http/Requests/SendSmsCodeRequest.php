<?php

namespace App\Http\Requests;

use App\Helpers\ViewHelper;
use App\Services\SmsService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @property string type
 * @property string phone
 */
class SendSmsCodeRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        parent::prepareForValidation();
        $this->merge([
            'phone' => ViewHelper::clearPhone($this->phone),
        ]);
    }

    public function rules()
    {
        return [
            'phone' => ['required', 'phone'],
            'type' => ['required', 'string', Rule::in(SmsService::getTypesList())],
        ];
    }
}
