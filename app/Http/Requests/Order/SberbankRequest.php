<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class SberbankRequest extends FormRequest {
    public function rules(): array {
        return [
            'mdOrder'     => ['required'],
            'orderNumber' => ['required', 'exists:orders,id'],
            'checksum'    => ['required'],
            'operation'   => ['required'],
            'status'      => ['required'],
        ];
    }
}
