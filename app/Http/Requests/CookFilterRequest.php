<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CookFilterRequest extends FormRequest
{

    public function rules()
    {
        return [
            'cook' => ['nullable'],
            'category' => ['nullable', 'array'],
            'speciality' => ['nullable', 'array'],
        ];
    }
}
