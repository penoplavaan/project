<?php

namespace App\Http\Requests;

use App\Helpers\BaseHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DocumentUploadRequest extends FormRequest
{

    public function rules()
    {
        $fileSize = ceil(BaseHelper::getMaxFileSize() / 1024); // to KB

        return [
            'type'     => ['required', 'string', Rule::in(['document', 'certificate'])],
            'document' => ['required', 'mimes:' . EXT_ALLOWED_FILES, "max:$fileSize"],
        ];
    }
}
