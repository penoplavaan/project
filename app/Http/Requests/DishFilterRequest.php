<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DishFilterRequest extends FormRequest
{

    public function rules()
    {
        return [
            'all'      => ['nullable', 'string'],
            'search'   => ['nullable', 'string'],
            'category' => ['nullable', 'array'],
            'price'    => ['nullable', 'array'],
            'price.*'  => ['numeric'],
            'tags'     => ['nullable', 'array'],
        ];
    }
}
