<?php

namespace App\Http\Requests;

use App\Helpers\BaseHelper;
use Illuminate\Foundation\Http\FormRequest;

class ChatMessageRequest extends FormRequest
{

    public function rules()
    {
        $fileSize = ceil(BaseHelper::getMaxFileSize() / 1024); // to KB
        $fileCount = BaseHelper::getMaxFileCount();

        return [
            'chatUserId'   => ['required', 'numeric'],
            'message'      => ['required', ],
            'images'       => ['array', "min:0", "max:$fileCount"],
            'images.*'     => ['image', "max:$fileSize"],
        ];
    }
}
