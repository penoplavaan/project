<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DocumentRemoveRequest extends FormRequest
{

    public function rules()
    {
        return [
            'type' => ['required', 'string', Rule::in(['document', 'certificate'])],
            'id'   => ['required', 'integer'],
        ];
    }
}
