<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Cook;
use Carbon\Carbon;

/**
 * @mixin Cook
 */
class CookProfileResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'avatar'        => $this->getPicture(),
            'name'          => $this->user ? $this->user->getFullName() : '',
            'speciality'    => $this->getPosition(),
            'specialityId'  => $this->speciality->id ?? 0,
            'categories'    => $this->getCategories(),
            'categoriesIds' => $this->getCategoriesId(),
            'labels'        => $this->getLabels(),
            'about'         => $this->about,
            'city_id'       => $this->city_id,
            'address'       => $this->address,
            'experience'    => $this->getExperienceYears(),
            'experienceRaw' => $this->experience,
            'registerDate'  => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d-m-Y'),
            'rating'        => $this->getRoundRating(),
            'reviewsCount'  => $this->reviews_count,
        ];
    }
}
