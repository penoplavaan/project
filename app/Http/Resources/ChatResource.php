<?php

namespace App\Http\Resources;

use App\Models\Chat;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Chat
 */
class ChatResource extends JsonResource
{
    public function toArray($request)
    {
        $currentUser = app()->make(UserService::class)->current();

        return [
            'fromUserId' => $currentUser->id,
            'toUserId'   => $this->getOtherUserId($currentUser->id),
            'lastReadId' => $this->getLastReadId($currentUser->id),
        ];
    }
}
