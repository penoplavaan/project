<?php

namespace App\Http\Resources;

use App\Helpers\BaseHelper;
use App\Models\Dish;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Dish
 */
class DishEditResource extends JsonResource
{
    public function toArray($request)
    {
        $pictures = [];
        $images = $this->attachment ?? '';
        if (!empty($images)) {
            foreach ($images as $image) {
                $pictures[] = [
                    'src' => $image->getImage()->getUrl(),
                    'id' => $image->id,
                ];
            }
        }
        $tags = $this->tags->pluck('id')->toArray();

        return [
            'id'                 => $this->id,
            'name'               => $this->name,
            'preview_text'       => $this->preview_text,
            'storage_conditions' => $this->storage_conditions,
            'cooking_time'       => $this->cooking_time,
            'package'            => $this->package,
            'calories'           => $this->calories,
            'protein'            => $this->protein,
            'fats'               => $this->fats,
            'carbohydrates'      => $this->carbohydrates,
            'tags'               => $tags,
            'existingPictures'   => $pictures,
            'price'              => $this->price,
            'category_id'        => $this->category_id,
            'weight'             => $this->weight,
            'quantity'           => is_numeric(trim($this->quantity))
                ? $this->quantity . ' порци' . BaseHelper::getWordForm((int)$this->quantity, ['я', 'и', 'й'])
                : trim($this->quantity),
        ];
    }
}
