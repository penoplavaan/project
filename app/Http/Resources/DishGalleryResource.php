<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Models\Dish;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Dish
 */
class DishGalleryResource extends JsonResource
{
    public function toArray($request)
    {
        $pictures = [];
        foreach ($this->attachment->sort()->all() as $image) {
            $pictures[] = $image->getImage()->resize('detail.dish.gallery')->getData();
        }

        if (empty($pictures)) {
            $pictures[] = Image::getFakeData('/images/empty-dish.svg');
        }

        return $pictures;
    }
}
