<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin User
 */
class ChatUserResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'id'      => $this->id,
            'name'    => $this->getFullName(),
            'avatar'  => $this->getAvatar()
                ? $this->getAvatar()->resize('chat.avatar')->getData()
                : Image::getFakeData(ViewHelper::getEmptyAvatar($this->isCook())),
            'address' => $this->cook && $this->cook->isVisible() ? ($this->cook->address ?? '') : '',
        ];

        if ($this->cook && $this->cook->isVisible()) {
            $data['url'] = $this->cook->getUrl();
        }

        return $data;
    }
}
