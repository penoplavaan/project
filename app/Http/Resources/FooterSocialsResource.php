<?php

namespace App\Http\Resources;

use App\Models\SiteSetting;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin SiteSetting
 */
class FooterSocialsResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'url'          => $this->text,
            'picture'      => isset($this->attachment[0]) ? $this->attachment[0]->getImage()->getData() : '',
            'pictureTitle' => $this->text,
            'code'         => str_replace('footer.icons.', '', $this->code),
        ];
    }
}
