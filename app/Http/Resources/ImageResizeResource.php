<?php

namespace App\Http\Resources;

use App\Models\Attachment;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Attachment
 */
class ImageResizeResource extends JsonResource
{
    protected string $resize = '';

    public function __construct($resource, $resize = '')
    {
        parent::__construct($resource);

        $this->resize = $resize;
    }

    public function toArray($request)
    {
        $image = $this->getImage();

        if ($this->resize) {
            $image = $image->resize($this->resize);
        }

        return $image->getData();
    }
}
