<?php

namespace App\Http\Resources;

use App\Models\Attachment;
use App\Models\ForCookFeature;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin ForCookFeature
 */
class ForCookFeatureResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name'  => $this->title,
            'text'  => $this->text,
            'image' => $this->getIcon(),
        ];
    }
}
