<?php

namespace App\Http\Resources;

use App\Dto\Basket\BasketByCook;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin BasketByCook
 */
class BasketByCookResource extends JsonResource
{
    public function toArray($request): array
    {
        $cook = $this->getCook();

        return [
            'cook' => [
                'id'         => $cook->id,
                'name'       => $cook->user?->getFullName() ?: '',
                'speciality' => $cook->speciality?->title ?: '',
                'picture'    => $cook->getPicture('order.basket'),
                'url'        => $cook->getUrl(),
            ],
            'rows' => BasketRowResource::collection($this->getRows()),
        ];
    }
}
