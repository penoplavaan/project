<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Models\ChatMessage;
use App\Models\OrderToDishes;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin ChatMessage
 */
class ChatMessageResource extends JsonResource
{
    public function toArray($request)
    {
        $currentUser = app()->make(UserService::class)->current();

        $data = [
            'id'         => $this->id,
            'chatUserId' => $this->chat->getOtherUserId($currentUser->id),
            'message'    => $this->message,
            'date'       => $this->getTimestamp(),
            'userId'     => $this->user_id,
        ];

        $pivots = $this->order?->orderToDishes;
        $emptyDish = Image::getFakeData('/images/empty-dish.svg');

        if ($pivots && $pivots->count()) {
            $data['products'] = [];

            /** @var OrderToDishes $pivot */
            foreach ($pivots as $pivot) {
                $dish = $pivot->dish;
                $data['products'][] = [
                    'id'        => $dish ? $dish->id : 0,
                    'name'      => $pivot->name,
                    'picture'   => $dish && $dish->getImage()
                        ? $dish->getImage()->resize('chat.dish.preview')->getData()
                        : $emptyDish,
                    'price'     => $pivot->price_one,
                    'count'     => $pivot->count,
                    'href'      => $dish ? $dish->getUrl() : 'javascript:void(0)',
                ];
            }
        }

        $images = $this->resource->attachment;
        if ($images && $images->count()) {
            $data['images'] = [];
            foreach ($images as $image) {
                $data['images'][] = ImageResizeResource::make($image, 'chat.dish.preview')->resolve();
            }
        }

        return $data;
    }
}
