<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use App\Models\Cook;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Cook
 */
class CookDetailResource extends JsonResource
{
    public function toArray($request)
    {
        $avatar = $this->user->getAvatar()
            ? $this->user->getAvatar()->resize('profile.avatar')->getData()
            : Image::getFakeData(ViewHelper::getEmptyAvatar(true));

        return [
            'avatar'       => $avatar,
            'name'         => $this->user->getFullName(),
            'speciality'   => $this->speciality->title,
            'categories'   => $this->category->map(fn($cat) => ['id' => $cat->id, 'name' => $cat->title, 'link' => $cat->getUrl()])->toArray(),
            'labels'       => $this->getLabels(),
            'about'        => $this->about,
            'address'      => $this->address,
            'experience'   => $this->getExperienceYears(),
            'registerDate' => $this->getRegisterDate(),
            'rating'       => $this->getRoundRating(),
            'reviewsCount' => $this->reviews_count,
        ];
    }
}
