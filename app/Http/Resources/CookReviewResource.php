<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use App\Models\CookReview;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin CookReview
 */
class CookReviewResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'user'   => [
                'name'   => $this->user->getFullName(),
                'avatar' => $this->user->getAvatar()
                    ? $this->user->getAvatar()->resize('chat.avatar')->getData()
                    : Image::getFakeData(ViewHelper::getEmptyAvatar($this->user->isCook())),
            ],
            'date'   => ViewHelper::localizedDate($this->created_at ?: new Carbon()),
            'rating' => $this->rating,
            'text'   => $this->text,
        ];
    }
}
