<?php

namespace App\Http\Resources;

use App\Dto\Basket\BasketRow;
use App\Helpers\Image;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin BasketRow
 */
class BasketRowResource extends JsonResource
{
    public function toArray($request): array
    {
        $dish = $this->getDish();
        if (empty($dish)) return [];
        $image = $dish->getImage();

        if (!empty($image)) {
            $picture = $image->resize('basket.row')->getData();
        } else {
            $picture = Image::getFakeData('/images/empty-dish.svg');
        }

        return [
            'id'          => $dish->id,
            'picture'     => $picture,
            'name'        => $dish->name,
            'price'       => $dish->price,
            'url'         => $dish->getUrl(),
            'weight'      => $dish->weight,
            'count'       => $this->getCount(),
        ];
    }
}
