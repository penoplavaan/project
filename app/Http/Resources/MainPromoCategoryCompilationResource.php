<?php

namespace App\Http\Resources;

use App\Models\MainPromoCategoryCompilation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin MainPromoCategoryCompilation
 */
class MainPromoCategoryCompilationResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'items' => PromoCardMainCompilationResource::collection($this->promoCards->slice(0, 12)),
        ];
    }
}
