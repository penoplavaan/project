<?php

namespace App\Http\Resources;


use App\Helpers\Image;
use App\Models\PromoCard;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PromoCard
 */
class PromoCardMainCompilationResource extends JsonResource
{
    public function toArray($request)
    {
        $image = $this->getImage();
        if (!empty($image)) {
            $picture = $image->resize('main.category.compilation')->getData();
        } else {
            $picture = Image::getFakeData('/images/empty-dish.svg');
        }

        return [
            'id'          => $this->id,
            'picture'     => $picture,
            'name'        => $this->name,
            'link'        => $this->link ?? '',
            'sort'        => $this->sort ?? 500,
        ];
    }

}
