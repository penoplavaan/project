<?php

namespace App\Http\Resources;

use App\Models\Attachment;
use App\Models\ForCookReview;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin ForCookReview
 */
class ForCookReviewResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name'        => $this->name,
            'position'    => $this->position,
            'previewText' => $this->text,
            'picture'     => $this->getImage(),
        ];
    }
}
