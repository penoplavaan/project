<?php

namespace App\Http\Resources;

use App\Models\MenuItem;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin MenuItem
 */
class TopMenuItemsResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name'    => $this->title,
            'url'     => $this->url,
            'current' => $this->isCurrent(),
            'items'   => $this->getManualSubmenu(),
        ];
    }
}
