<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Models\Basket;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Basket
 */
class BasketResource extends JsonResource
{

    public function toArray($request)
    {
        $emptyDish = Image::getFakeData('/images/empty-dish.svg');

        return [
            'id' => $this->id,
            'product' => [
                'id'      => $this->dish_id,
                'name'    => $this->dish->name,
                'picture' => $this->dish->getImage() ? $this->dish->getImage()->resize('chat.dish.preview')->getData() : $emptyDish,
                'price'   => $this->dish->price,
                'count'   => $this->count,
                'href'    => $this->dish->getUrl(),
            ]
        ];
    }
}
