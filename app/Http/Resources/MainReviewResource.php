<?php

namespace App\Http\Resources;

use App\Helpers\ViewHelper;
use App\Models\MainReview;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Helpers\Image;

/**
 * @mixin MainReview
 */
class MainReviewResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        $empty = Image::getFakeData(ViewHelper::getEmptyAvatar(false));

        return [
            'name'        => $this->name,
            'position'    => $this->position,
            'previewText' => $this->text,
            'picture'     => isset($this->attachment[0]) ? $this->attachment[0]->getImage()->resize('index.review')->getData()
                : $empty,
        ];
    }
}
