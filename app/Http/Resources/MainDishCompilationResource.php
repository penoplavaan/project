<?php

namespace App\Http\Resources;

use App\Models\MainDishCompilation;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin MainDishCompilation
 */
class MainDishCompilationResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'url'   => $this->url,
            'items' => DishInListResource::collection($this->dishes->filter(fn($dish) => !!$dish->cook)->slice(0, 12)),
        ];
    }
}
