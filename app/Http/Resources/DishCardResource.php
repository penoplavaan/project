<?php

namespace App\Http\Resources;

use App\Helpers\BaseHelper;
use App\Helpers\Image;
use App\Models\Dish;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Dish
 */
class DishCardResource extends JsonResource
{
    public function toArray($request)
    {
        $image = $this->getImage();
        if (!empty($image)) {
            $picture = $image->resize('dish.card')->getData();
        } else {
            $picture = Image::getFakeData('/images/empty-dish.svg');
        }

        return [
            'id'          => $this->id,
            'picture'     => $picture,
            'name'        => $this->name,
            'price'       => $this->price,
            'cook'        => [],
            'url'         => $this->getUrl(),
            'weight'      => $this->weight,
            'quantity'    => is_numeric(trim($this->quantity))
                ? $this->quantity . ' порци' . BaseHelper::getWordForm((int)$this->quantity, ['я', 'и', 'й'])
                : trim($this->quantity),
            'previewText' => $this->getShortPeviewText(),
            'labels'      => LabelResource::collection($this->labels)
        ];
    }
}
