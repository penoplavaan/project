<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Cook;

/**
 * @mixin Cook
 */
class CookShortResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'       => $this->id,
            'name'     => $this->user ? $this->user->getFullName() : '',
            'url'      => $this->getUrl(),
            'position' => $this->getPosition(),
            'picture'  => $this->getPicture('chat.avatar'),
        ];
    }
}
