<?php

namespace App\Http\Resources;

use App\Models\Dish;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Dish
 */
class DishDetailResource extends JsonResource
{
    public function toArray($request)
    {

        return [
            'id'                => $this->id,
            'gallery'           => DishGalleryResource::make($this)->resolve(),
            'previewText'       => $this->preview_text,
            'storageConditions' => $this->storage_conditions,
            'calories'          => $this->calories,
            'protein'           => $this->protein,
            'fats'              => $this->fats,
            'carbohydrates'     => $this->carbohydrates,
            'weight'            => $this->weight,
            'distance'          => $this->cook->getDistance(),
//            'quantity'          => $this->quantity,
            'cookingTime'       => $this->cooking_time,
            'package'           => $this->package,
            'delivery'          => $this->delivery,
            'tags'              => $this->tags->toArray(),
            'price'             => $this->price,
            'cook'              => CookResource::make($this->cook)->resolve(),
            'labels'            => LabelResource::collection($this->labels)->resolve(),
        ];
    }
}
