<?php

namespace App\Http\Resources;

use App\Models\Cook;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Cook
 */
class CookCoordsResource extends JsonResource
{
    public function toArray($request)
    {
        $coords = explode(',', $this->coords ?? '');

        return [
            'id'   => $this->id,
            'name' => $this->user?->getFullName() ?: '',
            'map'  => [
                (float)($coords[0] ?? '0'),
                (float)($coords[1] ?? '0'),
            ],
        ];
    }
}
