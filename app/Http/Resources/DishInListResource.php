<?php

namespace App\Http\Resources;

use App\Models\Dish;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Dish
 */
class DishInListResource extends DishCardResource
{
    public function toArray($request)
    {
        $parentData = parent::toArray($request);
        $cook = $this->cook;

        return array_merge($parentData, [
            'cook' => $cook ? [
                'name'         => $cook->user ? $cook->user->getFullName() : 'Повар',
                'url'          => $cook->getUrl(),
                'distance'     => $cook->getDistance(),
                'picture'      => $cook->getPicture(),
                'rating'       => $cook->getRoundRating(),
                'reviewsCount' => $cook->reviews_count,
            ] : [],
        ]);
    }
}
