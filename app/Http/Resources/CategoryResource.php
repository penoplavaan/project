<?php

namespace App\Http\Resources;

use App\Models\DishCategory;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin DishCategory
 */
class CategoryResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->title,
            'url'  => $this->getUrl(),
        ];
    }
}
