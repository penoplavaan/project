<?php

namespace App\Http\Resources;

use App\Models\ForCookSupport;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin ForCookSupport
 */
class ForCookSupportResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->title,
            'text' => $this->text,
        ];
    }
}
