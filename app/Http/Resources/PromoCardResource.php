<?php

namespace App\Http\Resources;


use App\Helpers\Image;
use App\Models\PromoCard;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PromoCard
 */
class PromoCardResource extends JsonResource
{

    public function toArray($request)
    {
        $image = $this->getImage();
        if (!empty($image)) {
            $picture = $image->resize('dish.card')->getData();
        } else {
            $picture = Image::getFakeData('/images/empty-dish.svg');
        }

        return [
            'id'          => $this->id,
            'picture'     => $picture,
            'name'        => $this->name,
            'price'       => $this->price,
            'previewText' => $this->preview_text,
            'buttonText'  => $this->button_text,
        ];
    }

}
