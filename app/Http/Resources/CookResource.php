<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Cook;

/**
 * @mixin Cook
 */
class CookResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'           => $this->id,
            'name'         => $this->user ? $this->user->getFullName() : '',
            'url'          => $this->getUrl(),
            'labels'       => $this->getLabels(),
            'position'     => $this->getPosition(),
            'distance'     => $this->getDistance(),
            'delivery'     => $this->delivery ?? '',
            'picture'      => $this->getPicture(),
            'tags'         => CategoryResource::collection($this->category),
            'address'      => $this->address ?? '',
            'rating'       => $this->getRoundRating(),
            'reviewsCount' => $this->reviews_count,
        ];
    }
}
