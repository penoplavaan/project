<?php

namespace App\Http\Resources;

use App\Models\ForCookFaq;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin ForCookFaq
 */
class ForCookFaqResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'title' => $this->title,
            'text'  => $this->text,
        ];
    }
}
