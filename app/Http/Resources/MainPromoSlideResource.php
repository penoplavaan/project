<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\MainPromoSlide;

/**
 * @mixin MainPromoSlide
 */
class MainPromoSlideResource extends JsonResource
{

    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        $cook = $this->dish->cook;

        $avatar = $cook->user->getAvatar()
            ? $cook->user->getAvatar()->resize('profile.avatar')->getData()
            : Image::getFakeData(ViewHelper::getEmptyAvatar(true));

        return [
            'id'           => $this->dish->id,
            'name'         => $this->dish->name,
            'price'        => $this->dish->price,
            'url'          => $this->url ?: route('menu.index'),
            'weight'       => $this->dish->weight ?? '',
            'previewText'  => $this->dish->preview_text ?? '',
            'picture'      => $this->getPicture(),
            'cook'         => [
                'name'     => $cook->user->getFullName(),
                'url'      => $cook->getUrl(),
                'distance' => $cook->getDistance(),
                'picture'  => $avatar,
                'position' => $cook->getPosition(),
            ],
        ];
    }
}
