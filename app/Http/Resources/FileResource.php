<?php

namespace App\Http\Resources;

use App\Models\Attachment;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Attachment
 */
class FileResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->original_name,
            'href' => $this->relative_url,
        ];
    }
}
