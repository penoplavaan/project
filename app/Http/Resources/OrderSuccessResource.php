<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Order
 */
class OrderSuccessResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'     => $this->id,
            'payed'  => $this->payed == 1,
            'cookId' => $this->cook_id,
            'hasChat' => $this->message_id && auth()->id() && auth()->id() === $this->user_id,
        ];
    }
}
