<?php

namespace App\Http\Resources;

use App\Forms\VerifyPhoneForm;
use App\Helpers\Image;
use App\Helpers\ViewHelper;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\User
 */
class UserProfileResource extends JsonResource
{

    public function toArray($request)
    {
        $avatar = $this->getAvatar()
            ? $this->getAvatar()->resize('profile.avatar')->getData()
            : Image::getFakeData(ViewHelper::getEmptyAvatar($this->isCook()));

        return [
            'avatar'          => $avatar,
            'fio'             => $this->getFullName(true),
            'address'         => $this->address,
            'about'           => $this->about,
            'registerDate'    => $this->getRegisterDate(),
            'phone'           => ViewHelper::formatPhone($this->phone),
            'phoneVerified'   => (bool)$this->phone_verified,
            'email'           => $this->email,
            'emailVerified'   => $this->emailVerified,
            'verifyPhoneForm' => !$this->phone_verified ? (new VerifyPhoneForm())->toArray() : null,
        ];
    }
}
