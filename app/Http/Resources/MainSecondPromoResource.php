<?php

namespace App\Http\Resources;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use App\Models\MainSecondPromo;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin MainSecondPromo
 */
class MainSecondPromoResource extends JsonResource
{

    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        $cook = $this->cook;

        $avatar = $cook->user->getAvatar()
            ? $cook->user->getAvatar()->resize('profile.avatar')->getData()
            : Image::getFakeData(ViewHelper::getEmptyAvatar(true));

        $picture = $this->attachment[0] ?? '';
        if ($picture) {
            $picture = $picture->getImage()->resize('index.promo')->getData();
        }

        return [
            'id'           => $this->id,
            'url'          => $this->url ?: route('menu.index'),
            'picture'      => $picture,
            'price'        => (int)$this->price,
            'name'         => $this->name,
            'cook'         => [
                'name'         => $cook->user->getFullName(),
                'url'          => $this->url ?: route('menu.index'),
                'distance'     => $cook->getDistance(),
                'picture'      => $avatar,
                'position'     => $cook->getPosition(),
                'rating'       => $cook->getRoundRating(),
                'reviewsCount' => $cook->reviews_count,
            ],
        ];
    }
}
