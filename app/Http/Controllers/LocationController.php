<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Models\City;
use App\Services\Geo\Detector;
use App\Services\YandexApiService;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function getData(Request $request, City $cityModel)
    {
        $filter = $request->get('q');

        if (empty($filter) || mb_strlen($filter) < 3) {
            return response()->json([
                'success' => true,
                'data' => CityResource::collection($cityModel->getTopList())
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => CityResource::collection($cityModel->getByFilter($filter))
        ]);
    }

    public function setCity(Request $request, City $cityModel, Detector $detector)
    {
        $cityId = (int)$request->get('city');

        $city = $cityModel->query()->whereId($cityId)->first();

        if ($city) {
            $detector->setCity($city);
            return response()->json([
                'success' => true,
                'reload' => 1
            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }

    public function suggest(Request $request, YandexApiService $yandexApi) {
        $cityId = (int)$request->get('city');
        $cityName = $request->get('cityName') ?? '';
        $address = $request->get('address');

        $prefix = 'Россия, ';
        if ($cityId) {
            $city = City::find($cityId);
            if ($city) {
                $prefix .= $city->name . ', ';
            }
        } elseif ($cityName) {
            $prefix .= $cityName . ', ';
        }

        $data = $yandexApi->suggest($prefix . $address);

        return response()->json([
            'success' => true,
            'items' => $data
        ]);
    }
}

