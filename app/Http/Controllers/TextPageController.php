<?php

namespace App\Http\Controllers;

use App\Models\TextPage;
use App\Services\BreadcrumbsService;
use App\Services\SeoService;

class TextPageController extends Controller
{
    public function index(
        string $url,
        TextPage $textPageModel,
        BreadcrumbsService $breadcrumbsService,
        SeoService $seoService
    ) {
        /** @var TextPage $textPage */
        $textPage = $textPageModel->whereUrl($url)->firstOrFail();

        $breadcrumbsService->add($textPage->title, $textPage->url);
        $seoService->setMeta('title', $textPage->title);
        $seoService->setMeta('og:prefix', 'prefix="https://ogp.me/ns/article#"');
        $seoService->setMeta('og', [
            'og:type'      => 'website',
            'og:url'       => $textPage->getUrl(),
            'og:site_name' => setting('site.name')->text ?? '',
        ]);

        return view('text.page', ['textPage' => $textPage]);
    }
}
