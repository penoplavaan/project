<?php

namespace App\Http\Controllers;

use App\Events\ChatMessageEvent;
use App\Helpers\BaseHelper;
use App\Http\Requests\ChatMessageRequest;
use App\Http\Resources\BasketResource;
use App\Http\Resources\ChatMessageResource;
use App\Http\Resources\ChatResource;
use App\Models\Attachment;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Services\BreadcrumbsService;
use App\Services\ChatService;
use App\Services\OrderService;
use App\Services\SeoService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use League\Flysystem\FilesystemException;

class ProfileChatController extends Controller
{
    public function __construct(
        protected BreadcrumbsService $breadcrumbsService,
        protected SeoService $seoService,
        protected UserService $userService,
        protected ChatService $chatService
    ) {
    }

    public function index()
    {
        $this->breadcrumbsService->add('Мой профиль', route('profile.index'));
        $this->seoService->setMeta('title', 'Чаты');
        $this->chatService->initWebsocketJsData();

        [$chats, $users] = $this->chatService->getUserChats();

        return view(
            'profile.chat.index',
            [
                'chats' => $chats,
                'users' => $users,
            ]
        );
    }

    /**
     * Загрузить все сообщения в чате (при его открытии)
     * @param Request $request
     * @param Chat $chatModel
     * @param UserService $userModel
     * @return JsonResponse
     */
    public function loadFullData(Request $request, Chat $chatModel, UserService $userModel)
    {
        $chatUserId = (int)$request->post('chatUserId');
        $user = $userModel->current();

        /** @var Chat $chat */
        $chat = $chatModel->getUserChats($user->id, $chatUserId)->first();
        $chatRes = [];

        // $orderedDishes = $this->chatService->getUserBasket($chatUserId);

        if (!$chat/* && !$orderedDishes*/) {
            return response()->json([ 'success' => false, ]);
        }

        if ($chat) {
            $messages = ChatMessage::query()
                ->where('chat_id', $chat->id)
                ->with('chat')
                ->with('order.orderToDishes.dish')
                ->get();

            $lastMessage = $messages->last();

            // Обновить ID последнего прочитанного сообщения в чате
            $chat->updateLastMessage($user->id, $lastMessage);

            $chatRes = ChatResource::make($chat)->resolve();
            $chatRes['messages'] = ChatMessageResource::collection($messages)->resolve();
        }

        /*
        if ($orderedDishes) {
            // Проверяем, есть ли товары в корзине - если есть, то создаём новый чат
            if (!$chat) {
                $chatRes = [
                    'id'         => 0,
                    'fromUserId' => $user->id,
                    'toUserId'   => $chatUserId,
                ];
            }

            $chatRes['order'] = BasketResource::collection($orderedDishes)->resolve();
        }
        */

        return response()->json([
            'success' => true,
            'data'    => $chatRes,
        ]);
    }

    /**
     * Отправка сообщения в чат
     * @param UserService $userService
     * @return JsonResponse
     * @throws FilesystemException
     */
    public function sendMessage(
        ChatMessageRequest $request,
        Chat $chatModel,
        UserService $userModel,
        OrderService $orderService,
    ) {
        $data = $request->validated();
        $chatUserId = (int)$data['chatUserId'];
        $text = $data['message'];
        $user = $userModel->current();

        $text = BaseHelper::stripEmptyStrings($text);

        // $orderedDishes = $this->chatService->getUserBasket($chatUserId);

        /** @var Chat|null $chat */
        $chat = $chatModel->getUserChats($user->id, $chatUserId)->first();

        if (!$chat) {
            // Проверяем, есть ли товары в корзине - если есть, то создаём новый чат
            //if (!$orderedDishes || $orderedDishes->isEmpty()) {
                return response()->json(['success' => false,]);
            /*}

            $chat = new Chat([
                'from_user_id' => $user->id,
                'to_user_id'   => $chatUserId,
            ]);
            $chat->save();
            $chat->refresh();*/
        }

        $message = new ChatMessage([
            'chat_id' => $chat->id,
            'user_id' => $user->id,
            'message' => $text
        ]);
        $message->save();
        $message->refresh();

        /*
        if ($orderedDishes && $orderedDishes->isNotEmpty()) {
            $orderService->createChatOrder($message, $orderedDishes);
        }
        */

        // Пользователь отправил изображения - прикрепить и их тоже к сообщению
        $group = 'images';
        $images = $request->file($group);

        if (!empty($images)) {
            $attachmentIds = [];
            $images = array_slice($images, 0, BaseHelper::getMaxFileCount());

            /** @var \Illuminate\Http\UploadedFile $uploadedFile */
            foreach ($images as $uploadedFile) {
                // Ресайзим загруженный файл
                $tmpDir = storage_path('tmp-resize');
                if (!file_exists($tmpDir)) {
                    mkdir($tmpDir, 0755, true);
                }

                $tmpFilePath = $tmpDir . DIRECTORY_SEPARATOR . time() . md5($uploadedFile->getRealPath());

                $image = \Intervention\Image\Facades\Image::make($uploadedFile->getRealPath())->orientate();
                $image->resize(2000, 2000, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $image->save($tmpFilePath, 90);

                $resizedFile = new UploadedFile(
                    $tmpFilePath,
                    $uploadedFile->getClientOriginalName()
                );

                $file = new \Orchid\Attachment\File($resizedFile, null, $group);
                /** @var Attachment $attachment */
                $attachment = $file->load();
                $attachmentIds[] = $attachment->id;

                unlink($tmpFilePath);
            }

            $message->attachment()->sync($attachmentIds);
        }

        // Обновить ID последнего прочитанного сообщения в чате
        $chat->updateLastMessage($user->id, $message);

        try {
            event(new ChatMessageEvent($message, $chat->fromUser));
            event(new ChatMessageEvent($message, $chat->toUser));
        } catch (\Exception $e) {
            // Отправка пуша не удалась. Но JSON с результатом надо вернуть.
        }

        return response()->json([
            'success' => true,
            'data'    => []
        ]);
    }

    /**
     * Обновить ID последнего прочитанного сообщения в чате
     * @param Request $request
     * @param Chat $chatModel
     * @param UserService $userModel
     * @return JsonResponse
     */
    public function updateLastRead(Request $request, Chat $chatModel, UserService $userModel)
    {
        $chatUserId = (int)$request->post('chatUserId');
        $user = $userModel->current();

        /** @var Chat $chat */
        $chat = $chatModel->getUserChats($user->id, $chatUserId)->first();
        if (!$chat) {
            return response()->json([ 'success' => false, ]);
        }

        $lastMessage = ChatMessage::query()
            ->where('chat_id', $chat->id)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get()
            ->first();

        $chat->updateLastMessage($user->id, $lastMessage);

        return response()->json(['success' => true,]);
    }
}

