<?php

namespace App\Http\Controllers;

use App\Data\Error;
use App\Forms\AuthCodeForm;
use App\Forms\AuthPasswordForm;
use App\Forms\BaseForm;
use App\Forms\RegisterForm;
use App\Forms\ResetPasswordForm;
use App\Forms\SendResetPasswordForm;
use App\Forms\VerifyPhoneForm;
use App\Http\Requests\SendSmsCodeRequest;
use App\Services\SmsService;
use App\Services\UserService;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\ArrayShape;

class AuthController extends Controller
{

    /**
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function auth(Request $request, UserService $userService)
    {
        if ($userService->isAuth()) {
            return response()->json([
                'success' => true,
                'reload'  => 1,
            ]);
        }

        $postData = $request->post();
        if (!empty($postData[BaseForm::PASSWORD])) {
            $type = 'password';
            $form = new AuthPasswordForm();
        } else {
            $type = 'code';
            $form = new AuthCodeForm();
        }

        $form->setData($request->post());
        if (!$form->isValid()) {
            return new JsonResponse([
                'success' => false,
                'errors'  => $form->getMessages(),
            ]);
        }

        $data = $form->getData();
        $phone = $data[$form::PHONE] ?? '';

        if ($type === 'password') {
            $result = $userService->loginByPhone($phone, $data[$form::PASSWORD]);
        } else {
            $result = $userService->loginByPhoneWithCode($phone, $data[$form::CODE]);
        }

        if (!$result->isSuccess()) {
            $data = $this->prepareErrorMessage(
                $type,
                array_map(fn(Error $obj) => $obj->getText(), $result->getErrors())
            );
            return response()->json($data);
        }

        $user = $userService->current(true);

        return response()->json([
            'success' => true,
            'reload'  => $user->phone_verified,
            'data' => $user->phone_verified ? [
                'token' => csrf_token(),
            ] : [
                'token' => csrf_token(),
                'needVerifyPhone' => (new VerifyPhoneForm())->toArray()
            ]
        ]);
    }

    /**
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function register(Request $request, UserService $userService)
    {
        if ($userService->isAuth()) {
            return response()->json([
                'success' => true,
                'reload'  => 1,
            ]);
        }

        $form = new RegisterForm();
        $form->setData($request->post());

        if (!$form->isValid()) {
            return response()->json([
                'success' => false,
                'errors'  => $form->getMessages(),
            ]);
        }

        $result = $userService->register($form->getData());

        if (!$result->isSuccess()) {
            $errors = $result->getErrors();
            $data =  [
                'success' => false,
                'errors'  => [],
            ];
            /** @var Error $err */
            foreach ($errors as $err) {
                $code = $err->getCode() ?: 'email';
                $data['errors'][$code][] = $err->getText();
            }

        } else {
            $messageCode = !empty($userData[RegisterForm::EMAIL]) ? 'register.success' : 'register.success-no-email';

            $data = [
                'success' => true,
                'message' => [
                    'title' => 'Успешная регистрация',
                    'text'  => setting($messageCode)->getBigText(),
                ],
                'reload'  => 3000,
            ];
        }

        return response()->json($data);
    }

    public function sendSmsCode(SendSmsCodeRequest $request, UserService $userService, SmsService $smsService)
    {
//        $needUserExists = [SmsService::TYPE_AUTH];
        $needUserNotExists = [SmsService::TYPE_REGISTER, SmsService::TYPE_VERIFY];
        $existVerifiedUser = $userService->getByPhone($request->phone, true);

        if (in_array($request->type, $needUserNotExists) && $existVerifiedUser) {
            return response()->json([
                'success' => false,
                'errors' => [
                    'phone' => ['Учетная запись с таким номером телефона уже существует.'],
                ]
            ]);
        }
//        elseif (in_array($request->type, $needUserExists) && !$existVerifiedUser) {
//            return response()->json([
//                'success' => false,
//                'errors' => [
//                    'phone' => ['Учетная запись не найдена.'],
//                ]
//            ]);
//        }

        $checkResult = $smsService->canSendCode($request->phone);

        if ($checkResult && $checkResult['canSend']) {
            try {
                $timeout = $smsService->sendSmsCode($request->phone, $request->type);
            } catch (\Exception $e) {
                logger([
                    $e->getMessage(),
                    $e->getTrace(),
                ]);

                return response()->json([
                    'success' => false,
                    'errors' => [
                        'phone' => ['Произошла ошибка при отправке СМС-кода. Пожалуйста, попробуйте позже.'],
                    ]
                ]);
            }

            return response()->json([
                'success' => true,
                'data' => [
                    'timeout' => $timeout,
                ]
            ]);

        } else {
            $timeout = $checkResult['timeout'] ?? -1;
            return response()->json([
                'success' => false,
                'data' => [
                    'timeout' => $timeout,
                ]
            ]);
        }
    }

    /**
     * @param Request $request
     * @param UserService $userService
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function verifyEmail(Request $request, UserService $userService)
    {
        $userId = (int)$request->route('id');
        $user = $userService->getById($userId);

        if (!$user) {
            abort(400);
        }

        if (!hash_equals(
            (string)$request->route('hash'),
            sha1($user->getEmailForVerification())
        )) {
            abort(400);
        }

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();

            event(new Verified($user));
        }

        return redirect('/');
    }

    /**
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function resetSend(Request $request, UserService $userService)
    {
        if ($userService->isAuth()) {
            return response()->json([
                'success' => true,
                'reload'  => 1,
            ]);
        }

        $form = new SendResetPasswordForm();
        $form->setData($request->post());

        if (!$form->isValid()) {
            return response()->json([
                'success' => false,
                'errors'  => $form->getMessages(),
            ]);
        }

        $data = $form->getData();
        $result = $userService->sendResetPassword($data[$form::EMAIL]);

        if ($result->isSuccess()) {
            $setting = setting('remind.sent');
            $data = [
                'success' => true,
                'message' => [
                    'title' => $setting->getText(),
                    'text'  => $setting->getBigText(),
                ],
            ];
        } else {
            $data = $this->prepareErrorMessage(
                'email',
                array_map(fn(Error $obj) => $obj->getText(), $result->getErrors())
            );
        }

        return response()->json($data);
    }

    /**
     * Сброс пароля пользователя
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function resetPassword(Request $request, UserService $userService)
    {
        if ($userService->isAuth()) {
            return response()->json([
                'success' => true,
                'reload'  => 1,
            ]);
        }

        $form = new ResetPasswordForm();
        $form->setData($request->post());

        if (!$form->isValid()) {
            return response()->json([
                'success' => false,
                'errors'  => $form->getMessages(),
            ]);
        }

        $data = $form->getData();
        unset($data[$form::SUBMIT]);
        $result = $userService->resetPassword($data);

        if ($result->isSuccess()) {
            $setting = setting('reset-password.done');
            $data = [
                'success' => true,
                'message' => [
                    'title' => $setting->getText(),
                    'text'  => $setting->getBigText(),
                ],
                'reload' => 3000
            ];
        } else {
            $data = $this->prepareErrorMessage(
                'email',
                array_map(fn(Error $obj) => $obj->getText(), $result->getErrors())
            );
        }

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function verifyPhone(Request $request, UserService $userService)
    {
        $form = new VerifyPhoneForm();
        $form->setData($request->post());

        if (!$form->isValid()) {
            return response()->json([
                'success' => false,
                'errors'  => $form->getMessages(),
            ]);
        }

        $data = $form->getData();
        $data['type'] = $request->type ?? '';
        $result = $userService->verifyPhone($data);

        if (!$result->isSuccess()) {
            $errors = $result->getErrors();
            $data =  [
                'success' => false,
                'errors'  => [],
            ];
            /** @var Error $err */
            foreach ($errors as $err) {
                $code = $err->getCode() ?: 'email';
                $data['errors'][$code][] = $err->getText();
            }

        } else {
            $data = [
                'success' => true,
                'message' => [
                    'title' => 'Номер подтвержен',
                    'text'  => setting('verify-phone.success')->getBigText(),
                ],
                'reload'  => 3000,
            ];
        }

        return response()->json($data);
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['success' => true]);
    }

    /**
     * @param string $field
     * @param string|array $message
     * @return array
     */
    #[ArrayShape(['success' => "false", 'errors' => "\string[][][]"])]
    protected function prepareErrorMessage(string $field, string|array $message): array
    {
        return [
            'success' => false,
            'errors'  => [
                $field => [
                    'invalid' => is_array($message) ? $message : [$message],
                ]
            ],
        ];
    }
}

