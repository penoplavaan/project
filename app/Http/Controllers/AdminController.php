<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{
    public function notBitrix()
    {
        return view('admin.bad-habbit');
    }
    public function goToCooks()
    {
        return view('admin.cooks');
    }
}
