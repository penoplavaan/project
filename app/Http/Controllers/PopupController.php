<?php

namespace App\Http\Controllers;

use App\Forms\CallbackForm;
use App\Forms\FeedbackForm;
use App\Http\Requests\CallbackFormRequest;
use App\Http\Requests\FeedbackFormRequest;
use App\Models\Callback;
use App\Models\Feedback;
use App\Models\FeedbackCategory;

class PopupController extends Controller
{

    public function getFeedback(): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'success' => true,
            'data'  => ((new FeedbackForm())->toArray()),
        ]);
    }

    public function getFeedbackPreopend($option): \Illuminate\Http\JsonResponse
    {
        $form = new FeedbackForm();
        $preopenedOption = (new FeedbackCategory())->query()->where('name', $option)->first();
        $form->populateValues(['topic_id' => $preopenedOption->id]);
        return response()->json(
            [
                'success' => true,
                'data' => ($form->toArray()),
            ]
        );
    }

    public function saveFeedback(FeedbackFormRequest $request): \Illuminate\Http\JsonResponse
    {
        // Всё уже отвалидировано request'ом
        /** @var Feedback $feedback */
        $fields   = $request->validated();

        $form = new FeedbackForm();
        if ($form->validateRecaptcha($fields[$form::RECAPTCHA])) {
            // Если рекапча прошла валидацию (либо отключена) - сохраняем форму
            // иначе не сохраняем, но присылаем Success
            $feedback = (new Feedback());
            $feedback->fill($fields)->save();
        }

        $setting = setting('profile.success');
        $data    = [
            'success' => true,
            'message' => [
                'title' => $setting->getText('Отлично!'),
                'text'  => $setting->getBigText('Ваше обращение принято'),
            ],
            'reload'  => 2000,
        ];

        return response()->json($data);
    }

    public function callbackForm(): \Illuminate\Http\JsonResponse
    {
        $setting = setting('callback.popup');

        return response()->json([
            'success' => true,
            'data'  => [
                'formData' => (new CallbackForm())->toArray(),
                'popup' => [
                    'title' => $setting->getText(),
                    'message'  => $setting->getBigText(),
                ]
            ],
        ]);
    }

    public function callbackSave(CallbackFormRequest $request): \Illuminate\Http\JsonResponse
    {

        $form = new CallbackForm();
        $fields   = $request->validated();

        if ($form->validateRecaptcha($fields[$form::RECAPTCHA])) {
            $callback = Callback::make($fields);
            $callback->save();
        }

        $setting = setting('callback.success');

        return response()->json([
            'success' => true,
            'message' => [
                'title' => $setting->getText('Отлично!'),
                'text'  => $setting->getBigText('Ваше обращение принято'),
            ],
        ]);
    }
}

