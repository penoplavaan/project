<?php

namespace App\Http\Controllers;

use App\Helpers\ViewHelper;
use App\Http\Resources\CookResource;
use App\Http\Resources\MainDishCompilationResource;
use App\Http\Resources\MainPromoCategoryCompilationResource;
use App\Http\Resources\MainReviewResource;
use App\Http\Resources\MainSecondPromoResource;
use App\Models\Cook;
use App\Models\DishCategory;
use App\Models\MainDishCompilation;
use App\Models\MainPromoCategoryCompilation;
use App\Models\MainPromoSlide;
use App\Models\MainReview;
use App\Models\MainSecondPromo;

class IndexController extends Controller
{
    /**
     * @param MainPromoSlide $mainPromoSlide
     * @param MainReview $mainReview
     * @param DishCategory $dishCategory
     * @param Cook $cook
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(
        MainSecondPromo $mainSecondPromo,
        MainDishCompilation $mainDishCompilation,
        MainReview $mainReview,
        DishCategory $dishCategory,
        Cook $cook,
    ) {
        $currentUser = \Auth::user();

        $slides = $mainSecondPromo->getForMainPage();

        $reviews = $mainReview->getForMainPage();

        $cooks = $cook->query()
            ->where(
                ['show_on_main' => true,],
                ['user_id', '!=', false])
            ->with('user')
            ->with('user.attachment')
            ->with('category')
            ->with('speciality')
            ->orderBy('rating', 'desc')
            ->orderBy('sort')
            ->limit(12)
            ->get();

        [$compilations, $compilationCount] = $mainDishCompilation->getForMain();

        $promoCardsCompilations = MainPromoCategoryCompilation::query()
            ->with('promocards')
            ->with('promocards.attachment')
            ->limit(12)
            ->get();


        return \view(
            'index.index',
            [
                'slides'                 => MainSecondPromoResource::collection($slides),
                'reviews'                => MainReviewResource::collection($reviews),
                'isCook'                 => !empty($currentUser) && $currentUser->isCook(),
                'isAutorized'            => !empty($currentUser),
                'dishCategories'         => $dishCategory->getForMain(),
                'mainSliderCooks'        => CookResource::collection($cooks)->resolve(),
                'compilations'           => MainDishCompilationResource::collection($compilations)->resolve(),
                'compilationCount'       => $compilationCount,
                'promoCardsCompilations' => MainPromoCategoryCompilationResource::collection($promoCardsCompilations)->resolve(),
            ]
        );
    }

    public function compilations(
        MainDishCompilation $mainDishCompilation
    ) {
        [$compilations] = $mainDishCompilation->getForMain(3, 10);

        return response()->json([
            'success' => true,
            'data'  => MainDishCompilationResource::collection($compilations)->resolve(),
        ]);
    }
}

