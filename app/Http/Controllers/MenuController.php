<?php

namespace App\Http\Controllers;

use App\Helpers\BaseHelper;
use App\Http\Requests\DishFilterRequest;
use App\Http\Resources\CookReviewResource;
use App\Http\Resources\DishCardResource;
use App\Http\Resources\DishDetailResource;
use App\Http\Resources\DishInListResource;
use App\Http\Resources\PromoCardResource;
use App\Models\CookReview;
use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\PromoCard;
use App\Services\BreadcrumbsService;
use App\Services\DishFilterService;
use App\Services\SeoService;
use App\Services\UserService;
use Illuminate\Http\Request;

class MenuController extends Controller
{

    public function __construct(
        protected BreadcrumbsService $breadcrumbsService,
        protected SeoService $seoService,
        protected DishFilterService $filterService
    )
    {
    }


    /**
     *
     */
    public function index(DishFilterRequest $request)
    {
        $this->breadcrumbsService->add('Все блюда', route('menu.index'));
        $this->seoService->setMeta('title', 'Список блюд');

        $dishCategoryName = $request->route()->parameter('dishCategory');
        $dishCategoryFilter = null;
        if (!empty($dishCategoryName)) {
            /** @var DishCategory $dishCategory */
            $dishCategory = DishCategory::query()->where('slug', $dishCategoryName)->get()->first();

            if (!$dishCategory) {
                abort(404);
            }

            $this->breadcrumbsService->add($dishCategory->getMenuTitle(), route('menu.dish.category', ['dishCategory' => $dishCategory->slug]));
            $this->seoService->setMeta('title', $dishCategory->getMetaTitle());
            $this->seoService->setMeta('description', $dishCategory->getDescription());

            $dishCategoryFilter = [$dishCategory->id];
        }

        $filterParams = array_merge(['category' => $dishCategoryFilter], $request->validated());
        [$dishes, $pageData] = $this->filterService->filterDishes($filterParams);

        $promoCardsQuery = PromoCard::whereCategories($filterParams['category'] ?? [])
            ->orderBy('named_sort');

        if (empty($filterParams['category'])) {
            $promoCardsQuery->where('show_on_main_category', '1');
        }

        $promoCards = $promoCardsQuery->get();

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'data' => [
                    'dishes'   => DishInListResource::collection($dishes),
                    'pageData' => $pageData,
                    'promoCards' => PromoCardResource::collection($promoCards),
                ]
            ])->withHeaders(BaseHelper::headersForAjaxResponse());
        }

        $this->seoService->setFooterTextCode('seo.footer.dishes.text');

        return view('menu.index', [
            'h1'           => !empty($dishCategory) ? $dishCategory->getH1() : '',
            'dishCategory' => $dishCategory ?? '',
            'dishes'       => DishInListResource::collection($dishes),
            'pageData'     => $pageData,
            'filterData'   => $this->filterService->getFilterFields($filterParams),
            'filterValues' => $filterParams,
            'promoCards'   => PromoCardResource::collection($promoCards),
        ]);
    }

    public function suggests(Request $request, Dish $dishModel)
    {
        $name = $request->post('name');

        $suggests = $this->filterService->suggests($name);

        return response()->json([
            'success' => true,
            'data' => [
                'suggests' => $suggests->map(fn($dish) => $dish->name)->toArray(),
            ],
        ]);
    }

    /**
     * @param Dish $dish
     * @param UserService $userService
     */
    public function detail(Dish $dish, UserService $userService)
    {
        $this->breadcrumbsService->add('Все блюда', route('menu.index'));
        $this->breadcrumbsService->add($dish->name, route('menu.dish.detail', ['dish' => $dish->id]));
        $this->seoService->setMeta('title', $dish->name);
        $this->seoService->setMeta('description', $dish->preview_text);

        $reviews      = CookReview::getCookReviews($dish->cook->id);
        $reviewsCount = CookReview::getCookReviewsCount($dish->cook->id);
        $totalPages   = ceil($reviewsCount / CookReview::PAGE_SIZE);
        $canAddReview = $dish->cook->isUserCanAddReview($userService->current());

        $products = DishCardResource::collection(
            $dish->cook->dish
                ->where('id', '!=', $dish->id)
                ->where('active', '!==', 0)
                ->slice(0, 12)->shuffle()
        );

        return view(
            'menu.detail',
            [
                'caption'      => $dish->name,
                'dish'         => DishDetailResource::make($dish)->resolve(),
                'reviews'      => CookReviewResource::collection($reviews),
                'products'     => $products,
                'totalPages'   => $totalPages,
                'reviewsCount' => $reviewsCount,
                'canAddReview' => $canAddReview
            ]
        );
    }
}

