<?php

namespace App\Http\Controllers;

use App\Data\Error;
use App\Forms\CookProfileDataForm;
use App\Forms\DishForm;
use App\Forms\UserDataForm;
use App\Helpers\BaseHelper;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\DishRemoveRequest;
use App\Http\Requests\DocumentRemoveRequest;
use App\Http\Requests\DocumentUploadRequest;
use App\Http\Requests\KitchenPhotoUploadRequest;
use App\Http\Requests\ProfileAddDishFormRequest;
use App\Http\Requests\ProfileCookFormRequest;
use App\Http\Requests\ProfileEditFormRequest;
use App\Http\Resources\CookProfileResource;
use App\Http\Resources\DishCardResource;
use App\Http\Resources\DishEditResource;
use App\Http\Resources\FileResource;
use App\Http\Resources\UserProfileResource;
use App\Models\Attachment;
use App\Models\Cook;
use App\Models\Dish;
use App\Models\ProfileUsefulMaterial;
use App\Models\User;
use App\Services\BreadcrumbsService;
use App\Services\SeoService;
use App\Services\UserService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct(
        protected BreadcrumbsService $breadcrumbsService,
        protected SeoService $seoService,
        protected UserService $userService
    )
    {
        $this->seoService->setMeta('title', 'Личный кабинет');
    }

    public function index()
    {
        $user = $this->userService->current();

        BaseHelper::extendJsApp([
            'changePasswordForm' => (new \App\Http\Requests\ChangePasswordRequest())->formDescription()
        ]);

        $data = [
            'userProfileData' => UserProfileResource::make($user),
            'formData'        => (new UserDataForm())->addFileFields()->toArray(),
        ];

        if ($user->isCook()) {
            $data['cookData'] = CookProfileResource::make($user->cook)->resolve();
            $data['сookFormData'] = (new CookProfileDataForm())->toArray();
            $data['cookKitchen'] = $this->getKitchenData($user->cook);
            $data['documents'] = $this->getDocumentsData($user->cook);
        }

        return view('profile.index', $data);
    }

    public function dishes(Request $request)
    {
        return view(
            'profile.dishes', $this->getDishes($request->collect()->keys()->contains('add-dish'))
        );
    }

    public function getDishes($preOpenAddDish = false): array
    {
        $user = $this->userService->current();
        $dishCards = $user->cook->dish->where('active', '!==', 0);

        return [
            'dishes'         => DishCardResource::collection($dishCards)->resolve(),
            'formData'       => (new DishForm())->addFileFields()->toArray(),
            'preOpenAddDish' => $preOpenAddDish,
        ];
    }

    public function getDishesAjax()
    {
        return response()->json(
            [
                'success' => true,
                'data' => $this->getDishes(),
            ]
        );
    }

    public function getDish(Dish $dish)
    {
        return [
            'success' => true,
            'data'   => DishEditResource::make($dish)->resolve()
        ];
    }

    public function addDish(ProfileAddDishFormRequest $request)
    {
        // Всё уже отвалидировано request'ом
        /** @var User $user */
        $user = $request->user();
        $fields = $request->validated();

        $dishId = (int)$request->id;
        $dish = Dish::query()->where('id', $dishId)->first();

        $cook = $user->cook;
        $fields['cook_id'] = $cook->id;

        // Если такого блюда ещё нет
        if (!$dish) {
            $dish = (new Dish)->fill($fields);
        } else {
            // Проверка на владельца блюда
            if ($dish->cook->id !== $cook->id) {
                return [
                    'success' => false,
                    'message' => [
                        'title' => 'Блюдо принадлежит другому повару!',
                    ],
                ];
            }
            $dish->fill($fields);
        }
        $dish->save();

        // сохранение файлов
        $attachmentIds = [];
        if (!empty($fields['attachmentsIds'])) {
            $attachmentIds = array_intersect(
                $dish->attachment->pluck('id')->toArray(),
                $fields['attachmentsIds']
            );
        }

        $filesByGroup = $request->file('images');
        if (!empty($filesByGroup)) {
            $filesByGroup = array_slice($filesByGroup, 0, BaseHelper::getMaxFileCount());
            /** @var \Illuminate\Http\UploadedFile $uploadedFile */
            foreach ($filesByGroup as $uploadedFile) {
                $file = new \Orchid\Attachment\File($uploadedFile, null, 'images');
                $attachment = $file->load();
                $attachmentIds[] = $attachment->id;
            }
        }

        $dish->attachment()->sync($attachmentIds);
        $dish->tags()->sync($fields['tags'] ?? []);


        $setting = setting('profile-dish.success');
        $data = [
            'success' => true,
            'message' => [
                'title' => $setting->getText('Блюдо добавлено!'),
                'text'  => $setting->getBigText('')
            ],
        ];

        return response()->json($data);
    }

    public function dishRemove(DishRemoveRequest $request)
    {
        $dishId = (int)$request->id;

        $dish = Dish::query()->where('id', $dishId)->first();

        $user = $this->userService->current();
        if ($dish->cook->id !== $user->cook->id) {
            return [
                'success' => false,
                'message' => [
                    'title' => 'Блюдо принадлежит другому повару!',
                ],
            ];
        }

        $dish->active = false;
        $dish->save();
        $result = $this->getDishes();

        return response()->json(
            [
                'success' => true,
                'data' => $result,
            ]
        );
    }

    public function useful(ProfileUsefulMaterial $materialModel)
    {
        $materials = $materialModel->query()->with('attachment')->get();

        return view('profile.useful-materials', [
            'materials' => $materials,
        ]);
    }

    public function changePassword(ChangePasswordRequest $request, UserService $userService)
    {
        try {
            $result = $userService->changePassword($request->password, $request->newPassword);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'errors' => [
                    'password' => [
                        'invalid' => $e->getMessage(),
                    ]
                ]
            ]);
        }

        if ($result->isSuccess()) {
            return response()->json([
                'success' => true,
                'message' => [
                    'title' => 'Успех!',
                    'text' => 'Ваш пароль изменён',
                ],
                'reload' => 2000,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'errors' => [
                    'password' => [
                        'invalid' => array_map(fn(Error $obj) => $obj->getText(), $result->getErrors()),
                    ]
                ]
            ]);
        }
    }

    public function changeProfileData(ProfileEditFormRequest $request)
    {
        // Всё уже отвалидировано request'ом
        /** @var User $user */
        $user = $request->user();
        $fields = $request->validated();
        $fields['about'] ??= '';
        $fields['address'] ??= '';

        if ($user->phone_verified) {
            unset($user['phone']);
        }

        if ($user->emailVerified) {
            unset($fields['email']);
        }

        $user->fill($fields)->save();

        // Аватар пользователя
        $avatarFiles = $request->file('avatar') ?: [];
        $avatarFile = $avatarFiles[0] ?? null;
        if ($avatarFile) {
            $file = new \Orchid\Attachment\File($avatarFile);
            $avatarAttachment = $file->load();

            $existAttachmentIds = $user->attachment->map(fn ($attach) => $attach->id);
            $user->attachment()->sync([$avatarAttachment->id]);
        }

        $setting = setting('profile.success');
        $data = [
            'success' => true,
            'message' => [
                'title' => $setting->getText('Отлично!'),
                'text'  => $setting->getBigText('Данные сохранены!')
            ],
            'reload' => 2000,
        ];

        return response()->json($data);
    }

    public function changeCookProfileData(ProfileCookFormRequest $request)
    {
        // Всё уже отвалидировано request'ом
        /** @var User $user */
        $user = $request->user();
        $fields = $request->validated();

        $cookRequest = $user->cook;
        $cookRequest->admin_comment .= "\n" . date('[Y-m-d H:i:s]') . ' Пользователь обновил профиль';
        $cookRequest->fill($fields)->save();

        $cookRequest->category()->sync($fields['categories'] ?? []);

        $setting = setting('profile-cook.success');
        $data = [
            'success' => true,
            'message' => [
                'title' => $setting->getText('Успешно!'),
                'text'  => $setting->getBigText('Ваш профиль изменен!')
            ],
            'reload' => 2000,
        ];

        return response()->json($data);
    }

    /**
     * Загрузка новой фотки кухни
     */
    public function kitchenUpload(KitchenPhotoUploadRequest $request)
    {
        $user = $this->userService->current();

        $kitchenPhotos = $user->cook->getKitchen();

        if ($kitchenPhotos->count() >= 10) {
            return response()->json([
                'success' => false,
                'error' => 'Достигнул лимит количества фотографий',
            ]);
        }

        $allAttaches = $user->cook->attachment;
        $maxSort = $allAttaches->map(fn($attach) => $attach->sort)->max();
        $attachmentIds = $allAttaches->map(fn($attach) => $attach->id)->toArray();

        $fileFromRequest = $request->file('kitchenPhoto');
        $file = new \Orchid\Attachment\File($fileFromRequest, null, 'kitchen');
        $attachment = $file->load();
        $attachment->sort = $maxSort + 1;
        $attachment->save();

        $attachmentIds[] = $attachment->id;

        $user->cook->attachment()->sync($attachmentIds);

        $kitchenData = $this->getKitchenData($user->cook);
        $kitchenData[] = [
            'id'      => $attachment->id,
            'picture' => $attachment->getImage()->resize('cook.attachment')->getData()
        ];

        return response()->json([
            'success' => true,
            'data'    => $kitchenData,
        ]);
    }

    public function kitchenRemove(Request $request)
    {
        $fileId = (int)$request->post('id');

        $user = $this->userService->current();

        $kitchenPhotos = $user->cook->getKitchen();
        $kitchenPhotoIds = $kitchenPhotos->map(fn($attach) => $attach->id)->toArray();

        if (!$fileId || !in_array($fileId, $kitchenPhotoIds)) {
            abort(403);
        }

        $allAttaches = $user->cook->attachment;
        $attachmentIds = $allAttaches->map(fn($attach) => $attach->id)->toArray();
        $index = array_search($fileId, $attachmentIds);
        array_splice($attachmentIds, $index, 1);

        $user->cook->attachment()->sync($attachmentIds);

        return response()->json([
            'success' => true,
            'data'    => $this->getKitchenData($user->cook()->get()->first()),
        ]);
    }

    /**
     * Загрузка документа/сертификата в профиль
     */
    public function documentUpload(DocumentUploadRequest $request)
    {
        $user = $this->userService->current();
        $type = $request->type;

        if ($type === 'document') {
            $currentFiles = $user->cook->getDocument();
        } else {
            $currentFiles = $user->cook->getCertificate();
        }

        if ($currentFiles->count() >= 10) {
            return response()->json([
                'success' => false,
                'error' => 'Достигнул лимит количества документов',
            ]);
        }

        $allAttaches = $user->cook->attachment;
        $maxSort = $allAttaches->map(fn($attach) => $attach->sort)->max();
        $attachmentIds = $allAttaches->map(fn($attach) => $attach->id)->toArray();

        $fileFromRequest = $request->file('document');
        $file = new \Orchid\Attachment\File($fileFromRequest, null, $type);
        $attachment = $file->load();
        $attachment->sort = $maxSort + 1;
        $attachment->save();

        $attachmentIds[] = $attachment->id;

        $user->cook->attachment()->sync($attachmentIds);

        return response()->json([
            'success' => true,
            'data'    => $this->getDocumentsData($user->cook()->get()->first()),
        ]);
    }

    public function documentRemove(DocumentRemoveRequest $request)
    {
        $type = $request->type;
        $fileId = (int)$request->id;

        $user = $this->userService->current();
        if ($type === 'document') {
            $currentFiles = $user->cook->getDocument();
        } else {
            $currentFiles = $user->cook->getCertificate();
        }
        $attachmentIds = $currentFiles->map(fn($attach) => $attach->id)->toArray();

        if (!$fileId || !in_array($fileId, $attachmentIds)) {
            abort(403);
        }

        $allAttaches = $user->cook->attachment;
        $attachmentIds = $allAttaches->map(fn($attach) => $attach->id)->toArray();
        $index = array_search($fileId, $attachmentIds);
        array_splice($attachmentIds, $index, 1);

        $user->cook->attachment()->sync($attachmentIds);

        return response()->json([
            'success' => true,
            'data'    => $this->getDocumentsData($user->cook()->get()->first()),
        ]);
    }

    protected function getKitchenData(Cook $cook)
    {
        return $cook->getKitchen()->map(function(Attachment $attach) {
            return [
                'id' => $attach->id,
                'picture' => $attach->getImage()->resize('cook.attachment')->getData()
            ];
        })->values()->toArray();
    }

    protected function getDocumentsData(Cook $cook)
    {
        $groups = [];
        $passport = $cook->getPassport();
        $sanitary = $cook->getSanitary();
        $document = $cook->getDocument();

        $items = [];
        if ($passport) {
            $items[] = FileResource::make($passport)->resolve();
        }
        foreach ($sanitary as $san) {
            $items[] = FileResource::make($san)->resolve();
        }
        foreach ($document as $doc) {
            $item = FileResource::make($doc)->resolve();
            $item['editable'] = true;
            $items[] = $item;
        }

        $groups[] = [
            'groupName' => 'Документы',
            'max'       => 10,
            'code'      => 'document',
            'items'     => $items
        ];

        $items = [];
        foreach ($cook->getCertificate() as $cert) {
            $item = FileResource::make($cert)->resolve();
            $item['editable'] = true;
            $items[] = $item;
        }
        $groups[] = [
            'groupName' => 'Дипломы и сертификаты',
            'max'       => 10,
            'code'      => 'certificate',
            'items'     => $items
        ];

        return $groups;
    }
}

