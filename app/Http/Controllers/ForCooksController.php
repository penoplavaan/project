<?php

namespace App\Http\Controllers;

use App\Forms\BecomeCookForm;
use App\Forms\RegisterForm;
use App\Helpers\BaseHelper;
use App\Http\Requests\BecomeCookFormRequest;
use App\Mail\SimpleMail;
use App\Models\City;
use App\Models\Cook;
use App\Models\ForCookFaq;
use App\Models\ForCookFeature;
use App\Models\ForCookHowWork;
use App\Models\ForCookReview;
use App\Models\ForCookSupport;
use App\Models\User;
use App\Services\SeoService;
use App\Services\UserService;
use Illuminate\Support\Facades\Mail;

class ForCooksController extends Controller
{
    /**
     * @param ForCookHowWork $forCookHowWork
     * @param ForCookFeature $forCookFeature
     * @param ForCookReview $forCookReview
     * @param ForCookSupport $forCookSupport
     * @param ForCookFaq $forCookFaq
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(
        ForCookHowWork $forCookHowWork,
        ForCookFeature $forCookFeature,
        ForCookReview $forCookReview,
        ForCookSupport $forCookSupport,
        ForCookFaq $forCookFaq,
        UserService $userService,
        SeoService $seoService,
    ) {
        $seoService->setMeta('title', 'Приглашаем к сотрудничеству');

        $formData = null;
        if (!$userService->isAuth()) {
            $formData = (new RegisterForm())
                ->setAttribute('class', 'form form--white registration-form')
                ->toArray();
        }

        return \view(
            'for-cooks.index',
            [
                'howWorks' => $forCookHowWork->query()->get(),
                'features' => $forCookFeature->query()->get()->filter(fn(ForCookFeature $feature) => $feature->getIcon()),
                'reviews'  => $forCookReview->query()->get()->filter(fn(ForCookReview $review) => $review->getImage()),
                'supports' => $forCookSupport->query()->get(),
                'faqs'     => $forCookFaq->query()->get(),
                'formData' => $formData,
            ]
        );
    }

    public function becomeCookPopup()
    {
        return response()->json([
            'success' => true,
            'data'    => [
                'form'     => (new BecomeCookForm())->addFileFields()->populateUser()->toArray(),
                'settings' => settingSection('for-cooks-form')->keyBy('code')->toArray(),
            ]
        ]);
    }

    public function becomeCookForm(BecomeCookFormRequest $request)
    {
        // Всё уже отвалидировано request'ом
        /** @var User $user */
        $user = $request->user();
        $fields = $request->validated();

        $fields['active'] = 1;
        $fields['passport_verified'] = 0;
        $fields['sanitary_verified'] = 0;

        // region Сохранение полей, принадлежащих самому юзеру (не его профилю повара)
        $user->name = $fields['name'];
        if (empty($user->email)) {
            $user->email = $fields['email'];
        }
        $user->save();
        // endregion

        $cookCreated = false;
        $cookRequest = $user->cook;
        if (!$cookRequest) {
            $cookRequest = new Cook();
            $cookCreated = true;
            $fields['verified'] = 1;
            $cookRequest->user_id = $user->id;
            $cookRequest->admin_comment = date('[Y-m-d H:i:s]') . ' Пользователь создал заявку';
        } else {
            if ($cookRequest->verified === 0)
                return [
                    'success' => false,
                    'message' => [
                        'title' => 'Вы уже создавали заявку на повара!',
                    ],
                ];
            $cookRequest->admin_comment .= "\n" . date('[Y-m-d H:i:s]') . ' Пользователь обновил заявку';
        }

        $cookRequest->fill($fields)->save();
        $cookRequest->category()->sync($fields['categories'] ?? []);

        // сохранение файлов
        // Аватар пользователя - отдельно, ниже
        $fileKeys = ['sanitary', 'passport', 'kitchen', 'certificate'];
        $attachmentIds = [];
        foreach ($fileKeys as $group) {
            $filesByGroup = $request->file($group);
            if (empty($filesByGroup)) {
                continue;
            }
            $filesByGroup = array_slice($filesByGroup, 0, BaseHelper::getMaxFileCount());
            /** @var \Illuminate\Http\UploadedFile $uploadedFile */
            foreach ($filesByGroup as $uploadedFile) {
                $file = new \Orchid\Attachment\File($uploadedFile, null, $group);
                $attachment = $file->load();
                $attachmentIds[] = $attachment->id;
            }
        }
        $cookRequest->attachment()->sync($attachmentIds);

        // Аватар пользователя
        $avatarFiles = $request->file('avatar') ?: [];
        $avatarFile = $avatarFiles[0] ?? null;
        if ($avatarFile) {
            $file = new \Orchid\Attachment\File($avatarFile);
            $avatarAttachment = $file->load();
            $user->attachment()->sync([$avatarAttachment->id]);
        }

        if ($cookCreated === true) {
            $mail = new SimpleMail('cook-created', ['cookId' => $cookRequest->id]);
            $mail->subject('Подана заявка на повара');
            BaseHelper::sendEmailToAdmins($mail);
        }

        $setting = setting('for-cooks.success');
        $data = [
            'success' => true,
            'message' => [
                'title' => $setting->getText('Заявка принята'),
                'text'  => $setting->getBigText('В ближайшее время с вами свяжутся наши специалисты!')
            ],
            'redirect' => route('profile.dishes') . '?add-dish'
        ];

        return response()->json($data);
    }
}

