<?php

namespace App\Http\Controllers;

use App\Services\BasketService;
use App\Services\BreadcrumbsService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Resources\BasketByCookResource;

class BasketController extends Controller
{
    public function __construct(
        protected BasketService $basketService,
        protected BreadcrumbsService $breadcrumbsService,
    ) {
    }

    public function index(Request $request, UserService $userService)
    {
        if (!$userService->isAuth()) {
            return redirect('/');
        }

        $this->breadcrumbsService->add('Корзина', route('basket.index'));

        return view('basket.index', [
            'basket' => $this->basketService->get()
        ]);
    }

    /**
     * Добавления блюда в корзину
     */
    public function add(Request $request)
    {
        $dishId = (int)($request->post('dishId') ?? 0);
        $count = (int)($request->post('count') ?? 1);

        try {
            $result = $this->basketService->add($dishId, $count);
        } catch (\Exception $e) {
            return response()->json([
                'success'  => false,
                'message' => [
                    'title' => 'Ошибка',
                    'text' => $e->getMessage()
                ],
            ]);
        }

        return response()->json([
            'success'  => $result,
            'redirect' => route('basket.index'),
        ]);
    }

    /**
     * Изменение количества
     */
    public function setCount(Request $request)
    {
        $dishId = (int)($request->post('dishId') ?? 0);
        $count = (int)($request->post('count') ?? 1);

        try {
            $result = $this->basketService->setCount($dishId, $count);
        } catch (\Exception $e) {
            return response()->json([
                'success'  => false,
                'message' => [
                    'title' => 'Ошибка',
                    'text' => $e->getMessage()
                ],
            ]);
        }

        return response()->json([
            'success'  => $result,
            'data' => [
                'items' => BasketByCookResource::collection($this->basketService->get()),
                'info' => $this->basketService->getInfo(),
            ]
        ]);
    }

    /**
     * Удаление товара из корзины
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request)
    {
        $dishId = (int)$request->post('dishId');
        $result = $this->basketService->remove($dishId);

        return response()->json([
            'success' => $result,
            'data' => [
                'items' => BasketByCookResource::collection($this->basketService->get()),
                'info' => $this->basketService->getInfo(),
            ]
        ]);
    }

    /**
     * Очистка корзины по повару
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clear(Request $request)
    {
        $cookId = (int)$request->post('cookId');
        $result = $this->basketService->clear($cookId);

        return response()->json([
            'success' => $result,
            'data' => [
                'items' => BasketByCookResource::collection($this->basketService->get()),
                'info' => $this->basketService->getInfo(),
            ]
        ]);
    }
}

