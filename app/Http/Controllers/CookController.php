<?php

namespace App\Http\Controllers;

use App\Forms\CookReviewForm;
use App\Helpers\BaseHelper;
use App\Helpers\Image;
use App\Helpers\ViewHelper;
use App\Http\Requests\CookFilterRequest;
use App\Http\Resources\CookCoordsResource;
use App\Http\Resources\CookDetailResource;
use App\Http\Resources\CookResource;
use App\Http\Resources\CookReviewResource;
use App\Http\Resources\CookShortResource;
use App\Http\Resources\DishCardResource;
use App\Http\Resources\FileResource;
use App\Http\Resources\ImageResizeResource;
use App\Models\Attachment;
use App\Models\Cook;
use App\Models\CookReview;
use App\Models\User;
use App\Services\BreadcrumbsService;
use App\Services\CookFilterService;
use App\Services\SeoService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CookController extends Controller
{

    public function __construct(
        protected BreadcrumbsService $breadcrumbsService,
        protected SeoService $seoService,
        protected CookFilterService $filterService,
    )
    {

    }

    /**
     *
     */
    public function index(CookFilterRequest $filterRequest, Cook $cookModel)
    {
        $this->breadcrumbsService->add('Все повара', route('cooks.index'));
        $this->seoService->setMeta('title', 'Список поваров');
        $this->seoService->setFooterTextCode('seo.footer.cooks.text');

        $suggest = '';
        if ($filterRequest->get('cook')) {
            $cook = $cookModel->query()
                ->whereActive(1)
                ->whereVerified(1)
                ->whereId($filterRequest->get('cook'))
                ->first();

            if ($cook) $suggest = strip_tags($cook->getSearchSuggestString());
        }

        BaseHelper::extendJsApp(['yandexKey' => setting('site.yandex-api-key')->getText()]);

        return view('cook.index', [
            'cookCoords' => $this->filterService->filterCooks($filterRequest->validated()),
            'filterData' => $this->filterService->getFilterFields($filterRequest->validated()),
            'suggest' => $suggest,
        ]);
    }

    public function filter(CookFilterRequest $filterRequest)
    {
        return response()->json([
            'success' => true,
            'data' => $this->filterService->filterCooks($filterRequest->validated()),
        ]);
    }

    public function suggests(Request $request, Cook $cookModel)
    {
        $name = $request->post('name');
        $main = $cookModel->getTable();
        $user = (new User())->getTable();

        $cooks = $cookModel->query()
            ->select(["$main.*"])
            ->leftJoin($user, "$user.id", '=', "$main.user_id")
            ->where("$user.name", 'LIKE', '%' . $name . '%')
            ->whereActive(1)
            ->whereVerified(1)
            ->with('user')
            ->limit(10)
            ->get();

        $cookMarkers = $cooks->keyBy('id')->map(fn(Cook $cook) => CookCoordsResource::make($cook));
        $cookData = CookResource::collection($cooks->keyBy('id'));
        $suggests = $cooks
            ->keyBy('id')
            ->map(fn (Cook $cook) => $cook->getSearchSuggestString());

        return response()->json([
            'success' => true,
            'data' => [
                'suggests' => $suggests,
                'cooks' => $cookData,
                'markers' => $cookMarkers,
            ],
        ]);
    }

    public function pin(Cook $cook)
    {
        return response()->json([
            'success' => true,
            'data' => [CookResource::make($cook)],
        ]);
    }

    /**
     *
     */
    public function detail(Cook $cook, UserService $userService)
    {
        if (!$cook->active || !$cook->verified) {
            abort(404);
        }

        $this->breadcrumbsService->add('Все повара', route('cooks.index'));
        $this->breadcrumbsService->add($cook->user->getFullName(), route('cooks.detail', ['cook' => $cook->id]));
        $this->seoService->setMeta('title', 'Профиль повара ' . $cook->user->getFullName());

        $dishes = $cook->dish()->with('attachment')->where('active', 1)->get();

        $avatar = $cook->user->getAvatar()
            ? $cook->user->getAvatar()->getData()
            : Image::getFakeData(ViewHelper::getEmptyAvatar(true));
        $this->seoService->setMeta('og:prefix', 'prefix="https://ogp.me/ns/website#"');
        $this->seoService->setMeta('og', [
            'og:title'     => 'Профиль повара ' . $cook->user->getFullName(),
            'og:url'       => $cook->getUrl(),
            'og:site_name' => setting('site.name')->text ?? '',
            'og:image'     => config('app.url') . $avatar['src'],
        ]);

        $reviews = CookReview::getCookReviews($cook->id);
        $reviewsCount = CookReview::getCookReviewsCount($cook->id);
        $totalPages = ceil($reviewsCount / CookReview::PAGE_SIZE);

        $currentUser = $userService->current();
        $canAddReview = $cook->isUserCanAddReview($currentUser);

        $kitchenPhotos = $cook->getKitchen()
            ->map(fn (Attachment $attach) => ImageResizeResource::make($attach, 'cook.attachment'))
            ->values()
            ->toArray();

        return view('cook.detail', [
            'cook'          => $cook,
            'cookData'      => CookDetailResource::make($cook),
            'dishes'        => DishCardResource::collection($dishes),
            'kitchenPhotos' => $kitchenPhotos,
            'certificates'  => FileResource::collection($cook->getCertificate()),
            'reviews'       => CookReviewResource::collection($reviews),
            'reviewsCount'  => $reviewsCount,
            'totalPages'    => $totalPages,
            'canAddReview'  => $canAddReview,
        ]);
    }

    /**
     *
     */
    public function reviews(Cook $cook, Request $request)
    {
        if (!$cook->active || !$cook->verified) {
            return response()->json(['success' => false]);
        }

        $page = (int)$request->get('page') ?: 1;
        if ($page < 1) $page = 1;

        $reviewsCount = CookReview::getCookReviewsCount($cook->id);
        $totalPages = ceil($reviewsCount / CookReview::PAGE_SIZE);

        if ($page > $totalPages) {
            return response()->json(['success' => false]);
        }

        $reviews = CookReview::getCookReviews($cook->id, $page);

        return response()->json([
            'success' => true,
            'data' => CookReviewResource::collection($reviews),
        ]);
    }

    public function reviewForm(Cook $cook, UserService $userService)
    {
        if (!$cook->active || !$cook->verified) {
            return response()->json(['success' => false]);
        }

        $currentUser = $userService->current();
        $canAddReview = $cook->isUserCanAddReview($currentUser);
        if (!$canAddReview) {
            return response()->json(['success' => false]);
        }

        return response()->json([
            'success' => true,
            'data'    => [
                'cookData' => CookShortResource::make($cook)->resolve(),
                'formData' => ((new CookReviewForm($cook->id))->toArray()),
            ],
        ]);
    }

    public function reviewSubmit(Cook $cook, UserService $userService, Request $request)
    {
        if (!$cook->active || !$cook->verified) {
            return response()->json(['success' => false]);
        }

        $currentUser = $userService->current();
        $canAddReview = $cook->isUserCanAddReview($currentUser);
        if (!$canAddReview) {
            return response()->json(['success' => false]);
        }

        $form = new CookReviewForm($cook->id);
        $form->setData($request->post());

        if (!$form->isValid()) {
            return response()->json([
                'success' => false,
                'errors'  => $form->getMessages(),
            ]);
        }

        $data = $form->getData();
        // Сохранение имени в пользователя
        if ($currentUser && empty(trim($currentUser->name))) {
            $currentUser->name = $data[$form::NAME];
            $currentUser->save();
        }

        // сохранение отзыва
        $reviewId = CookReview::create([
            'active'  => 0,
            'rating'  => (int)$data[$form::RATING],
            'cook_id' => $cook->id,
            'user_id' => $currentUser->id,
            'text'    => $data[$form::TEXT],
        ]);

        if ($reviewId) {
            $data = [
                'success' => true,
                'message' => [
                    'title' => 'Спасибо за отзыв!',
                    'text'  => setting('reviews.success')->getBigText(),
                ],
            ];
        }

        return response()->json($data);
    }
}

