<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\SberbankRequest;
use App\Models\Order;
use App\Services\OrderService;
use App\Services\PaymentService;
use App\Traits\LoggableTrait;
use Illuminate\Http\JsonResponse;

class PaymentController extends Controller {

    use LoggableTrait;

    /**
     * Сюда присылает статус оплаты из Сбера.
     *
     * @param SberbankRequest $request
     * @param OrderService $orderService
     * @param PaymentService $paymentService
     * @return JsonResponse
     */
    public function sberbankStatusChange(
        SberbankRequest $request,
        OrderService $orderService,
        PaymentService $paymentService
    ): JsonResponse {
        $validated = $request->validated();

        $this->makeLogger('payment' . DIRECTORY_SEPARATOR . 'sberbank');
        $this->log('Получение статуса оплаты заказа', $validated);

        $order = Order::find($validated['orderNumber']);

        return $this->statusChange($validated, $order, $orderService, $paymentService);
    }

    /**
     * В идеале все запросы из платежных шлюзов должны приходить сюда
     * и обрабатываться каждый своим классом на основе выбранной платежки у заказа
     *
     * Юкасса пока работает не так
     *
     * @param array $paymentInfo
     * @param Order $order
     * @param OrderService $orderService
     * @param PaymentService $paymentService
     * @return JsonResponse
     */
    public function statusChange(
        array $paymentInfo,
        Order $order,
        OrderService $orderService,
        PaymentService $paymentService
    ): JsonResponse {
        $payment = $order->orderPayments->first();
        if (!$payment) {
            return response()->json();
        }

        $handler = $paymentService->getHandler($payment);
        $isValid = $handler->isValidStatusChange($paymentInfo);

        $isPayed = $paymentService->isOrderPayed($order, $paymentInfo);

        if ($isValid && $isPayed) {
            $orderService->setPayed($order);
            $order->save();
        }

        return response()->json();
    }
}
