<?php

namespace App\Http\Controllers;

use App\Forms\OrderForm;
use App\Http\Resources\OrderSuccessResource;
use App\Models\Cook;
use App\Models\Order;
use App\Services\BasketService;
use App\Services\BreadcrumbsService;
use App\Services\OrderService;
use App\Services\Payment\PaymentResult;
use App\Services\SmsService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Resources\BasketByCookResource;

class OrderController extends Controller
{
    public function __construct(
        protected BasketService $basketService,
        protected BreadcrumbsService $breadcrumbsService,
    )
    {
    }


    public function index(Request $request, $cookId) {
        $cookId = (int)$cookId ?? 0;
        $basket = $this->basketService->get();

        if (!$cookId || !isset($basket[$cookId])) {
            return redirect(route('basket.index'));
        }

        $this->breadcrumbsService->add('Корзина', route('basket.index'));

        /** @var Cook $cook */
        $cook = $basket[$cookId]->getCook();

        return view('order.index', [
            'formData' => (new OrderForm($cookId))->populateUser()->toArray(),
            'cookCity' => $cook->city?->name ?? '',
            'cookAddress' => $cook->address,
            'basket'   => BasketByCookResource::make($basket[$cookId]),
        ]);
    }

    public function checkout(Request $request, $cookId, OrderService $orderService, UserService $userService, SmsService $smsService) {
        $cookId = (int)$cookId ?? 0;
        $basket = $this->basketService->get();

        if (!$cookId || !isset($basket[$cookId])) {
            return redirect(route('basket.index'));
        }

        $basketByCook = $basket[$cookId];
        $form = (new OrderForm($cookId))->populateUser();
        $form->setData($request->post());

        if (!$form->isValid()) {
            return response()->json([
                'success' => false,
                'errors'  => $form->getMessages(),
            ]);
        }

        $formData = $form->getData();
        if (!$userService->isAuth()) {
            $phone = $formData[OrderForm::PHONE];
            $code = $formData[OrderForm::CODE];
            $codeIsValid = $smsService->checkSmsCode($phone, $code, SmsService::TYPE_ORDER);

            if (!$codeIsValid) {
                return response()->json([
                    'success' => false,
                    'errors'  => [
                        OrderForm::CODE => ['Неправильный код подтверждения']
                    ],
                ]);
            }
        }

        $result = $orderService->createOrder($basketByCook, $formData);
        /** @var Order $order */
        $order = isset($result) && isset($result['order']) ? $result['order'] : null;
        /** @var PaymentResult $paymentResult */
        $paymentResult = isset($result) && isset($result['paymentResult']) ? $result['paymentResult'] : null;
        // todo обработка ошибок

        $successUrl = false;
        $errors = [];
        if (!empty($paymentResult)) {
            if ($paymentResult->isSuccess() && !empty($paymentResult->getPaymentUrl())) {
                $successUrl = $paymentResult->getPaymentUrl();
            } else {
                $errors[] = $paymentResult->getMessages();
            }
        } elseif ($order) {
            $errors[] = '';
        }

        return response()->json([
            'success'  => !empty($order) && empty($errors),
            'errors'   => $errors ?: false,
            'redirect' => $successUrl,
        ]);
    }

    public function success(Request $request, OrderService $orderService, UserService $userService) {
        $orderId = (int)($request->route('orderId') ?? 0);
        $hash = $request->get('hash') ?? '';

        $order = Order::findOrFail($orderId);
        abort_if(!$orderService->checkHash($orderId, $hash), 404);

        $this->breadcrumbsService->add('Корзина', route('basket.index'));

        return view('order.success', [
            'order' => OrderSuccessResource::make($order),
            'basket' => BasketByCookResource::make($order->getBasket()),
        ]);
    }
}

