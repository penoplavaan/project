<?php

declare(strict_types=1);

namespace App\Data;

use JsonSerializable;

class Error implements JsonSerializable
{
    protected string $text = '';
    protected string $code = '';

    public function __construct(string $text, string $code = '')
    {
        $this->text = $text;
        $this->code = $code;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function jsonSerialize(): array
    {
        return [
            'text' => $this->getText(),
            'code' => $this->getCode(),
        ];
    }
}
