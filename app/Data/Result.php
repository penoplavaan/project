<?php

declare(strict_types=1);

namespace App\Data;

use JsonSerializable;

class Result implements JsonSerializable
{
    protected array $errors = [];
    protected array $data = [];

    public function __construct(Result $result = null)
    {
        if ($result) {
            $this->data = $result->getData();
            $this->errors = $result->getErrors();
        }
    }

    public function addError(Error $error): Result
    {
        $this->errors[] = $error;
        return $this;
    }

    public function addErrors(array $errors): Result
    {
        $this->errors = array_merge($this->errors, $errors);
        return $this;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getImplodedErrors(string $separator = ''): string
    {
        return implode($separator, array_map(fn(Error $error) => $error->getText(), $this->getErrors()));
    }

    public function isSuccess(): bool
    {
        return empty($this->getErrors());
    }

    public function setData(array $data): Result
    {
        $this->data = $data;
        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function get(string $key)
    {
        if (!isset($this->data[$key])) {
            return null;
        }
        return $this->data[$key];
    }

    public function set(string $key, $value): Result
    {
        $this->data[$key] = $value;
        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'errors' => $this->getErrors(),
            'data'   => $this->getData(),
        ];
    }
}
