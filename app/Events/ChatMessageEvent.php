<?php

namespace App\Events;

use App\Http\Resources\ChatMessageResource;
use App\Models\ChatMessage;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;


class ChatMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id;
    public $data;
    public $channel;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ChatMessage $message, User $user)
    {
        $this->id = $message->id;
        $this->data = ChatMessageResource::make($message)->resolve();
        $this->data['chatUserId'] = $message->chat->getOtherUserId($user->id);
        $this->channel = $user->getChatChannel();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel($this->channel);
    }
}
