<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Gallery
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property int|null $show_on_main
 * @property-read Collection|Attachment[] $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery query()
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|Gallery whereShowOnMain($value)
 */
class Gallery extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Attachable;
    use Filterable;
    use CacheTrait;

    protected $fillable = ['name', 'show_on_main'];

    protected array $allowedFilters = [
        'id',
        'name',
    ];

    protected array $allowedSorts = [
        'id',
        'name',
        'created_at',
        'updated_at',
    ];

    public function hasActiveFlag(): bool
    {
        return false;
    }

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function getImagesForMain(): Collection
    {
        $elements = $this->getForMain();
        $images   = new Collection();
        /** @var Collection|\App\Models\Attachment[] $images */
        $images = $images->merge($elements->pluck('attachment')->collapse());

        return $images->map(function ($image) {
            return $image->getImage()->resize('main.gallery')->getData();
        });
    }


    public function getForMain(): Collection
    {
        return Cache::tags([
            static::getCacheTag()
        ])->remember('gallery_get_for_main', static::getCacheTime(), function () {
            return static::whereShowOnMain(true)->get();
        });
    }
}
