<?php

namespace App\Models;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use App\Mail\SimpleMail;
use App\Services\YandexApiService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Cook
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property int|null $speciality_id
 * @property int $active
 * @property int $sort
 * @property int $verified
 * @property int $passport_verified
 * @property int $sanitary_verified
 * @property string|null $about
 * @property string|null $admin_comment
 * @property int|null $show_on_main
 * @property string|null $address
 * @property string|null $coords
 * @property string|null $experience
 * @property float|null $rating
 * @property int|null $reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DishCategory[] $category
 * @property-read int|null $category_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dish[] $dish
 * @property-read int|null $dish_count
 * @property-read string $orchid_rel_user_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CookReview[] $reviews
 * @property-read \App\Models\CookSpeciality|null $speciality
 * @property-read \App\Models\User $user
 * @method static Builder|Cook defaultSort(string $column, string $direction = 'asc')
 * @method static Builder|Cook filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static Builder|Cook filtersApply(iterable $filters = [])
 * @method static Builder|Cook filtersApplySelection($selection)
 * @method static Builder|Cook newModelQuery()
 * @method static Builder|Cook newQuery()
 * @method static Builder|Cook query()
 * @method static Builder|Cook whereAbout($value)
 * @method static Builder|Cook whereActive($value)
 * @method static Builder|Cook whereAddress($value)
 * @method static Builder|Cook whereAdminComment($value)
 * @method static Builder|Cook whereCoords($value)
 * @method static Builder|Cook whereCreatedAt($value)
 * @method static Builder|Cook whereExperience($value)
 * @method static Builder|Cook whereId($value)
 * @method static Builder|Cook wherePassportVerified($value)
 * @method static Builder|Cook whereRating($value)
 * @method static Builder|Cook whereReviewsCount($value)
 * @method static Builder|Cook whereSanitaryVerified($value)
 * @method static Builder|Cook whereShowOnMain($value)
 * @method static Builder|Cook whereSort($value)
 * @method static Builder|Cook whereSpecialityId($value)
 * @method static Builder|Cook whereUpdatedAt($value)
 * @method static Builder|Cook whereUserId($value)
 * @method static Builder|Cook whereVerified($value)
 * @method static Builder|Cook withUserModel()
 * @mixin \Eloquent
 * @property int|null $city_id
 * @method static Builder|Cook whereCityId($value)
 * @property-read \App\Models\City|null $city
 * @property float|null $manual_rating
 * @method static Builder|Cook whereManualRating($value)
 * @property string|null $delivery
 * @method static Builder|Cook whereDelivery($value)
 */
class Cook extends BaseModel
{
    use HasFactory;
    use Filterable;
    use AsSource;
    use Attachable;

    protected $guarded = [
        'id',
    ];

    protected $allowedFilters = [
        'id',
        'active',
        'sort',
        'show_on_main',
        'user_id',
        'address'
    ];

    protected $allowedSorts = [
        'id',
        'active',
        'verified',
        'sort',
        'show_on_main',
    ];

    public function hasSortFlag(): bool
    {
        // не надо поваров сорировать по полю sort автоматически
        return false;
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function (Cook $cook) {
            if ($cook->isDirty('address')) {
                $cook->coords = static::geocodeCookCoords($cook->address, true, $cook->id);
            }

            if ($cook->isDirty('verified') && $cook->verified) {
                if (!empty($cook->user->email)) {
                    $mail = new SimpleMail('cook-verified');
                    $mail->subject('Профиль повара подтверждён');
                    Mail::to($cook->user->email)->send($mail);
                }
            }
        });
//        Убрана модерация при регистрации повара.
//        static::saved(function (Cook $cook) {
//            if (!$cook->verified && !OrchidDetector::isOrchid()) {
//                $mail = new SimpleMail('cook-created', ['cookId' => $cook->id]);
//                $mail->subject('Подана заявка на повара');
//                \App\Helpers\BaseHelper::sendEmailToAdmins($mail);
//            }
//        });
    }

    /**
     * Получить координаты по адресу с проверкуй на уникальность координат
     * @param $address
     * @param $checkExist
     * @param $existId
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function geocodeCookCoords($address, $checkExist = true, $existId = 0): string {
        // Если пользователь новый, то на saving приходит id = ''
        if (empty($existId)) {
            $existId = 0;
        }

        /** @var YandexApiService $yandexApi */
        $yandexApi = app()->make(YandexApiService::class);
        $coords = $yandexApi->geocode($address);
        $coordStr = ($coords && $coords['lat'] && $coords['lon'])
            ? $coords['lat'] . ',' . $coords['lon']
            : '';

        if (!empty($coordStr)) {

            // Если в БД уже есть в ТОЧНОСТИ такие же координаты - прибавить рандомные несколько метров в стороны
            if ($checkExist) {
                $tries = 5;
                $isExist = static::getCookByCoordsStr($coordStr, $existId);

                while ($isExist && $tries > 0) {
                    $tries--;

                    $coords['lat'] += (rand(0, 11) * 2 - 11) / 60000;
                    $coords['lon'] += (rand(0, 11) * 2 - 11) / 60000;

                    $coordStr = $coords['lat'] . ',' . $coords['lon'];
                    $isExist = static::getCookByCoordsStr($coordStr, $existId);
                }
            }
        }

        return $coordStr;
    }

    /**
     * Получить повара по точным координатам
     * @param string $coordStr
     * @param int $excludeId
     * @return bool
     */
    public static function getCookByCoordsStr(string $coordStr, int $excludeId = 0): bool
    {
        $query = Cook::query()->where('coords', '=', $coordStr);

        if ($excludeId) {
            $query->whereNot('id', $excludeId);
        }

        $exist = $query->first();

        return !empty($exist);
    }

    /**
     * Отдельный кастомный скоуп для поиска поваров в админке Орчида в ресурсе блюд
     * по имени пряивязанного к повару пользователя
     * @param Builder $query
     * @return Builder
     */
    public function scopeWithUserModel(Builder $query)
    {
        $users = DB::table('users')
            ->selectRaw("users.name as name, users.id as users_id");

        return $query
            ->select(['cooks.*', 'users.name'])
            ->joinSub($users, 'users', function ($join)
            {
                $join->on("users.users_id", '=', 'cooks.user_id');
            });
    }

    /**
     * Должен ли этот повар быть видимым для пользователей? проверяется активнось, верифицированность и .т.д
     * @return bool
     */
    public function isVisible(): bool {
        return $this->active && $this->verified;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function getOrchidRelUserNameAttribute(): string
    {
        return $this->id . ': ' . ($this->user?->getFullName() ?? '') . ' [userId: ' . $this->user_id . ']';
    }

    // Специальность повара: Кондитер, Пекарь и т.д
    public function speciality()
    {
        return $this->belongsTo(CookSpeciality::class, 'speciality_id');
    }

    // Категории блюд: Супы, Салаты и т.д
    public function category(): BelongsToMany
    {
        return $this->belongsToMany(DishCategory::class, 'cook_categories', 'cook_id', 'category_id');
    }

    public function dish()
    {
        return $this->hasMany(Dish::class);
    }

    /**
     * Привязка только к активным отзывам
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany(CookReview::class)->where('active', 1);
    }

    public function getPassport(): ?\App\Models\Attachment
    {
        return $this->attachment->firstWhere('group', 'passport');
    }

    public function passportId(): Attribute
    {
        return Attribute::make(fn() => $this->getPassport()?->id);
    }

    public function getSanitary(): ?Collection
    {
        return $this->attachment->where('group', 'sanitary');
    }

    public function sanitaryId(): Attribute
    {
        return Attribute::make(fn() => $this->getSanitary()->map(fn($el) => $el->id)->toArray());
    }

    public function getDocument(): ?Collection
    {
        return $this->attachment->where('group', 'document');
    }

    public function documentId(): Attribute
    {
        return Attribute::make(fn() => $this->getDocument()->map(fn($el) => $el->id)->toArray());
    }

    public function getKitchen(): ?Collection
    {
        return $this->attachment->where('group', 'kitchen');
    }

    public function kitchenId(): Attribute
    {
        return Attribute::make(fn() => $this->getKitchen()->map(fn($el) => $el->id)->toArray());
    }

    public function getCertificate(): ?Collection
    {
        return $this->attachment->where('group', 'certificate');
    }

    public function certificateId(): Attribute
    {
        return Attribute::make(fn() => $this->getCertificate()->map(fn($el) => $el->id)->toArray());
    }

    public function getPicture(string $resizeName = 'index.promo.cook'): array
    {
        $image = $this->user?->getAvatar()
            ? $this->user->getAvatar()->resize($resizeName)
            : Image::make(ViewHelper::getEmptyAvatar(true));

        return $image->getData();
    }

    /**
     * todo Добавить дистанцию до повара, float
     * @return string
     */
    public function getDistance(): string
    {
        //todo
        return '';
    }

    public function getUrl(): string
    {
        return route('cooks.detail', ['cook' => $this->id]);
    }

    public function getRegisterDate(): string
    {
        return ViewHelper::localizedDate($this->created_at ?: new Carbon());
    }

    public function getSearchSuggestString()
    {
        return $this->user->getFullName() . ', <small>' . $this->address . '</small>';
    }

    public function getPosition(): string {
        return $this->speciality?->title ?? 'Повар';
    }

    public function getLabels()
    {
        $labels = [
            $this->passport_verified ? [
                'color' => '#F5DF4D',
                'icon'  => Image::getFakeData('/images/cook-labels/passport.svg'),
                'name'  => 'Паспорт подтвержден',
            ] : null,
            $this->sanitary_verified ? [
                'color' => '#06C160',
                'icon'  => Image::getFakeData('/images/cook-labels/sanitary.svg'),
                'name'  => 'Есть санкнижка',
            ] : null,
        ];

        return array_filter($labels);
    }

    public function getCategories(): array
    {
        $categories = [];
        foreach ($this->category as $category) {
            $categories[] = [
                'id'   => $category->id,
                'name' => $category->title,
                'link' => $category->getUrl(),
            ];
        }

        return $categories;
    }

    public function getCategoriesId(): array
    {
        $categories = [];
        foreach ($this->category as $category) {
            $categories[] = $category->id;
        }

        return array_filter($categories);
    }

    public function getExperienceYears(): string
    {
        if (!$this->experience) {
            return '';
        }

        if (!(int)$this->experience) {
            return $this->experience;
        }

        $years = ((int)date('Y') - (int)$this->experience) + 1;

        return $years . ' ' . ViewHelper::getWordForm($years, ['год', 'года', 'лет']);
    }

    /**
     * Может ли переданный пользователь оставлять отзыв на текущего повара
     * @param User|null $user
     * @return bool
     */
    public function isUserCanAddReview(?User $user): bool
    {
        if (!$user) return false;

        $hasOrders = Order::query()
            ->where('user_id', $user->id)
            ->where('cook_id', $this->id)
            ->select('id')
            ->limit(1)
            ->first();

        return !empty($hasOrders);
    }

    public function getRoundRating(): string
    {
        if ($this->manual_rating) {
            return number_format($this->manual_rating, 1, '.', '');
        }

        return ($this->rating > 0) ? number_format($this->rating, 1, '.', '') : '';
    }
}
