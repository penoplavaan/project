<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\ForCookSupport
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $active
 * @property string $title
 * @property string $text
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport query()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookSupport whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ForCookSupport extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];
}
