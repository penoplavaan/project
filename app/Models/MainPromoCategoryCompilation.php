<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\MainDishCompilation
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property int $sort
 * @property string $name
 * @property string|null $url
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation whereUrl($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dish[] $dishes
 * @property-read int|null $dishes_count
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilation filtersApplySelection($selection)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MainPromoCategoryCompilationPromoCard[] $mainDishCompilationDish
 * @property-read int|null $main_dish_compilation_dish_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MainPromoCategoryCompilationPromoCard[] $MainPromoCategoryCompilationPromoCard
 * @property-read int|null $main_promo_category_compilation_promo_card_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PromoCard[] $promoCards
 * @property-read int|null $promo_cards_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MainPromoCategoryCompilationPromoCard[] $mainPromoCategoryCompilationPromoCard
 */
class MainPromoCategoryCompilation extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use CacheTrait;

    protected $guarded = ['id'];


    public function promoCards(): BelongsToMany {
        return $this->belongsToMany(PromoCard::class, 'main_promo_category_compilation_promo_cards', 'compilation_id', 'promo_card_id');
    }

    public function mainPromoCategoryCompilationPromoCard() {
        return $this->hasMany(MainPromoCategoryCompilationPromoCard::class, 'compilation_id');
    }
}
