<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\FeedbackSpeciality
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @mixin \Eloquent
 * @property int|null $active
 * @property int|null $sort
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackCategory whereUpdatedAt($value)
 */
class FeedbackCategory extends BaseModel {
    use HasFactory;
    use AsSource;
    use Filterable;

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort',
    ];

    protected $fillable = [
      'sort',
      'name',
      'active',
    ];
}
