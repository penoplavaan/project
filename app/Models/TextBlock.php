<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\TextBlock
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property string $code
 * @property string $title
 * @property string $text1
 * @property string $text2
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock query()
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereText1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereText2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextBlock whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TextBlock extends BaseModel
{
    use AsSource;
    use Filterable;
    use HasFactory;
    use CacheTrait;

    protected $guarded = ['id'];

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function getText(): string
    {
        return $this->text1 . $this->text2;
    }
}
