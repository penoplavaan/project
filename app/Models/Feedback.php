<?php

namespace App\Models;

use App\Helpers\BaseHelper;
use App\Mail\SimpleMail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Feedback
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property int $topic_id
 * @property string $text
 * @property string $question
 * @property-read \App\Models\FeedbackCategory|null $topic
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback query()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereTopicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Feedback extends BaseModel
{
    use HasFactory;
    use Filterable;
    use AsSource;

    protected $guarded = [
        'id',
    ];

    protected array $allowedFilters = [
        'sort',
        'name',
        'phone',
    ];

    protected array $allowedSorts = [
        'sort',
        'name',
        'phone',
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function (Feedback $feedback) {
            $mail = new SimpleMail('feedback-created', ['feedbackId' => $feedback->id]);
            $mail->subject('Новая заявка из формы обратной связи');
            BaseHelper::sendEmailToAdmins($mail);
        });
    }

    public function topic(): \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo(FeedbackCategory::class);
    }
}
