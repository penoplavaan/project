<?php

namespace App\Models;

use App\Orchid\OrchidDetector;
use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Cache;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\MainDishCompilation
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property int $sort
 * @property string $name
 * @property string|null $url
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation whereUrl($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dish[] $dishes
 * @property-read int|null $dishes_count
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilation filtersApplySelection($selection)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MainDishCompilationDish[] $mainDishCompilationDish
 * @property-read int|null $main_dish_compilation_dish_count
 */
class MainDishCompilation extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use CacheTrait;

    protected $guarded = ['id'];

    const TOTAL_MAX_COMPILATIONS = 7;

    public function dishes(): BelongsToMany {
        return $this->belongsToMany(Dish::class, 'main_dish_compilation_dishes', 'compilation_id', 'dish_id');
    }

    public function mainDishCompilationDish() {
        return $this->hasMany(MainDishCompilationDish::class, 'compilation_id');
    }

    public function getForMain($offset = 0, $limit = 3) {
        return Cache::tags([static::getCacheTag()])
            ->remember(
                "main-dish-compilation-$offset-$limit",
                static::getCacheTime(),
                fn () => $this->doGetForMain($offset, $limit)
            );
    }

    /**
     * Получение подборок для главной
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function doGetForMain(int $offset = 0, int $limit = 3): array {
        $main = $this->getTable();
        $pivot = (new MainDishCompilationDish)->getTable();
        $dish = (new Dish())->getTable();
        $cook = (new Cook())->getTable();

        $compilations = $this->query()
            ->select(["$main.*"])
            ->distinct()
            ->leftJoin($pivot, "$main.id", '=', "$pivot.compilation_id")
            ->leftJoin($dish, "$dish.id", '=', "$pivot.dish_id")
            ->leftJoin($cook, "$cook.id", '=', "$dish.cook_id")
            ->where("$main.active", 1)
            ->where("$dish.active", 1)
            ->where("$cook.active", 1)
            ->where("$cook.verified", 1)
            ->with('dishes')
            ->with('dishes.cook')
            ->with('dishes.cook.user')
            ->with('dishes.cook.user.attachment')
            ->with('dishes.attachment')
            ->with('dishes.labels')
            ->get();

        $compilations = $compilations->filter(fn ($comp) => $comp->dishes->count() > 0);

        $compilations = $compilations->slice(0, static::TOTAL_MAX_COMPILATIONS);

        return [
            $compilations->slice($offset, $limit),
            $compilations->count()
        ];
    }
}
