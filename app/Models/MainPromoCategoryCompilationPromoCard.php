<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MainDishCompilationDish
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $dish_id
 * @property int $compilation_id
 * @property-read \App\Models\MainDishCompilation|null $compilation
 * @property-read \App\Models\Dish $dish
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard whereCompilationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $active
 * @property int $promo_card_id
 * @property-read \App\Models\PromoCard $promoCard
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoCategoryCompilationPromoCard wherePromoCardId($value)
 */
class MainPromoCategoryCompilationPromoCard extends Model
{
    use HasFactory;

    public function compilation()
    {
        return $this->belongsTo(MainPromoCategoryCompilation::class, 'compilation_id');
    }

    public function promoCard()
    {
        return $this->belongsTo(PromoCard::class, 'promo_card_id');
    }
}
