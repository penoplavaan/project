<?php

namespace App\Models;

use App\Helpers\ViewHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;

/**
 * App\Models\ChatMessage
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $chat_id
 * @property int $user_id
 * @property string $message
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Chat $chat
 * @property-read \App\Models\User $user
 * @property-read Attachment[]|Collection $attachment
 * @property-read \App\Models\Order|null $order
 */
class ChatMessage extends BaseModel
{
    use HasFactory;
    use Attachable;

    protected $guarded = ['id'];

    public function hasActiveFlag(): bool
    {
        return false;
    }

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'message_id');
    }

    public function getDate(): string
    {
        return ViewHelper::localizedDateTime($this->created_at ?? new Carbon());
    }

    public function getTimestamp(): int
    {
        return ($this->created_at ?? new Carbon())->getTimestamp();
    }
}
