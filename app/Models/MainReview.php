<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\MainReview
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $active
 * @property string $name
 * @property string $position
 * @property string $text
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainReview whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MainReview extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;
    use CacheTrait;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];

    public function getForMainPage() {
        return Cache::tags([static::getCacheTag()])
            ->remember(
                'main_reviews',
                static::getCacheTime(),
                fn() => $this->doGetForMainPage()
            );
    }

    public function doGetForMainPage() {
        return $this->query()
            ->with('attachment')
            ->get();
    }
}
