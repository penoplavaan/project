<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\DishesToTags
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $dish_id
 * @property int $tag_id
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags query()
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToTags whereUpdatedAt($value)
 */
class DishesToTags extends BaseModel
{
    use HasFactory;
    use Filterable;
    use AsSource;

    protected $fillable = [
        'dish_id',
        'tag_id',
    ];
}
