<?php

namespace App\Models;

use App\Traits\Model\CompileComponentTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\TextPage
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property string $title
 * @property string $url
 * @property string $text
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage query()
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TextPage whereUrl($value)
 * @mixin \Eloquent
 */
class TextPage extends BaseModel
{
    use AsSource;
    use Filterable;
    use CompileComponentTrait;
    use HasFactory;

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function getText(): string
    {
        return $this->compileComponents('text');
    }

    public function getUrl($absolute = true): string
    {
        $domain = $absolute ? config('app.url') : '';
        return $domain . '/' . ltrim($this->url, '/');
    }
}
