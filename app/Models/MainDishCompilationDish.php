<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MainDishCompilationDish
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $dish_id
 * @property int $compilation_id
 * @property-read \App\Models\MainDishCompilation|null $compilation
 * @property-read \App\Models\Dish $dish
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish whereCompilationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainDishCompilationDish whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MainDishCompilationDish extends Model
{
    use HasFactory;

    public function compilation()
    {
        return $this->belongsTo(MainDishCompilation::class, 'compilation_id');
    }

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }
}
