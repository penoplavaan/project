<?php

namespace App\Models;

use App\Helpers\Image;
use App\Helpers\ViewHelper;
use App\Orchid\Presenters\UserPresenter;
use Carbon\Carbon;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Platform\Models\User as OrchidUser;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property array|null $permissions
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Orchid\Platform\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \App\Models\Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|User averageByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User byAccess(string $permitWithoutWildcard)
 * @method static \Illuminate\Database\Eloquent\Builder|User byAnyAccess($permitsWithoutWildcard)
 * @method static \Illuminate\Database\Eloquent\Builder|User countByDays($startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User countForGroup(string $groupColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|User defaultSort(string $column, string $direction = 'asc')
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|User filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|User maxByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User minByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User sumByDays(string $value, $startDate = null, $stopDate = null, ?string $dateColumn = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User valuesByDays(string $value, $startDate = null, $stopDate = null, string $dateColumn = 'created_at')
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Cook|null $cook
 * @property string|null $phone
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @property string|null $address
 * @property string|null $about
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAddress($value)
 * @property int|null $advertisement
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAdvertisement($value)
 * @property int $phone_verified
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneVerified($value)
 */
class User extends OrchidUser implements MustVerifyEmail, CanResetPassword
{

    use Notifiable;
    use Attachable;
    use CanResetPasswordTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'phone_verified',
        'about',
        'address',
        'password',
        'permissions',
        'advertisement',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'permissions',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'permissions'       => 'array',
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes for which you can use filters in url.
     *
     * @var array
     */
    protected $allowedFilters = [
        'id',
        'name',
        'email',
        'phone',
        'permissions',
    ];

    /**
     * The attributes for which can use sort in url.
     *
     * @var array
     */
    protected $allowedSorts = [
        'id',
        'name',
        'email',
        'phone',
        'updated_at',
        'created_at',
        'email_verified_at',
    ];

    protected $isCook = null;

    const CHAT_SALT = 'm5sk39imif9Oa7bKe2DXfBDq81dkNkQa';

    public function cook()
    {
        return $this->hasOne(Cook::class);
    }

    public function isCook(): bool
    {
        if ($this->isCook === null) {
            $this->isCook = !empty($this->cook) && $this->cook->verified;
        }

        return $this->isCook;
    }

    public function getFullName($onlyRealName = false): ?string
    {
        if ($onlyRealName) return $this->name;

        return $this->name ?: $this->email ?: $this->phone;
    }

    public function avatarId(): Attribute
    {
        return Attribute::make(fn() => $this->attachment->map(fn ($el) => $el->id)->first());
    }

    public function emailVerified(): Attribute
    {
        return Attribute::make(fn() => !empty($this->email) && !empty($this->email_verified_at));
    }

    public function getChatChannel() {
        return 'user-' . md5(static::CHAT_SALT . '|' . $this->id);
    }

    public function getAvatar(): ?Image
    {
        if ($this->attachment && $this->attachment->count() > 0) {
            return $this->attachment[0]->getImage();
        }

        return null;
    }

    public function getRegisterDate(): string {
        return ViewHelper::localizedDate($this->created_at ?: new Carbon());
    }

    /**
     * @return UserPresenter
     */
    public function presenter()
    {
        return new UserPresenter($this);
    }
}
