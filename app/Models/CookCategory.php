<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\CookCategory
 *
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory query()
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $category_id
 * @property int $cook_id
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory whereCookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookCategory whereUpdatedAt($value)
 */
class CookCategory extends BaseModel
{
    use HasFactory;
    use Filterable;
    use AsSource;

    protected $fillable = [
        'category_id',
        'cook_id',
    ];
}
