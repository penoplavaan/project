<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\City
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $is_top
 * @property string $name
 * @property string|null $region Регион
 * @property string|null $district Федеральный округ
 * @property string $lat Широта
 * @property string $lng Долгота
 * @method static \Illuminate\Database\Eloquent\Builder|City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City query()
 * @method static \Illuminate\Database\Eloquent\Builder|City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereIsTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|City defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|City filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|City filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|City filtersApplySelection($selection)
 */
class City extends BaseModel
{
    use HasFactory;
    use Filterable;
    use AsSource;

    protected $guarded = [
        'id',
    ];

    protected $allowedFilters = [
        'is_top',
        'sort',
        'name',
    ];

    protected $allowedSorts = [
        'is_top',
        'sort',
    ];


    public function hasActiveFlag(): bool
    {
        return false;
    }

    /**
     * @return \Illuminate\Support\Collection|City[]
     */
    public function getTopList(): \Illuminate\Support\Collection
    {
        return $this->whereIsTop(1)->limit(20)->get();
    }

    /**
     * @return \Illuminate\Support\Collection|City[]
     */
    public function getByFilter(string $filter): \Illuminate\Support\Collection
    {
        return $this->where('name', 'LIKE', '%' . $filter . '%')
            ->limit(20)
            ->get();
    }
}
