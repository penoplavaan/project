<?php

namespace App\Models;

use App\Orchid\OrchidDetector;
use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Orchid\Attachment\Attachable;
use Orchid\Screen\AsSource;

/**
 * App\Models\MenuItem
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property string $title
 * @property string $url
 * @property string $prop
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereProp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereUrl($value)
 * @mixin \Eloquent
 * @property string|null $code
 * @property int|null $sort
 * @property int|null $parent_id
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereSort($value)
 * @property int|null $icon_id
 * @property-read \App\Models\Attachment|null $icon
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereIconId($value)
 */
class MenuItem extends BaseModel
{
    use HasFactory;
    use AsSource;
    use CacheTrait;
    use Attachable;

    protected ?Collection $children = null;
    public int $level = 0;
    protected static ?Collection $items = null;
    protected array $manualSubmenu = [];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'url',
        'prop',
        'active',
        'code',
        'sort',
        'parent_id',
        'icon_id',
    ];

    public function icon(): BelongsTo
    {
        return $this->belongsTo(Attachment::class, 'icon_id');
    }

    /**
     * @return Collection|MenuItem[]
     */
    public function getChildren(): Collection
    {
        if (!$this->children) {
            $this->children = new Collection();
        }

        return $this->children;
    }

    /**
     * Находися на странице из пункта меню
     */
    public function isSelected(): bool
    {
        return request()->getPathInfo() == $this->url;
    }

    /**
     * Находися на странице из пункта меню или дочерней
     */
    public function isCurrent(): bool
    {
        return str_starts_with(request()->getRequestUri(), $this->url);
    }

    /**
     * @param \Illuminate\Support\Collection|\App\Models\DishCategory[] $dishesCategoryCollection
     * @return void
     */
    public function setDishesSubmenu($dishesCategoryCollection)
    {
        foreach ($dishesCategoryCollection as $category) {
            if (!$category->total) continue;

            $url = $category->getUrl();
            $this->manualSubmenu[] = [
                'name'    => $category->getMenuTitle(),
                'url'     => $url,
                'current' => str_starts_with(request()->getRequestUri(), $url)
            ];
        }
    }

    public function getManualSubmenu(): ?array
    {
        if (!empty($this->manualSubmenu)) {
            return $this->manualSubmenu;
        }

        return null;
    }

    public static function tree($code = false): ?static
    {
        /** @var $item static */
        /** @var $items static[]|Collection */
        $list = static::getAll();

        $tree = new Collection();

        foreach ($list as $item) {
            if ($item->parent_id && $list->get($item->parent_id)) {
                /** @var MenuItem $menuItem */
                $menuItem = $list->get($item->parent_id);
                $menuItem->getChildren()->push($item);
                continue;
            }
            $tree->push($item);
        }

        if ($code) {
            return $list->keyBy('code')->get($code);
        }

        return static::createRoot($tree);
    }

    public static function getAll(): Collection
    {
        return Cache::tags([
            static::getCacheTag(),
            OrchidDetector::isOrchid() ? 'menu_admin' : 'menu_front'
        ])
            ->remember(
                'menu_item_get_all',
                static::getCacheTime(),
                function () {
                    return static::query()
                        ->with('icon')
                        ->orderBy('sort')
                        ->get()
                        ->keyBy('id');
                }
            );
    }

    protected static function createRoot($children): static
    {
        $root = new static();
        $root->children = $children;
        return $root;
    }

    public static function leftMargin(): Collection
    {
        $tree = static::tree()->getChildren();

        $res = new Collection();

        function needle(Collection $tree, $level, $res)
        {
            /** @var $item MenuItem */
            foreach ($tree as $item) {
                $item->level = $level;
                $res->push($item);
                needle($item->getChildren(), $level + 1, $res);
            }
        }

        needle($tree, 0, $res);

        return $res;
    }

    public function deleteWithChildren(): bool
    {
        $list = static::leftMargin();
        $level = -1;

        foreach ($list as $item) {
            if ($level > -1) {
                if ($item->level > $level) {
                    $item->delete();
                } else {
                    break;
                }
            }

            if ($this->id == $item->id) {
                $level = $item->level;
            }
        }

        return $this->delete();
    }
}
