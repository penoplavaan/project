<?php

namespace App\Models;

use App\Helpers\BaseHelper;
use App\Mail\SimpleMail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Mail;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Callback
 *
 * @property-read \App\Models\PromoCard|null $promoCard
 * @method static \Illuminate\Database\Eloquent\Builder|Callback defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Callback filters(?mixed $kit = null, ?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Callback filtersApplySelection($class)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Callback newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Callback query()
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $promo_card_id
 * @property string|null $phone
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback wherePromoCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Callback whereUpdatedAt($value)
 */
class Callback extends BaseModel
{
    use HasFactory;
    use Filterable;
    use AsSource;

    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'promo_card_id',
        'phone'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function (Callback $model) {
            $mail = new SimpleMail('callback-created', ['callbackId' => $model->id]);
            $mail->subject('Новая заявка на обратный звонок');
            BaseHelper::sendEmailToAdmins($mail);
        });
    }

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function promoCard(): \Illuminate\Database\Eloquent\Relations\BelongsTo {
        return $this->belongsTo(PromoCard::class);
    }
}
