<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PaySystem
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $code
 * @property string $name
 * @property string|null $description
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem query()
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PaySystem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PaySystem extends Model
{
    use HasFactory;
}
