<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\SiteSetting
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $active Активность
 * @property string|null $code Код элемента
 * @property string|null $text Текст
 * @property string|null $big_text Подробный текст
 * @property int|null $condition Да/Нет
 * @property int|null $section_id Id Секции сайта
 * @property mixed|null $test
 * @mixin \Eloquent
 * @property int|null $sort
 * @property string|null $color
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereBigText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereCondition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereIdSection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereTest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereUpload($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSetting whereSectionId($value)
 */
class SiteSetting extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Attachable;
    use Filterable;
    use CacheTrait;

    protected $fillable = [
        'text',
        'code',
        'active',
        'big_text',
        'section_id',
        'sort',
        'color'
    ];

    protected array $allowedFilters = [
        'code',
        'active',
        'sort',
        'text',
    ];

    protected array $allowedSorts = [
        'code',
        'active',
        'sort',
        'text',
    ];

    public function isEmpty(): bool
    {
        return !$this->getText() && !$this->getBigText();
    }

    public function getMoreSplit(): array
    {
        $more = '#MORE#';

        $text    = $this->big_text ?: '';
        $morePos = mb_strpos($text, $more);

        return [mb_substr($text, 0, $morePos), mb_substr($text, $morePos + mb_strlen($more))];
    }

    public function getText(string $default = ''): string
    {
        return $this->text ?: $default;
    }

    public function getBigText(string $default = ''): string
    {
        return $this->big_text ?: $default;
    }

    public function getImage(): ?Attachment
    {
        return $this->getImages()->first() ?: null;
    }

    public function getImageUrl(): string
    {
        return $this->getImage()?->getRelativeUrlAttribute() ?: '';
    }

    public function getImages(): Collection
    {
        $files = $this->getFiles();

        return $files->filter(fn(Attachment $item) => $item->isImage())->values();
    }

    public function getFiles(): Collection
    {
        return $this->attachment;
    }

    public function getColor(): string
    {
        return $this->color ?: '';
    }
}
