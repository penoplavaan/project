<?php

namespace App\Models;

use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Image;

/**
 * App\Models\Attachment
 *
 * @property int $id
 * @property string $name
 * @property string $original_name
 * @property string $mime
 * @property string|null $extension
 * @property int $size
 * @property int $sort
 * @property string $path
 * @property string|null $description
 * @property string|null $alt
 * @property string|null $hash
 * @property string $disk
 * @property int|null $user_id
 * @property string|null $group
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string|null $relative_url
 * @property-read string|null $title
 * @property-read string|null $url
 * @property-read \Illuminate\Database\Eloquent\Collection|\Orchid\Attachment\Models\Attachmentable[] $relationships
 * @property-read int|null $relationships_count
 * @property-read \Orchid\Platform\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereAlt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereDisk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereMime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereUserId($value)
 * @mixin \Eloquent
 */
class Attachment extends \Orchid\Attachment\Models\Attachment
{
    use AsSource;
    use Filterable;

    protected $allowedFilters = [
        'id',
        'original_name',
    ];

    protected $allowedSorts = [
        'id',
        'original_name',
        'size',
        'created_at',
        'updated_at',
    ];

    public function hasActiveFlag(): bool
    {
        return false;
    }

    public function isImage(): bool
    {
        return strpos($this->mime, 'image/') === 0;
    }

    public function getFileData(): ?string
    {
        return Storage::disk($this->disk)->get($this->physicalPath());
    }

    public function getImage(): Image
    {
        return Image::make($this->relative_url);
    }

    public function getResizeData(string $resize): array
    {
        return $this->getImage()->resize($resize)->getData();
    }
}
