<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\DishesToAdditionalCategories
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $dish_id
 * @property int $category_id
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories filters(?mixed $kit = null, ?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories filtersApplySelection($class)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories query()
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishesToAdditionalCategories whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DishesToAdditionalCategories extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;

    protected $fillable = [
        'dish_id',
        'category_id',
    ];

    public function hasActiveFlag(): bool
    {
        return false;
    }

    public function hasSortFlag(): bool
    {
        return false;
    }
}
