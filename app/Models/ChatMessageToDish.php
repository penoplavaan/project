<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChatMessageToDish
 *
 * @deprecated 
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $count
 * @property int $price
 * @property string $name
 * @property int $chat_message_id
 * @property int $dish_id
 * @property-read \App\Models\ChatMessage $chatMessage
 * @property-read \App\Models\Dish $dish
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish whereChatMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessageToDish whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ChatMessageToDish extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function chatMessage()
    {
        return $this->belongsTo(ChatMessage::class);
    }

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }
}
