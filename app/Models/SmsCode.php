<?php

namespace App\Models;

use App\Services\SmsService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SmsCode
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $client_ip
 * @property string $phone
 * @property string $code
 * @property string|null $type
 * @property int $confirmed
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereClientIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $confirmed_session_id
 * @method static \Illuminate\Database\Eloquent\Builder|SmsCode whereConfirmedSessionId($value)
 */
class SmsCode extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    const DEFAULT_LENGTH = 4;

    public function cleanupOldCodes()
    {
        $list = static::query()
            ->where('created_at', '<', \Carbon\Carbon::now()->subSeconds(SmsService::LIMIT_TIME))
            ->orWhere('confirmed', '1')
            ->get();

        foreach ($list as $oldCode) {
            $oldCode->delete();
        }
    }
}
