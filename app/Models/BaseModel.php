<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use App\Scopes\SortScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class BaseModel
 * @package App\Models
 * @mixin \Eloquent
 */
abstract class BaseModel extends Model
{

    public static function booted(): void
    {
        parent::booted();

        static::addGlobalScope(new ActiveScope());
        static::addGlobalScope(new SortScope());
    }

    /**
     * Для полиморфных связей переопределяем, что будет писатсья в *_type колонке
     * https://laravel.com/docs/9.x/eloquent-relationships#custom-polymorphic-types
     *
     * @return \Illuminate\Support\Stringable|string
     */
    public function getMorphClass()
    {
        return Str::of(static::class)->classBasename()->kebab()->value();
    }

    public function hasActiveFlag(): bool
    {
        return true;
    }

    public function hasSortFlag(): bool
    {
        return true;
    }

    public static function getTableName(): string
    {
        return static::make()->getTable();
    }

    public static function column(string $columnName): string
    {
        $table = static::getTableName();

        return $table . '.' . $columnName;
    }
}
