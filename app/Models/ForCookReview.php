<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\ForCookReview
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $active
 * @property string $name
 * @property string $position
 * @property string $text
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview query()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookReview whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ForCookReview extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];

    public function getImage()
    {
        $image = $this->attachment;

        return $image && !$image->isEmpty() && $image[0] ? $image[0]?->getImage()->resize('for-cooks.review')->getData() : null;
    }
}
