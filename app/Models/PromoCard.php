<?php

namespace App\Models;

use App\Helpers\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\PromoCard
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $active
 * @property int|null $named_sort
 * @property string $name
 * @property string|null $preview_text
 * @property string|null $button_text
 * @property string|null $price
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard whereButtonText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard whereNamedSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard wherePreviewText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard filters(?mixed $kit = null, ?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|PromoCard filtersApplySelection($class)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DishCategory[] $categories
 * @property-read int|null $categories_count
 * @method static Builder|PromoCard whereCategories(array $value)
 * @property string|null $link
 * @method static Builder|PromoCard whereLink($value)
 * @property int $sort
 * @method static Builder|PromoCard whereSort($value)
 */
class PromoCard extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;

    protected $allowedFilters = [
        'active',
        'sort',
        'show_on_main_category',
    ];

    protected $allowedSorts = [
        'active',
        'sort',
    ];

    protected $fillable = [
        'active',
        'name',
        'named_sort',
        'preview_text',
        'button_text',
        'price',
        'link',
        'sort',
        'show_on_main_category',
    ];

    public function hasActiveFlag(): bool
    {
        return true;
    }

    public function categories(): BelongsToMany {
        return $this->belongsToMany(DishCategory::class, 'promo_card_categories');
    }

    public function hasSortFlag(): bool
    {
        return true ;
    }

    public function scopeWhereCategories(Builder $query, array $value): Builder
    {
        $query->with('categories');

        if (!empty($value)) {
            $query->whereHas('categories', fn($q) => $q->whereIn('dish_category_id', $value));
        }

        return $query;
    }

    public function getImage(): ?Image
    {
        $image = $this?->attachment->sort()->first();

        return !empty($image) ? $image->getImage() : null;
    }
}
