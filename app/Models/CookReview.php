<?php

namespace App\Models;

use App\Helpers\BaseHelper;
use App\Mail\SimpleMail;
use App\Scopes\IdDescSortScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\CookReview
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property int $user_id
 * @property int $cook_id
 * @property int $rating
 * @property string $text
 * @property string $visible_name
 * @method static Builder|CookReview newModelQuery()
 * @method static Builder|CookReview newQuery()
 * @method static Builder|CookReview query()
 * @method static Builder|CookReview whereActive($value)
 * @method static Builder|CookReview whereCookId($value)
 * @method static Builder|CookReview whereCreatedAt($value)
 * @method static Builder|CookReview whereId($value)
 * @method static Builder|CookReview whereRating($value)
 * @method static Builder|CookReview whereText($value)
 * @method static Builder|CookReview whereUpdatedAt($value)
 * @method static Builder|CookReview whereUserId($value)
 * @method static Builder|CookReview whereVisibleName($value)
 * @mixin \Eloquent
 * @method static Builder|CookReview defaultSort(string $column, string $direction = 'asc')
 * @method static Builder|CookReview filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static Builder|CookReview filtersApply(iterable $filters = [])
 * @method static Builder|CookReview filtersApplySelection($selection)
 * @property-read \App\Models\Cook $cook
 * @property-read \App\Models\User $user
 */
class CookReview extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;

    public const PAGE_SIZE = 5;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'id',
        'user_id',
        'cook_id',
        'rating',
        'active',
    ];

    protected $allowedSorts = [
        'id',
        'created_at',
        'updated_at',
        'user_id',
        'cook_id',
        'rating',
        'active',
    ];

    public function hasActiveFlag(): bool
    {
        return true;
    }

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cook()
    {
        return $this->belongsTo(Cook::class);
    }

    protected static function boot()
    {
        parent::boot();

        // Пересчитать рейтинг повара при измении его отзывов
        static::saved(function (CookReview $review) {
            $cookTable = (new Cook)->getTable();
            static::recalcCookRating(["$cookTable.id" => $review->cook_id]);
        });
        static::deleted(function (CookReview $review) {
            $cookTable = (new Cook)->getTable();
            static::recalcCookRating(["$cookTable.id" => $review->cook_id]);
        });
        static::created(function(CookReview $review) {
            static::sendNewReviewNotification($review);
        });
    }

    protected static function booted()
    {
        parent::booted();
        static::addGlobalScope(new IdDescSortScope());
    }

    /**
     * Пересчитать рейтинг всех поваров (либо только указанных) по их отзывам
     * @return void
     */
    public static function recalcCookRating($filter = []): void
    {
        $cookTable = (new Cook)->getTable();
        $reviewTable = (new static)->getTable();

        $query = Cook::query()
            ->select([
                "$cookTable.id",
                "$cookTable.rating",
                "$cookTable.reviews_count",
                DB::raw("avg($reviewTable.rating) as avg_rating"),
                DB::raw("count($reviewTable.id) as counted_reviews"),
            ])
            ->leftJoin($reviewTable, "$reviewTable.cook_id", '=', "$cookTable.id")
            ->where(function (Builder $builder) use ($reviewTable) {
                return $builder
                    ->where("$reviewTable.active", 1)
                    ->orWhere("$reviewTable.cook_id", null);
            })
            ->where("$cookTable.verified", 1)
            ->groupBy("$cookTable.id");

        if (!empty($filter)) {
            $query->where($filter);
        }

        $cooksWithRating = $query->get();

        $precision = 100;
        foreach ($cooksWithRating as $cook) {
            // Считаем изменение рейтинга с точностью до сотых
            $currentRating = (int)round($cook->rating * $precision);
            $newRating = (int)round((float)$cook->avg_rating * $precision);
            if ($currentRating != $newRating || $cook->reviews_count != $cook->counted_reviews) {
                $cook->rating = (float)$cook->avg_rating;
                $cook->reviews_count = (int)$cook->counted_reviews;
                $cook->save();
            }
        }
    }

    /**
     * Отправка уведомления админу о новом отзыве
     * @param CookReview $review
     * @return void
     */
    protected static function sendNewReviewNotification(CookReview $review): void
    {
        $paragraphs = [
            'Дата: ' . $review->created_at->format('Y.m.d в H:i'),
            'От пользователя: ' . $review->user->getFullName(),
            'На повара: ' . $review->cook->user->getFullName(),
            'Рейтинг: ' . $review->rating,
            'Текст: ',
        ];

        $mail = new SimpleMail('new-review', [
            'header'    => 'Отзыв на повара ' . $review->cook->user->getFullName(),
            'paragraph' => $paragraphs,
            'text'      => $review->text,
            'url'       => route('platform.resource.edit', ['resource' => 'cook-review-resources', 'id' => $review->id]
            ),
        ]);

        $mail->setType('html');
        $mail->subject('Пользователь оставил отзыв на повара');
        BaseHelper::sendEmailToAdmins($mail);
    }

    public static function getCookReviews(int $cookId, int $page = 1): Collection
    {
        return static::query()
            ->orderBy('created_at', 'desc')
            ->where('cook_id', $cookId)
            ->where('active', 1)
            ->with('user')
            ->with('user.attachment')
            ->limit(CookReview::PAGE_SIZE)
            ->offset(CookReview::PAGE_SIZE * ($page - 1))
            ->get();
    }

    public static function getCookReviewsCount(int $cookId): int
    {
        $reviewsCount = CookReview::query()
            ->where('cook_id', $cookId)
            ->where('active', 1)
            ->select(DB::raw('count(*) as total'))
            ->get()
            ->first();

        return (int)$reviewsCount->total;
    }
}
