<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DeliveryType
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $code
 * @property string $name
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType query()
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DeliveryType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DeliveryType extends Model
{
    use HasFactory;
}
