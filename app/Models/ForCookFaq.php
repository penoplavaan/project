<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\ForCookFaq
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $active
 * @property string $title
 * @property string $text
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq query()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFaq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ForCookFaq extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];
}
