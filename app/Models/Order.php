<?php

namespace App\Models;

use App\Dto\Basket\BasketByCook;
use App\Dto\Basket\BasketRow;
use App\Jobs\SendNewOrderEmailToAdmin;
use App\Jobs\SendNewOrderSmsToCook;
use App\Scopes\IdDescSortScope;
use App\Services\OrderService;
use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Order
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $message_id
 * @property int $user_id
 * @property int $cook_id
 * @property int $sum
 * @property-read \App\Models\Cook $cook
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Dish[] $dishes
 * @property-read int|null $dishes_count
 * @property-read \App\Models\ChatMessage $message
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderToDishes[] $orderToDishes
 * @property-read int|null $order_to_dishes_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Order filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Order filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Order filtersApplySelection($selection)
 * @property int $pay_system_id
 * @property int $delivery_type_id
 * @property int $payed
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaySystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePayed($value)
 * @property-read \App\Models\DeliveryType $deliveryType
 * @property-read \App\Models\PaySystem $paySystem
 * @property string $client_name
 * @property string $client_phone
 * @property string|null $recipient_name
 * @property string|null $recipient_phone
 * @property string|null $delivery_address
 * @property string|null $delivery_date
 * @property string|null $delivery_time
 * @property string|null $comment
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereClientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereClientPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereRecipientName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereRecipientPhone($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderPayment[] $orderPayments
 * @property-read int|null $order_payments_count
 */
class Order extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use CacheTrait;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'id',
        'user_id',
        'cook_id',
        'client_phone',
        'sum',
        'payed',
    ];

    protected $allowedSorts = [
        'id',
        'created_at',
        'updated_at',
        'user_id',
        'cook_id',
        'client_phone',
        'sum',
        'payed',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function (Order $order) {
            if ($order->isDirty('payed')) {
                // отправка уведомлений - только после оплаты заказа
                static::sendOrderNotificationsOnPayed($order->id);
            }
        });

        static::created(function (Order $order) {
            if ($order->payed == 1) {
                // отправка уведомлений - если заказ сразу оплачен
                static::sendOrderNotificationsOnPayed($order->id);
            }

            if ($order->paySystem->code === 'cook-card') {
                // отправка уведомлений - если заказ не оплачен
                static::sendUnpayedNewOrderNotiification($order->id);
            }
        });
    }

    protected static function booted()
    {
        parent::booted();
        static::addGlobalScope(new IdDescSortScope());
    }

    protected static function sendOrderNotificationsOnPayed(int $orderId) {
        SendNewOrderEmailToAdmin::dispatch($orderId);
        SendNewOrderSmsToCook::dispatch($orderId);

        /** @var OrderService $orderService */
        $orderService = app()->make(OrderService::class);
        $orderService->sendNewOrderChatNotification($orderId);
    }

    protected static function sendUnpayedNewOrderNotiification(int $orderId) {
        SendNewOrderEmailToAdmin::dispatch($orderId);
        SendNewOrderSmsToCook::dispatch($orderId);

        /** @var OrderService $orderService */
        $orderService = app()->make(OrderService::class);
        $orderService->sendNewOrderChatNotification($orderId);
    }

    public function hasActiveFlag(): bool
    {
        return false;
    }

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function orderToDishes(): HasMany
    {
        return $this->hasMany(OrderToDishes::class);
    }

    public function dishes(): BelongsToMany
    {
        return $this->belongsToMany(Dish::class, 'order_to_dishes', 'order_id', 'dish_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function cook(): BelongsTo
    {
        return $this->belongsTo(Cook::class);
    }

    public function orderPayments()
    {
        return $this->hasMany(OrderPayment::class);
    }

    public function message(): BelongsTo
    {
        return $this->belongsTo(ChatMessage::class, 'message_id');
    }

    public function deliveryType(): BelongsTo
    {
        return $this->belongsTo(DeliveryType::class);
    }

    public function paySystem(): BelongsTo
    {
        return $this->belongsTo(PaySystem::class);
    }

    public function getSuccessUrl() {
        /** @var OrderService $orderService */
        $orderService = app()->make(OrderService::class);
        $successUrl = route('order.success', ['orderId' => $this->id]);
        return $successUrl . '?hash=' . $orderService->getHash($this->id);
    }

    public function getBasket(): BasketByCook
    {
        $rows = $this
            ->orderToDishes
            ->map(fn (OrderToDishes $el) => new BasketRow($el->dish, $el->count))
            ->toArray();

        return new BasketByCook($this->cook, $rows);
    }
}
