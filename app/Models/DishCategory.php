<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\DishCategory
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $active
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereActive($value)
 * @property string $slug
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory filtersApplySelection($selection)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cook[] $cooks
 * @property int|null $sort
 * @property-read int|null $cooks_count
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereSort($value)
 * @property string|null $h1
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereH1($value)
 * @property string|null $menu_title
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereMenuTitle($value)
 * @property string|null $meta_title
 * @method static \Illuminate\Database\Eloquent\Builder|DishCategory whereMetaTitle($value)
 */
class DishCategory extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use CacheTrait;

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];

    public function getUrl(): string
    {
        return route('menu.dish.category', ['dishCategory' => $this->slug]);
    }

    /**
     * Находися на странице из пункта меню или дочерней
     */
    public function isCurrent(): bool
    {
        return str_starts_with(request()->getRequestUri(), $this->getUrl());
    }

    public function cooks(): BelongsToMany
    {
        return $this->belongsToMany(Cook::class, 'cook_category');
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getMetaTitle(): string
    {
        return $this->meta_title ?: $this->title;
    }

    public function getH1(): string
    {
        return $this->h1 ?: $this->title;
    }

    public function getMenuTitle(): string
    {
        return $this->menu_title ?: $this->title;
    }

    public function getDescription(): string
    {
        return $this->description ?: $this->title;
    }

    public function getForMain() {
        return Cache::tags([
            static::getCacheTag()
        ])->remember('dish_category_with_count_for_main', static::getCacheTime(), function () {
            $main = $this->getTable();
            $dish = (new Dish())->getTable();
            $cook = (new Cook())->getTable();

            $categories = $this->query()
                ->select("$main.*", DB::raw('count(*) as total'))
                ->leftJoin($dish, "$main.id", '=', "$dish.category_id")
                ->leftJoin($cook, "$cook.id", '=', "$dish.cook_id")
                ->where("$main.active", 1)
                ->where("$dish.active", 1)
                ->where("$cook.active", 1)
                ->where("$cook.verified", 1)
                ->groupBy("$main.id")
                ->get();

            $dishAdditionalCategories = $this->getDishAdditionalCategories($dish, $cook, $this->getTable());

            //todo унести в ресурс
            $dishCategories = [];
            /** @var $category static */
            foreach ($categories as $key => $category) {
                $additionalCategory = $dishAdditionalCategories[$category->id] ?? null;
                if ($additionalCategory) {
                    $total = $category->total + ($additionalCategory['total'] ?? 0);
                }

                $dishCategories[] = [
                    'id' => $key,
                    'title' => $category->getMenuTitle(),
                    'total' => ($total ?? $category->total) . ' блюд' . \App\Helpers\BaseHelper::getWordForm(($total ?? $category->total), ['о', 'а', '']),
                    'url' => $category->getUrl(),
                ];
            }

            return $dishCategories;
        });
    }

    /**
     * Подсчёт количества товаров по разделам привязанных через доп. категории через админку
     * (чтобы просуммировать со статистикой привязок через основное поле)
     * @param $dishTable
     * @param $cookTable
     * @param $dishCategoryTable
     * @return \Illuminate\Support\Collection
     */
    public function getDishAdditionalCategories($dishTable, $cookTable, $dishCategoryTable) {
        $model = (new DishesToAdditionalCategories());
        $dishToAdditionalCategoryTable = $model->getTable();

        return $model->query()
            ->select("$dishToAdditionalCategoryTable.category_id", DB::raw('count(*) as total'))
            ->leftJoin($dishTable, "$dishToAdditionalCategoryTable.dish_id", '=', "$dishTable.id")
            ->leftJoin($cookTable, "$dishTable.cook_id", '=', "$cookTable.id")
            ->where("$dishTable.active", '=', '1')
            ->where("$cookTable.active", '=', '1')
            ->where("$cookTable.verified", '=', '1')
            ->whereRaw("$dishTable.category_id <> $dishToAdditionalCategoryTable.category_id") // Исключаем из подсчёта те записи, у которых доп. категория совпадает с основной
            ->groupBy("$dishToAdditionalCategoryTable.category_id")
            ->get()
            ->keyBy('category_id');
    }
}
