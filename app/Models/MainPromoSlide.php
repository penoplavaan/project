<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\MainPromoSlide
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $dish_id
 * @property string|null $url
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Dish|null $dish
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide filtersApplySelection($selection)
 * @property int|null $active
 * @method static \Illuminate\Database\Eloquent\Builder|MainPromoSlide whereActive($value)
 */
class MainPromoSlide extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;
    use CacheTrait;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }

    public function getPicture(): ?array
    {
        $image = $this->attachment[0] ?? $this->dish->attachment[0] ?? '';
        if (empty($image)) {
            return null;
        }

        return  $image->getImage()->resize('index.promo')->getData();
    }

    /**
     * Выборка слайдов для главной со всеми фильтрами
     */
    public function getForMainPage()
    {
        return Cache::tags([static::getCacheTag()])
            ->remember(
                'main_promo_slides',
                static::getCacheTime(),
                fn() => $this->doGetForMainPage()
        );
    }

    public function doGetForMainPage() {
        $main = $this->getTable();
        $dish = (new Dish())->getTable();
        $cook = (new Cook())->getTable();

        return $this->query()
            ->select("$main.*")
            ->with('dish.cook.speciality')
            ->with('dish.cook.user')
            ->with('dish.cook.user.attachment')
            ->with('dish.attachment')
            ->with('attachment')
            ->leftJoin($dish, "$main.dish_id", '=', "$dish.id")
            ->leftJoin($cook, "$cook.id", '=', "$dish.cook_id")
            ->where("$dish.active", 1)
            ->where("$cook.active", 1)
            ->where("$cook.verified", 1)
            ->orderBy("$main.sort")
            ->get();
    }
}
