<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\MainSecondPromo
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property int $sort
 * @property int|null $cook_id
 * @property string|null $url
 * @property string|null $name
 * @property string|null $price
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereCookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereUrl($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Cook|null $cook
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSecondPromo wherePrice($value)
 */
class MainSecondPromo extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;
    use CacheTrait;

    protected $guarded = ['id'];

    public function cook()
    {
        return $this->belongsTo(Cook::class);
    }

    /**
     * Выборка слайдов для главной со всеми фильтрами
     */
    public function getForMainPage()
    {
        return Cache::tags([static::getCacheTag()])
            ->remember(
                'main_second_promo',
                static::getCacheTime(),
                fn() => $this->doGetForMainPage()
            );
    }

    public function doGetForMainPage() {
        $main = $this->getTable();
        $cook = (new Cook())->getTable();

        return $this->query()
            ->select("$main.*")
            ->with('cook.user.attachment')
            ->with('cook.speciality')
            ->with('attachment')
            ->leftJoin($cook, "$cook.id", '=', "$main.cook_id")
            ->where("$cook.active", 1)
            ->where("$cook.verified", 1)
            ->orderBy("$main.sort")
            ->get();
    }
}
