<?php

namespace App\Models;

use App\Helpers\BaseHelper;
use App\Helpers\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\Dish
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $cook_id
 * @property int $category_id
 * @property string $name
 * @property int $price
 * @property string $preview_text
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|Dish newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dish newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dish query()
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\DishCategory|null $category
 * @property-read \App\Models\Cook|null $cook
 * @method static \Illuminate\Database\Eloquent\Builder|Dish defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Dish filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Dish filtersApplySelection($selection)
 * @property string|null $weight
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereSize($value)
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereActive($value)
 * @property string|null $storage_conditions
 * @property string|null $calories
 * @property string|null $protein
 * @property string|null $fats
 * @property string|null $carbohydrates
 * @property string $quantity
 * @property string|null $cooking_time
 * @property string|null $package
 * @property Tag|null $tags
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCalories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCarbohydrates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereCookingTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereFats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish wherePackage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish wherePreviewText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereProtein($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereStorageConditions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dish whereWeight($value)
 * @property-read int|null $tags_count
 * @property-read string $orchid_rel_cook_name
 * @method static Builder|Dish withCookUserModel()
 * @method static Builder|Dish active()
 * @property int $named_sort
 * @method static Builder|Dish whereNamedSort($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Label[] $labels
 * @property-read int|null $labels_count
 * @property string|null $delivery
 * @method static Builder|Dish whereDelivery($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DishCategory[] $additionalCategories
 * @property-read int|null $additional_categories_count
 */
class Dish extends BaseModel
{
    use HasFactory;
    use Filterable;
    use AsSource;
    use Attachable;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'name',
        'cook_id',
        'category_id',
        'price',
        'active',
    ];

    protected $allowedSorts = [
        'active',
        'name',
        'price',
        'created_at',
    ];

    public function hasActiveFlag(): bool
    {
        return true;
    }

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function scopeActive(Builder $query)
    {
        $dish = $this->getTable();
        $cook = (new Cook())->getTable();

        return $query
            ->select(["$dish.*"])
            ->leftJoin($cook, "$dish.cook_id", '=', "$cook.id")
            ->where("$dish.active", 1)
            ->where("$cook.active", 1)
            ->where("$cook.verified", 1);
    }

    public function cook()
    {
        return $this->belongsTo(Cook::class);
    }

    public function category()
    {
        return $this->belongsTo(DishCategory::class, 'category_id');
    }

    public function additionalCategories(): BelongsToMany {
        return $this->belongsToMany(DishCategory::class, 'dishes_to_additional_categories', 'dish_id', 'category_id');
    }

    public function tags(): BelongsToMany {
        return $this->belongsToMany(Tag::class, 'dishes_to_tags', 'dish_id', 'tag_id');
    }

    public function labels(): BelongsToMany {
        return $this->belongsToMany(Label::class, 'dish_labels');
    }

    public function getUrl()
    {
        return route('menu.dish.detail', ['dish' => $this->id]);
    }

    public function canBuy(): bool
    {
        return $this->active && $this->cook && $this->cook->isVisible();
    }

    public function getImage(): ?Image
    {
        $image = $this?->attachment->sort()->first();

        return !empty($image) ? $image->getImage() : null;
    }

    public function getOrchidRelCookNameAttribute(): string
    {
        return $this->name . '[' . $this->id . '] (' . ($this->cook->user?->getFullName() ?? '') . ', user: ' . $this->cook->user_id . ', cook: ' . $this->cook_id . ')';
    }

    public function getShortPeviewText() {
        $text = strip_tags($this->preview_text ?: '');
        return BaseHelper::truncateString($text, 130);
    }
}
