<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Chat
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $active
 * @property int $from_user_id
 * @property int $to_user_id
 * @method static \Illuminate\Database\Eloquent\Builder|Chat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Chat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Chat query()
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ChatMessage[] $chatMessage
 * @property-read int|null $chat_message_count
 * @property-read \App\Models\User $fromUser
 * @property-read \App\Models\User $toUser
 * @property int|null $from_last_read_id
 * @property int|null $to_last_read_id
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereFromLastReadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereToLastReadId($value)
 * @property int $from_email_sent
 * @property int $to_email_sent
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereFromEmailSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Chat whereToEmailSent($value)
 */
class Chat extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function fromUser()
    {
        return $this->belongsTo(User::class);
    }

    public function toUser()
    {
        return $this->belongsTo(User::class);
    }

    public function chatMessage()
    {
        return $this->hasMany(ChatMessage::class);
    }

    /**
     * Список чатов пользователя либо 1 конкретный чат (если он пользователь в нём состоит)
     * @param $userId
     * @param $chatId
     * @return Chat[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getUserChats($userId, $toUserId = null)
    {
        $query = static::query();

        if ($toUserId) {
            $query->where(function($query) use ($userId, $toUserId) {
                $query->where('from_user_id', $userId)
                    ->where('to_user_id', $toUserId);
            })->orWhere(function($query) use ($userId, $toUserId) {
                $query->where('from_user_id', $toUserId)
                    ->where('to_user_id', $userId);
            });
        } else {
            $query->where(function($query) use ($userId) {
                    $query->where('from_user_id', $userId)
                        ->orWhere('to_user_id', $userId);
                });
        }

        return $query->get();
    }

    /**
     * загрузить последние сообщения по спику чатов, по 1 сообщению в чате
     * @param array $chatIdList
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getLastMessages(array $chatIdList = [])
    {
        if (empty($chatIdList)) return null;

        $chatMessages = (new ChatMessage())->getTable();

        $internalQuery = DB::query()
            ->from($chatMessages)
            ->select([
                "$chatMessages.*",
                DB::raw('row_number() over (partition by chat_id order by created_at desc) i')
            ]);

        $result = DB::query()
            ->select(['windowTable.*'])
            ->fromSub($internalQuery, 'windowTable')
            ->where('i', '<=', 1)
            ->whereIn('chat_id', $chatIdList)
            ->get();

        $result = $result->map(fn($raw) => (array)$raw);
        $messages = ChatMessage::hydrate($result->toArray());

        return $messages;
    }

    /**
     * Обновить ID последнего прочитанного сообщения в чате для пользователя
     * Заодно обновляет и ID оповещений
     * @param $userId
     * @param $lastMessage
     * @return void
     */
    public function updateLastMessage($userId, $lastMessage): void
    {
        if (!$lastMessage || !$lastMessage->id) return;

        if ($this->from_user_id == $userId) {
            if ($this->from_last_read_id < $lastMessage->id) {
                $this->from_last_read_id = $lastMessage->id;
                $this->from_email_sent = 0;
                $this->save();
                $this->refresh();
            }
        } else {
            if ($this->to_last_read_id < $lastMessage->id) {
                $this->to_last_read_id = $lastMessage->id;
                $this->to_email_sent = 0;
                $this->save();
                $this->refresh();
            }
        }
    }

    public function getOtherUserId($firstUserId): int
    {
        return ($this->from_user_id == $firstUserId) ? $this->to_user_id : $this->from_user_id;
    }

    public function getLastReadId($firstUserId): ?int
    {
        return ($this->from_user_id == $firstUserId) ? $this->from_last_read_id : $this->to_last_read_id;
    }
}
