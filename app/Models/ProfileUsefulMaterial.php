<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\ProfileUsefulMaterial
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $active
 * @property string|null $title
 * @property string|null $text
 * @property string|null $anchor
 * @property-read Collection|Attachment[] $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|ProfileUsefulMaterial whereAnchor($value)
 */
class ProfileUsefulMaterial extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Attachable;
    use Filterable;

    protected $guarded = ['id'];

    protected array $allowedFilters = [
        'id',
        'name',
    ];

    protected array $allowedSorts = [
        'id',
        'name',
        'created_at',
        'updated_at',
    ];
}
