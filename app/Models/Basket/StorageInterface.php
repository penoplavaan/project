<?php
declare(strict_types=1);

namespace App\Models\Basket;

use App\Dto\Basket\BasketByCook;
use App\Models\Dish;
use Illuminate\Support\Collection;

interface StorageInterface {

    /**
     * @return Collection<int, BasketByCook>
     */
    public function get(): Collection;

    public function add(Dish $dish, int $count): bool;

    public function setCount(Dish $dish, int $count): bool;

    public function remove(int $dishId): bool;

    /**
     * Очистить корзину, по повару (если задан cookId) или полностью
     * @param int $cookId
     * @return mixed
     */
    public function clear(int $cookId = 0): bool;
}
