<?php

declare(strict_types=1);

namespace App\Models\Basket;

use App\Dto\Basket\BasketByCook;
use App\Dto\Basket\BasketRow;
use App\Models\Cook;
use App\Models\Dish;
use App\Services\BasketService;
use Illuminate\Support\Collection;

/**
 * Хранилице корзины в куках, для неавторизованного
 */
class CookieStorage implements StorageInterface
{
    protected array $storage = [];

    const COOKIE_KEY = 'basket';
    const COOKIE_TIME = 60 * 24 * 30; // 1 month

    public function get(): Collection
    {
        $exist = $this->internalGet();
        $dishIds = array_keys($exist);

        $list = Dish::query()->whereIn('id', $dishIds)->get();

        $byCooks = $list->groupBy('cook_id');
        $cookIds = $byCooks->keys();
        $cookRef = Cook::query()->whereIn('id', $cookIds->all())
            ->with('user')
            ->get()
            ->keyBy('id');

        $result = [];
        foreach ($byCooks as $cookId => $dishes) {
            $cook = $cookRef[$cookId] ?? null;
            if (!$cook) {
                // Повар не найден - сразу очищаем корзину по нему
                $this->clear($cookId);
                continue;
            }

            $rows = [];
            foreach ($dishes as $dish) {
                if (!$dish->canBuy()) {
                    // Нет товара или его нельзя купить - удаляем из корзины
                    $this->remove($dish->id);
                    continue;
                }

                $rows[] = new BasketRow(
                    $dish,
                    $exist[$dish->id] ?? 1,
                );
            }

            $result[$cook->id] = new BasketByCook($cook, $rows);
        }

        return collect($result);
    }

    protected function internalGet(): array
    {
        static $existRaw;
        if ($existRaw === null) {
            $existRaw = request()->cookie(static::COOKIE_KEY) ?? '';
            $this->storage = (array)(json_decode($existRaw) ?? []);
        }

        return $this->storage;
    }

    protected function internalSave(array $exist)
    {
        $this->storage = $exist;
        \Cookie::queue(static::COOKIE_KEY, json_encode($exist), static::COOKIE_TIME);
    }

    public function add(Dish $dish, int $count): bool
    {
        if ($count < 1 || $count > BasketService::MAX_IN_BASKET) {
            $count = 1;
        }

        // Находим строку этого блюда в корзине пользвоателя
        $exist = $this->internalGet();

        if (isset($exist[$dish->id])) {
            $exist[$dish->id] += $count;
            if ($exist[$dish->id] > BasketService::MAX_IN_BASKET) {
                $exist[$dish->id] = BasketService::MAX_IN_BASKET;
            }
        } else {
            $exist[$dish->id] = $count;
        }

        $this->internalSave($exist);

        return true;
    }

    public function setCount(Dish $dish, int $count): bool
    {
        if ($count < 1 || $count > BasketService::MAX_IN_BASKET) {
            $count = 1;
        }

        $exist = $this->internalGet();

        if (isset($exist[$dish->id])) {
            $exist[$dish->id] = $count;
            $this->internalSave($exist);
            return true;
        } else {
            return false;
        }
    }

    public function remove(int $dishId): bool
    {
        $exist = $this->internalGet();

        if (isset($exist[$dishId])) {
            unset($exist[$dishId]);
            $this->internalSave($exist);
            return true;
        } else {
            return false;
        }
    }

    public function clear(int $cookId = 0): bool
    {
        $exist = $this->internalGet();
        $dishIds = array_keys($exist);

        $query = Dish::query()->whereIn('id', $dishIds);

        if ($cookId) {
            $query->where("cook_id", $cookId);
        }

        $dishByCook = $query->get()->keyBy('id');

        foreach ($exist as $dishId => $count) {
            if (isset($dishByCook[$dishId])) {
                unset($exist[$dishId]);
            }
        }

        $this->internalSave($exist);

        return true;
    }

}
