<?php

declare(strict_types=1);

namespace App\Models\Basket;

use App\Dto\Basket\BasketByCook;
use App\Dto\Basket\BasketRow;
use App\Models\Basket;
use App\Models\Cook;
use App\Models\Dish;
use App\Services\BasketService;
use App\Services\UserService;
use Illuminate\Support\Collection;

/**
 * Хранилице корзины в БД
 */
class DbStorage implements StorageInterface
{

    public function __construct(
        protected UserService $userService,
    ) {
    }

    public function get(): Collection
    {
        $user = $this->userService->current();

        $dishTable = (new Dish())->getTable();
        $basketTable = (new Basket())->getTable();

        $list = Basket::query()->where('user_id', $user->id)
            ->leftJoin($dishTable, "$basketTable.dish_id", "$dishTable.id")
            ->select(["$basketTable.*", "$dishTable.cook_id"])
            ->with('dish.cook')
            ->get();

        $byCooks = $list->groupBy('cook_id');
        $cookIds = $byCooks->keys();
        $cookRef = Cook::query()->whereIn('id', $cookIds->all())
            ->with('user')
            ->get()
            ->keyBy('id');

        $result = [];
        foreach ($byCooks as $cookId => $basketRows) {
            $cook = $cookRef[$cookId] ?? null;
            if (!$cook) {
                // Повар не найден - сразу очищаем корзину по нему
                $this->clear($cookId);
                continue;
            }

            $rows = [];
            /** @var Basket $row */
            foreach ($basketRows as $row) {
                if (!$row->dish || !$row->dish->canBuy()) {
                    // Нет товара или его нельзя купить - удаляем из корзины
                    $this->remove($row->id);
                    continue;
                }

                $rows[] = new BasketRow(
                    $row->dish,
                    $row->count,
                );
            }

            $result[$cook->id] = new BasketByCook($cook, $rows);
        }

        return collect($result);
    }


    /**
     * @throws \Exception
     */
    public function add(Dish $dish, int $count): bool
    {
        $user = $this->userService->current();

        if ($count < 1 || $count > BasketService::MAX_IN_BASKET) {
            $count = 1;
        }

        if ($dish->cook->user_id === $user->id) {
            throw new \Exception('Вы не можете заказывать свои же блюда');
        }

        // Находим строку этого блюда в корзине пользвоателя
        $exist = Basket::query()
            ->where('user_id', $user->id)
            ->where('dish_id', $dish->id)
            ->first();

        if ($exist) {
            $exist->count += $count;
            if ($exist->count > BasketService::MAX_IN_BASKET) {
                $exist->count = BasketService::MAX_IN_BASKET;
            }
            $exist->save();
        } else {
            (new Basket([
                'user_id' => $user->id,
                'dish_id' => $dish->id,
                'count'   => $count,
            ]))->save();
        }

        return true;
    }

    public function setCount(Dish $dish, int $count): bool
    {
        $user = $this->userService->current();
        if ($count < 1 || $count > BasketService::MAX_IN_BASKET) {
            $count = 1;
        }

        $exist = Basket::query()
            ->where('user_id', $user->id)
            ->where('dish_id', $dish->id)
            ->first();

        if ($exist) {
            $exist->count = $count;
            $exist->save();
            return true;
        } else {
            return false;
        }
    }

    public function remove(int $dishId): bool
    {
        $user = $this->userService->current();
        $exist = Basket::query()
            ->where('user_id', $user->id)
            ->where('dish_id', $dishId)
            ->get()
            ->first();

        if (!$exist) {
            return false;
        } else {
            $exist->delete();
            return true;
        }
    }

    public function clear(int $cookId = 0): bool
    {
        $user = $this->userService->current();
        $query = Basket::query()->where('user_id', $user->id);

        if ($cookId) {
            $dishTable = (new Dish())->getTable();
            $basketTable = (new Basket())->getTable();
            $query->leftJoin($dishTable, "$basketTable.dish_id", "$dishTable.id");
            $query->select(["$basketTable.*", "$dishTable.cook_id"]);
            $query->where("$dishTable.cook_id", $cookId);
        }

        $exist = $query->get();

        foreach ($exist as $row) {
            $row->delete();
        }

        return ($exist->count() > 0);
    }
}
