<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\ForCookFeature
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $active
 * @property string $title
 * @property string $text
 * @property-read Attachment[]|Collection $attachment
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature query()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookFeature whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ForCookFeature extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Attachable;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];

    public function getIcon() {
        $image = $this->attachment;

        return $image && !$image->isEmpty() && $image[0] ? $image[0]->getImage()->getUrl() : null;
    }
}
