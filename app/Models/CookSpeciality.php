<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\CookSpeciality
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality query()
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $active
 * @property int|null $sort
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CookSpeciality whereSort($value)
 */
class CookSpeciality extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];
}
