<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\ForCookHowWork
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $sort
 * @property int $active
 * @property string $title
 * @property string $text
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork query()
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ForCookHowWork whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ForCookHowWork extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Filterable;

    protected $guarded = ['id'];

    protected $allowedFilters = [
        'active',
        'sort',
    ];

    protected $allowedSorts = [
        'sort'
    ];
}
