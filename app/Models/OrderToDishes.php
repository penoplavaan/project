<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderToDishes
 *
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes query()
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $count
 * @property int $price_one
 * @property string $name
 * @property int $order_id
 * @property int $dish_id
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes whereDishId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes wherePriceOne($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderToDishes whereUpdatedAt($value)
 * @property-read \App\Models\Dish $dish
 * @property-read \App\Models\Order $order
 */
class OrderToDishes extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }

}
