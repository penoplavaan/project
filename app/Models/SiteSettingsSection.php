<?php

namespace App\Models;

use App\Traits\Model\CacheTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * App\Models\SiteSettingsSection
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name Название
 * @property string $code Код элемента
 * @property string|null $parent_section_code
 * @property int|null $parent_section_id
 * @property-read Attachment[]|Collection $attachment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SiteSetting[] $settings
 * @property-read int|null $settings_count
 * @property-read SiteSettingsSection|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection defaultSort(string $column, string $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection filters(?\Orchid\Filters\HttpFilter $httpFilter = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection filtersApply(iterable $filters = [])
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection filtersApplySelection($selection)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection query()
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection whereParentSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SiteSettingsSection whereParentSectionCode($value)
 * @mixin \Eloquent
 */
class SiteSettingsSection extends BaseModel
{
    use HasFactory;
    use AsSource;
    use Attachable;
    use Filterable;
    use CacheTrait;

    protected $fillable = ['code', 'name', 'parent_section_id'];

    public function hasActiveFlag(): bool
    {
        return false;
    }

    public function hasSortFlag(): bool
    {
        return false;
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(static::class, 'parent_section_id');
    }

    public function settings(): HasMany
    {
        return $this->hasMany(SiteSetting::class, 'section_id');
    }
}
