<?php

namespace App\Services;


use App\View\Components\Text\ITextEmbedable;
use App\View\Components\Text\ITextEmbedableCache;
use DOMElement;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\View\Compilers\ComponentTagCompiler;
use Masterminds\HTML5;
use Str;

/**
 * Class TextComponentService
 * @package App\Services
 */
class TextComponentService {
    protected array $components = [];
    protected array $cacheTagsStack = [];
    protected int $deep = 0;

    public function __construct(protected ComponentTagCompiler $componentTagCompiler)
    {
    }

    public function registerComponent(string $componentName): void {
        if (!in_array($componentName, $this->components)) {
            $this->components[] = $componentName;
        }
    }

    public function getComponentList(): array {
        $items = [];

        foreach ($this->components as $componentName) {
            $class = $this->getComponentClass($componentName);

            if (!$class) continue;

            $items[$componentName] = ['name' => $componentName, 'title' => $class::getTitle()];
        }

        return $items;
    }

    public function getComponentParams(string $componentName): ?Collection {
        $className = $this->getComponentClass($componentName);

        return $className ? $className::getParams() : null;
    }

    public function replaceComponents(string $content): string {
        $html = new HTML5();
        $dom = $html->loadHTML($this->htmlHeader() . $content . $this->htmlFooter());

        $components = $dom->getElementsByTagName('section');

        /** @var \DOMElement $component */
        // инкремент не нужен, т.к. $components следит за иходным деревом и при удалении элемента из дерева итератор тоже обновляется
        for ($i = 0; $i < $components->count();) {
            $component = $components->item($i);

            $cssClass = $component->getAttribute('class');

            if (!preg_match_all('/(^|\s)sx-mce-component(\s|$)/i', $cssClass)) continue;

            // Не смогли определить имя компонента, удаляем узел
            $componentName = $component->getAttribute('data-component-name');

            if (!$componentName) {
                $component->parentNode->removeChild($component);
                continue;
            }

            // Не смогли распарсить параметры компонента, удаляем узел
            $componentParams = json_decode($component->getAttribute('data-component-params'), true);

            if (json_last_error() !== JSON_ERROR_NONE || !is_array($componentParams)) {
                $component->parentNode->removeChild($component);
                continue;
            }

            // Не нашли класс компонента, удаляем узел
            $className = $this->getComponentClass($componentName);

            if (!$className) {
                $component->parentNode->removeChild($component);
                continue;
            }

            // kebab-case to camelCase
            $componentParams = $this->prepareComponentParams($componentParams);

            // Пытаемся отрендерить компонент
            try {
                $this->incrementDeep();
                $renderedComponent = $className::renderComponent($componentParams);
                $this->decrementDeep();

                if (is_a($className, ITextEmbedableCache::class, true)) {
                    $this->addCacheTagStackItem($className::getEmbeddableCacheTags());
                }
            } catch (Exception $e) {
                Log::channel('daily')->debug($e->getMessage(), ['className' => $className, 'componentParams' => $componentParams]);
                $renderedComponent = null;
            }

            // Не удалось отрендерить компонент или отрендерилась пустая строка, удаляем узел
//            if (!is_string($renderedComponent) || !mb_strlen($renderedComponent)) {
//                $component->parentNode->removeChild($component);
//                continue;
//            }

            // Создаем новый документ из отрендеренного компонента
            $newHtml = new HTML5();
            $newDom = $newHtml->loadHTML($this->htmlHeader() . $renderedComponent . $this->htmlFooter());
            /** @var DOMElement $newDomBody */
            $newDomBody = $newDom?->getElementsByTagName('body')->item(0);

            if (!$newDomBody) {
                $component->parentNode->removeChild($component);
                continue;
            }

            // Создаем фрагмент документа и переносим в него сождержимое тега body из нового документа
            $newDiv = $dom->createDocumentFragment();

            while ($newDomBody->firstChild) {
                $node = $dom->importNode($newDomBody->firstChild, true);

                if ($node) {
                    $newDiv->appendChild($node);
                }

                $newDomBody->removeChild($newDomBody->firstChild);
            }

            // Заменяем имеющийся узел на новый
            $component->parentNode->replaceChild($newDiv, $component);
        }

        // Переносим содержимое тега body в корень документа
        /** @var DOMElement $domBody */
        $domBody = $dom->getElementsByTagName('body')->item(0);
        while ($domBody->firstChild) {
            $dom->insertBefore($domBody->firstChild);
        }

        // Удаляем узел html
        $dom->removeChild($dom->documentElement);

        // Сохраняем итоговый html в строку
        $outputHtml = $html->saveHTML($dom);

        // Вырезаем doctype
        /** @noinspection PhpUnnecessaryLocalVariableInspection */
        $outputHtml = str_replace($this->doctype(), '', $outputHtml);

        return $outputHtml;
    }

    /**
     * @param string $componentName
     * @return string|null|ITextEmbedable
     */
    protected function getComponentClass(string $componentName): ?string {
        try {
            $className = $this->componentTagCompiler->componentClass($componentName);
        } catch(Exception $e) {
            $className = null;
            Log::channel('daily')->debug($e->getMessage());
        }

        if (is_null($className)) return null;

        $className = '\\' . $className;

        $isComponentClass = class_exists($className) && is_a($className, ITextEmbedable::class, true);

        if (!$isComponentClass) return null;

        return $className;
    }

    protected function htmlHeader(): string {
        return "<?xml version='1.0' encoding='utf-8'?>" . $this->doctype() . "<html lang='ru'><body>";
    }

    protected function htmlFooter(): string {
        return "</body></html>";
    }

    protected function doctype(): string {
        return '<!DOCTYPE html>';
    }

    protected function prepareComponentParams(array $params): array {
        $preparedParams = [];
        foreach ($params as $key => $value) {
            $preparedParams[Str::camel($key)] = $value;
        }
        return $preparedParams;
    }

    /**
     * Метод сохраняет теги отрендереного компонента (если реализует интерфейс TextEmbeddableCache)
     *
     * !!! Использовать аккуратно!!!
     * После окончания рекурсивного рендера необходимо очистить стэк, иначе можно получить лишние теги в другую цепочку компонентов
     *
     * @param array $stackItems
     */
    protected function addCacheTagStackItem(array $stackItems): void {
        $this->cacheTagsStack = array_merge($this->cacheTagsStack, $stackItems);
    }

    public function getClearStackCacheTags(): array {
        $stack = $this->cacheTagsStack;
        $this->clearCacheTagsStack();

        return $stack;
    }

    public function clearCacheTagsStack(): void {
        $this->cacheTagsStack = [];
    }

    /**
     * Текущий уровень вложенности рендера цепочки компонентов
     * 0 = инициатор
     *
     * @return int
     */
    public function getDeep(): int {
        return $this->deep;
    }

    protected function incrementDeep(): int {
        return ++$this->deep;
    }

    protected function decrementDeep(): int {
        return --$this->deep;
    }
}
