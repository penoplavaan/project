<?php

namespace App\Services\Payment;

use App\Helpers\Result;
use App\Models\Order;
use App\Models\OrderPayment;

interface PaymentInterface {

    public function __construct(OrderPayment $paymentModel);

    /**
     * Создание платежа в системе, с которой интегрируемся
     * @param Order $order
     * @return PaymentResult
     */
    public function create(Order $order): PaymentResult;

    /**
     * Получаем ссылку, по которой пользователю нужно перейти для оплаты платежа
     *
     * @return string
     */
    public function confirmationUrl(): string;

    /**
     * Получение информации о платеже
     *
     * @param string $paymentId
     * @return Result
     */
    public function info(string $paymentId): Result;

    /**
     * Повторная попытка оплаты
     *
     * @param Order $order
     * @return PaymentResult
     */
    public function repeat(Order $order): PaymentResult;

    /**
     * Метод для webhook платежных систем.
     * По пришедшим данным платежной системы определяет, успешно ли оплачен заказ
     *
     * @param array $info ассоциативный массив, который приходит от платежного шлюза
     * @return bool
     */
    public function isPayed(array $info): bool;

    /**
     * Валидация для webhook платежных систем.
     * Чтобы однозначно идентифицировать, что запрос пришел от нужной платежки
     *
     * @param array $info ассоциативный массив, который приходит от платежного шлюза
     * @return bool
     */
    public function isValidStatusChange(array $info): bool;


}
