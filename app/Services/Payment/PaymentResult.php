<?php

namespace App\Services\Payment;

use App\Helpers\Result;
use JetBrains\PhpStorm\Pure;

class PaymentResult extends Result {

    protected string $paymentUrl  = '';
    protected string $paymentUuid = '';

    #[Pure]
    public function getData(): array {
        return array_merge(
            [
                'payment_url'  => $this->getPaymentUrl(),
                'payment_uuid' => $this->getPaymentUuid(),
            ],
            $this->data
        );
    }

    /**
     * @return string
     */
    public function getPaymentUrl(): string {
        return $this->paymentUrl;
    }

    /**
     * @param string $paymentUrl
     */
    public function setPaymentUrl(string $paymentUrl): void {
        $this->paymentUrl = $paymentUrl;
    }

    /**
     * @return string
     */
    public function getPaymentUuid(): string {
        return $this->paymentUuid;
    }

    /**
     * @param string $paymentUuid
     */
    public function setPaymentUuid(string $paymentUuid): void {
        $this->paymentUuid = $paymentUuid;
    }

}
