<?php

namespace App\Services\Payment;

use App\Models\Order;
use App\Models\OrderPayment;
use App\Traits\LoggableTrait;


class CookCard implements PaymentInterface {

    use LoggableTrait;

    const ORDER_NOT_PAYED = 0;

    const ORDER_APPROVED  = 'approved';
    const ORDER_DEPOSITED = 'deposited';

    public function __construct(
        protected OrderPayment $paymentModel,
    ) {
        $this->makeLogger('payment' . DIRECTORY_SEPARATOR . 'cook-card');
    }

    public function create(Order $order): PaymentResult {
        $result      = new PaymentResult();
        $returnUrl   = $order->getSuccessUrl();

        $this->log('Create Cook-card order', [
            'orderId'            => $order->id,
            'price'              => $order->sum,
            'returnUrl'          => $returnUrl,
        ]);

        $result->setPaymentUrl($order->getSuccessUrl());
        $result->setPaymentUuid($order->id);

        return $result;
    }

    public function confirmationUrl(): string {
        return '';
    }

    public function info(string $paymentId): PaymentResult {
        return new PaymentResult();
    }

    public function repeat(Order $order): PaymentResult {
        $result = $this->info($order->payment_uuid ?? '');

        if (!$result->isSuccess()) {
            return $result;
        }

        if ($result->get('orderStatus') === static::ORDER_NOT_PAYED) {
            return $result;
        }

        $result = $this->create($order);

        $result->set('need_update', true);

        return $result;
    }

    public function isPayed(array $info): bool {
        return in_array($info['operation'], [static::ORDER_DEPOSITED, static::ORDER_APPROVED])
            && $info['status'];
    }

    /**
     * Проверка валидности чексуммы запроса
     * @param array $info
     * @return bool
     */
    public function isValidStatusChange(array $info): bool {
        return true;
    }
}
