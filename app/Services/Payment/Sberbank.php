<?php

namespace App\Services\Payment;

use App\Models\Order;
use App\Models\OrderPayment;
use App\Traits\LoggableTrait;
use Voronkovich\SberbankAcquiring\Client;
use Voronkovich\SberbankAcquiring\Exception\SberbankAcquiringException;


class Sberbank implements PaymentInterface {

    use LoggableTrait;

    const ORDER_NOT_PAYED = 0;

    const ORDER_APPROVED  = 'approved';
    const ORDER_DEPOSITED = 'deposited';

    protected Client $client;

    public function __construct(
        protected OrderPayment $paymentModel,
    ) {
        $this->makeLogger('payment' . DIRECTORY_SEPARATOR . 'sberbank');

        $params = [
            'language' => 'ru',
            'userName' => setting('order.payment.sber.userName')?->text ?? '0',
            'password' => setting('order.payment.sber.password')?->text ?? '0',
        ];

        $testMode = setting('order.payment.sber.test')?->text ?? '0';
        if ($testMode === '1') {
            $params['apiUri'] = Client::API_URI_TEST;
        }

        $this->client = new Client($params);
    }

    public function create(Order $order): PaymentResult {
        $price       = $order->sum * 100;
        $result      = new PaymentResult();
        $returnUrl   = $order->getSuccessUrl();
        $callbackUrl = route('webhook.order-sberbank-status', [], true);

        $this->log('Create Sberbank order', [
            'orderId'            => $order->id,
            'price'              => $order->sum,
            'returnUrl'          => $returnUrl,
            'dynamicCallbackUrl' => $callbackUrl,
        ]);

        try {
            $response  = $this->client->registerOrder(
                $order->id,
                $price,
                $returnUrl,
                ['dynamicCallbackUrl' => $callbackUrl],
            );
        } catch (SberbankAcquiringException $e) {
            $message = 'Не удалось оформить заказ на стороне Сбербанка. Попробуйте позднее.';
            $result->addErrorMessage($message);
            $this->error($message . " ||| \n" . $e->getMessage() . " || \n" . $e->getTraceAsString());
            return $result;
        }

        $this->log('Sberbank success response', $response);

        $result->setPaymentUrl($response['formUrl']);
        $result->setPaymentUuid($response['orderId']);

        return $result;
    }

    public function confirmationUrl(): string {
        return '';
    }

    public function info(string $paymentId): PaymentResult {
        $result = new PaymentResult();

        try {
            $response  = $this->client->getOrderStatus($paymentId);
        } catch (SberbankAcquiringException $e) {
            $result->addErrorMessage('Не удалось получить информацию о заказе от Сбербанка. Попробуйте позднее.');
            $this->error($e->getMessage() . "\n" . $e->getTraceAsString());
            return $result;
        }

        $result->setData($response);

        return $result;
    }

    public function repeat(Order $order): PaymentResult {
        $result = $this->info($order->payment_uuid ?? '');

        if (!$result->isSuccess()) {
            return $result;
        }

        if ($result->get('orderStatus') === static::ORDER_NOT_PAYED) {
            $result->setPaymentUuid($order->payment_uuid);
            $result->setPaymentUrl($order->payment_url);

            return $result;
        }

        $result = $this->create($order);

        $result->set('need_update', true);

        return $result;
    }

    public function isPayed(array $info): bool {
        return in_array($info['operation'], [static::ORDER_DEPOSITED, static::ORDER_APPROVED])
            && $info['status'];
    }

    /**
     * Проверка валидности чексуммы запроса
     * @param array $info
     * @return bool
     */
    public function isValidStatusChange(array $info): bool {
        $checkSum = $info['checksum'];
        unset($info['checksum']);

        $keys = array_keys($info);
        sort($keys);
        $curCheckSum = '';

        foreach ($keys as $key) {
            $curCheckSum .= $key . ';' . $info[$key] . ';';
        }

        $token = setting('order.payment.sber.token')?->text ?? '';
        $localCheckSum = hash_hmac('sha256', $curCheckSum, $token);
        $localCheckSum = strtoupper($localCheckSum);

        $isValid = $localCheckSum === $checkSum;

        $this->log('Пришел запрос изменения статуса заказа. ' . ($isValid ? '' : 'НЕ') . 'валидно', $info);

        return $isValid;
    }
}
