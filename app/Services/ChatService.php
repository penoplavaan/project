<?php

declare(strict_types=1);

namespace App\Services;

use App\Helpers\BaseHelper;
use App\Http\Resources\ChatMessageResource;
use App\Http\Resources\ChatResource;
use App\Http\Resources\ChatUserResource;
use App\Models\Basket;
use App\Models\Chat;
use App\Models\Cook;
use App\Models\Dish;
use App\Models\User;

class ChatService
{
    public function __construct(
        protected UserService $userService,
        protected Chat $chatModel,
    ) {

    }


    public function initWebsocketJsData() {
        $user = $this->userService->current();
        $params = config('broadcasting.connections.pusher');
        BaseHelper::extendJsApp([
            'chatParams' => [
                'appKey'    => $params['key'],
                'cluster'   => '',
                'host'      => $params['options']['host'],
                'port'      => $params['options']['port'],
                'channelId' => $user->getChatChannel()
            ]
        ]);
    }

    public function getUserChats($userId = null) {
        if (!$userId) {
            $user = $this->userService->current();
            $userId = $user->id;
        }

        // Уже начатые чаты
        $myChats = $this->chatModel->getUserChats($userId);
        $chats = [];
        $users = [];
        $chatUserIds = [];
        $orderUserIds = [];

        if (!empty($myChats)) {
            $chatIds = $myChats->pluck('id')->all();
            // Загрузим последние собщения
            $lastMessages = $this->chatModel->getLastMessages($chatIds);
            if (!empty($lastMessages)) {
                $lastMessages = $lastMessages->keyBy('chat_id');
            } else {
                $lastMessages = collect([]);
            }

            $chatUserIds = $myChats->map(fn(Chat $chat) => $chat->getOtherUserId($userId))->toArray();

            // Подготовить объекты чатов
            foreach ($myChats as $chat) {
                $chatRes = ChatResource::make($chat)->resolve();
                $message = $lastMessages->get($chat->id);
                if ($message) {
                    $chatRes['messages'] = [
                        ChatMessageResource::make($message)->resolve()
                    ];
                }

                $chats[$chat->getOtherUserId($userId)] = $chatRes;
            }
        }

        // Ещё не начатые чаты (заказы в процессе оформления)
        /* Заказы теперь создаются только через форму заказа
        $basket = $this->getUserBasket();
        if ($basket) {
            $cooks = $basket->pluck('dish')->pluck('cook');
            $orderUserIds = $cooks->pluck('user')->pluck('id')->toArray();

            foreach ($cooks as $cook) {
                if (!isset($chats[$cook->user_id])) {
                    $chats[$cook->user_id] = [
                        'id'         => 0,
                        'fromUserId' => $userId,
                        'toUserId'   => $cook->user_id,
                    ];
                }
            }
        }
        */

        $allUserIds = array_unique(array_merge($chatUserIds, $orderUserIds, [$userId]));
        if (!empty($allUserIds)) {
            $usersData = User::query()
                ->whereIn('id', $allUserIds)
                ->with('attachment')
                ->with('cook')
                ->get()
                ->keyBy('id');
            $users = ChatUserResource::collection($usersData)->resolve();
            $users = collect($users)->keyBy('id')->toArray();
        }

        return [$chats, $users];
    }

    /**
     * @param int $cookUserId
     * @return \Illuminate\Support\Collection
     */
    public function getUserBasket(int $cookUserId = 0)
    {
        $user = $this->userService->current();
        $mainTable = (new Basket())->getTable();

        $query = Basket::query()
            ->select(["$mainTable.*"])
            ->where("$mainTable.user_id", $user->id)
            ->with('dish.cook.user');

        if ($cookUserId) {
            $dishTable = (new Dish())->getTable();
            $cookTable = (new Cook())->getTable();

            $query->leftJoin($dishTable, "$mainTable.dish_id", '=', "$dishTable.id")
                ->leftJoin($cookTable, "$dishTable.cook_id", '=', "$cookTable.id")
                ->where("$cookTable.user_id", $cookUserId);
        }

        $basket = $query->get();

        // Берём только те товары, которые сейчас доступны для покупки
        return $basket->filter(fn(Basket $basketRow) => $basketRow->dish && $basketRow->dish->canBuy());
    }
}
