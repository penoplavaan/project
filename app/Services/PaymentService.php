<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderPayment;
use App\Services\Payment\CookCard;
use App\Services\Payment\PaymentInterface;
use App\Services\Payment\PaymentResult;
use App\Services\Payment\Sberbank;

class PaymentService {

    const PAYMENT_SERIVCES = [
        'sberbank'  => Sberbank::class,
        'cook-card' => CookCard::class,
    ];

    public function getHandler(OrderPayment $payment): ?PaymentInterface {
        $code = $payment->paySystem?->code ?? '';

        if (isset(static::PAYMENT_SERIVCES[$code])) {
            $class = static::PAYMENT_SERIVCES[$code];
            return new $class($payment);
        }

        return null;
    }

    /**
     * Создать объект оплаты и запрос в платёжную систему
     * @param Order $order
     * @return PaymentResult
     */
    public function create(Order $order): PaymentResult {
        $paymentObj = OrderPayment::create([
            'order_id'      => $order->id,
            'pay_system_id' => $order->pay_system_id,
            'sum'           => $order->sum,
        ]);

        $handler = $this->getHandler($paymentObj);

        if ($handler) {
            $result = $handler->create($order);
        } else {
            $result = new PaymentResult();
            $result->addErrorMessage('Не удалось создать оплату. Попробуйте позднее');
        }

        return $result;
    }

    public function isOrderPayed(Order $order, array $info): bool {
        if (!$order->orderPayments || $order->orderPayments->isEmpty()) {
            return false;
        }

        /** @var \App\Models\OrderPayment $firstPayment */
        $firstPayment = $order->orderPayments->first();
        $handler = $this->getHandler($firstPayment);

        if (!$handler) {
            return false;
        }

        return $handler->isPayed($info);
    }
}
