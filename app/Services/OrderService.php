<?php

declare(strict_types=1);

namespace App\Services;

use App\Dto\Basket\BasketByCook;
use App\Dto\Basket\BasketRow;
use App\Forms\OrderForm;
use App\Helpers\ViewHelper;
use App\Models\Basket;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\Cook;
use App\Models\Order;
use App\Models\OrderToDishes;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Работа с заказами
 */
class OrderService
{
    protected const HASH_SALT = 'orderSalt';

    public function __construct(
        protected UserService $userService,
        protected BasketService $basketService,
        protected PaymentService $paymentService,
    )
    {
    }


    /**
     * Создаём новый заказ через чат
     * @param ChatMessage $message
     * @param Collection<Basket> $orderedDishes
     * @return int
     */
    public function createChatOrder(ChatMessage $message, Collection $orderedDishes): int
    {
        // Сейчас создание заказа через чат недоступно, не вызывается
        // todo сейчас не работает, т.к. в заказ добавилось много доп. полей - требуется дописать код
        $sum = $orderedDishes->reduce(fn($acc, Basket $row) => $acc + $row->count * $row->dish->price, 0);

        $fromUser = $message->user_id;
        $toCook = $message->chat->from_user_id == $fromUser ? $message->chat->toUser->cook : $message->chat->fromUser->cook;

        if (empty($toCook)) {
            throw new \Exception('Cant create order - user is not a cook! ' . $message->chat->to_user_id);
        }

        $order = Order::create([
            'message_id' => $message->id,
            'user_id'    => $fromUser,
            'cook_id'    => $toCook->id,
            'sum'        => $sum,
        ]);

        // Прикрепить товары из корзины их к заказу
        foreach ($orderedDishes as $dishToOrder) {
            $dish = $dishToOrder->dish;
            $pivot = new OrderToDishes([
                'order_id'  => $order->id,
                'count'     => $dishToOrder->count,
                'price_one' => $dish->price,
                'name'      => $dish->name,
                'dish_id'   => $dishToOrder->dish_id,
            ]);
            $pivot->save();
            $dishToOrder->delete();
        }

        // Уведомления отправляются на хуке в модели Order

        return $order->id;
    }

    /**
     * Создаём заказ через форму создания заказа
     * @param BasketByCook $basketByCook
     * @param array $formData
     * @return int|mixed
     */
    public function createOrder(BasketByCook $basketByCook, array $formData): ?array {
        $basketRows = collect($basketByCook->getRows());

        /** @var int $sum */
        $sum = $basketRows->reduce(fn($acc, BasketRow $row) => $acc + $row->getCount() * $row->getDish()->price, 0);

        $currentUser = $this->userService->current();

        $orderFields = [
            'cook_id'          => $basketByCook->getCook()->id,
            'sum'              => $sum,
            'pay_system_id'    => $formData[OrderForm::PAY_SYSTEM],
            'delivery_type_id' => $formData[OrderForm::DELIVERY_TYPE],
            'client_name'      => $formData[OrderForm::NAME],
            'client_phone'     => ViewHelper::clearPhone($formData[OrderForm::PHONE] ?? ''),
            'recipient_name'   => $formData[OrderForm::RECIPIENT_NAME] ?? '',
            'recipient_phone'  => ViewHelper::clearPhone($formData[OrderForm::RECIPIENT_PHONE] ?? ''),
            'delivery_address' => $formData[OrderForm::DELIVERY_ADDRESS] ?? '',
            'delivery_date'    => $formData[OrderForm::DELIVERY_DATE] ?? new Carbon(), // todo проверить если на фронте меняли дату и не меняли - может приходить разный формат
            'delivery_time'    => $formData[OrderForm::DELIVERY_TIME] ?? '',
            'comment'          => $formData[OrderForm::COMMENT] ?? '',
        ];

        if ($currentUser) {
            // если авторизован - отправить повару сообщение в чат
            $orderFields['user_id'] = $currentUser->id;
            $orderFields['client_name'] = $currentUser->getFullName();
            $orderFields['client_phone'] = $currentUser->phone;
        }

        DB::beginTransaction();
        $order = Order::create($orderFields);

        // Прикрепить товары из корзины к заказу
        foreach ($basketByCook->getRows() as $basketRow) {
            $dish = $basketRow->getDish();
            $pivot = new OrderToDishes([
                'order_id'  => $order->id,
                'count'     => $basketRow->getCount(),
                'price_one' => $dish->price,
                'name'      => $dish->name,
                'dish_id'   => $dish->id,
            ]);

            $pivot->save();
        }

        // Создать оплату
        $paymentResult = $this->paymentService->create($order);

        DB::commit();

        // Уведомления отправляются на хуке в модели Order

        // Очистить корзину по этому повару
        $this->basketService->clear($basketByCook->getCook()->id);

        return [
            'order' => $order,
            'paymentResult' => $paymentResult
        ];
    }

    public function getHash(int $orderId): string
    {
        return md5($orderId . config('app.key') . static::HASH_SALT);
    }

    public function checkHash(int $orderId, string $hash): bool
    {
        return $this->getHash($orderId) === $hash;
    }

    public function setPayed(Order $order)
    {
        DB::beginTransaction();
        $order->payed = 1;
        $order->save();

        foreach ($order->orderPayments as $payment) {
            $payment->payed = 1;
            $payment->save();
        }

        DB::commit();
    }

    /**
     * Оповещение повару в чат о новом заказе
     * @param $orderId
     * @return void
     */
    public function sendNewOrderChatNotification($orderId): void
    {
        $order = Order::find($orderId);
        if (!$order) {
            return;
        }

        $text = $this->prepareMessageText($order, $order->sum, $order->user);
        if ($order->user) {
            // если заказ от авторизованного - отправить повару сообщение в чат
            $messageId = $this->sendNewOrderMessage($order->user, $order->cook, $text);
        } else {
            $messageId = $this->sendNewOrderMessage($order->cook->user, $order->cook, $text);
        }

        $order->message_id = $messageId;
        $order->save();
    }

    /**
     * Отправить в чат сообщение о новом заказе
     * @param User $user
     * @param Cook $cook
     * @param string $text
     * @return int
     */
    protected function sendNewOrderMessage(User $user, Cook $cook, string $text): int {
        $chatModel = new Chat;
        $chat = $chatModel->getUserChats($user->id, $cook->user->id)->first();

        if (!$chat) {
            $chat = new Chat([
                'from_user_id' => $user->id,
                'to_user_id'   => $cook->user->id,
            ]);
            $chat->save();
            $chat->refresh();
        }

        $message = new ChatMessage([
            'chat_id' => $chat->id,
            'user_id' => $user->id,
            'message' => $text,
        ]);
        $message->save();
        $message->refresh();

        return $message->id;
    }

    /**
     * Текст сообщения в чате с поваром
     * @param array $formData
     * @param int $sum
     * @param User|null $user
     * @return string
     */
    protected function prepareMessageText(Order $order, int $sum, User|null $user = null): string {

        $lines = ['Оформлен заказ на сумму ' . ViewHelper::price($sum) . ' ₽'];

        if ($user) {
            $lines[] = 'Покупатель: ' . $user->getFullName() . ', ' . $user->phone;
        } else {
            $lines[] = 'Покупатель: Неавторизованный (' . $order->client_name . ', ' . $order->client_phone . ')';
        }

        if (!empty($order->recipient_name)) {
            $lines[] = 'Получатель заказа: ' . $order->recipient_name . ', ' . $order->recipient_phone;
        }

        if (!empty($order->deliveryType)) {
            $lines[] = 'Доставка: ' . $order->deliveryType->name;

            if (!empty($order->delivery_address)) {
                $lines[] = $order->delivery_address;
            }
        }

        if (!empty($order->paySystem)) {
            $lines[] = 'Оплата: ' . $order->paySystem->name;
        }

        $lines[] = 'Дата и время: ' . $order->delivery_date . ', ' . $order->delivery_time;

        if (!empty($order->comment)) {
            $lines[] = 'Комментарий покупателя: ' . $order->comment;
        }

        return implode("\n", $lines);
    }
}
