<?php

namespace App\Services;

use App\Http\Resources\CookCoordsResource;
use App\Models\Cook;
use App\Models\CookCategory;
use App\Models\CookSpeciality;
use App\Models\DishCategory;
use Illuminate\Support\Facades\DB;

class CookFilterService
{
    public function __construct(
        protected DishCategory $dishCategoryModel,
        protected CookCategory $cookCategoryModel,
        protected Cook $cookModel,
        protected CookSpeciality $cookSpecialityModel,
    )
    {
    }

    public function filterCooks(array $filterArray) {
        $main = $this->cookModel->getTable();
        $query = $this->cookModel->query();

        $query->select(["$main.id", "$main.coords", "$main.user_id"])
            ->distinct()
            ->with('user')
            ->whereActive(1)
            ->whereVerified(1)
            ->whereNot('coords', '')
            ->whereNotNull('coords');

        if (!empty($filterArray['category'])) {
            $pivot = (new CookCategory)->getTable();
            $query->leftJoin($pivot, "$main.id", '=', "$pivot.cook_id")
                ->whereIn("$pivot.category_id", $filterArray['category']);
        }

        if (!empty($filterArray['speciality'])) {
            $query->whereIn('speciality_id', $filterArray['speciality']);
        }

        if (!empty($filterArray['cook'])) {
            $query->where('id', $filterArray['cook']);
        }

        $cookCoords = $query->get();

        return CookCoordsResource::collection($cookCoords);
    }

    public function getFilterFields($currentFilter = []): array
    {
        $main = $this->dishCategoryModel->getTable();
        $pivot = $this->cookCategoryModel->getTable();
        $cook = $this->cookModel->getTable();

        $dishCategories = $this->dishCategoryModel->query()
            ->select("$main.*", DB::raw('count(*) as total'))
            ->leftJoin($pivot, "$main.id", '=', "$pivot.category_id")
            ->leftJoin($cook, "$cook.id", '=', "$pivot.cook_id")
            ->where("$cook.verified", '=', '1')
            ->groupBy("$main.id")
            ->get();

        // Категории меню
        $categoryBlock = [
            'type'        => 'checkbox',
            'label'       => 'Все категории',
            'name'        => 'category',
            'collapsable' => false,
            'items'       => $dishCategories
                ->map(function(DishCategory $cat) use ($currentFilter) {
                    return [
                        'value' => $cat->id,
                        'label' => $cat->getMenuTitle() . '<sup>' . $cat->total . '</sup>',
                        'checked' => in_array($cat->id . '', $currentFilter['category'] ?? []),
                    ];
                })
            ->toArray(),
        ];

        $main = $this->cookSpecialityModel->getTable();
        $cook = $this->cookModel->getTable();
        $specialities = $this->cookSpecialityModel->query()
            ->select("$main.*", DB::raw('count(*) as total'))
            ->leftJoin($cook, "$cook.speciality_id", '=', "$main.id")
            ->where("$cook.verified", '=', '1')
            ->groupBy("$main.id")
            ->get();

        // Специалищации поваров
        $specialityBlock = [
            'type'        => 'checkbox',
            'label'       => 'Специализации',
            'name'        => 'speciality',
            'collapsable' => false,
            'items'       => $specialities
                ->map(function(CookSpeciality $cat) use ($currentFilter) {
                    return [
                        'value' => $cat->id,
                        'label' => $cat->title . '<sup>' . $cat->total . '</sup>',
                        'checked' => in_array($cat->id . '', $currentFilter['speciality'] ?? []),
                    ];
                })
                ->toArray(),
        ];

        return [
            $categoryBlock,
            $specialityBlock,
        ];
    }
}
