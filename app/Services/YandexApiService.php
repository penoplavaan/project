<?php

declare(strict_types=1);

namespace App\Services;

class YandexApiService
{
    const TIMEOUT = 3;
    protected int $timeout = self::TIMEOUT;

    public function __construct()
    {
        $this->key = setting('site.yandex-api-key')->getText();
    }

    public function setTimeout(int $timeout) {
        $this->timeout = $timeout;
    }

    public function suggest($address): array {
        $domain = 'https://suggest-maps.yandex.ru/suggest-geo';
        $params = [
            'apikey'      => $this->key,
            'v'           => 5,
            'search_type' => 'tp',
            'part'        => $address,
            'lang'        => 'ru_RU',
        ];

        $url = $domain . '?' . http_build_query($params);
        $rawData = $this->makeCurlRequest($url);

        if (!empty($rawData)) {
            $first = strpos($rawData, '(');
            $cutData = substr($rawData, $first + 1, -1);
            $data = json_decode($cutData);
            $list = array_filter($data[1], fn($obj) => $obj[0] === 'geo');
            $result = array_map(fn($obj) => $obj[2], $list);

            return $result;
        }

        return [];
    }

    public function geocode($text): ?array {
        $domain = 'https://geocode-maps.yandex.ru/1.x/';
        $params = [
            'apikey' => $this->key,
            'format' => 'json',
            'geocode' => $text,
        ];
        $url = $domain . '?' . http_build_query($params);
        $rawData = $this->makeCurlRequest($url);

        if (!empty($rawData)) {
            $data = json_decode($rawData, true);
            $points = $data['response']['GeoObjectCollection']['featureMember'] ?? [];
            $point = reset($points);
            $coordString = $point['GeoObject']['Point']['pos'] ?? '';
            $coords = explode(' ', $coordString);

            return [
                'lon' => $coords[0] ?? null, // Долгота
                'lat' => $coords[1] ?? null, // Широта
            ];
        }

        return null;
    }

    protected function makeCurlRequest($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

        return curl_exec($ch);
    }
}
