<?php

declare(strict_types=1);

namespace App\Services;

use App\Dto\Basket\BasketByCook;
use App\Dto\Basket\BasketRow;
use App\Models\Basket\CookieStorage;
use App\Models\Basket\DbStorage;
use App\Models\Basket\StorageInterface;
use App\Models\Dish;

/**
 * Сервис работы с корзиной
 */
class BasketService
{
    const MAX_IN_BASKET = 99;

    protected StorageInterface|null $storage;

    public function __construct(
        protected UserService $userService,
    ) {

    }

    public function get() {
        return $this->getStorage()->get();
    }

    public function getCount() {
        $basket = $this->getStorage()->get();

        return $basket->sum(fn($byCook) => count($byCook->getRows()));
    }

    public function getInfo() :array
    {
        return [
            'count' => $this->getCount(),
        ];
    }

    public function add(int $dishId, int $count) {
        $dish = Dish::find($dishId);
        if (!$dish || !$dish->canBuy()) {
            return false;
        }

        return $this->getStorage()->add($dish, $count);
    }

    public function setCount(int $dishId, int $count) {
        $dish = Dish::find($dishId);
        if (!$dish || !$dish->canBuy()) {
            return false;
        }

        return $this->getStorage()->setCount($dish, $count);
    }

    public function remove(int $dishId): bool {
        return $this->getStorage()->remove($dishId);
    }

    public function clear(int $cookId = 0): bool {
        return $this->getStorage()->clear($cookId);
    }

    /**
     * Объединение корзин из кук и БД при авторизации
     * @return void
     * @throws \Exception
     */
    public function mergeBaskets() {
        /** @var CookieStorage $cookieStorage */
        $cookieStorage = app(CookieStorage::class);
        $cookieBasket = $cookieStorage->get();
        /** @var DbStorage $dbStorage */
        $dbStorage = app(DbStorage::class);

        /** @var BasketByCook $byCook */
        foreach ($cookieBasket as $byCook) {
            /** @var BasketRow $row */
            foreach ($byCook->getRows() as $row) {
                $dbStorage->add($row->getDish(), $row->getCount());
            }
        }

        $user = $this->userService->current();
        if ($user->isCook() && $user?->cook?->id) {
            $dbStorage->clear($user->cook->id);
        }

        $cookieStorage->clear();
    }

    protected function getStorage(): StorageInterface {
        if (!isset($this->storage)) {
            $this->storage = $this->createNewStorage();
        }

        return $this->storage;
    }

    protected function createNewStorage(): StorageInterface {
        if ($this->userService->isAuth()) {
            return app(DbStorage::class);
        } else {

            return app(CookieStorage::class);
        }
    }
}
