<?php

namespace App\Services;

class SeoService
{
    protected string $footerTextCode = 'seo.footer.text';
    protected array $meta = [];

    public function __construct()
    {
        $domain = config('app.url');

        $this->meta['og'] = [
            'og:title'     => config('app.ruName'),
            'og:url'       => url()->current(),
            'og:site_name' => setting('site.name')->text ?? '',
            'og:image'     => $domain . '/images/logo.svg',
        ];
    }

    public function setFooterTextCode(string $code) {
        $this->footerTextCode = $code;
    }

    public function getFooterTextCode() {
        return $this->footerTextCode;
    }

    public function setMeta(string $type, $value) {
        $this->meta[$type] = $value;
    }

    public function getMeta(string $type) {
        return $this->meta[$type] ?? '';
    }
}
