<?php

namespace App\Services;

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class SettingsService
{
    protected Collection $setting;
    protected SiteSetting $defaultSetting;

    public function __construct()
    {
        $this->setting = $this->loadSettings();
        $this->defaultSetting = new SiteSetting();
    }

    public function get(string $code): SiteSetting
    {
        return $this->setting->get($code, $this->defaultSetting);
    }

    protected function loadSettings()
    {
        return Cache::tags([SiteSetting::getCacheTag()])
            ->remember('site_settings_load_settings', SiteSetting::getCacheTime(), function () {
                return SiteSetting::with('attachment')->get()->keyBy('code');
            });
    }

    /**
     * @param string $code
     * @return Collection<int, SiteSetting>
     */
    public function section(string $code): Collection
    {
        return Cache::tags([
            SiteSettingsSection::getCacheTag(),
            SiteSetting::getCacheTag()
        ])
            ->remember(
                'site_settings_section_' . $code,
                SiteSetting::getCacheTime(),
                function () use ($code) {
                    $section = SiteSettingsSection::whereCode($code)->with('settings.attachment')->first();
                    return $section
                        ? $section->settings
                        : new Collection();
                }
            );
    }
}
