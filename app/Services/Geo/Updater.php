<?php

namespace App\Services\Geo;

/**
 * Class Updater
 * @package Sibirix\Geobase
 */
class Updater
{

    protected static $sourceUrl = 'https://sypexgeo.net/files/SxGeoCity_utf8.zip';

    /**
     * Обновление файла базы данных Sypex Geo
     * @return bool
     * @throws \Exception
     */
    public static function doUpdate()
    {
        // $url = 'https://sypexgeo.net/files/SxGeoCountry.zip';  // Путь к скачиваемому файлу
        $dataDir = storage_path('geo' . DIRECTORY_SEPARATOR);

        if (!file_exists($dataDir)) {
            mkdir($dataDir, 0755, true);
        }

        $lastUpdatedFile = $dataDir . '/SxGeo.upd'; // Файл в котором хранится дата последнего обновления

        $types = [
            'Country' => 'SxGeo.dat',
            'City'    => 'SxGeoCity.dat',
            'Max'     => 'SxGeoMax.dat',
        ];

        // Скачиваем архив
        preg_match("/(Country|City|Max)/", pathinfo(static::$sourceUrl, PATHINFO_BASENAME), $m);
        $type = $m[1];
        $dataFile = $types[$type];

        // Скачиваем архив с сервера

        $tempPath = storage_path('geo' . DIRECTORY_SEPARATOR . 'tmp.zip');
        $fileHandle = fopen($tempPath, 'wb');
        $curlHandle = curl_init(static::$sourceUrl);
        curl_setopt_array($curlHandle, [
            CURLOPT_FILE => $fileHandle,
            CURLOPT_HTTPHEADER => file_exists($lastUpdatedFile) ? [
                "If-Modified-Since: " . file_get_contents(
                    $lastUpdatedFile
                )
            ] : [],
        ]);

        if (!curl_exec($curlHandle)) {
            return static::logError('Ошибка при скачивании архива данных об IP-адресах');
        }

        $code = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        curl_close($curlHandle);
        fclose($fileHandle);

        if ($code == 304) {
            unlink($tempPath);

            // Архив не обновился, с момента предыдущего скачивания
            return true;
        }

        // Проверить, что на стороне SXGeo не падает ошибка
        if (file_exists($tempPath)) {
            $tmpFullFiledata = file_get_contents($tempPath);
            if (strlen($tmpFullFiledata) < 10000) {
                if (strpos($tmpFullFiledata, 'Fatal error') !== false || strpos(
                        $tmpFullFiledata,
                        'Error ('
                    ) !== false) {
                    unlink($tempPath);
                    return static::logError('Ошибка при скачивании архива данных об IP-адресах, ' . static::$sourceUrl);
                }
            }
        }

        // Архив с сервера скачан
        // Распаковываем архив
        $zipFileHandle = fopen('zip://' . $tempPath . '#' . $dataFile, 'rb');
        $unzipFileHandle = fopen($dataDir . $dataFile, 'wb');
        if (!$zipFileHandle) {
            return static::logError('Не получается открыть скачаный файл данных об IP-адресах');
        }

        // Распаковываем архив
        stream_copy_to_stream($zipFileHandle, $unzipFileHandle);
        fclose($zipFileHandle);
        fclose($unzipFileHandle);
        if (filesize($dataFile) == 0) {
            return static::logError('Ошибка при распаковке архива данных об IP-адресах');
        }

        unlink($tempPath);
        rename(__DIR__ . '/' . $dataFile, $dataDir . $dataFile);
        file_put_contents($lastUpdatedFile, gmdate('D, d M Y H:i:s') . ' GMT');

        return true;
    }

    /**
     * @param $message
     * @return bool
     * @throws \Exception
     */
    protected static function logError($message)
    {
        // todo logger
        throw new \Exception($message);
    }
}
