<?php

namespace App\Services\Geo;

use App\Models\City;
use Exception;
use Illuminate\Support\Facades\Cookie;

/**
 *
 */
class Detector
{

    const COOKIE_NAME = 'city';
    const DETECT_NONE = 0;
    const DETECT_COOKIE = 1;
    const DETECT_GEOIP = 2;
    const DETECT_DEFAULT = 3;

    /**
     * @var array
     */
    protected $ipParams = [
        'HTTP_CLIENT_IP',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_REAL_IP',
        'REMOTE_ADDR'
    ];

    /**
     * @var array
     */
    protected array $serverParams = [];
    protected mixed $clientIp = '';

    protected $currentLocation = null;
    protected int $detectionType = self::DETECT_NONE;
    protected City $model;

    /**
     * @param array $serverParams
     */
    public function __construct()
    {
        $this->serverParams = $_SERVER;
        $this->clientIp = $this->getClientIp();
        $this->model = new City();
    }

    /**
     * @return bool|mixed|string
     */
    public function getClientIp()
    {
//         return '217.118.93.15';  // - Нижний Новгород
//         return '37.21.182.85';   // - Томск
//         return '91.144.140.15';  // - Казань
//         return '89.223.64.19';   // - Питер
//         return '212.46.198.142'; // - Москва
//         return '62.78.80.112';   // - Барнаул

        foreach ($this->ipParams as $paramName) {
            if (!empty($this->serverParams[$paramName])) {
                $ip = $this->serverParams[$paramName];
                if (strpos($ip, ',') !== false) {
                    $parts = explode(',', $ip);
                    return trim(reset($parts));
                } else {
                    return $ip;
                }
            }
        }

        return false;
    }

    /**
     * Определяем местоположения по ip
     */
    public function getClientLocation()
    {
        if ($this->currentLocation !== null) {
            return $this->currentLocation;
        }

        $cookieValue = (int)request()->cookie(self::COOKIE_NAME);

        if ($cookieValue) {
            // получение по ID из базы
            $city = $this->model->find($cookieValue);

            if ($city) {
                $this->detectionType = self::DETECT_COOKIE;
                return $this->setCity($city);
            }
        }

        $this->detectionType = self::DETECT_DEFAULT;

        // По задаче #580543
        $city = $this->model->query()->whereName('Тюмень')->first();
        if (!empty($city)) {
            return $this->setCity($city);
        }

        // Ищем город по IP
        try {
            $sxGeo = new SxGeo();
            $city = $sxGeo->get($this->clientIp);
            if ($city && $city['city']) {
                $cityName = $city['city']['name_ru'];
            }
        } catch (Exception $e) {
        }

        // Если не нашли - возвращаем город по умолчанию
        if (empty($cityName)) {
            return $this->setDefault();
        }

        // Определяем города из базы по названию найденного города
        $city = $this->model->query()->whereName($cityName)->first();

        // Если определённый город не найден в базе - устанавливаем дефолтный
        if (empty($city)) {
            return $this->setDefault();
        }

        // Валидация города по бизнес-логике текущего проекта (если нужна)
        $this->detectionType = self::DETECT_GEOIP;
        return $this->setCity($city);
    }

    /**
     * @return City
     */
    public function getDefault(): City
    {
        $cities = $this->model->query()->orderBy('sort', 'desc')->limit(1)->get();

        return $cities->first();
    }

    /**
     * @param City $city
     * @return City
     */
    public function setCity(City $city): City
    {
        $this->currentLocation = $city;
        $this->setCityCookie($city->id);

        return $city;
    }

    /**
     *
     */
    public function setDefault(): City
    {
        $default = $this->getDefault();
        $this->currentLocation = $default;
        $this->setCityCookie($default ? $default->id : 0);

        return $default;
    }

    /**
     * @param int $cityId
     */
    public function setCityCookie($cityId): void
    {
        Cookie::queue(self::COOKIE_NAME, $cityId, 60 * 24 * 365);
    }

    /**
     * @return int
     */
    public function getDetectionType(): int
    {
        return $this->detectionType;
    }
}
