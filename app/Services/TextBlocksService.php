<?php

namespace App\Services;

use App\Models\TextBlock;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class TextBlocksService
{
    protected Collection $blocks;
    protected TextBlock $empty;

    public function __construct()
    {
        $this->blocks = $this->loadBlocks();
        $this->empty = new TextBlock();
    }

    public function get(string $code): TextBlock
    {
        return $this->blocks->get($code, $this->empty);
    }

    protected function loadBlocks()
    {
        return Cache::tags([TextBlock::getCacheTag()])
            ->remember('text_blocks_load_blocks', TextBlock::getCacheTime(), function () {
                return TextBlock::query()->get()->keyBy('code');
            });
    }
}
