<?php

declare(strict_types=1);

namespace App\Services;

use App\Data\Error;
use App\Data\Result;
use App\Forms\RegisterForm;
use App\Forms\VerifyPhoneForm;
use App\Helpers\ViewHelper;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class UserService
{
    public function isAuth(): bool
    {
        return Auth::check();
    }

    /**
     * @return \App\Models\User
     */
    public function current(bool $force = false): ?\App\Models\User
    {
        static $user;

        if ($force || $user === null) {
            $user = auth()->user();
        }

        return $user;
    }

    /**
     * Авторизция по емейлу+паролю
     * @param string $email
     * @param string $password
     * @return Result
     */
    public function loginByEmail(string $email, string $password): Result
    {
        $result = new Result();

        $user = $this->getByEmail($email);
        if (!$user || !$user->email_verified_at) {
            $result->addError(new Error('Пользователь не найден'));

            return $result;
        }

        $success = Auth::attempt(['email' => $email, 'password' => $password], true);

        if ($success) {
            request()->session()->regenerate();
        } else {
            $result->addError(new Error('Проверьте правильность ввода логина и пароля'));
        }

        return $result;
    }

    /**
     * Авторизация по телефону+коду
     * Код должен быть заранее отправлен
     * @param string $phone
     * @param string $code
     * @return Result
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function loginByPhoneWithCode(string $phone, string $code): Result {
        /** @var SmsService $smsService */
        $smsService = app()->make(SmsService::class);
        $codeIsValid = $smsService->checkSmsCode($phone, $code, SmsService::TYPE_AUTH);

        if ($codeIsValid) {
            $existVerifiedUser = $this->getByPhone($phone, true);

            // Если нет пользователя с таким телефоном такого то создаем
            if (!$existVerifiedUser) {
                return $this->registerByPhoneOnly($phone);
            }

            $result = $this->loginById($existVerifiedUser->id);
        } else {
            $result = new Result();
            $result->addError(new Error('Введён неправильный код'));
        }

        return $result;
    }

    /**
     * Прямая авторизация пользователя по ID
     * Использовать аккуратно!!!
     * @param int $userId
     * @return Result
     */
    protected function loginById(int $userId): Result
    {
        $result = new Result();

        $user = $this->getById($userId);
        if (!$user) {
            $result->addError(new Error('Пользователь не найден'));

            return $result;
        }

        $success = Auth::loginUsingId($userId, true);

        if ($success) {
            request()->session()->regenerate();
        } else {
            $result->addError(new Error('Пользователь не найден'));
        }

        return $result;
    }

    public function loginByPhone(string $phone, string $password): Result
    {
        $result = new Result();
        $phone = ViewHelper::clearPhone($phone);

        $user = $this->getByPhone($phone, true);
        if (!$user) {
            $result->addError(new Error('Пользователь не найден'));

            return $result;
        }

        $success = Auth::attempt(['phone' => $phone, 'password' => $password], true);

        if ($success) {
            request()->session()->regenerate();
        } else {
            $result->addError(new Error('Проверьте правильность ввода телефона и пароля'));
        }

        return $result;
    }

    public function logout(): void
    {
        Auth::logout();
    }

    public function getByEmail(string $email)
    {
        return User::query()->where(['email' => $email])->first();
    }

    public function getByPhone(string $phone, bool $verified = false)
    {
        $phone = ViewHelper::clearPhone($phone);
        $query = User::query()->where(['phone' => $phone]);
        if ($verified) {
            $query->where('phone_verified', 1);
        }

        return $query->first();
    }

    public function getById(int $id)
    {
        return User::query()->whereId($id)->first();
    }

    public function register(array $userData): Result
    {
        $result = new Result();

        $email = $userData[RegisterForm::EMAIL] ?? ''; // email - не обязательное
        $password = $userData[RegisterForm::PASSWORD];
        $phone = $userData[RegisterForm::PHONE];
        $advertisement = $userData[RegisterForm::ADVERTISEMENT] ?? 0;
        $code = $userData[RegisterForm::CODE];
        $clearPhone = ViewHelper::clearPhone($phone);

        $existPhone = $this->getByPhone($phone, true);

        /** @var SmsService $smsService */
        $smsService = app()->make(SmsService::class);
        $codeIsValid = $smsService->checkSmsCode($phone, $code, SmsService::TYPE_REGISTER);

        if (!empty($email)) {
            $existEmail = $this->getByEmail($email);
            if ($existEmail) {
                $result->addError(new Error('Учетная запись с таким email уже существует.', RegisterForm::EMAIL));
            }
        }

        if ($existPhone) {
            $result->addError(new Error('Учетная запись с таким номером телефона уже существует.', RegisterForm::PHONE));
        }

        if (!$codeIsValid) {
            $result->addError(new Error('Неправильный код подтверждения', RegisterForm::CODE));
        }

        if (!empty($result->getErrors())) {
            return $result;
        }

        $user = new User();
        $user->fill(
            [
                'name'           => '',
                'email'          => $email,
                'phone'          => $clearPhone,
                'phone_verified' => 1,
                'advertisement'  => $advertisement,
                'password'       => \Hash::make($password),
            ]
        );

        $user->save();
        $user->refresh();
        Auth::loginUsingId($user->id, true);

        // Отправка письма
        // todo - вынести на очереди, чтобы не тормозить ответ пользователю
        event(new Registered($user));

        return $result;
    }

    public function registerByPhoneOnly(string $phone): Result
    {
        $result = new Result();

        $clearPhone = ViewHelper::clearPhone($phone);
        $existPhone = $this->getByPhone($phone, true);

        if ($existPhone) {
            $result->addError(new Error('Учетная запись с таким номером телефона уже существует.', $clearPhone));
        }

        if (!empty($result->getErrors())) {
            return $result;
        }

        $user = new User();
        $user->fill(
            [
                'name'           => '',
                'email'          => '',
                'password'       => '',
                'phone'          => $clearPhone,
                'phone_verified' => 1,
                'advertisement'  => 0,
            ]
        );

        $user->save();
        $user->refresh();
        Auth::loginUsingId($user->id, true);

        // Отправка письма
        // todo - вынести на очереди, чтобы не тормозить ответ пользователю
        event(new Registered($user));

        return $result;
    }

    public function verifyPhone(array $userData): Result
    {
        $result = new Result();
        $phone = $userData[VerifyPhoneForm::PHONE];
        $phone = ViewHelper::clearPhone($phone);
        $code = $userData[VerifyPhoneForm::CODE];
        $type = $userData['type'] ?? SmsService::TYPE_REGISTER;
        $user = $this->current();

        if ($user->phone_verified) {
            $result->addError(new Error('Ваш номер телефона уже подтверждён.', RegisterForm::PHONE));
            return $result;
        }

        $existPhone = $this->getByPhone($phone, true);

        /** @var SmsService $smsService */
        $smsService = app()->make(SmsService::class);
        $codeIsValid = $smsService->checkSmsCode($phone, $code, $type);

        if ($existPhone) {
            $result->addError(new Error('Учетная запись с таким номером телефона уже существует.', RegisterForm::PHONE));
        }

        if (!$codeIsValid) {
            $result->addError(new Error('Неправильный код подтверждения', RegisterForm::CODE));
        }

        if (!empty($result->getErrors())) {
            return $result;
        }

        $user->phone = $phone;
        $user->phone_verified = 1;
        $user->save();

        return $result;
    }

    /**
     * @param string $email
     * @return Result
     */
    public function sendResetPassword(string $email): Result
    {
        $status = Password::sendResetLink(['email' => $email]);

        $result = new Result();

        if ($status !== Password::RESET_LINK_SENT) {
            $result->addError(new Error(__($status)));
        }

        return $result;
    }

    /**
     * @param string $email
     * @return Result
     */
    public function resetPassword(array $data): Result
    {
        $data['password_confirmation'] = $data['password'];

        $status = Password::reset($data, function ($user, $password) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->setRememberToken(Str::random(60));

            $user->save();

            // Помечаем емейл как подтверждённый, потому что пользователь фактически через сброс пароля подтвердил его
            $user->markEmailAsVerified();

            // Сразу авторизуем пользователя
            auth()->login($user);

            event(new PasswordReset($user));
        });

        $result = new Result();

        if ($status !== Password::PASSWORD_RESET) {
            $result->addError(new Error(__($status)));
        }

        return $result;
    }

    public function changePassword(string $oldPass, string $newPass): Result
    {
        $result = new Result();

        $user = $this->current();
        if ($user->phone_verified) {
            $success = Auth::attempt(['phone' => $user->phone, 'password' => $oldPass], true);
        } else {
            $success = Auth::attempt(['email' => $user->email, 'password' => $oldPass], true);
        }

        if ($success) {
            $user->fill([
                'password' => \Hash::make($newPass),
            ]);
            $user->save();
            request()->session()->regenerate();

        } else {
            $result->addError(new Error('Введён неправильный пароль'));
        }

        return $result;
    }
}
