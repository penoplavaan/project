<?php

declare(strict_types=1);

namespace App\Services;

use App\Helpers\ViewHelper;
use App\Models\SmsCode;
use App\Services\SmsSender\Fake;
use App\Services\SmsSender\SmsSenderInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 *
 */
class SmsService
{

    const LIMIT_IP_NUM = 5;
    const LIMIT_PHONE_NUM = 3;
    const LIMIT_TIME = 3600; // Сколько надо ждать, если упёрлись в лимит
    const TIMEOUT_TIME = 60; // Сколько ждать между соседними отправками смс, если ещё не дошли до лимита
    const CODE_USE_TIMEOUT = 3600; // Премя активности кода, через сколько он протухает

    const TYPE_REGISTER = 'register';
    const TYPE_VERIFY = 'verify';
    const TYPE_AUTH = 'auth';
    const TYPE_ORDER = 'order';
    const TYPE_NEW_ORDER_COOK = 'ordercook';
    const TYPE_NEW_UNPAYED_ORDER_COOK = 'unpayedordercook';

    public function __construct(
        private SmsSenderInterface $sender,
        private SmsCode $smsCode,
        private Request $request
    )
    {

    }

    public static function getTypesList(): array
    {
        return [
            static::TYPE_REGISTER,
            static::TYPE_VERIFY,
            static::TYPE_AUTH,
            static::TYPE_ORDER,
        ];
    }

    public static function getSenderClass(): SmsSenderInterface|string
    {
        $type = config('sms.sender', 'fake');
        $class = config("sms.settings.$type.class", '');

        return !empty($class) && class_exists($class) ? $class : Fake::class;
    }

    /**
     * Можно ли сейчас отправить ещё один код на указанный номер и с указанного IP
     * @param string $phone
     * @return array|bool[]
     */
    public function canSendCode(string $phone): array
    {
        $phone = ViewHelper::clearPhone($phone);
        $ip = $this->request->ip();

        $sentCodes = $this->smsCode
            ->where('created_at', '>', Carbon::now()->subSeconds(static::LIMIT_TIME))
            ->where(function (Builder $builder) use ($ip, $phone) {
                return $builder
                    ->where('client_ip', $ip)
                    ->orWhere('phone', $phone);
            })
            ->orderBy('id', 'desc')
            ->limit(static::LIMIT_IP_NUM + static::LIMIT_PHONE_NUM)
            ->get();

        /** @var SmsCode $code */
        $byIp = $sentCodes->filter(fn($code) => $code->client_ip == $ip)->count();
        $byPhone = $sentCodes->filter(fn($code) => $code->phone == $phone)->count();

        if ($byIp >= static::LIMIT_IP_NUM || $byPhone >= static::LIMIT_PHONE_NUM) {
            /** @var SmsCode $oldestCode */
            $oldestCode = $sentCodes->last();
            $secondsPassed = $oldestCode->created_at->diffInSeconds(Carbon::now());
            $timeout = static::LIMIT_TIME - $secondsPassed;

            if ($timeout > 0) {
                return [
                    'canSend' => false,
                    'timeout' => $timeout
                ];
            }
        }

        /** @var SmsCode $newestCode */
        $newestCode = $sentCodes->first();
        if ($newestCode) {
            $secondsPassed = $newestCode->created_at->diffInSeconds(Carbon::now());
            $timeout = static::TIMEOUT_TIME - $secondsPassed;

            if ($timeout > 0) {
                return [
                    'canSend' => false,
                    'timeout' => $timeout
                ];
            }
        }

        return [
            'canSend' => true,
        ];
    }

    /**
     * Отправить на номер телефона сообщение определённого типа
     * @param string $phone
     * @param string $type
     * @param array $params
     * @return int Таймаут до следующей отправки смс
     * @throws \Exception
     */
    public function sendSmsCode(string $phone, string $type, array $params = []): int
    {
        $phone = ViewHelper::clearPhone($phone);
        $templates = [
            static::TYPE_REGISTER => 'Код подтверждения регистрации: #CODE#',
            static::TYPE_VERIFY => 'Код подтверждения номера телефона: #CODE#',
            static::TYPE_AUTH => 'Код для авторизации: #CODE#',
            static::TYPE_ORDER => 'Код для подтверждения заказа: #CODE#',
        ];
        if (!isset($templates[$type])) {
            throw new \Exception('Unknown sms type');
        }

        $code = $this->sender->getRandomCode($this->smsCode::DEFAULT_LENGTH);
        SmsCode::create([
            'client_ip' => $this->request->ip(),
            'phone'     => $phone,
            'code'      => $code,
            'type'      => $type,
            'confirmed' => 0,
        ]);
        $params['CODE'] = $code;
        $text = $templates[$type];
        foreach ($params as $key => $value) {
            $text = str_replace("#$key#", $value, $text);
        }

        // todo что и как возвращаем? или через Exception'ы?
        $this->sender->send($phone, $text);

        $canSend = $this->canSendCode($phone);
        $timeout = (int)($canSend['timeout'] ?? -1);

        return $timeout;
    }

    /**
     * Отправка текстового СМС без кода и без проверки ограничений
     * @param string $phone
     * @param string $type
     * @param array $params
     * @return void
     */
    public function sendSmsText(string $phone, string $type, array $params = []): void
    {
        $phone = ViewHelper::clearPhone($phone);
        $templates = [
            static::TYPE_NEW_ORDER_COOK => 'У вас 1 новый заказ. Перейдите в чат для просмотра блюд.',
            static::TYPE_NEW_UNPAYED_ORDER_COOK => 'У вас 1 новый заказ. Перейдите в чат для просмотра блюд. Не оплачен.',
        ];

        $text = $templates[$type];
        foreach ($params as $key => $value) {
            $text = str_replace("#$key#", $value, $text);
        }

        $this->sender->send($phone, $text);
    }

    /**
     * Проверить валидность кода
     * @param string $phone
     * @param string $code
     * @param string $type
     * @return bool
     */
    public function checkSmsCode(string $phone, string $code, string $type): bool
    {
        $phone = ViewHelper::clearPhone($phone);

        /** @var SmsCode $sentCode */
        $sentCode = $this->smsCode
            ->where('phone', $phone)
            ->where('type', $type)
            ->orderBy('id', 'desc')
            ->first();

        if (!$sentCode || $sentCode->code !== $code) {
            return false; // Кода на этот телефон нет, или введённый код с ним не совпадает
        }

        // Проверяем, что код ещё не протух
        $secondsPassed = $sentCode->created_at->diffInSeconds(Carbon::now());
        $timeout = static::CODE_USE_TIMEOUT - $secondsPassed;
        if ($timeout < 0) {
            return false; // код протух
        }

        if ($sentCode->confirmed) {
            // Если код уже использован - должен совпадать ID сессии
            return ($sentCode->confirmed_session_id === session()->getId());
        } else {
            // Если не использован - ставим пометку и сохраняем ID сессии
            $sentCode->confirmed = 1;
            $sentCode->confirmed_session_id = session()->getId();
            $sentCode->save();
            return true;
        }
    }
}
