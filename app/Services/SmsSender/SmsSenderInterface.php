<?php

namespace App\Services\SmsSender;

interface SmsSenderInterface {
    public function send(string $phone, string $text): void;
    public function getRandomCode(int $length): string;
}
