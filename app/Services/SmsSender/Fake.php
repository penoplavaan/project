<?php

namespace App\Services\SmsSender;

use App\Helpers\BaseHelper;
use App\Helpers\ViewHelper;

/**
 * Фейковая отправка СМС, для разработки
 */
class Fake implements SmsSenderInterface
{

    public function getRandomCode(int $length): string
    {
        return '1234';
    }

    public function send(string $phone, string $text): void
    {
        logger("Sms to $phone: $text");
    }
}
