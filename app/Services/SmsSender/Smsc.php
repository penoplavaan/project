<?php

namespace App\Services\SmsSender;

use App\Helpers\BaseHelper;
use App\Helpers\ViewHelper;

/**
 * Отправка СМС через SMSЦентр
 * https://smsc.ru/api/
 */
class Smsc implements SmsSenderInterface
{

    private string $login = '';
    private string $password = '';
    private string $sender = '';
    private const API_ADDRESS = 'https://smsc.ru/sys/send.php';
    private const TIMEOUT = 5;

    public function __construct()
    {
        $this->login = config('sms.settings.smsc.login', '');
        $this->password = config('sms.settings.smsc.password', '');
        $this->sender = config('sms.settings.smsc.sender', '');
    }

    public function getRandomCode(int $length): string
    {
        $characters = '0123456789';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public function send(string $phone, string $text): void
    {
        // https://smsc.ru/sys/send.php?login=<login>&psw=<password>&phones=<phones>&mes=<message>
        logger("Sms to $phone: $text");
        $params = [
            'login'  => $this->login,
            'psw'    => $this->password,
            'phones' => ViewHelper::clearPhone($phone),
            'mes'    => $text,
            'sender' => $this->sender,
//            'cost'   => 1, // Без реальной отправки, только запрос стоимости
        ];

        $url = static::API_ADDRESS . '?' . http_build_query($params);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, static::TIMEOUT);
        curl_setopt($ch, CURLOPT_TIMEOUT, static::TIMEOUT);

        $result = curl_exec($ch);
        logger($result);
    }
}
