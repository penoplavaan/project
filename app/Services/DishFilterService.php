<?php

namespace App\Services;

use App\Helpers\BaseHelper;
use App\Models\Cook;
use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\DishesToAdditionalCategories;
use App\Models\DishesToTags;
use App\Models\Tag;
use App\Services\Geo\Detector;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class DishFilterService
{
    const PAGE_SIZE = 18;

    public function __construct(
        protected DishCategory $dishCategoryModel,
        protected Dish $dishModel,
        protected Cook $cookModel,
        protected Tag $tagModel,
        protected DishesToTags $dishToTag,
        protected DishesToAdditionalCategories $dishesToAdditionalCategories,
        protected Detector $geoDetector
    )
    {
    }

    public function filterDishes(array $filterArray)
    {
        $dish = $this->dishModel->getTable();
        $cook = (new Cook())->getTable();
        $dishToTag = $this->dishToTag->getTable();
        $dishToAdditionalCategory = $this->dishesToAdditionalCategories->getTable();

        $query = $this->dishModel->query();

        $query
            ->select(["$dish.*"])
            ->with('attachment')
            ->with('cook')
            ->with('cook.user')
            ->with('labels')
            ->leftJoin($cook, "$cook.id", '=', "$dish.cook_id")
            ->where("$dish.active", 1)
            ->where("$cook.active", 1)
            ->where("$cook.verified", 1);

        if (!empty($filterArray['price'])) {
            if (isset($filterArray['price'][0])) {
                $query->where("$dish.price", '>=', $filterArray['price'][0]);
            }

            if (isset($filterArray['price'][1])) {
                $query->where("$dish.price", '<=', $filterArray['price'][1]);
            }
        }

        if (empty($filterArray['all'])) {
            $location = $this->geoDetector->getClientLocation();
            $query->where("$cook.city_id", '=', $location->id);
        }

        if (!empty($filterArray['category'])) {
            foreach ($filterArray['category'] as $category) {
                $query->leftJoin("$dishToAdditionalCategory as category$category",
                                 "$dish.id",
                                 '=',
                                 "category$category.dish_id");

                $query->where(function ($query) use ($dish, $filterArray, $category) {
                    $query
                        ->where("$dish.category_id", $filterArray['category'])
                        ->orWhere("category$category.category_id", $filterArray['category']);

                });
            }
            // Иначе из-за доп категорий будет несколько одинаковых товаров
            $query->groupBy("$dish.id");
        }

        if (!empty($filterArray['tags'])) {
            foreach ($filterArray['tags'] as $tag) {
                $query->leftJoin("$dishToTag as tag$tag", "$dish.id", '=', "tag$tag.dish_id");
                $query->where("tag$tag.tag_id", $tag);
            }
        }

        if (!empty($filterArray['search'])) {
            $query->where("$dish.name", 'LIKE', '%' . $filterArray['search'] . '%');
        }

        $sessionHash = session()->getId();
        $sessionHash = substr($sessionHash, 0, 8);
        $num = unpack("n", base64_decode($sessionHash));
        $num = reset($num);
        $query->orderBy('named_sort', 'asc');
        $query->orderBy(DB::raw("rand($num)"));

        /** @var LengthAwarePaginator $pagination */
        $pagination = $query->paginate(static::PAGE_SIZE);
        $pageData = BaseHelper::preparePageData($pagination, $filterArray);
        $dishes = $pagination->items();

        return [$dishes, $pageData];
    }

    public function suggests(string $name) {
        $dishModel = new Dish;
        $main = $dishModel->getTable();
        $cook = (new Cook())->getTable();
        $query = $dishModel->query();

        return $query
            ->select(["$main.name"])
            ->distinct()
            ->leftJoin($cook, "$cook.id", '=', "$main.cook_id")
            ->orderBy("$main.name", 'asc')
            ->where("$main.active", 1)
            ->where("$cook.active", 1)
            ->where("$cook.verified", 1)
            ->where("$main.name", 'LIKE', '%' . $name . '%')
            ->limit(10)
            ->get();
    }

    public function getFilterFields($currentFilter = []): array
    {
        $dishCategory = $this->dishCategoryModel->getTable();
        $dish = $this->dishModel->getTable();
        $cook = $this->cookModel->getTable();
        $tag = $this->tagModel->getTable();
        $dishToTag = $this->dishToTag->getTable();

        // Все блюда или только текущий город
        $city = [
            'name'        => 'all',
            'type'        => 'toggle',
            'label'       => 'По городу',
            'collapsable' => false,
            'items'       => [
                [
                    'value' => '',
                    'label' => 'Текущий',
                    'checked' => empty($currentFilter['all']),
                ],
                [
                    'value' => '1',
                    'label' => 'Все города',
                    'checked' => !empty($currentFilter['all']),
                ],
            ]
        ];

        // Цена
        $priceData = DB::query()
            ->from($dish)
            ->select([
                DB::raw('min(price) as min'),
                DB::raw('max(price) as max'),
            ])
            ->leftJoin($cook, "$dish.cook_id", '=', "$cook.id")
            ->where("$dish.active", '=', '1')
            ->where("$cook.active", '=', '1')
            ->where("$cook.verified", '=', '1')
            ->get()
            ->first();

        $price = $priceData ? [
            'name'        => 'price',
            'type'        => 'range',
            'label'       => 'Цена, &#8381;',
            'collapsable' => true,
            'range' => [
                'min'    => 0,
                'max'    => $priceData->max,
                'curMin' => (int)($currentFilter['price'][0] ?? 0),
                'curMax' => (int)($currentFilter['price'][1] ?? $priceData->max),
            ]
        ] : null;

        // Категории
        $dishCategories = $this->dishCategoryModel->query()
            ->select("$dishCategory.*", DB::raw('count(*) as total'))
            ->leftJoin($dish, "$dishCategory.id", '=', "$dish.category_id")
            ->leftJoin($cook, "$dish.cook_id", '=', "$cook.id")
            ->where("$dish.active", '=', '1')
            ->where("$cook.active", '=', '1')
            ->where("$cook.verified", '=', '1')
            ->groupBy("$dishCategory.id")
            ->get();

        $dishAdditionalCategories = $this->dishCategoryModel->getDishAdditionalCategories($dish, $cook, $dishCategory);

        $categoryBlock = [
            'type'        => 'checkbox',
            'label'       => 'Все категории',
            'name'        => 'category',
            'collapsable' => false,
            'items'       => $dishCategories
                ->map(function(DishCategory $cat) use ($currentFilter, $dishAdditionalCategories) {
                    $additionalCategory = $dishAdditionalCategories[$cat->id] ?? null;
                    if ($additionalCategory) {
                        $total = $cat->total + ($additionalCategory->total ?? 0);
                    }
                    return [
                        'value' => $cat->id,
                        'label' => $cat->getMenuTitle() . '<sup>' . ($total ?? $cat->total) . '</sup>',
                        'checked' => in_array($cat->id . '', $currentFilter['category'] ?? []),
                    ];
                })
            ->toArray(),
        ];

        // Теги (вкусовые предпочения)
        $tags = $this->tagModel->query()
            ->select("$tag.*", DB::raw('count(*) as total'))
            ->leftJoin($dishToTag, "$tag.id", '=', "$dishToTag.tag_id")
            ->leftJoin($dish, "$dish.id", '=', "$dishToTag.dish_id")
            ->leftJoin($cook, "$dish.cook_id", '=', "$cook.id")
            ->groupBy("$tag.id")
            ->where("$tag.active", 1)
            ->where("$dish.active", '=', '1')
            ->where("$cook.active", '=', '1')
            ->where("$cook.verified", '=', '1')
            ->get();

        $tagsBlock = [
            'type'        => 'checkbox',
            'label'       => 'Вкусовые предпочтения',
            'name'        => 'tags',
            'collapsable' => true,
            'items'       => $tags
                ->map(function(Tag $tag) use ($currentFilter) {
                    return [
                        'value' => $tag->id,
                        'label' => $tag->name . '<sup>' . $tag->total . '</sup>',
                        'checked' => in_array($tag->id . '', $currentFilter['tags'] ?? []),
                    ];
                })
                ->toArray(),
        ];

        return array_filter([
            $city,
            $categoryBlock,
            $price,
            $tagsBlock,
        ]);
    }
}
