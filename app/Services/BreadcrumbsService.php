<?php

namespace App\Services;

class BreadcrumbsService
{
    protected array $links = [
        [
            'link' => '/',
            'name' => 'Главная',
        ]
    ];

    public function add(string $name, string $link)
    {
        $this->links[] = [
            'link' => $link,
            'name' => $name,
        ];
    }

    public function getList(): array
    {
        return $this->links;
    }
}
