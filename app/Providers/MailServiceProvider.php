<?php

namespace App\Providers;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\ServiceProvider;

class MailServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        VerifyEmail::toMailUsing(function ($notifiable, $url) {
            return (new MailMessage())
                ->subject('Подтвердите ваш email')
                ->line('Нажмите на кнопку ниже, чтобы подтвердить ваш email-адрес')
                ->action('Подтвердить', $url)
                ->line('Если вы не создавали аккаунт на нашем сайте - просто игнорируйте это письмо');
        });

        ResetPassword::toMailUsing(function ($user, $token) {
            $minutes = config('auth.passwords.' . config('auth.defaults.passwords') . '.expire');
            $action = route('password.reset', ['token' => $token]) . '?email=' . $user->email;

            return (new MailMessage())
                ->subject('Сброс пароля от аккаунта')
                ->line('Вам отправлено это письмо, потому что для вашего аккаунта был запрошен сброс пароля')
                ->action('Сбросить пароль', $action)
                ->line("Эта ссылка истечёт через $minutes минут.")
                ->line('Если вы не запрашивали сброс пароля на вашем аккаунту - просто игнорируйте это письмо');
        });
    }
}
