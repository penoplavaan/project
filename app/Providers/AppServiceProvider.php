<?php

namespace App\Providers;

use App\Rules;
use App\Services\BasketService;
use App\Services\BreadcrumbsService;
use App\Services\Geo\Detector;
use App\Services\SeoService;
use App\Services\SettingsService;
use App\Services\SmsSender\SmsSenderInterface;
use App\Services\SmsService;
use App\Services\TextBlocksService;
use App\Services\TextComponentService;
use App\Services\UserService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Orchid\Crud\Screens\CreateScreen;
use Orchid\Crud\Screens\EditScreen;
use Orchid\Crud\Screens\ListScreen;
use Orchid\Crud\Screens\ViewScreen;
use Orchid\Platform\Dashboard;

class AppServiceProvider extends ServiceProvider
{

    protected array $rules = [
        Rules\PhoneRule::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BreadcrumbsService::class);
        $this->app->singleton(SeoService::class);
        $this->app->singleton(UserService::class);
        $this->app->singleton(BasketService::class);
        $this->app->singleton(TextComponentService::class);
        $this->app->singleton(SettingsService::class);
        $this->app->singleton(TextBlocksService::class);
        $this->app->singleton(Detector::class);

        Dashboard::useModel(\Orchid\Attachment\Models\Attachment::class, \App\Models\Attachment::class);
        Dashboard::useModel(\Orchid\Platform\Models\User::class, \App\Models\User::class);

        // переопределяем классы экранов пакета orchid/crud
        $this->app->bind(ListScreen::class, \App\Orchid\Screens\Crud\ListScreen::class);
        $this->app->bind(CreateScreen::class, \App\Orchid\Screens\Crud\CreateScreen::class);
        $this->app->bind(EditScreen::class, \App\Orchid\Screens\Crud\EditScreen::class);
        $this->app->bind(ViewScreen::class, \App\Orchid\Screens\Crud\ViewScreen::class);

        $this->app->bind(SmsSenderInterface::class, SmsService::getSenderClass());
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dashboard $dashboard, TextComponentService $textComponentService)
    {
        $jsPath = '/orchid/js/app.bundle.js';
        $cssPath = '/build/css/style-admin.css';
        $jsAbsolutePath = public_path($jsPath);
        $cssAbsolutePath = public_path($cssPath);

        if (file_exists($jsAbsolutePath)) {
            $time = filemtime($jsAbsolutePath);
            $dashboard->registerResource('scripts', $jsPath . '?' . $time);
        }

        if (file_exists($cssAbsolutePath)) {
            $time = filemtime($cssAbsolutePath);
            $dashboard->registerResource('stylesheets', $cssPath . '?' . $time);
        }

        $textComponentService->registerComponent('text.button');
        $textComponentService->registerComponent('text.gallery');
        $this->registerValidationRules();
    }

    private function registerValidationRules()
    {
        foreach ($this->rules as $class) {
            $alias = (new $class())->__toString();
            if ($alias) {
                Validator::extend($alias, $class . '@passes');
            }
        }
    }
}
