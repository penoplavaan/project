<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\Dashboard;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard)
    {
        $permissions = ItemPermission::group('content')
            ->addPermission('content.settings', 'Настройки сайта')
            ->addPermission('content.refs', 'Справочники')
            ->addPermission('content.main', 'Контент для главной')
            ->addPermission('content.text', 'Текстовые блоки и страницы')
            ->addPermission('content.for-cooks', 'Блоки на странице "Для поваров"')
            ->addPermission('content.useful', 'Полезные материалы для поваров')
            ->addPermission('content.cook-reviews', 'Отзывы на поваров')
        ;
        $dashboard->registerPermissions($permissions);

        $permissions = ItemPermission::group('content-cooks')
            ->addPermission('content-cooks.read', 'Просмотр')
            ->addPermission('content-cooks.write', 'Редактирование')
        ;
        $dashboard->registerPermissions($permissions);

        $permissions = ItemPermission::group('content-dishes')
            ->addPermission('content-dishes.read', 'Просмотр')
            ->addPermission('content-dishes.write', 'Редактирование')
        ;
        $dashboard->registerPermissions($permissions);

        $permissions = ItemPermission::group('orders-list')
            ->addPermission('orders-list.read', 'Просмотр')
            ->addPermission('orders-list.write', 'Редактирование')
        ;
        $dashboard->registerPermissions($permissions);
    }
}
