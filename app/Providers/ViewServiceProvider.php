<?php

namespace App\Providers;

use App\View\Composers\BreadcumbComposer;
use App\View\Composers\EmailComposer;
use App\View\Composers\LayoutComposer;
use App\View\Composers\ProfileComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('partials.breadcrumbs', BreadcumbComposer::class);
        View::composer('layout.*', LayoutComposer::class);
        View::composer('partials.layout.*', LayoutComposer::class);

        View::composer('mail.*', EmailComposer::class);

        View::composer('profile.*', ProfileComposer::class);

        View::addNamespace('orchid', resource_path('/orchid/views'));
    }
}
