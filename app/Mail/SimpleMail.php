<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SimpleMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    private string $type = 'markdown';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        public $template,
        protected $templateData = []
    ) {
        $from = setting('mail.sender.from')->getText();
        $confMailType = config('mail.default');
        $confMailFrom = config('mail.from.address');

        if ($confMailType === 'smtp' && $confMailFrom != $from) {
            $from = $confMailFrom;
        }

        $this->from($from, config('app.ruName'));
    }

    public function setType(string $type) {
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->type === 'markdown') {
            return $this->markdown('mail.' . $this->template, $this->templateData);
        } else {
            return $this->view('mail.' . $this->template, $this->templateData);
        }
    }
}
