<?php

namespace App\Rules;

use App\Helpers\ViewHelper;
use Illuminate\Contracts\Validation\Rule;

class PhoneRule implements Rule
{
    protected string $alias = 'phone';


    public function __toString()
    {
        return $this->alias;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $phone = ViewHelper::clearPhone($value);
        return strlen($phone) == 11;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Некорректный телефон';
    }
}
