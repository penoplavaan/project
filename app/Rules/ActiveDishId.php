<?php

namespace App\Rules;

use App\Models\Dish;
use Illuminate\Contracts\Validation\Rule;

class ActiveDishId implements Rule
{
    /**
     * Правила валидации.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @var Dish $dish */
        $dish = Dish::query()->where('id', (int)$value)->get()->first();
        if (empty($dish) || !$dish->active) {
            return false;
        }
        return true;
    }

    /**
     * Вывод сообщений.
     *
     * @return string
     */
    public function message()
    {
        return 'Блюдо не активно';
    }
}
