<?php

declare(strict_types=1);

namespace App\Dto\Basket;

use App\Models\Dish;

class BasketRow
{
    public function __construct(
        protected Dish|null $dish,
        protected int $count,
    ) {
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return Dish|null
     */
    public function getDish(): ?Dish
    {
        return $this->dish;
    }
}
