<?php

declare(strict_types=1);

namespace App\Dto\Basket;

use App\Models\Cook;

/**
 * DTO содержимого корзины по повару
 */
class BasketByCook
{
    public function __construct(
        protected Cook $cook,
        protected array $rows,
    ) {
    }

    /**
     * @return Cook
     */
    public function getCook(): Cook
    {
        return $this->cook;
    }

    /**
     * @return array<BasketRow>
     */
    public function getRows(): array
    {
        return $this->rows;
    }
}
