<?php

namespace App\View\Composers;

use App\Http\Resources\FooterSocialsResource;
use App\Models\Cook;
use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\MenuItem;
use App\Services\BreadcrumbsService;
use App\Services\Geo\Detector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class LayoutComposer
{

    private static ?MenuItem $headerTopMenu = null;

    public function __construct(
        protected BreadcrumbsService $breadcrumbsService,
        protected Detector $geoDetector,
    )
    {
        if (static::$headerTopMenu === null) {
            static::$headerTopMenu = MenuItem::tree('top');
            $topMenuItemsCollection = static::$headerTopMenu->getChildren();
            $topMenuMenuItem = $topMenuItemsCollection->keyBy('code')->get('menu');

            if ($topMenuMenuItem) {
                $dishCategoryModel = new DishCategory();
                $main = $dishCategoryModel->getTable();
                $dish = (new Dish)->getTable();
                $cook = (new Cook())->getTable();

                $dishCategories = $dishCategoryModel->query()
                    ->select("$main.*", DB::raw('count(*) as total'))
                    ->leftJoin($dish, "$main.id", '=', "$dish.category_id")
                    ->leftJoin($cook, "$dish.cook_id", '=', "$cook.id")
                    ->where("$dish.active", '=', '1')
                    ->where("$cook.active", '=', '1')
                    ->where("$cook.verified", '=', '1')
                    ->groupBy("$main.id")
                    ->get();

                $topMenuMenuItem->setDishesSubmenu($dishCategories);
            }
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $styleFile = 'build/css/style.css';
        $stylePath = public_path($styleFile);
        $view->with('stylePath', '/' . $styleFile . '?' . (file_exists($stylePath) ? filemtime($stylePath) : '0'));

        $view->with('headerTopMenu', static::$headerTopMenu->getChildren());

        $view->with('footerTermsMenu', MenuItem::tree('footer-terms')->getChildren());
        $view->with('footerMobileMenu', MenuItem::tree('footer-mobile')->getChildren());
        $view->with('footerSocials', FooterSocialsResource::collection(settingSection('footer.icons'))->resolve());

        $user = auth()->user();
        $view->with('isAuth', !empty($user));
        $view->with('isCook', !empty($user) && $user->isCook());

        $view->with('headerCity', $this->geoDetector->getClientLocation());
        $view->with('headerCityDetection', $this->geoDetector->getDetectionType());
    }
}
