<?php

namespace App\View\Composers;

use App\Http\Resources\FooterSocialsResource;
use Illuminate\View\View;

class EmailComposer
{

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('footerSocials', FooterSocialsResource::collection(settingSection('footer.icons'))->resolve());

        $mailDomain = config('app.url');
        $view->with('mailDomain', $mailDomain);
    }
}
