<?php

namespace App\View\Composers;

use App\Models\MenuItem;
use App\Models\User;
use App\Services\UserService;
use Illuminate\View\View;

class ProfileComposer
{

    public function compose(View $view)
    {
        /** @var User $user */
        $user = app()->make(UserService::class)->current();

        $navigation = [
            [
                'name' => 'Профиль',
                'link' => route('profile.index', [], false),
            ],/*
            [
                'name' => 'Избранное<sup>18</sup>',
                'link' => '/06-02-profile.php',
            ],*/
            [
                'name' => 'Чаты',
                'link' => route('profile.chat-list', [], false),
            ],
            $user->isCook() ? [
                'name' => 'Мои блюда',
                'link' => route('profile.dishes', [], false),
            ] : null,
            $user->isCook() ? [
                'name' => 'Полезные материалы',
                'link' => route('profile.useful', [], false),
            ] : null,
        ];

        $navigation = array_filter($navigation);

        $view->with('profileNavigation', $navigation);
    }
}
