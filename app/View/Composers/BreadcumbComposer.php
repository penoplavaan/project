<?php

namespace App\View\Composers;

use App\Services\BreadcrumbsService;
use Illuminate\View\View;

class BreadcumbComposer
{

    public function __construct(protected BreadcrumbsService $breadcrumbsService)
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('breadcrumbs', $this->breadcrumbsService->getList());
    }
}
