<?php

namespace App\View\Components;

use Illuminate\View\Component;

class StaticSpoiler extends Component
{
    public function render()
    {
        return view('components.static-spoiler', [
            'hideText' => 'Скрыть',
            'showText' => 'Прочитать подробнее',
        ]);
    }
}
