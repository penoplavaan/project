<?php

namespace App\View\Components;


use Illuminate\View\Component;


class Cookie extends Component
{
    public function render()
    {
        if (!empty($_COOKIE['acceptCookie'])) {
            return '';
        }

        $data = [
            'text'        => setting('cookie.text')->getText(),
            'description' => strip_tags(setting('cookie.text')->getBigText(), '<a>'),
            'btnText'     => setting('cookie.btnText')->getText(),
        ];

        if (empty($data['text']) || empty($data['description']) || empty($data['btnText'])) {
            return '';
        }

        return view('components.cookie', ['data' => $data]);
    }
}
