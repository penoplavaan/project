<?php

namespace App\View\Components\Text;

interface ITextEmbedableCache {
    /**
     * @return string[]
     */
    public static function getEmbeddableCacheTags(): array;
}
