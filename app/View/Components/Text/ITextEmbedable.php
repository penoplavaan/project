<?php

namespace App\View\Components\Text;


use Illuminate\Support\Collection;

interface ITextEmbedable {
    public static function getTitle(): string;
    public static function getParams(): Collection;
    public static function renderComponent(array $params): string;
}
