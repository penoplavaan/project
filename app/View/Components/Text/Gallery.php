<?php

namespace App\View\Components\Text;

use Illuminate\Support\Collection;
use Orchid\Attachment\Models\Attachment;


class Gallery extends BaseComponent implements ITextEmbedable
{

    protected int $galleryId = 0;

    public function setGalleryId(int $galleryId): static
    {
        $this->galleryId = $galleryId;
        return $this;
    }

    public static function getTitle(): string
    {
        return 'Галерея';
    }

    protected function getView(): string
    {
        return 'components.text.gallery';
    }

    protected function getViewVariable(): array
    {
        $gallery = \App\Models\Gallery::find($this->galleryId);

        return [
            'slideList' => $gallery->attachment->map(fn(Attachment $attachment) => [
                'image' => $attachment?->getImage()->resize('gallery.attachment')->getData(),
                'thumb' => $attachment?->getImage()->resize('gallery.thumb')->getData(),
            ])
        ];
    }

    public static function getParams(): Collection
    {
        $galleryIdList = \App\Models\Gallery::limit(300)->pluck('name', 'id');

        $params = new Collection();
        $params->add([
            'name'   => 'galleryId',
            'type'   => 'selectbox',
            'label'  => 'Галерея',
            'items'  => $galleryIdList->map(fn($name, $key) => ['text' => $name, 'value' => (string) $key])->values()->all()
        ]);

        return $params;
    }

    public static function normalizeParams(array $params): array {
        $params['galleryId'] = strval($params['galleryId'] ?? 0);

        return $params;
    }

    public static function renderComponent(array $params): string
    {
        $params = static::normalizeParams($params);
        $instance = (new static)->setGalleryId($params['galleryId']);

        return $instance->shouldRender() ? $instance->render()->toHtml() : "";
    }
}
