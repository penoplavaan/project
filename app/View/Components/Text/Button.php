<?php

namespace App\View\Components\Text;

use Illuminate\Support\Collection;


class Button extends BaseComponent implements ITextEmbedable
{

    protected string $caption  = '';
    protected string $url      = '';

    public function setCaption(string $caption): static
    {
        $this->caption = $caption;
        return $this;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;
        return $this;
    }

    public static function getTitle(): string
    {
        return 'Кнопка';
    }

    protected function getView(): string
    {
        return 'components.text.button';
    }

    protected function getViewVariable(): array
    {
        return [
            'caption' => $this->caption,
            'url'     => $this->url,
        ];
    }

    public static function getParams(): Collection
    {
        $params = new Collection();
        $params->add([
             'name'   => 'caption',
             'type'   => 'input',
             'label'  => 'Текст кнопки',
        ]);

        $params->add([
             'name'  => 'url',
             'type'  => 'input',
             'label' => 'Ссылка',
         ]);

        return $params;
    }

    public static function normalizeParams(array $params): array {
        $params['caption'] = strval($params['caption'] ?? "");
        $params['url']     = strval($params['url'] ?? "");

        return $params;
    }

    public static function renderComponent(array $params): string
    {
        $params = static::normalizeParams($params);
        $instance = (new static)->setCaption($params['caption'])->setUrl($params['url']);

        return $instance->shouldRender() ? $instance->render()->toHtml() : "";
    }
}
