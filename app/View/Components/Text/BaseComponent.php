<?php


namespace App\View\Components\Text;

use App\Helpers\BaseHelper;
use Cache;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;
use Illuminate\View\Component;

abstract class BaseComponent extends Component {
    /** @var int Время кеширования, в секундах */
    protected int $cacheTime = 0;
    protected array $cacheTags = [];
    protected bool $todo = false;

    public function shouldRender() {
        return (!$this->todo || !BaseHelper::isProd())
            && (!request()->isXmlHttpRequest() || (request()->isXmlHttpRequest() && $this->isMyRequest()));
    }

    protected function isMyRequest(): bool {
        return request()->get('ajaxId') === $this->getAjaxId();
    }

    protected function needClearOutput(): bool {
        return false;
    }

    protected function getAjaxId(): string {
        return md5(static::class);
    }

    public static function getTitle(): string
    {
        return '';
    }

    protected abstract function getViewVariable(): array;

    protected abstract function getView(): string;

    /**
     * Метод должен возвращать массив тегов, от которых зависит компонент
     *
     * @return array
     */
    public static function prepareCacheTags(): array {
        return [];
    }

    protected function getCacheKey(): string {
        return "";
    }

    protected function getCacheTime(): int {
        return $this->cacheTime;
    }

    protected function setCacheTime(int $cacheTime = CACHE_TIME): BaseComponent {
        $this->cacheTime = $cacheTime;
        return $this;
    }

    protected function addCacheTag(string $tag): BaseComponent {
        $this->cacheTags[$tag] = true;
        return $this;
    }

    protected function addCacheTags(array $tags): BaseComponent {
        $this->cacheTags = array_merge($this->cacheTags, array_fill_keys($tags, true));

        return $this;
    }

    protected function removeCacheTag(string $tag): BaseComponent {
        unset($this->cacheTags[$tag]);
        return $this;
    }

    protected function getCacheTags(): array {
        return array_merge(static::prepareCacheTags(), array_keys(array_filter($this->cacheTags)));
    }

    /**
     * Срабатывает перед сохранением данных в кэш.
     * На вход получает данные для кэширования
     * Если вернуть false - данные не будут закэшированы
     *
     * @param array $data
     * @return bool
     * @noinspection PhpUnusedParameterInspection
     */
    protected function beforeCacheSave(array $data): bool {
        return true;
    }

    /**
     * Описывать в наследниках, если в них нужно кэширование.
     * Должна возвращать данные, которые необходимо положить в кэш.
     * Вызывается методом getCachedData
     */
    protected function getData(): array {
        return [];
    }

    /**
     * Обертка над getData, реализующая кэширование.
     * Возвращает все те же самые данные, что и getData
     * @return array
     */
    protected function getCachedData(): array {
        $cacheTags = $this->getCacheTags();
        $cacheTime = $this->getCacheTime();
        $cacheKey = $this->getCacheKey();

        // кэш не включен, получаем данные как обычно
        if (!$cacheTime || !mb_strlen($cacheKey)) {
            return $this->getData();
        }

        $cache = null;
        if (empty($cacheTags)) {
            // Теги не указаны, работаем с обычным кешированием
            /** @var \Illuminate\Cache\Repository $cache */
            $cache = \App::make('cache');
        } elseif (Cache::supportsTags()) {
            // Указаны теги, подключаем тегированный кеш, если доступен
            $cache = Cache::tags($cacheTags);
        }

        // Не удалось получить объект для работы с кэшем, возвращаем данные напрямую
        if (!$cache) {
            return $this->getData();
        }

        $data = $cache->get($cacheKey);

        // данных в кэше нет, добавляем, если нужно
        if (is_null($data)) {
            $data = $this->getData();
            if ($this->beforeCacheSave($data)) {
                $cache->put($cacheKey, $data, $cacheTime);
            }
        }

        return $data;
    }

    public function render() {
        $viewVariable = $this->getViewVariable();

        if (request()->isXmlHttpRequest()) {
            if ($this->needClearOutput()) {
                ob_clean();
            }

            \response()->json($viewVariable)->send();
            exit;
        }

        return view($this->getView(), array_merge([
                                                      'ajaxId' => $this->getAjaxId(),
                                                  ], $viewVariable));
    }

    protected function isTemplateValid(string $name): bool {
        try {
            $factory = Container::getInstance()->make('view');
        } catch (BindingResolutionException $e) {
            Log::channel('daily')->debug($e->getMessage());

            return false;
        }

        return $factory->exists($name);
    }
}
