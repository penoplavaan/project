<?php

namespace App\Scopes;

use App\Orchid\OrchidDetector;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class IdDescSortScope implements Scope
{

    public function apply(Builder $builder, Model $model)
    {
        if (!OrchidDetector::isOrchid()) {
            return;
        }

        $builder->orderBy('id', 'desc');
    }
}
