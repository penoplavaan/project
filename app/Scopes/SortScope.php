<?php

namespace App\Scopes;

use App\Orchid\OrchidDetector;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class SortScope implements Scope
{

    public function apply(Builder $builder, Model $model)
    {
        if (OrchidDetector::isOrchid()) {
            return;
        }

        if (method_exists($model, 'hasSortFlag') && $model->hasSortFlag()) {
            $builder->orderBy('sort');
        }
    }
}
