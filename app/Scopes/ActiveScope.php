<?php

namespace App\Scopes;

use App\Orchid\OrchidDetector;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ActiveScope implements Scope
{

    public function apply(Builder $builder, Model $model)
    {
        if (OrchidDetector::isOrchid()) {
            return;
        }

        if (method_exists($model, 'hasActiveFlag') && $model->hasActiveFlag()) {
            $builder->where($model->column('active'), true);
        }
    }
}
