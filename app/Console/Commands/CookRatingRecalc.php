<?php

namespace App\Console\Commands;

use App\Models\Cook;
use App\Models\CookReview;
use Illuminate\Console\Command;

class CookRatingRecalc extends Command
{
    protected $signature = 'cook:rating-recalc {id? : ID повара}';

    protected $description = 'Пересчёт рейтинга всех поваров, либо только указанного. Можно указать несколько ID через запятую.';

    public function handle()
    {
        $cookIds = $this->input->getArgument('id');
        if (!empty($cookIds)) {
            $cookIds = explode(',', $cookIds);
            $cookIds = array_map(fn($obj) => (int)trim($obj), $cookIds);
            $cookIds = array_filter($cookIds, fn($obj) => !empty($obj));
        }

        $filter = [];
        if ($cookIds) {
            $cookTable = (new Cook)->getTable();
            $filter = ["$cookTable.id" => $cookIds];
        }

        CookReview::recalcCookRating($filter);

        return 0;
    }
}
