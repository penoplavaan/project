<?php

namespace App\Console\Commands;

use App\Models\Cook;
use App\Models\Dish;
use App\Models\DishCategory;
use App\Models\TextPage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Sitemap extends Command
{
    protected $signature = 'sitemap';

    protected $description = 'Сгенерировать sitemap.xml';
    /**
     * @var false|resource
     */
    private string $generationPath;
    private $xmlFile;

    public function handle()
    {
        // Что должно быть в сайтмапе
        // 1. главная
        // 2. все текстовые страницы (из таблицы text_pages)
        // 3. роуты основных разделов (/menu, /cooks)
        // 4. роуты фиксированных страниц (лендинг Поварам)
        // 5. Ссылки на активных поваров
        // 6. Ссылки на блюда поваров (????)
        // 7. Ссылки на разделы блюд в меню

        $domain = config('app.url');
        $cookTable = (new Cook)->getTable();
        $dishTable = (new Dish)->getTable();
        $dishCategoryTable = (new DishCategory())->getTable();

        $this->openFile();
        $this->writeLine($domain);
        $this->writeLine(route('for-cooks.index'));
        $this->writeLine(route('cooks.index'));
        $this->writeLine(route('menu.index'));

        $dishCategories = DishCategory::query()
            ->select("$dishCategoryTable.*", DB::raw('count(*) as total'))
            ->leftJoin($dishTable, "$dishCategoryTable.id", '=', "$dishTable.category_id")
            ->leftJoin($cookTable, "$dishTable.cook_id", '=', "$cookTable.id")
            ->where("$dishTable.active", '=', '1')
            ->where("$cookTable.active", '=', '1')
            ->where("$cookTable.verified", '=', '1')
            ->groupBy("$dishCategoryTable.id")
            ->get();

        foreach ($dishCategories as $dishCategory) {
            if (!$dishCategory->total) continue;
            $this->writeLine($dishCategory->getUrl());
        }
        unset($dishCategories);

        $cooks = Cook::query()->select('id')->where('active', 1)->where('verified', 1)->get();
        /** @var Cook $cook */
        foreach ($cooks as $cook) {
            $this->writeLine($cook->getUrl());
        }
        unset($cooks);

        Dish::query()
            ->select(["$dishTable.*"])
            ->leftJoin($cookTable, "$cookTable.id", '=', "$dishTable.cook_id")
            ->where("$dishTable.active", 1)
            ->where("$cookTable.active", 1)
            ->where("$cookTable.verified", 1)
            ->chunk(500, function ($dishes) {
                /** @var Dish $dish */
                foreach ($dishes as $dish) {
                    $this->writeLine($dish->getUrl());
                }
            });

        $textPages = TextPage::query()->get();
        /** @var TextPage $textPage */
        foreach ($textPages as $textPage) {
            $this->writeLine($textPage->getUrl());
        }
        unset($textPages);

        if ($this->finishFile()) {
            $this->addToRobots();
        } else {
            $this->error('Не получилось перенести сгенерированный файл. Проверьте права на запись в папку /public/');
        }

        return 0;
    }

    protected function openFile()
    {
        $this->generationPath = storage_path('sitemap-' . date('Y-m-d-H-i-s') . '.xml');
        $this->xmlFile = fopen($this->generationPath, "w");

        fwrite(
            $this->xmlFile,
            '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL
        );
    }

    protected function writeLine($url)
    {
        fwrite($this->xmlFile, sprintf('<url><loc>%s</loc></url>' . PHP_EOL, $url));
    }

    protected function finishFile()
    {
        fwrite($this->xmlFile, '</urlset>');
        fclose($this->xmlFile);

        $outputPath = public_path('sitemap.xml');
        if (file_exists($outputPath)) {
            unlink($outputPath);
        }

        return rename($this->generationPath, $outputPath);
    }

    protected function addToRobots()
    {
        $robotsPath = public_path('robots.txt');
        if (!file_exists($robotsPath)) return;

        $url = config('app.url') . '/sitemap.xml';
        $sitemapLine = 'Sitemap: ' . $url . PHP_EOL;

        $robotsContent = file($robotsPath);
        $foundSitemap = false;

        foreach ($robotsContent as $index => $line) {
            if (str_starts_with(mb_strtolower($line), 'sitemap:')) {
                $robotsContent[$index] = $sitemapLine;
                $foundSitemap = true;
            }
        }

        if (!$foundSitemap) {
            $robotsContent[] = PHP_EOL;
            $robotsContent[] = $sitemapLine;
        }

        file_put_contents($robotsPath, implode('', $robotsContent));
    }
}
