<?php

namespace App\Console\Commands;

use App\Mail\SimpleMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendTestEmail extends Command
{

    protected $signature = 'test:send-email {email : Почта получателя}';

    protected $description = 'Отправить тестовое письмо на указанный email';

    public function handle()
    {
        $email = $this->input->getArgument('email');
        if (empty($email)) {
            throw new \Exception('Укажите email!');
        }

        $paragraphs = [
            'Текст тестового письма, строка 1 ',
            'Текст тестового письма, строка 2 ',
            'Текст тестового письма, строка 3 ',
        ];

        $mail = new SimpleMail('new-review', [
            'header'    => 'Отзыв на повара Тестов Тест Тестович',
            'paragraph' => $paragraphs,
            'text'      => "Текст отзыва на повара, полный",
            'url'       => route('platform.resource.edit', ['resource' => 'cook-review-resources', 'id' => 1]
            ),
        ]);

        $mail->setType('html');
        $mail->subject('Пользователь оставил отзыв на повара');

        Mail::to($email)->send($mail);

        return 0;
    }
}
