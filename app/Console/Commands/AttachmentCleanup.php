<?php

namespace App\Console\Commands;

use App\Models\Attachment;
use Illuminate\Console\Command;
use Orchid\Attachment\Models\Attachmentable;

class AttachmentCleanup extends Command
{
    protected $signature = 'attachment:cleanup';

    protected $description = 'Удалить все НЕ привязанные к сущностям файлы, загруженные более 1 суток назад';

    public function handle()
    {

        $main = (new Attachment)->getTable();
        $pivot = (new Attachmentable)->getTable();

        $list = Attachment::query()
            ->select(["$main.*"])
            ->leftJoin($pivot, "$main.id", '=', "$pivot.attachment_id")
            ->whereNull("$pivot.attachment_id")
            ->where('updated_at', '<', \Carbon\Carbon::now()->subDay()) // Загруженные/изменённые больше суток назад
            ->limit(100)
            ->get();

        foreach ($list as $oldAttach) {
            $oldAttach->delete();
        }

        return 0;
    }
}
