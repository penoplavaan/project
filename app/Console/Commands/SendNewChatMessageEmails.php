<?php

namespace App\Console\Commands;

use App\Mail\SimpleMail;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class SendNewChatMessageEmails extends Command
{
    protected $signature = 'chat:send-notifications';

    protected $description = 'Отправить уведомления о новых непрочитанных сообщенях в чатах';

    /*** @var User[] */
    protected array $userObjectsCache = [];

    public function handle(Chat $chatModel, ChatMessage $chatMessageModel)
    {
        $setting = setting('chat.send-unread-email')->text ?? '';
        if ($setting === '' || ((int)$setting === -1)) {
            // Отправка оповещений отключена
            return 0;
        }

        // Смотрим только чаты, которые менялись за последние сутки
        $lastDay = (new Carbon())->subDay();
        /** @var Chat[]|Collection $lastDayChats */
        $lastDayChats = $chatModel->query()
            ->where('updated_at', '>', $lastDay) // Обновлённые за последние сутки
            ->where('from_email_sent', 0) // по которым ещё не отправлены оповещения
            ->where('to_email_sent', 0)
            ->get();

        $now = new Carbon();

        foreach ($lastDayChats as $chat) {
            // last_read_id совпадают - у всех всё прочитано, пропускаем
            if ($chat->from_last_read_id == $chat->to_last_read_id) continue;

            // Если какое-то из оповещений уже отправлено - пропускаем
            if ($chat->from_email_sent || $chat->to_email_sent) continue;

            // Для меньшего last_read_id получаем это сообщение в чате, и по его дате проверяем - надо ли слать уведомление
            $minLastReadId = min([(int)$chat->from_last_read_id, (int)$chat->to_last_read_id]);

            /** @var ChatMessage $message */
            $message = $chatMessageModel->query()
                ->where('chat_id', $chat->id)
                ->where('id', '>', $minLastReadId)
                ->orderBy('id')
                ->limit(1)
                ->get()
                ->first();

            if (!$message) continue;

            // По дате этого сообщения и настройке chat.send-unread-email проверяем, не пора ли отправить уведомление
            if ($message->created_at) {
                $diffInMinutes = $message->created_at->diffInMinutes($now);
                if ($diffInMinutes > $setting) {
                    // Надо отправлять оповещение
                    // Сначала - обновить объект чата, чтобы при падении отправки уведомления не слалось повторное

                    if ($chat->from_user_id === $message->user_id) {
                        $chat->to_email_sent = 1;
                    } else {
                        $chat->from_email_sent = 1;
                    }
                    $chat->save();

                    $this->sendNotification($chat, $message);
                }
            }
        }

        return 0;
    }

    protected function loadUsers(array $userIds) {
        foreach ($userIds as $ind => $userId) {
            if (isset($this->userObjectsCache[$userId])) {
                unset($userIds[$ind]);
            }
        }

        $userIds = array_filter($userIds);
        if (empty($userIds)) return;

        $users = User::query()->whereIn('id', $userIds)->get();
        foreach ($users as $user) {
            $this->userObjectsCache[$user->id] = $user;
        }
    }

    /**
     * Отправка одного оповещения
     * @param Chat $chat
     * @param ChatMessage $message
     * @return void
     */
    protected function sendNotification(Chat $chat, ChatMessage $message) {
        $this->loadUsers([$chat->from_user_id, $chat->to_user_id]);

        // Если сообщение отправил один пользователь - оповещение надо слать второму из чата
        $messageAuthorId = $message->user_id;
        $messageAuthor = $this->userObjectsCache[$messageAuthorId];

        $userToSendNotificationId = $chat->getOtherUserId($messageAuthorId);
        $userToSendNotification = $this->userObjectsCache[$userToSendNotificationId];

        $paragraphs = [$message->message];
        $list = [];
        if ($message->order && $message->order->orderToDishes && $message->order->orderToDishes->count()) {
            $paragraphs[] = 'Список блюд в заказе:';

            foreach ($message->order->orderToDishes as $link) {
                $list[] = sprintf(
                    '%s, %dшт x %d руб = %d руб',
                    $link->name,
                    $link->count,
                    $link->price_one,
                    $link->count * $link->price_one
                );
            }
        }


        $mail = new SimpleMail('new-chat-message', [
            'header' => 'Новое сообщение в&nbsp;чате',
            'subheader' => 'От пользователя ' . $messageAuthor->getFullName(),
            'paragraph' => $paragraphs,
            'list' => $list,
            'url' => route('profile.chat-list') . '#u' . $messageAuthor->id,
        ]);

        $mail->setType('html');
        $mail->subject('Новое сообщение в чате');

        Mail::to($userToSendNotification->email)->send($mail);
    }
}
