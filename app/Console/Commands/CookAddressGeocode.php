<?php

namespace App\Console\Commands;

use App\Models\Cook;
use App\Services\YandexApiService;
use Illuminate\Console\Command;

class CookAddressGeocode extends Command
{
    protected $signature = 'cook:address-geocode {id? : ID повара}';

    protected $description = 'Для поваров с незаполнеными координатами выполнить геокодирование адреса. ID повара - принудительное геокодирование';

    public function handle()
    {
        $query = (new Cook)->query();

        $cookIds = $this->input->getArgument('id');
        if (!empty($cookIds)) {
            $cookIds = explode(',', $cookIds);
            $cookIds = array_map(fn($obj) => (int)trim($obj), $cookIds);
            $cookIds = array_filter($cookIds, fn($obj) => !empty($obj));
        }

        if ($cookIds) {
            $cooksWithoutCoords = $query
                ->whereIn('id', $cookIds)
                ->whereNot('address', '')
                ->get();
        } else {
            $cooksWithoutCoords = $query
                ->whereActive(1)
                ->whereVerified(1)
                ->whereNot('address', '')
                ->where(function ($query) {
                    $query->where('coords', '')
                        ->orWhere('coords', null);
                })
                ->get();
        }

        /** @var YandexApiService $yandexApi */
        $yandexApi = app()->make(YandexApiService::class);
        $yandexApi->setTimeout(30);

        foreach ($cooksWithoutCoords as $cook) {
            $address = trim($cook->address);
            if (empty($address)) continue;

            $coords = Cook::geocodeCookCoords($cook->address, true, $cook->id);
            if (!empty($coords)) {
                $cook->coords = $coords;
                $cook->save();
            }
        }

        return 0;
    }
}
