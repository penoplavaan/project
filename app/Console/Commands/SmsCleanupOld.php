<?php

namespace App\Console\Commands;

use App\Mail\SimpleMail;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\SmsCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class SmsCleanupOld extends Command
{
    protected $signature = 'sms:cleanup';

    protected $description = 'Удалить старые коды смс из таблицы';

    public function handle()
    {
        (new SmsCode())->cleanupOldCodes();
    }
}
