<?php

namespace App\Console;

use App\Console\Commands\AttachmentCleanup;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Отправка оповещений о новых сообщениях в чатах
        $schedule->command('chat:send-notifications')->everyMinute();

        // Для поваров с незаполненными координатами выполнить запрос на геокодирование адреса
        $schedule->command('cook:address-geocode')->everyFifteenMinutes();

        // Генерация сайтмапа сайта
        $schedule->command('sitemap')->daily();

        // Пересчёт рейтингов поваров
        $schedule->command('cook:rating-recalc')->daily();

        // Удалить старые смс-коды
        $schedule->command('sms:cleanup')->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
