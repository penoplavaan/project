<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Verified;

class SignInVerifiedUser
{
    public function handle(Verified $event)
    {
        $user = $event->user;

        auth()->login($user);

        return true;
    }
}
