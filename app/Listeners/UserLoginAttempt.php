<?php

namespace App\Listeners;

use App\Services\UserService;
use Illuminate\Auth\Events\Attempting;

class UserLoginAttempt
{
    public function handle(Attempting $event)
    {
        $credentials = $event->credentials;

        if (!empty($credentials['email'])) {
            $userService = app()->make(UserService::class);
            $user = $userService->getByEmail($credentials['email']);

            if ($user && !$user->email_verified_at) {
                if (str_starts_with(request()->path(), 'admin/')) {
                    abort(403);
                } else {
                    throw new \Exception('Емейл не подтверждён');
                }
            }
        }

        return true;
    }
}
