<?php

namespace App\Listeners;

use App\Services\BasketService;
use Illuminate\Auth\Events\Login;

/**
 * Объединить корзины при авторизации из кук и из базы
 */
class MergeBasketOnAuth
{
    public function handle(Login $event)
    {
        /** @var BasketService $basketService */
        $basketService = app(BasketService::class);
        $basketService->mergeBaskets();

        return true;
    }
}
