<?php

declare(strict_types=1);

namespace App\Forms;

/**
 * Форма "Заказать обратный звонок"
 *
 */
class CallbackForm extends BaseForm
{
    const SUBMIT_LABEL = 'Отправить';

    public function __construct()
    {
        parent::__construct('callback-form');

        $this->createPhone();
        $this->createRecaptcha();

        $this->createSubmit();
        $this->setAttribute('action', route('popup.callback.save'));
    }

}
