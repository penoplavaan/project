<?php

declare(strict_types=1);

namespace App\Forms;

use App\Forms\Validators\Email;
use App\Helpers\BaseHelper;
use App\Models\City;
use Illuminate\Support\Str;
use Laminas\Form\Element;
use Laminas\Form\Element\Select;
use Laminas\Form\View\Helper\AbstractHelper as FormAbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Renderer\RendererInterface;
use Revolution\LaminasForm\Form;
use Laminas\Validator;

class BaseForm extends Form
{
    const MAX_LENGTH_TEXT = 255;
    const MAX_LENGTH_TEXTAREA = 2000;

    const NAME = 'name';
    const NAME_LABEL = 'Фамилия и имя';
    const NAME_MAX_LENGTH = self::MAX_LENGTH_TEXT;
    const PHONE = 'phone';
    const PHONE_LABEL = 'Телефон';
    const PHONE_MAX_LENGTH = self::MAX_LENGTH_TEXT;

    const EMAIL = 'email';
    const EMAIL_LABEL = 'Email';
    const EMAIL_MAX_LENGTH = self::MAX_LENGTH_TEXT;

    const TEXT = 'text';
    const TEXT_LABEL = 'О себе';
    const TEXT_MAX_LENGTH = self::MAX_LENGTH_TEXTAREA;

    const PASSWORD = 'password';
    const PASSWORD_LABEL = 'Пароль';

    const MIN_LENGTH_PASSWORD = 6;
    const PASSWORD_MIN_LENGHT = self::MIN_LENGTH_PASSWORD;

    const ADVERTISEMENT = 'advertisement';
    const ADVERTISEMENT_LABEL = 'Я хочу получать рекламу';

    const RECAPTCHA = 'recaptcha';

    const SUBMIT = 'submit';
    const SUBMIT_LABEL = 'Отправить';

    const CODE = 'code';
    const CODE_LABEL = 'Код подтверждения';

    const CITY = 'city_id';
    const CITY_LABEL = 'Город';

    protected string $privacySetting = 'site.privacy-text';

    /**
     * Create a new form.
     *
     * @param null|string $name
     *
     * @return void
     */
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->createSubmit();
    }

    public static function extendAttributes()
    {
        $renderer = app(RendererInterface::class);
        if (!$renderer instanceof PhpRenderer) {
            return;
        }

        $plugins = ['formtext', 'formtel', 'formnumber', 'forminput', 'formhidden'];
        foreach ($plugins as $pluginName) {
            $plugin = $renderer->plugin($pluginName);
            if (!$plugin || !($plugin instanceof FormAbstractHelper)) {
                continue;
            }

            $plugin->addValidAttribute('inputmode');
            $plugin->addValidAttribute('autoload');
        }
    }

    public function getJsMessages(): array
    {
        return $this->getMessages();
    }

    public function toArray(): array
    {
        $result = [
            'action'     => $this->getAttribute('action'),
            'type'       => Str::kebab(Str::afterLast(get_class($this), '\\')),
            'fields'     => [],
            'options'    => $this->getOptions(),
            'attributes' => $this->getAttributes(),
            'privacy'    => $this->getPrivacyText(),
            'validation' => [],
        ];

        $this->getIterator()->rewind();
        foreach ($this->getIterator() as $element) {
            $result['fields'][$element->getName()] = static::toArrayElement($element);

            // Не добавляем пустые валидаторы, т.к. они в json кодируются не в пустой объект, а в пустой массив
            $validation = $this->buildValidation($element->getName());
            if (!empty($validation)) {
                $result['validation'][$element->getName()] = $validation;
            } else {
                $result['validation'][$element->getName()] = [];
            }
        }

        return $result;
    }

    public static function toArrayElement(Element $element): array
    {
        $info = [
            'type'        => Str::kebab(Str::afterLast(get_class($element), '\\')),
            'name'        => $element->getName(),
            'label'       => $element->getLabel(),
            'required'    => !!$element->getAttribute('required'),
            'disabled'    => !!$element->getAttribute('disabled'),
            'checked'     => !!$element->getAttribute('checked'),
            'multiple'    => !!$element->getAttribute('multiple'),
            'placeholder' => '',
            'options'     => array_diff_key($element->getOptions()),
            'attrs'       => $element->getAttributes(),
        ];

        if ($element instanceof Element\File && $element->getAttribute('multiple')) {
            $info['inputName'] = $element->getName() . '[]';
        }

        if (method_exists($element, 'getValueOptions')) {
            $info['valueOptions'] = $element->getValueOptions();
        }

        $value = $element->getValue();
        if ($value !== null) {
            $info['value'] = $value;
        } else {
            $info['value'] = '';
        }

        return $info;
    }

    protected function buildValidation(string $name): array
    {
        $result = [];

        if ($this->has($name)) {
            $element = $this->get($name);

            if ($element instanceof Element\Tel) {
                $result['phone'] = ['name' => 'phone'];
            }

            if ($element instanceof Element\Email) {
                $result['email'] = ['name' => 'email'];
            }
        }

        $inputFilter = $this->getInputFilter();
        if (!$inputFilter->has($name)) {
            return $result;
        }
        $input = $inputFilter->get($name);

        if ($input->isRequired()) {
            $result['required'] = ['name' => 'required'];
        }

        foreach ($input->getValidatorChain()->getValidators() as $validator) {
            switch (true) {
                case $validator['instance'] instanceof Validator\EmailAddress:
                    $result['email'] = [
                        'name' => 'email',
                        'params' => []
                    ];
                    break;
                case $validator['instance'] instanceof Validator\StringLength:
                    $result['stringLength'] = [
                        'min' => $validator['instance']->getMin(),
                        'max' => $validator['instance']->getMax()
                    ];
                    break;
                case $validator['instance'] instanceof Validator\Between:
                    $result['between'] = [
                        'min' => $validator['instance']->getMin(),
                        'max' => $validator['instance']->getMax()
                    ];
                    break;
                case $validator['instance'] instanceof Validator\NotEmpty:
                    $result['notEmpty'] = [
                        'name'   => 'notEmpty',
                        'params' => []
                    ];
                    break;
                case $validator['instance'] instanceof Validator\Identical:
                    $result['identical'] = [
                        'name'   => 'identical',
                        'params' => ['token' => $validator['instance']->getToken()]
                    ];
                    break;
                case $validator['instance'] instanceof Validator\File\IsImage:
                    $result['fileIsImage'] = [
                        'name'   => 'fileIsImage',
                        'params' => []
                    ];
                    break;
                case $validator['instance'] instanceof Validator\File\Extension:
                    $result['fileExt'] = [
                        'name'   => 'fileExt',
                        'params' => explode(',', $validator['instance']->getOption('extension'))
                    ];
                    break;
                case $validator['instance'] instanceof Validator\File\Size:
                    $result['fileSize'] = [
                        'name'   => 'fileSize',
                        'params' => ($validator['instance']->getOption('max') ?: 0) / (1024 * 1024) // На фронтенде надо количество мегабайт
                    ];
                    break;
                case $validator['instance'] instanceof Validator\File\Count:
                    $result['fileCount'] = [
                        'name'   => 'fileCount',
                        'params' => ['max' => $validator['instance']->getOption('max'), 'min' => $validator['instance']->getOption('min')]
                    ];
                    break;
            }
        }

        if ($element instanceof Element\File) {
            $result['file'] = ['name' => 'file'];
        }

        return $result;
    }

    protected function getPrivacyText(): string
    {
        $submit = $this->get(static::SUBMIT);

        $privacy = setting($this->privacySetting, '')?->big_text;
        $privacy = str_replace('#BTN#', $submit->getLabel(), $privacy);
        $privacy = str_replace('#PRIVACY_URL#', setting('site.privacy-url', '')->text, $privacy);

        return $privacy;
    }

    public function setElementRequired(string $fieldName, bool $required)
    {
        if (!$this->has($fieldName)) {
            return;
        }

        $this->get($fieldName)->setAttribute('required', $required);
    }

    public function setInputRequired(string $fieldName, bool $required)
    {
        $inputFilter = $this->getInputFilter();
        if (!$inputFilter->has($fieldName)) {
            return;
        }

        $inputFilter->get($fieldName)->setRequired($required);
    }

    protected function createTextField(string $name, bool $required = true, string $label = '', int $maxLenght = self::MAX_LENGTH_TEXT )
    {
        $this->add([
            'type'       => Element\Text::class,
            'name'       => $name,
            'attributes' => [
                'required'    => $required,
                'maxlength'   => $maxLenght,
            ],
            'options'    => [
                'label' => $label,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => $name,
            'required' => $required,
            'validators' => [
                [
                    'name' => 'stringLength',
                    'options' => [
                        'max' => $maxLenght,
                    ]
                ],
            ],
        ]);
    }

    protected function createNumericTextField(
        string $name,
        bool $required = true,
        string $label = '',
        int $max = null,
        int $min = null,
    ) {
        $this->add(
            [
                'type' => Element\Text::class,
                'name' => $name,
                'attributes' => [
                    'required' => $required,
                    'maxlength' => $max ? mb_strlen('' . $max) : static::NAME_MAX_LENGTH,
                    'inputmode' => 'numeric',
                ],
                'options' => [
                    'label' => $label,
                ],
            ]
        );

        $this->getInputFilter()->add(
            [
                'name' => $name,
                'required' => $required,
                'validators' => [
                    [
                        'name' => 'between',
                        'options' => [
                            'max' => $max ?? '',
                            'min' => $min ?? '',
                        ]
                    ],
                ],
            ]
        );
    }

    protected function createName(bool $required = true)
    {
        $this->add([
            'type'       => Element\Text::class,
            'name'       => static::NAME,
            'attributes' => [
                'required'    => $required,
                'maxlength'   => static::NAME_MAX_LENGTH,
            ],
            'options'    => [
                'label' => static::NAME_LABEL,
            ],
        ]);

        $this->getInputFilter()->add(
            [
                'name'     => static::NAME,
                'required' => $required,
                'validators' => [
                    [
                        'name' => 'stringLength',
                        'options' => [
                            'max' => static::NAME_MAX_LENGTH,
                        ]
                    ],
                ],
            ]
        );
    }

    protected function createPhone(bool $required = true)
    {
        $this->add([
            'type'       => Element\Tel::class,
            'name'       => static::PHONE,
            'attributes' => [
                'required'    => $required,
                'maxlength'   => static::PHONE_MAX_LENGTH,
                'inputmode'   => 'numeric',
            ],
            'options'    => [
                'label' => static::PHONE_LABEL,
                'mask'  => '+7 (999) 999-99-99',
            ],
        ]);
        $this->getInputFilter()->add([
            'name'     => static::PHONE,
            'required' => $required,
        ]);
    }

    protected function createPassword(string $name, bool $required = true, $addValidator = true): Element\Password
    {
        $this->add([
            'type'       => Element\Password::class,
            'name'       => $name,
            'attributes' => [
                'required' => $required,
            ],
            'options'    => [
                'label' => static::PASSWORD_LABEL,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'       => $name,
            'required'   => $required,
            'validators' =>
                $addValidator
                    ? [
                        [
                            'name'    => 'stringLength',
                            'options' => [
                                'min' => static::PASSWORD_MIN_LENGHT,
                            ]
                        ]
                    ]
                    : [],
        ]);

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->get($name);
    }

    protected function createEmail(bool $required = true)
    {
        $this->add([
            'type'       => Element\Email::class,
            'name'       => static::EMAIL,
            'attributes' => [
                'required'    => $required,
                'maxlength'   => static::EMAIL_MAX_LENGTH,
            ],
            'options'    => [
                'label' => static::EMAIL_LABEL,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'       => static::EMAIL,
            'required'   => $required,
            'validators' => [
                [
                    'name' => Email::class,
                ],
            ],
        ]);
    }

    protected function createTextArea(string $name, bool $required = true, string $label = '', int $max = 0)
    {
        $this->add([
            'type'       => Element\Textarea::class,
            'name'       => $name,
            'attributes' => [
                'required'    => $required,
                'maxlength'   => static::TEXT_MAX_LENGTH,
            ],
            'options'    => [
                'label' => $label,
            ],
        ]);

        $this->getInputFilter()->add(
            [
                'name' => $name,
                'required' => $required,
                'validators' => [
                    [
                        'name' => 'stringLength',
                        'options' => [
                            'max' => $max ?? '',
                        ]
                    ],
                ],
            ]
        );
    }


    protected function createAdvertisement(): Element\Checkbox
    {
        $this->add(
            [
                'type' => Element\Checkbox::class,
                'name' => static::ADVERTISEMENT,
                'attributes' => [
                    'value'=> true,
                    'checked' => true,
                ],
                'options' => [
                    'label' => static::ADVERTISEMENT_LABEL,
                ]
            ]
        );

        $this->getInputFilter()->add(
            [
                'name' => 'advertisement',
            ]
        );

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->get(static::ADVERTISEMENT);
    }

    protected function createSubmit(): Element\Submit
    {
        $this->add([
            'type'    => Element\Submit::class,
            'name'    => static::SUBMIT,
            'options' => [
                'label' => static::SUBMIT_LABEL,
            ]
        ], ['priority' => PHP_INT_MIN]);

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->get(static::SUBMIT);
    }

    /**
     * @param string $name
     * @param bool $isMultiple
     * @param string $label
     * @param bool $required
     * @return Element\File
     */
    public function createFile(string $name, bool $isMultiple = false, bool $required = false): Element\File
    {
        $this->add([
            'type'    => Element\File::class,
            'name'    => $name,
            'attributes' => [
                'multiple' => $isMultiple,
                'required' => $required
            ]
        ]);

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->get($name);
    }

    public function addFileValidators($name, $required = true, $multiple = false, $allowPdf = false)
    {
        $this->getInputFilter()->add(
            [
                'name' => $name,
                'required' => $required,
                'validators' => [
                    $allowPdf
                        ? [
                        'name' => 'fileExtension',
                        'options' => ['extension' => EXT_ALLOWED_FILES]
                    ]
                        : [
                        'name' => 'fileIsImage',
                    ],
                    [
                        'name' => 'fileSize',
                        'options' => [
                            'max' => BaseHelper::getMaxFileSize(),
                        ]
                    ],
                    [
                        'name' => 'fileCount',
                        'options' => [
                            'max' => $multiple ? BaseHelper::getMaxFileCount() : 1,
                            'min' => 0,
                        ]
                    ],
                ],
            ]
        );
    }

    public function createCitySelect()
    {
        $model = new City();

        $options = $model->query()
            ->get()
            ->map(function (City $item) {
                return [
                    'value' => $item->id,
                    'label' => $item->name,
                ];
            })
            ->all();

        $this->add([
            'type'       => Select::class,
            'name'       => static::CITY,
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'label'         => static::CITY_LABEL,
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => static::CITY,
            'required' => true,
        ]);
    }

    protected function createRecaptcha()
    {
        $active = (int)setting('recaptcha.active')->text;
        if (!$active) {
            return;
        }

        $reCaptcha = new \Laminas\Captcha\ReCaptcha([
            'site_key'   => setting('recaptcha.site-key')->text,
            'secret_key' => setting('recaptcha.secret-key')->text,
        ]);

        $this->add([
            'type'    => Element\Captcha::class,
            'name'    => static::RECAPTCHA,
            'options' => [
                'captcha' => $reCaptcha,
                'siteKey' => $reCaptcha->getSiteKey(),
            ],
        ]);

        /** @var Element\Captcha $element */
        $element = $this->get(static::RECAPTCHA);
        $this->getInputFilter()->add($element->getInputSpecification());
    }

    public function validateRecaptcha($value): bool
    {
        $active = (int)setting('recaptcha.active')->text;
        if (!$active) {
            return true;
        }

        $reCaptcha = new \Laminas\Captcha\ReCaptcha([
            'site_key'   => setting('recaptcha.site-key')->text,
            'secret_key' => setting('recaptcha.secret-key')->text,
        ]);

        return $reCaptcha->isValid($value, []);
    }
}
