<?php

declare(strict_types=1);

namespace App\Forms;

use App\Helpers\BaseHelper;

/**
 * Данные пользователя на странице профиля
 */
class UserDataForm extends BaseForm
{
    const SUBMIT_LABEL = 'Сохранить изменения';

    protected $fileFields = false;

    public function __construct(string $name = 'profile.details')
    {
        parent::__construct($name);

        $this->createEmail();
        $this->createName();
        $this->createTextField('address', false, 'Укажите адрес');
        $this->createTextArea('about', false, 'О себе', static::MAX_LENGTH_TEXTAREA);
        $this->createPhone();
        $this->createSubmit();
        $this->setAttribute('action', route('profile.change-profile-data', false));
    }

    public function addFileFields()
    {
        if ($this->fileFields) {
            return $this;
        }
        $this->fileFields = true;

        $this->createFile('avatar');
        // Для прокидывания на фронт
        $this->addFileValidators('avatar', false, false);

        return $this;
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        $data['fields']['avatar']['placeholder'] = 'Рекомендуем форматы: jpeg, png, не более ' . BaseHelper::getMaxFileSizeMb();

        return $data;
    }
}
