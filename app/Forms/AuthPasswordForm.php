<?php

declare(strict_types=1);

namespace App\Forms;

/**
 * Авторизация по телефону+паролю
 */
class AuthPasswordForm extends BaseForm
{
    const SUBMIT_LABEL = 'Войти';

    public function __construct(string $name = 'account-auth')
    {
        parent::__construct($name);

        $this->createPhone();
        $this->createPassword(static::PASSWORD, true, false);
        $this->createSubmit();
        $this->setAttribute('action', route('account.auth', false));
    }
}
