<?php

declare(strict_types=1);

namespace App\Forms;

use App\Models\SmsCode;

/**
 * Авторизация по телефон+код
 */
class AuthCodeForm extends BaseForm
{
    const SUBMIT_LABEL = 'Войти';

    public function __construct(string $name = 'account-auth')
    {
        parent::__construct($name);

        $this->createPhone();
        $this->createTextField(static::CODE, true, static::CODE_LABEL, SmsCode::DEFAULT_LENGTH);
        $this->createSubmit();
        $this->setAttribute('action', route('account.auth', false));
    }
}
