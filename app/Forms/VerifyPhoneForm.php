<?php

declare(strict_types=1);

namespace App\Forms;

use App\Helpers\ViewHelper;
use App\Models\SmsCode;
use App\Services\UserService;

/**
 * Подтверждение номера телефона
 */
class VerifyPhoneForm extends BaseForm
{
    const SUBMIT_LABEL = 'Подтвердить';

    public function __construct(string $name = 'verify-phone')
    {
        parent::__construct($name);

        $this->createPhone();
        $this->createTextField(static::CODE, true, static::CODE_LABEL, SmsCode::DEFAULT_LENGTH);
        $this->createSubmit();
        $this->setAttribute('action', route('account.verify-phone', false));

        /** @var UserService $userService */
        $userService = app()->make(UserService::class);
        $currentUser = $userService->current();

        if ($currentUser) {
            /** @var \Laminas\Form\Element\Text $field */
            $field = $this->get(static::PHONE);
            $field->setValue(ViewHelper::formatPhone($currentUser->phone));
        }
    }
}
