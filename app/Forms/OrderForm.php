<?php

declare(strict_types=1);

namespace App\Forms;

use App\Models\DeliveryType;
use App\Models\PaySystem;
use App\Models\SmsCode;
use App\Services\UserService;

/**
 * Форма оформления заказа
 */
class OrderForm extends BaseForm
{

    const SUBMIT_LABEL = 'Оформить заказ';
    const RECIPIENT_NAME = 'recipientName';
    const RECIPIENT_PHONE = 'recipientPhone';
    const COMMENT = 'comment';
    const DELIVERY_TYPE = 'deliveryType';
    const DELIVERY_ADDRESS = 'deliveryAddress';
    const DELIVERY_DATE = 'deliveryDate';
    const DELIVERY_TIME = 'deliveryTime';
    const PAY_SYSTEM = 'paySystem';

    public function __construct($cookId)
    {
        parent::__construct();

        $this->setAttribute('action', route('order.checkout', ['cookId' => $cookId], false));

        $this->createName();
        $this->createPhone();

        $this->createTextField(static::RECIPIENT_NAME, false, 'Имя');
        $this->createTextField(static::RECIPIENT_PHONE, false, 'Телефон');

        $this->createDeliveryType();
        $this->createDeliveryAddress();
        $this->createDeliveryDate();
        $this->createDeliveryTime();
        $this->createPaySystem();

        // Инпут кода подтверждения телефона
        $this->createTextField(static::CODE, false, static::CODE_LABEL, SmsCode::DEFAULT_LENGTH);

        $this->createTextArea(static::COMMENT, false, 'Впишите все свои пожелания к заказу', static::MAX_LENGTH_TEXTAREA);
        $this->createSubmit();
    }

    /**
     * Заполнить форму данными из авторизованного пользователя
     * @return $this
     */
    public function populateUser(): static {
        /** @var UserService $userService */
        $userService = app(UserService::class);
        $user = $userService?->current();

        if (!$user) return $this;

        $this->populateValues([static::PHONE => $user->phone]);
        $this->get(static::PHONE)->setAttribute('disabled', true);

        if (!empty($user->getFullName(true))) {
            $this->populateValues([static::NAME => $user->getFullName(true)]);
            $this->get(static::NAME)->setAttribute('disabled', true);
        }

        $this->populateValues([static::DELIVERY_DATE => date('d.m.Y')]);

        return $this;
    }

    public function createDeliveryType()
    {
        $model = new DeliveryType();
        $options = $model->query()->get()->map(function (DeliveryType $item) {
            return [
                'value' => $item->id,
                'label' => $item->name,
                'descr' => $item->description ?? '',
            ];
        })->all();

        $this->add([
            'type'       => \Laminas\Form\Element\Radio::class,
            'name'       => static::DELIVERY_TYPE,
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => static::DELIVERY_TYPE,
            'required' => true,
        ]);
    }

    public function createDeliveryAddress()
    {
        // todo надо ли тут required?
        $this->createTextField(static::DELIVERY_ADDRESS, false, 'Адрес');
    }

    public function createDeliveryDate()
    {
        $this->createTextField(static::DELIVERY_DATE, true, 'Дата');
    }

    public function createDeliveryTime()
    {
        $options = [];
        $date = (new \DateTime())->setTime(0, 0);
        $step = \DateInterval::createFromDateString('30 minute');
        for ($i = 0; $i < 48; $i++) {
            $curTime = $date->format('H:i');
            $date->add($step);
            $nextTime = $date->format('H:i');
            $str = "$curTime - $nextTime";

            $options[] = [
                'value' => $str,
                'label' => $str,
            ];
        }

        $this->add([
            'type'       => \Laminas\Form\Element\Select::class,
            'name'       => static::DELIVERY_TIME,
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'label'         => 'Время',
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => static::DELIVERY_TIME,
            'required' => true,
        ]);
    }

    public function createPaySystem()
    {
        $model = new PaySystem();
        $options = $model->query()->where('active', '1')->get()->map(function (PaySystem $item) {
            return [
                'value' => $item->id,
                'label' => $item->name,
                'descr' => $item->description ?? '',
            ];
        })->all();

        $this->add([
            'type'       => \Laminas\Form\Element\Radio::class,
            'name'       => static::PAY_SYSTEM,
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => static::PAY_SYSTEM,
            'required' => true,
        ]);
    }
}
