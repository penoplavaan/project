<?php

declare(strict_types=1);

namespace App\Forms;

/**
 * Отправка запроса ан сброс пароля
 */
class SendResetPasswordForm extends BaseForm
{
    const SUBMIT_LABEL = 'Отправить';

    public function __construct(string $name = 'send-reset')
    {
        parent::__construct($name);

        $this->createEmail();
        $this->createSubmit();
        $this->setAttribute('action', route('account.reset-send', false));
    }
}
