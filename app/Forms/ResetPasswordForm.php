<?php

declare(strict_types=1);

namespace App\Forms;

/**
 * Сброс пароля - указание нового паоля
 */
class ResetPasswordForm extends BaseForm
{
    const SUBMIT_LABEL = 'Применить';

    public function __construct(string $name = 'account-auth')
    {
        parent::__construct($name);

        $this->add([
            'type'       => \Laminas\Form\Element\Text::class,
            'name'       => 'token',
            'attributes' => [
                'required'    => true,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => 'token',
            'required' => true,
        ]);


        $this->add([
            'type'       => \Laminas\Form\Element\Text::class,
            'name'       => $this::EMAIL,
            'attributes' => [
                'required'    => true,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => $this::EMAIL,
            'required' => true,
        ]);

        $this->createPassword($this::PASSWORD);
        $this->createSubmit();
        $this->setAttribute('action', route('account.reset-password', false));
    }
}
