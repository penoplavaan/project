<?php

declare(strict_types=1);

namespace App\Forms;

use App\Models\FeedbackCategory;

/**
 * Форма "Обратная связь"
 *
 */
class FeedbackForm extends BaseForm
{
    const SUBMIT_LABEL = 'Отправить';

    public function __construct(string $name = 'feedback-form')
    {
        parent::__construct($name);

        $this->createEmail();
        $this->createTextField('name', true, 'Имя');
        $this->createPhone();
        $this->createSpeciality();

        $this->createTextArea('question', true, 'Ваш вопрос', static::MAX_LENGTH_TEXTAREA);
        $this->createRecaptcha();

        $this->createSubmit();
        $this->setAttribute('action', route('popup.save.feedback', false));
    }

    public function createSpeciality()
    {
        $model = new FeedbackCategory();
        $options = $model->query()->get()->map(function (FeedbackCategory $item) {
            return [
                'value' => $item->id,
                'label' => $item->name,
            ];
        })->all();

        $this->add([
            'type'       => \Laminas\Form\Element\Select::class,
            'name'       => 'topic_id',
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'label'         => 'Тема вопроса',
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => 'topic_id',
            'required' => true,
        ]);
    }
}
