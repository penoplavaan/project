<?php

namespace App\Forms\Validators;

use Laminas\Validator\AbstractValidator;

use App\Helpers\BaseHelper;

class Email extends AbstractValidator
{
    const INVALID = 'invalid';

    protected array $messageTemplates = [
        self::INVALID => 'Введите корректный емейл',
    ];

    public function isValid($value)
    {
        $valid = static::checkEmail($value);

        if (!$valid) {
            $this->error(static::INVALID);
        }

        return $valid;
    }

    public static function checkEmail($email, $strict = false)
    {
        if (!$strict) {
            $email = trim($email);
            if (preg_match("#.*?[<\\[\\(](.*?)[>\\]\\)].*#i", $email, $arr) && $arr[1] <> '') {
                $email = $arr[1];
            }
        }

        if (mb_strlen($email) > 320) {
            return false;
        }

        $atom = "\\p{L}=_0-9a-z+~'!\$&*^`|\\#%/?{}-";
        $domain = "\\p{L}a-z0-9-";
        if (preg_match("#^[{$atom}]+(\\.[{$atom}]+)*@(([{$domain}]+\\.)+)([{$domain}]{2,20})$#ui", $email)) {
            return true;
        }

        return false;
    }

}
