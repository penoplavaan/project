<?php

declare(strict_types=1);

namespace App\Forms;

use App\Helpers\BaseHelper;
use App\Models\CookSpeciality;
use App\Models\DishCategory;
use App\Services\UserService;

/**
 * Форма "Стать поваром"
 *
 * todo Выкинуть и использовать Request-классы Ларавеля для валидации
 */
class BecomeCookForm extends BaseForm
{
    const SUBMIT_LABEL = 'Отправить заявку';

    protected $fileFields = false;

    public function __construct(string $name = 'account-auth')
    {
        parent::__construct($name);

        $this->createName();
        $this->createEmail();
        $this->createTextArea('about', false, 'О себе', static::MAX_LENGTH_TEXTAREA);

        $this->createSpeciality();
        $this->createMenuCategories();

        $this->createCitySelect();
        $this->createTextField('address', true, 'Укажите адрес кухни');
        $this->createTextField('experience', false, 'С какого года в деле');

        $this->createSubmit();
        $this->setAttribute('action', route('for-cooks.form-submit', false));
    }

    public function addFileFields()
    {
        if ($this->fileFields) {
            return $this;
        }

        $this->fileFields = true;

        $this->createFile('avatar');
        $this->createFile('kitchen', true);
        $this->createFile('passport');
        $this->createFile('sanitary', true);
        $this->createFile('certificate', true);

        // Для прокидывания на фронт
        $this->addFileValidators('avatar', false, false);
        $this->addFileValidators('kitchen', false, true);
        $this->addFileValidators('passport', false, false, true);
        $this->addFileValidators('sanitary', false, true, true);
        $this->addFileValidators('certificate', false, true, true);

        return $this;
    }

    /**
     * Заполнить форму данными из авторизованного пользователя
     * @return $this
     */
    public function populateUser(): static {
        /** @var UserService $userService */
        $userService = app(UserService::class);
        $user = $userService?->current();

        if (!$user) return $this;

        if (!empty($user->getFullName(true))) {
            $this->populateValues([static::NAME => $user->getFullName(true)]);
            $this->get(static::NAME)->setAttribute('disabled', true);
        }

        if (!empty($user->email) && $user->hasVerifiedEmail()) {
            $this->populateValues([static::EMAIL => $user->email]);
            $this->get(static::EMAIL)->setAttribute('disabled', true);
        }

        return $this;
    }

    public function addFileValidators($name, $required = true, $multiple = false, $allowPdf = false)
    {
        $this->getInputFilter()->add([
            'name'       => $name,
            'required'   => $required,
            'validators' => [
                $allowPdf
                    ? [
                        'name' => 'fileExtension',
                        'options' => ['extension' => EXT_ALLOWED_FILES]
                    ]
                    : [
                        'name' => 'fileIsImage',
                    ],
                [
                    'name'    => 'fileSize',
                    'options' => [
                        'max' => BaseHelper::getMaxFileSize(),
                    ]
                ],
                [
                    'name'    => 'fileCount',
                    'options' => [
                        'max' => $multiple ? BaseHelper::getMaxFileCount() : 1,
                        'min' => 0,
                    ]
                ],
            ],
        ]);
    }

    public function createSpeciality()
    {
        $model = new CookSpeciality();
        $options = $model->query()
            ->get()
            ->map(function (CookSpeciality $item) {
                return [
                    'value' => $item->id,
                    'label' => $item->title,
                ];
            })
            ->all();

        $this->add([
            'type'       => \Laminas\Form\Element\Select::class,
            'name'       => 'speciality_id',
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'label'         => 'Специализация',
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => 'speciality_id',
            'required' => true,
        ]);
    }

    public function createMenuCategories()
    {
        $model = new DishCategory();
        $options = $model->query()
            ->get()
            ->map(function (DishCategory $item) {
                return [
                    'value' => $item->id,
                    'label' => $item->title,
                ];
            })
            ->all();

        $this->add([
            'type'       => \Laminas\Form\Element\MultiCheckbox::class,
            'name'       => 'categories',
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'label'         => 'Выберите категории меню',
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add(
            [
                'name' => 'categories',
                'required' => true,
            ]
        );
    }

    public function toArray(): array
    {
        $data = parent::toArray();

        $data['fields']['avatar']['placeholder'] = 'Рекомендуем форматы: jpeg, png, не более ' . BaseHelper::getMaxFileSizeMb();
        $data['fields']['kitchen']['placeholder'] = 'Прикрепите фото кухни';
        $data['fields']['passport']['placeholder'] = 'Прикрепите фото лица с паспортом';
        $data['fields']['sanitary']['placeholder'] = 'Прикрепите фото санитарной книжки';
        $data['fields']['certificate']['placeholder'] = 'Прикрепите фото сертификата или диплома';

        return $data;
    }


    protected function getYear()
    {
        return intval(date("Y"));
    }
}
