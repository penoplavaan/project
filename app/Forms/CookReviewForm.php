<?php

declare(strict_types=1);

namespace App\Forms;

use App\Services\UserService;

/**
 * Форма отзыва на повара
 */
class CookReviewForm extends BaseForm
{
    const NAME_LABEL = 'Имя';
    const RATING = 'rating';

    public function __construct(int $cookId, string $name = 'cook-review')
    {
        parent::__construct($name);

        $this->createRating();

        /** @var UserService $userService */
        $userService = app()->make(UserService::class);
        $currentUser = $userService->current();

        $name = trim($currentUser->name ?? '');
        if ($currentUser && !empty($name)) {
            $this->createName(false);
            /** @var \Laminas\Form\Element\Text $field */
            $field = $this->get(static::NAME);
            $field->setValue($name);
            $field->setAttribute('disabled', true);
        } else {
            $this->createName();
        }

        $this->createTextArea('text', true, 'Текст отзыва', 400);
        $this->createSubmit();

        $this->setAttribute('action', route('cooks.review-submit', ['cook' => $cookId]));
    }


    protected function createRating(bool $required = true)
    {
        $this->add([
            'type'       => \Laminas\Form\Element\Number::class,
            'name'       => static::RATING,
            'attributes' => [
                'required'  => $required,
            ],
            'options'    => [
                'label' => 'Рейтинг',
            ],
        ]);

        /** @var \Laminas\Form\Element\Text $field */
        $field = $this->get(static::RATING);
        $field->setValue(5);

        $this->getInputFilter()->add(
            [
                'name'     => static::RATING,
                'required' => $required,
                'validators' => [
                    [
                        'name' => 'lessThan',
                        'options' => [
                            'inclusive' => true,
                            'max' => 5,
                        ]
                    ],
                    [
                        'name' => 'greaterThan',
                        'options' => [
                            'inclusive' => true,
                            'min' => 1,
                        ]
                    ],
                ],
            ]
        );
    }
}
