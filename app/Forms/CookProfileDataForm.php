<?php

declare(strict_types=1);

namespace App\Forms;

use App\Models\CookSpeciality;
use App\Models\DishCategory;

/**
 * Форма "Стать поваром"
 *
 */
class CookProfileDataForm extends BaseForm
{
    const SUBMIT_LABEL = 'Сохранить данные';

    public function __construct(string $name = 'account-auth')
    {
        parent::__construct($name);

        $this->createTextArea('about', false, setting('for-cooks-form.about')->text ?? 'О себе', static::MAX_LENGTH_TEXTAREA);
        $this->createSpeciality();
        $this->createMenuCategories();

        $this->createCitySelect();
        $this->createTextField('address', true, setting('for-cooks-form.kitchen-address')->text ?? 'Укажите адрес кухни');
        $this->createTextField('experience', false, setting('for-cooks-form.experience')->text ?? 'С какого года в деле');

        $this->createSubmit();
        $this->setAttribute('action', route('profile.change-cook-profile-data', false));
    }

    public function createSpeciality()
    {
        $model = new CookSpeciality();
        $options = $model->query()
            ->get()
            ->map(function (CookSpeciality $item) {
                return [
                    'value' => $item->id,
                    'label' => $item->title,
                ];
            })
            ->all();

        $this->add([
            'type'       => \Laminas\Form\Element\Select::class,
            'name'       => 'speciality_id',
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'label'         => 'Специализация',
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => 'speciality_id',
            'required' => true,
        ]);
    }

    public function createMenuCategories()
    {
        $model = new DishCategory();
        $options = $model->query()
            ->get()
            ->map(function (DishCategory $item) {
                return [
                    'value' => $item->id,
                    'label' => $item->title,
                ];
            })
            ->all();

        $this->add([
            'type'       => \Laminas\Form\Element\MultiCheckbox::class,
            'name'       => 'categories',
            'attributes' => [
                'required' => true,
                'multiple' => true,
            ],
            'options'    => [
                'label'         => 'Выберите категории меню',
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add(
            [
                'name' => 'categories',
                'required' => true,
            ]
        );
    }

    protected function getYear()
    {
        return intval(date("Y"));
    }
}
