<?php

declare(strict_types=1);

namespace App\Forms;

interface FormDescriptionInterface
{
    public function formDescription(): array;
}
