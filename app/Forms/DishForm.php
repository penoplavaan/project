<?php

declare(strict_types=1);

namespace App\Forms;

use App\Helpers\BaseHelper;
use App\Models\DishCategory;
use App\Models\Tag;

/**
 * Форма "Добавить блюдо"
 *
 */
class DishForm extends BaseForm
{
    const SUBMIT_LABEL = 'Добавить';

    protected $fileFields = true;

    public function __construct(string $name = 'add-dish')
    {
        parent::__construct($name);

        // Категория блюда
        $this->createCategory();
        // Категория название
        $this->createTextField('id', false);
        $this->createTextField('attachmentsIds', false);
        $this->createTextField('name', true, 'Название блюда');
        $this->createTextArea('preview_text', true, 'Состав', static::MAX_LENGTH_TEXTAREA);
        $this->createTextArea('storage_conditions', false, 'Условия хранения', static::MAX_LENGTH_TEXTAREA);

        $this->createTextField('calories', false, 'Калорийность', 20);
        $this->createTextField('protein', false, 'Белки, г', 20);
        $this->createTextField('fats', false, 'Жиры, г', 20);
        $this->createTextField('carbohydrates', false, 'Углеводы, г', 20);

        $this->createTextField('quantity', true, 'Количество порций', 40);
        $this->createTextField('weight', true, 'Вес', 40);
        $this->createTextField('cooking_time', false, 'Время приготовления', 100);
        $this->createTextField('package', false, 'Упаковка', 40);
        $this->createNumericTextField('price', true, 'Цена, р', 1000000, 0);

        $this->createTags();


        $this->createSubmit();
        $this->setAttribute('action', route('profile.add-dish', false));
    }

    public function addFileFields()
    {
        $this->fileFields = true;
        $this->createFile('images', true);

        // Для прокидывания на фронт
        $this->addFileValidators('images', false, true);

        return $this;
    }

    public function createCategory()
    {
        $model = new DishCategory();
        $options = $model->query()
            ->get()
            ->map(function (DishCategory $item) {
                return [
                    'value' => $item->id,
                    'label' => $item->title,
                ];
            })
            ->all();

        $this->add([
            'type'       => \Laminas\Form\Element\Select::class,
            'name'       => 'category_id',
            'attributes' => [
                'required' => true,
                'multiple' => false,
            ],
            'options'    => [
                'label'         => 'Категория',
                'value_options' => $options,
            ],
        ]);

        $this->getInputFilter()->add([
            'name'     => 'category_id',
            'required' => true,
        ]);
    }

    public function createTags()
    {
        $model = new Tag();
        $options = $model->query()
            ->get()
            ->map(function (Tag $item) {
                return [
                    'id'    => $item->id,
                    'value' => $item->name,
                ];
            })
            ->all();
        $this->add(
            [
                'type' => \Laminas\Form\Element\MultiCheckbox::class,
                'name' => 'tags',
                'attributes' => [
                    'required' => false,
                    'multiple' => true,
                ],
                'options' => [
                    'label'         => 'Вкусовые предпочтения:',
                    'value_options' => $options,
                ],
            ]
        );

        $this->getInputFilter()->add(
            [
                'name' => 'tags',
                'required' => false,
            ]
        );
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        // Это количество файлов
        $data['fields']['images']['placeholder'] = 'Рекомендуем форматы: jpeg, png, не более ' . BaseHelper::getMaxFileCount() . ' файлов';

        return $data;
    }
}
