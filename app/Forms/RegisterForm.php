<?php

declare(strict_types=1);

namespace App\Forms;

use App\Models\SmsCode;

/**
 * Регистрация
 */
class RegisterForm extends BaseForm
{
    const SUBMIT_LABEL = 'Зарегистрироваться';

    public function __construct(string $name = 'account-auth')
    {
        parent::__construct($name);

        $this->createEmail(false);
        $this->createPhone();
        $this->createPassword(static::PASSWORD);
        $this->createTextField(static::CODE, true, static::CODE_LABEL, SmsCode::DEFAULT_LENGTH);
//        $this->createAdvertisement();
        $this->createSubmit();
        $this->setAttribute('action', route('account.register', false));
    }
}
