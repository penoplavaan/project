<?php

namespace App\Jobs;

use App\Helpers\ViewHelper;
use App\Models\Order;
use App\Services\SmsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Оповещения о новом заказе повару
 */
class SendNewOrderSmsToCook implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected int $orderId = 0;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }

    public function handle(SmsService $smsService)
    {
        $setting = (int)(setting('chat.send-order-sms')->text ?? '0');
        if ($setting !== 1) {
            // Отправка СМС отключена в настройках
            return;
        }

        // SMS-оповещение повару
        /** @var Order $order */
        $order = Order::find($this->orderId);

        $cookUser = $order->cook->user;
        if ($cookUser->phone_verified) {
            // Отправляем только если телефон подтверждён
            $phone = ViewHelper::clearPhone($cookUser->phone);

            $smsServiceType = $order->payed ? $smsService::TYPE_NEW_ORDER_COOK : $smsService::TYPE_NEW_UNPAYED_ORDER_COOK;

            $smsService->sendSmsText($phone, $smsServiceType, []);
        }
    }
}
