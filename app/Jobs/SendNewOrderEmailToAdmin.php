<?php

namespace App\Jobs;

use App\Helpers\BaseHelper;
use App\Helpers\ViewHelper;
use App\Mail\SimpleMail;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

/**
 * Оповещения о новом заказе админу
 */
class SendNewOrderEmailToAdmin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected int $orderId = 0;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }

    public function handle()
    {
        /** @var Order $order */
        $order = Order::find($this->orderId);

        $paragraphs = [
            'Дата: ' . $order->created_at->format('Y.m.d в H:i'),
            'От пользователя: ' . ($order->user ? $order->user->getFullName() : 'Неавторизованный (' . $order->client_name . ', ' . $order->client_phone . ')'),
            'Повару: ' . $order->cook->user->getFullName(),
            'Общая сумма: ' . ViewHelper::price($order->sum) . ' ₽ ' . (!$order->payed ? 'НЕ ОПЛАЧЕН' : ''),
        ];

        $list = [];
        foreach ($order->orderToDishes as $pivot) {
            $list[] = $pivot->dish->name . ', ' . $pivot->count . 'шт на сумму ' . ViewHelper::price($pivot->count * $pivot->price_one) . ' ₽';
        }

        $mail = new SimpleMail('new-order', [
            'header'    => 'На сайте создан новый заказ №' . $order->id,
            'subheader' => '',
            'paragraph' => $paragraphs,
            'list'      => $list,
            'url'       => route('platform.resource.edit', ['resource' => 'order-resources', 'id' => $order->id]),
        ]);

        $mail->setType('html');
        $mail->subject('Новый заказ на сайте');
        BaseHelper::sendEmailToAdmins($mail);
    }
}
