<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TagPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return false
     */
    public function delete()
    {
         return false;
    }

    public function forceDelete()
    {
        return false;
    }

    public function update()
    {
        return true;
    }

    public function create()
    {
        return true;
    }

    public function viewAny()
    {
        return true;
    }

    public function view()
    {
        return true;
    }
}
