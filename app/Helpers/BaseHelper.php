<?php

namespace App\Helpers;

use App\Forms\AuthCodeForm;
use App\Forms\AuthPasswordForm;
use App\Forms\RegisterForm;
use App\Forms\ResetPasswordForm;
use App\Forms\SendResetPasswordForm;
use App\Pagination\LengthAwarePaginator as FixedLengthAwarePaginator;
use App\Services\BasketService;
use App\Services\UserService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class BaseHelper
{

    protected static array $jsApp = [];

    /**
     * Получение состояния приложения для глобальной переменной APP на фронте
     *
     * @return false|string
     * @throws BindingResolutionException
     */
    public static function jsApp(): bool|string
    {
        /** @var UserService $userService */
        $userService = app()->make(UserService::class);
        /** @var BasketService $basketService */
        $basketService = app()->make(BasketService::class);
        $currentUser = $userService->current();
        $isAuth = !empty($currentUser);
        $jsData = [
            'isAuth' => $isAuth,
            'isCook' => false,
            'routes' => static::collectJsRoutes(),
            'basket' => $basketService->getInfo(),
            'ymId'   => 90186311, // ID Яндекс-метрики todo вынести в настройки
        ];

        if ($isAuth) {
            $isCook = $currentUser->isCook();
            $jsData['isCook'] = $isCook;

            $jsData['userData'] = [
                'id'     => $currentUser->id,
                'name'   => $currentUser->getFullName(),
                'avatar' => $currentUser->getAvatar()
                    ? $currentUser->getAvatar()->resize('header.avatar')->getUrl()
                    : ViewHelper::getEmptyAvatar($isCook),
            ];

        } else {
            $jsData['authForms'] = [
                'authCode'  => (new AuthCodeForm())->toArray(),
                'authPass'  => (new AuthPasswordForm())->toArray(),
                'register'  => (new RegisterForm())->toArray(),
                'resetSend' => (new SendResetPasswordForm())->toArray(),
                'reset'     => (new ResetPasswordForm())->toArray(),
            ];
        }

        static::extendJsApp($jsData);

        $content = 'window.APP = ' . ViewHelper::jsonEncode(static::$jsApp) . ';';

        return '<script>' . $content . '</script>';
    }

    /**
     * Получаем роуты, помеченные middleware js.router
     * @return array
     */
    public static function collectJsRoutes(): array {
        $routes = Route::getRoutes();
        $collectedRoutes = [];

        /** @var \Illuminate\Routing\Route $route */
        foreach ($routes->getRoutesByName() as $route) {
            $middleware = $route->middleware();
            if (empty($middleware)) continue;

            $middleware = array_map(fn($m) => is_object($m) ? get_class($m) : $m, $middleware);
            if (!in_array('js.router', $middleware, true)) continue;

            $collectedRoutes[$route->getName()] = '/' . $route->uri();
        }

        return $collectedRoutes;
    }

    public static function extendJsApp(array $array, bool $recursive = false): void
    {
        static::$jsApp = call_user_func_array(
            $recursive ? 'array_merge_recursive' : 'array_merge',
            [static::$jsApp, $array]
        );
    }

    /**
     * Из строки удаляем все символы, не являющиеся цифрой
     *
     * @param string|null $string $string
     * @return string
     */
    public static function onlyDigits(?string $string): string
    {
        if (!is_string($string)) {
            return '';
        }

        return preg_replace("/\D/", '', $string);
    }

    public static function isUseragentGooglebot(): bool
    {
        static $isBot;
        if ($isBot !== null) {
            return $isBot;
        }

        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $agents    = [
            'Chrome-Lighthouse',
            'bingbot',
            'AhrefsBot',
            'BLEXBot',
            'YandexBot',
            'DotBot',
            'SemrushBot',
            'Mail.RU_Bot',
            'MJ12bot',
            'Googlebot',
            'AdsBot-Google',
            'CCBot',
            'Sogou web spider',
            'Screaming Frog SEO Spider',
            'Google Page Speed Insights',
            'vkShare',
            'facebook',
            'Twitter',
        ];

        $isBot = false;
        foreach ($agents as $agent) {
            if (strpos($userAgent, $agent) !== false) {
                $isBot = true;
                break;
            }
        }
        return $isBot;
    }

    public static function getImageFileDimensions(string $path): ?array
    {
        if (!$path) {
            return null;
        }
        if (!file_exists($path) || !is_readable($path) || is_dir($path)) {
            return null;
        }
        $size = getimagesize($path) ?: null;
        $size = $size ? [$size[0], $size[1]] : null;

        if (
            !$size
            && function_exists('simplexml_load_file')
            && mb_strpos(mime_content_type($path), 'image/svg') === 0
        ) {
            $xml = simplexml_load_file($path);
            if (!$xml) {
                return null;
            }

            $size = array_filter([intval($xml['width']), intval($xml['height'])]);

            if (count($size) != 2) {
                $viewBox = $xml['viewBox'] ?: $xml['viewbox'];
                $viewBox = $viewBox ? array_map('trim', explode(' ', (string)$viewBox)) : null;
                $size    = $viewBox && count($viewBox) == 4 ? [$viewBox[2], $viewBox[3]] : null;
            }
        }

        return $size;
    }

    /**
     * Обрезка строки с учетом кодировки
     * @param string $string
     * @param int $length
     * @param string $charcode String
     * @return string
     */
    public static function truncateString(string $string, int $length, string $charcode = 'UTF8'): string
    {
        $string = htmlspecialchars_decode($string);

        return mb_substr($string, 0, $length, $charcode) . (mb_strlen($string, $charcode) > $length ? '...' : '');
    }

    public static function returnBytes($val): int
    {
        $last = mb_strtolower(mb_substr($val, mb_strlen($val) - 1));
        $val = (int) trim($val);

        switch ($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= (1024 * 1024 * 1024); //1073741824
                break;
            case 'm':
                $val *= (1024 * 1024); //1048576
                break;
            case 'k':
                $val *= 1024;
                break;
        }

        return (int) $val;
    }

    public static function getPostMaxSize(): int
    {
        return static::returnBytes((string)ini_get('post_max_size'));
    }

    public static function getUploadMaxFileSize(): int
    {
        return static::returnBytes((string)ini_get('upload_max_filesize'));
    }

    public static function getMaxFileSize(): int
    {
        return min(static::getUploadMaxFileSize(), static::getPostMaxSize(), static::returnBytes(setting('upload.max-file-size')->getText()));
    }

    public static function getMaxFileSizeMb(): string
    {
        return static::formatBytes(min(static::getUploadMaxFileSize(), static::getPostMaxSize(), static::returnBytes(setting('upload.max-file-size')->getText())));
    }

    public static function getMaxFileCount(): int
    {
        return (int)setting('max.file.count')->text ?? 10;
    }

    public static function stripEmptyStrings(string $string): string
    {
        $string = trim($string);

        $string = preg_replace('/\s+\n/', "\n", $string);
        $string = preg_replace('/\n+/', "\n", $string);

        return $string;
    }

    /**
     * @param int $count
     * @param string[] $forms
     * @return string
     */
    public static function getWordForm(int $count, array $forms = []): string {
        $n100 = $count % 100;
        $n10  = $count % 10;

        if (($n100 > 10) && ($n100 < 21)) {
            return $forms[2];
        } elseif ((!$n10) || ($n10 >= 5)) {
            return $forms[2];
        } elseif ($n10 == 1) {
            return $forms[0];
        }

        return $forms[1];
    }

    public static function preparePageData(LengthAwarePaginator $pager, array $queryParams = []): array
    {
        $fixedPager = FixedLengthAwarePaginator::buildByExists($pager, $queryParams);
        $pageData   = $fixedPager->onEachSide(2)->jsonSerialize();
        unset($pageData['data']);

        foreach ($pageData['links'] as $index => $link) {
            if (!is_numeric($link['label']) && $link['label'] != '...') {
                unset($pageData['links'][$index]);
            }

            if ($link['label'] == '...') {
                $pageData['links'][$index]['url']    = 'javascript:void(0)';
                $pageData['links'][$index]['active'] = false;
            }
        }

        $pageData['links'] = array_values($pageData['links']);

        return $pageData;
    }

    public static function headersForAjaxResponse(): array
    {
        // Иначе Chrome кэширует запросы json/html без разбора типа
        return [
            'Vary' => ['X-Requested-With', 'Content-Type'],
        ];
    }

    protected static function formatBytes($bytes, $precision = 2): string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public static function isProd(): bool
    {
        return config('app.env') === 'production';
    }

    /**
     * Отправка емейла всем админам, указанным в настройке
     * @param $mail
     * @return void
     */
    public static function sendEmailToAdmins($mail) {
        $adminEmails = explode(',', setting('mail.admin')->text);

        foreach ($adminEmails as $adminEmail) {
            $oneMail = clone($mail);
            $adminEmail = trim($adminEmail);

            if (!empty($adminEmail)) {
                Mail::to(trim($adminEmail))->send($oneMail);
            }
        }
    }
}
