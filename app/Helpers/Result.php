<?php

namespace App\Helpers;

use JetBrains\PhpStorm\ArrayShape;

class Result {

    protected bool $success = true;
    protected array $messages = [];
    protected array $data = [];

    public function __construct(Result $result = null) {
        if ($result) {
            $this->data = $result->getData();
            $this->messages = $result->getMessages();
            $this->success = $result->isSuccess();
        }
    }

    public function addErrorMessage(string $message) {
        $this->success = false;
        $this->messages[] = $message;
    }

    public function addErrors(array $messages) {
        $this->messages = array_merge($this->messages, $messages);
    }

    public function getMessages(): array {
        return $this->messages;
    }

    public function setSuccess(bool $isSuccess) {
        $this->success = $isSuccess;
    }

    public function isSuccess(): bool {
        return $this->success;
    }

    public function setData(array $data) {
        $this->data = $data;
    }

    public function getData(): array {
        return $this->data;
    }

    #[ArrayShape([
        'success' => "bool",
        'messages' => "array",
        'data' => "array"
    ])]
    public function toJson(): array {
        return [
            'success'  => $this->success,
            'messages' => $this->messages,
            'data'     => $this->data,
        ];
    }

    public function get(string $key) {
        if (!isset($this->data[$key])) {
            return null;
        }
        return $this->data[$key];
    }

    public function set(string $key, $value) {
        $this->data[$key] = $value;
    }

    public function getMessagesImplode($separator = '<br>'): string {
        return implode($separator, $this->getMessages());
    }
}
