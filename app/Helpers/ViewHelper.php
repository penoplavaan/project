<?php

namespace App\Helpers;

use Carbon\Carbon;

/**
 * Class ViewHelper
 * Класс хелперов для Blade-шаблонов
 * @package App\Helper
 */
class ViewHelper
{

    /**
     * @param string $id
     * @param string $additionalClass
     * @return string
     */
    public static function getSymbol(string $id, string $additionalClass = ''): string
    {
        return sprintf(
            '<svg class="symbol symbol-%s %s"><use xlink:href="%s#%s"></use></svg>',
            $id,
            $additionalClass,
            SYMBOLS_SVG,
            $id
        );
    }

    /**
     * @param string $path
     * @return string
     */
    public static function getImage(string $path): string
    {
        $fullPath = P_PUBLIC_PATH . P_IMAGES . $path;
        $time = file_exists($fullPath) ? filemtime($fullPath) : 0;

        return P_IMAGES . $path . "?" . $time;
    }

    /**
     * @param int $count
     * @param string[] $forms
     * @return string
     */
    public static function getWordForm(int $count, array $forms = []): string
    {
        $n100 = $count % 100;
        $n10 = $count % 10;

        if (($n100 > 10) && ($n100 < 21)) {
            return $forms[2];
        } elseif ((!$n10) || ($n10 >= 5)) {
            return $forms[2];
        } elseif ($n10 == 1) {
            return $forms[0];
        }

        return $forms[1];
    }

    /**
     * Форматирование телефона.
     * Удаляет все, кроме цифр, подставляет +7 (или 7) в начале вместо 8.
     *
     * @param string|null $phone
     * @param string $prefix
     * @return string
     */
    public static function clearPhone(?string $phone, string $prefix = '+7'): string
    {
        if (empty($phone)) {
            return '';
        }

        $clearPhone = preg_replace('/\D/', '', $phone);
        $len = strlen($clearPhone);

        if ($len == 10) {
            return $prefix . $clearPhone;
        }

        if ($len == 11) {
            return preg_replace('/^8/', $prefix, $clearPhone);
        }

        return $clearPhone;
    }

    /**
     * Форматирование телефона
     * приводит телефон к виду +7 (903) 995-45-57
     *
     * @param $phone
     * @return string
     */
    public static function formatPhone($phone): string
    {
        $phone = self::clearPhone($phone);
        $phone = str_replace("+", "", $phone);

        $sCode = substr($phone, 0, 1);
        $sArea = substr($phone, 1, 3);
        $sPrefix = substr($phone, 4, 3);
        $sNumberS = substr($phone, 7, 2);
        $sNumberE = substr($phone, 9, 2);
        $phone = "+" . $sCode . " (" . $sArea . ") " . " " . $sPrefix . "-" . $sNumberS . "-" . $sNumberE;

        return $phone;
    }

    public static function monthName(int $month): string
    {
        $rusMonth = [
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря',
        ];

        return $rusMonth[$month];
    }

    /**
     * На вход получает обычную ссылку YouTube.
     * Отдает ссылку пригодную для iframe
     *
     * @param string $url
     * @return string
     */
    public static function getYTIframeUrl(string $url): string
    {
        $pos = mb_strrpos($url, '/');
        $videoId = substr($url, $pos);
        return 'https://www.youtube.com/embed/' . $videoId;
    }

    public static function jsonEncode(array $data): string
    {
        return json_encode($data, JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    /**
     * Вывести тег для рендера Vue-компонента на фронте
     * CSS-класс надо указывать полностью, с префиксом "vue-"
     * это сделано для того, чтобы по коду проекта было проще найти все использования такого класса.
     * @param string $class
     * @param array $data
     * @param string $tag
     * @param string $content
     * @return string
     */
    public static function vueTag(
        string $class = 'vue-',
        array $data = [],
        string $tag = 'div',
        string $content = ''
    ): string {
        return sprintf(
            '<%s class="%s" data-vue=\'%s\'>%s</%s>',
            $tag,
            $class,
            static::jsonEncode($data),
            $content,
            $tag
        );
    }

    /**
     * Автоматический прелоад шрифтов
     * @return string
     */
    public static function pagePreloadFonts(): string
    {
        $list = [];
        if ($dirh = opendir(P_DR . P_PUBLIC . P_FONTS)) {
            while ($file = readdir($dirh)) {
                if (in_array($file, ['.', '..'])) {
                    continue;
                }
                if (!preg_match('/.*\.woff2$/i', $file)) {
                    continue;
                }

                $list[] = sprintf(
                    '<link rel="preload" href="%s" as="font" crossorigin="anonymous">',
                    '/fonts/' . $file
                );
            }
        }

        return implode("\n    ", $list);
    }

    /**
     * JS'ки подключаются так, чтобы к ним добавлялось время последней модификации
     * @param $pageType
     * @return string
     */
    public static function pageJsList($pageType): string
    {
        $jsFileList = ['commons.chunk'];

        if ($pageType) {
            $jsFileList[] = $pageType . ".bundle";
        }

        $list = [];
        foreach ($jsFileList as $jsFilename) {
            $jsFilePath = "/build/js/$jsFilename.js";
            $jsFileAbsolutePath = public_path($jsFilePath);

            if (!file_exists($jsFileAbsolutePath)) {
                continue;
            }
            $list[] = sprintf('<script src="%s"></script>', $jsFilePath . '?' . filemtime($jsFileAbsolutePath));
        }

        return implode("\n", $list);
    }

    public static function getEmptyAvatar($isCook = false)
    {
        if ($isCook) {
            return '/images/empty-cook.svg';
        } else {
            return '/images/empty-user.svg';
        }
    }

    public static function localizedDate(Carbon $date, $form = 1): string
    {
        $montheNames = [
            ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
            ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
        ];
        $monthNumber = (int)$date->format('n');
        $monthName = $montheNames[$form][$monthNumber - 1];

        return $date->format('d ' . $monthName . ' Y');
    }

    public static function localizedDateTime(Carbon $date, $form = 1): string
    {
        return static::localizedDate($date, $form) . $date->format(' H:i');
    }

    /**
     * форматирует цену, разбивает на разряды
     * @param float $price
     * @return string
     */
    public static function price(float $price): string
    {
        return number_format((float)$price, 0, '.', ' ');
    }
}
