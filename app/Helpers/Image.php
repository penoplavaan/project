<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Devfactory\Imagecache\Facades\ImagecacheFacade as Imagecache;

class Image
{
    protected ?string $filePath;
    protected array $attributes;
    protected string $url;
    protected string $original;
    protected static int $transformCount = 0;

    public function __construct(?string $filePath)
    {
        $this->filePath = $filePath;
        $this->original = $this->url = $filePath ?? P_BLANK_IMG;
    }

    public static function make(?string $filePath): static
    {
        return new static($filePath);
    }

    public static function getResizePathPrefix(): string
    {
        return config('imagecache.config.imagecache_directory');
    }

    public static function getOriginalPathPrefix(): string
    {
        return config('imagecache.config.files_directory') . '/';
    }

    /**
     * @return bool true if limit reached
     */
    public static function checkTransformCountLimit(): bool
    {
        return self::getMaxTransformKey() > 0 && static::$transformCount >= self::getMaxTransformKey();
    }

    public static function transformCountIncrement(): void
    {
        static::$transformCount++;
    }

    public static function getMaxTransformKey()
    {
        return config('app.max_transform_key') ?? 20;
    }

    public function resize(?string $preset): static
    {
        if (!$preset) {
            return $this;
        }

        // Создание пути c пресетом без /storage
        $filePath = $this->prepareLink($this->getResizePathPrefix() . $preset . $this->filePath);
        $filePath = str_replace($this->getOriginalPathPrefix(), '', $filePath);

        // Перезаписываем путь на новый
        $this->filePath = $filePath;

        // Получаем урл и ресайзим несуществующие файлы
        $url = $this->getUrlFile();
        $this->resizeNotExists($url);

        $this->url = $url;

        return $this;
    }

    public function getUrlFile(): string
    {
        // если файл начинается со слеша, считаем, что сразу был передан URL, в storage файл не ищем

        if (mb_substr($this->filePath, 0, 1) === '/') {
            return $this->filePath;
        }
        return $this->prepareLink(\Storage::url($this->filePath));
    }


    public function getUrl(): string
    {
        return $this->url;
    }

    public function getWebpUrl(): ?string
    {
        $source = $this->getUrlFile();
        try {
            $type = mime_content_type(P_PUBLIC_PATH . $source);
        } catch (\Exception $e) {
            $type = null;
        }

        $allowedTypes = ['image/jpeg', 'image/png'];
        if (!in_array($type, $allowedTypes)) {
            return null;
        }

        return $this->getUrlFile() . '.webp';
    }

    protected function isResizedImage(string $url): bool
    {
        $needle = $this->prepareLink('/' . $this->getResizePathPrefix());
        return mb_strpos($url, $needle) === 0;
    }

    protected function isConvertedImage(string $url): bool
    {
        return mb_substr($url, -5) === '.webp';
    }

    protected function resizeNotExists($url): void
    {
        if (!$this->isResizedImage($url)) {
            return;
        }
        if (file_exists(public_path($url))) {
            return;
        }
        if (static::checkTransformCountLimit()) {
            return;
        }

        [$originalFilePath, $preset] = $this->getResizeInfoByUrl($url);

        // Оригинального файла нет, выходим
        // Может произойти при ресайзе webp картинки, если она еще не создана, поэтому ошибку не кидаем
        if (!file_exists(public_path($this->getOriginalPathPrefix() . $originalFilePath))) {
            return;
        }

        static::transformCountIncrement();
        Imagecache::get($originalFilePath, $preset);
    }

    protected function convertToWebpIfNotExists($url): bool
    {
        // Это путь к обычной картинке, конвертировать не нужно
        if (!$this->isConvertedImage($url)) {
            return false;
        }

        // Уже сконвертировано
        if (file_exists(public_path($url))) {
            return true;
        }

        // Достигнут лимит на запрос
        if (static::checkTransformCountLimit()) {
            return false;
        }

        $originalFilePath = mb_substr($url, 0, (mb_strlen($url) - 5));
        $source           = public_path($originalFilePath);
        $destination      = public_path($url);

        // Оригинального файла нет, выходим
        if (!file_exists($source)) {
            return false;
        }

        try {
            $type = mime_content_type($source);
        } catch (\Exception $e) {
            $type = null;
        }

        if ($type === 'image/jpeg') {
            $imageObject = imagecreatefromjpeg($source);
        } elseif ($type === 'image/png') {
            $imageObject = imagecreatefrompng($source);
        } else {
            $imageObject = null;
        }

        $saveResult = null;
        if ($imageObject) {
            // Иначе падает на изображениях с палитрой цветов
            imagepalettetotruecolor($imageObject);

            if ($type === 'image/png') {
                imagealphablending($imageObject, true);
                imagesavealpha($imageObject, true);
            }

            $quality = 80;
            try {
                if (BaseHelper::isProd()) {
                    $saveResult = @imagewebp($imageObject, $destination, $quality);
                } else {
                    $saveResult = imagewebp($imageObject, $destination, $quality);
                }
            } catch (\Error $e) {
                Log::error($e->getMessage());
            }

            imagedestroy($imageObject);
        }

        // Увеличиваем счетчик только при успешной конвертации, иначе исчерпаем весь лимит на ошибочных файлах
        if ($saveResult) {
            static::transformCountIncrement();
        }

        return !!$saveResult;
    }

    /**
     * Возвращает массив, у которгго первый элемент - путь к оригинальному файлу, второй - название пресета
     * @param string $url
     * @return array|string[]
     */
    protected function getResizeInfoByUrl(string $url): array
    {
        if (!$this->isResizedImage($url)) {
            return [$url, ''];
        }

        // убираем префикс сресайзенной картинки
        $url = str_replace('/' . $this->getResizePathPrefix(), '', $url);

        // получаем пресет для ресайза и путь к оригинальному файлу
        $pathParts        = explode('/', $url);
        $preset           = array_shift($pathParts);
        $originalFilePath = implode('/', $pathParts);

        return [$originalFilePath, $preset];
    }

    public function getData(): array
    {
        $this->prepareAttributes();

        // Получаем Webp урл и конвертируем + ресайзим
        $webpUrl   = $this->getWebpUrl();
        $converted = $webpUrl && $this->convertToWebpIfNotExists($webpUrl);
        if ($converted) {
            $this->resizeNotExists($webpUrl);
        }

        $source = $this->getUrlFile();
        try {
            $type = mime_content_type(P_PUBLIC_PATH . $source);
        } catch (\Exception $e) {
            $type = 'image/jpg';
        }

        $type = str_replace(['image/', 'jpeg'], ['', 'jpg'], $type);

        return [
            'webp'     => $webpUrl,
            'original' => $this->original ?: $this->url,
            'src'      => $this->url,
            $type      => $this->url,
            'size' => [
                'width'  => $this->attributes['width'] ?? '',
                'height' => $this->attributes['height'] ?? '',
            ],
        ];
    }

    public static function getFakeData($src): array
    {
        return [
            'src' => $src,
        ];
    }

    protected function prepareAttributes(): void
    {
        if (!isset($this->attributes['width']) && !isset($this->attributes['height'])) {
            $size = BaseHelper::getImageFileDimensions(public_path($this->getUrlFile()));

            if ($size) {
                $this->attributes['width']  = $size[0];
                $this->attributes['height'] = $size[1];
            }
        }
    }

    protected function prepareLink(?string $link): ?string
    {
        if (!$link) {
            return null;
        }

        return str_replace('\\', '/', $link);
    }
}

