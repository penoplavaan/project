<?php

namespace App\Pagination;

use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorInterface;
use Illuminate\Pagination\LengthAwarePaginator as LengthAwarePaginatorBase;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * Снаследован, чтобы переопределить реализацию UrlWindow
 */
class LengthAwarePaginator extends LengthAwarePaginatorBase {
    public static function buildByExists(
        LengthAwarePaginatorInterface $paginator,
        array $queryParams = []
    ): LengthAwarePaginatorInterface
    {
        return (new static(
            $paginator->items(),
            $paginator->total(),
            $paginator->perPage(),
            $paginator->currentPage(),
            array_merge($paginator->getOptions(), ['query' => $queryParams])
        ))->appends($paginator->query);
    }

    /**
     * Переопределен, чтобы не дописывал для первой страницы параметр в URL
     *
     * @param int $page
     * @return string
     */
    public function url($page): string {
        if ($page <= 0) {
            $page = 1;
        }

        $parameters  = [];

        // Не дописываем параметр для первой страницы, т.к. чтобы не было дубля страниц
        if ($page > 1) {
            $parameters[$this->pageName] = $page;
        }

        // If we have any extra query string key / value pairs that need to be added
        // onto the URL, we will put them in query string form and then attach it
        // to the URL. This allows for extra information like sortings storage.
        if (count($this->query) > 0) {
            $parameters = array_merge($this->query, $parameters);
        }

        $path = $this->path();

        // Чтобы не остался в урле символ "?", если нет параметров
        if (!empty($parameters)) {
            $path .= (Str::contains($path, '?') ? '&' : '?');
            $path .= Arr::query($parameters);
        }

        return $path . $this->buildFragment();
    }

    protected function elements(): array {
        $window = UrlWindow::make($this);

        return array_filter([
            $window['first'],
            is_array($window['slider']) ? '...' : null,
            $window['slider'],
            is_array($window['last']) ? '...' : null,
            $window['last'],
        ]);
    }
}
