<?php

namespace App\Pagination;

use Illuminate\Pagination\UrlWindow as UrlWindowBase;

class UrlWindow extends UrlWindowBase {
    /**
     * Смещение текущей страницы от минимальной или максимальной, при котором появляется многоточие.
     * Высчитывается как 1 для первой/последней страницы + 1 для многоточия + 1 для второй/предпоследней страницы
     * Вторую/предпоследнюю страницу учитываем для того, чтобы не получить стейт,
     * когда под многоточием скрыта всего одна страница, вида [1] [...] [3]
     *
     * @return int
     */
    public function getEdgeOffset(): int {
        return 3;
    }

    /**
     * Подменен, из-за того, что оригинальный метод в условии вызова $this->getSmallSlider прибавляет к ($onEachSide * 2) число 8
     * Что это за магическое число - непонятно, скорее всего это магическое число 4 из метода getUrlSlider, умноженное на 2
     *
     * @return array
     */
    public function get():array {
        $onEachSide = $this->paginator->onEachSide;

        if ($this->paginator->lastPage() <= (($onEachSide + $this->getEdgeOffset()) * 2)) {
            return $this->getSmallSlider();
        }

        return $this->getUrlSlider($onEachSide);
    }

    /**
     * Подменен, потому что оригинальный метод выставляет значение $window равным $onEachSide + 4
     * Что это за магическое число 4 не понятно, но из-за него пагинация считается некорректно
     *
     * Вместо ожидаемого стейта [1!] [2] [3] [4] [...] [N]
     * Получаем [1!] [2] [3] [4] [5] [6] [7] [8] [...] [N]
     *
     * @param int $onEachSide
     * @return array
     */
    protected function getUrlSlider($onEachSide): array {
        if (!$this->hasPages()) {
            return ['first' => null, 'slider' => null, 'last' => null];
        }

        $window = $onEachSide;
        $edgeOffset = $this->getEdgeOffset();

        if ($this->currentPage() <= $window + $edgeOffset) {
            return $this->getSliderTooCloseToBeginning($window, $this->currentPage());
        }

        elseif ($this->currentPage() > ($this->lastPage() - ($window + $edgeOffset))) {
            return $this->getSliderTooCloseToEnding($window, $this->lastPage() - $this->currentPage() + 1);
        }

        return $this->getFullSlider($onEachSide);
    }

    /**
     * Подмененен, чтобы в начале пагинатора был только один элемент
     * @return array
     */
    public function getStart(): array {
        return $this->paginator->getUrlRange(1, 1);
    }

    /**
     * Подмененен, чтобы в конце пагинатора был только один элемент
     * @return array
     */
    public function getFinish(): array {
        return $this->paginator->getUrlRange($this->lastPage(), $this->lastPage());
    }
}
