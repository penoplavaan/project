<?php

namespace App\Traits;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

trait LoggableTrait {

    protected Logger $logger;

    public function makeLogger($logFile = 'simple') {
        $this->logger = new Logger('simple');
        $this->logger->pushHandler(
            new StreamHandler(
                storage_path(
                    'logs'
                    . DIRECTORY_SEPARATOR . $logFile . '.log'
                ),
                Logger::INFO
            )
        );
    }

    public function log($message, $context = []) {
        $this->logger->log(Logger::INFO, $message, $context);
    }

    public function warning($message, $context = []) {
        $this->logger->log(Logger::WARNING, $message, $context);
    }

    public function error($message, $context = []) {
        $this->logger->log(Logger::ERROR, $message, $context);
    }

}
