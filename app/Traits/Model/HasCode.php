<?php

namespace App\Traits\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Автогенерация символьного кода из поля name, если таковой еще не заполнен. 
 * Сохраняет в поле code
 */
trait HasCode
{

    public function getCodeGenerateColumn()
    {
        return 'name';
    }

    public function getCodeColumn(): string
    {
        return 'code';
    }

    public static function bootHasCode() {
        static::creating(function (Model $model) {
            $model->setCodeBy($model->getCodeGenerateColumn());
        });

        static::updating(function (Model $model) {
            $model->setCodeBy($model->getCodeGenerateColumn());
        });
    }

    public function setCodeBy(string $column)
    {
        if ($this->getAttribute($this->getCodeColumn())) {
            return;
        }
        
        $code = Str::slug($this->getAttribute($column));
        
        $this->code = $code;
    }
}