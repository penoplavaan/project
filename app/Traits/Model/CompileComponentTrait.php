<?php

namespace App\Traits\Model;

use App\Services\TextComponentService;

trait CompileComponentTrait {

    public function compileComponents(string $column): string {
        /** @var TextComponentService $textComponentService */
        $textComponentService = app()->get(TextComponentService::class);

        $text = $this->getAttribute($column);

        if (!$text) return '';

        return $textComponentService->replaceComponents($text);
    }
}
