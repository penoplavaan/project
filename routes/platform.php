<?php

declare(strict_types=1);

use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\Menu\MenuScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\SiteSettings\SiteSettingsEditScreen;
use App\Orchid\Screens\SiteSettings\SiteSettingsScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

use App\Orchid\Http\Controllers as Con;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

Route::name('platform.')->group(function () {
    Route::name('site-settings.')->prefix('site-settings')->group(function () {
        // Platform > SiteSettings
        Route::screen('/', SiteSettingsScreen::class)
            ->name('index')
            ->breadcrumbs(function (Trail $trail) {
                return $trail
                    ->parent('platform.index')
                    ->push('Настройки', route('platform.site-settings.index'));
            });

        // Platform > SiteSettings > Create
        Route::screen('create', SiteSettingsEditScreen::class)
            ->name('create')
            ->breadcrumbs(function (Trail $trail) {
                return $trail
                    ->parent('platform.site-settings.index')
                    ->push('Создание настройки', route('platform.site-settings.create'));
            });

        // Platform > SiteSettings > Edit
        Route::screen('{siteSetting}/edit', SiteSettingsEditScreen::class)
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, $siteSettings) {
                return $trail
                    ->parent('platform.site-settings.index')
                    ->push('Редактирование настройки', route('platform.site-settings.edit', $siteSettings));
            });
    });


    Route::name('text.')->prefix('text')->group(function () {
        Route::get('templates', [Con\TextController::class, 'templateList'])->name('templates');
        Route::get('component', [Con\TextController::class, 'componentList'])->name('components');
    });


    /**
     * Штатные роуты
     */


    // Platform > Menu
    Route::screen('menu', MenuScreen::class)
        ->name('menu');

    // Main
    Route::screen('/main', PlatformScreen::class)
        ->name('main');

    // Platform > Profile
    Route::screen('profile', UserProfileScreen::class)
        ->name('profile')
        ->breadcrumbs(function (Trail $trail) {
            return $trail
                ->parent('platform.index')
                ->push(__('Profile'), route('platform.profile'));
        });

    // Platform > System > Users
    Route::screen('users/{user}/edit', UserEditScreen::class)
        ->name('systems.users.edit')
        ->breadcrumbs(function (Trail $trail, $user) {
            return $trail
                ->parent('platform.systems.users')
                ->push(__('User'), route('platform.systems.users.edit', $user));
        });

    // Platform > System > Users > Create
    Route::screen('users/create', UserEditScreen::class)
        ->name('systems.users.create')
        ->breadcrumbs(function (Trail $trail) {
            return $trail
                ->parent('platform.systems.users')
                ->push(__('Create'), route('platform.systems.users.create'));
        });

    // Platform > System > Users > User
    Route::screen('users', UserListScreen::class)
        ->name('systems.users')
        ->breadcrumbs(function (Trail $trail) {
            return $trail
                ->parent('platform.index')
                ->push(__('Users'), route('platform.systems.users'));
        });

    // Platform > System > Roles > Role
    Route::screen('roles/{role}/edit', RoleEditScreen::class)
        ->name('systems.roles.edit')
        ->breadcrumbs(function (Trail $trail, $role) {
            return $trail
                ->parent('platform.systems.roles')
                ->push(__('Role'), route('platform.systems.roles.edit', $role));
        });

    // Platform > System > Roles > Create
    Route::screen('roles/create', RoleEditScreen::class)
        ->name('systems.roles.create')
        ->breadcrumbs(function (Trail $trail) {
            return $trail
                ->parent('platform.systems.roles')
                ->push(__('Create'), route('platform.systems.roles.create'));
        });

    // Platform > System > Roles
    Route::screen('roles', RoleListScreen::class)
        ->name('systems.roles')
        ->breadcrumbs(function (Trail $trail) {
            return $trail
                ->parent('platform.index')
                ->push(__('Roles'), route('platform.systems.roles'));
        });


    /**
     * Примеры
     */

    // Example...
    Route::screen('example', ExampleScreen::class)
        ->name('example')
        ->breadcrumbs(function (Trail $trail) {
            return $trail
                ->parent('platform.index')
                ->push('Example screen');
        });

    Route::screen('example-fields', ExampleFieldsScreen::class)->name('example.fields');
    Route::screen('example-layouts', ExampleLayoutsScreen::class)->name('example.layouts');
    Route::screen('example-charts', ExampleChartsScreen::class)->name('example.charts');
    Route::screen('example-editors', ExampleTextEditorsScreen::class)->name('example.editors');
    Route::screen('example-cards', ExampleCardsScreen::class)->name('example.cards');
    Route::screen('example-advanced', ExampleFieldsAdvancedScreen::class)->name('example.advanced');
});
