<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers as Con;

/**
 * Вебхуки, в частности - для платёжек
 */

Route::get('/sberbank/status', [Con\Order\PaymentController::class, 'sberbankStatusChange'])->name('order-sberbank-status');
