<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers as Con;

// Специально для людей, которые путают админки на проектах
if (config('app.debug')) {
    Route::get('/bitrix', [Con\AdminController::class, 'notBitrix']);
}

Route::get('/', [Con\IndexController::class, 'index'])->name('home');
Route::get('/?email-verified', [Con\IndexController::class, 'index'])->name('login');
Route::get('/reset-password/{token}', [Con\IndexController::class, 'index'])->name('password.reset');

Route::get('/compilations', [Con\IndexController::class, 'compilations'])->name('home.compilations'); // Подборки на главной

Route::prefix('/location')->name('location.')->group(function() {
    Route::get('/data', [Con\LocationController::class, 'getData'])->name('data');
    Route::post('/set', [Con\LocationController::class, 'setCity'])->name('set');
    Route::get('/suggest', [Con\LocationController::class, 'suggest'])->middleware(['js.router'])->name('suggest');
});

Route::prefix('/popup')->name('popup.')->group(function() {
    Route::get('/feedback', [Con\PopupController::class, 'getFeedback'])->name('feedback');
    Route::get('/feedback/{option}', [Con\PopupController::class, 'getFeedbackPreopend'])->name('feedback');
    Route::post('/save/feedback', [Con\PopupController::class, 'saveFeedback'])->name('save.feedback');

    Route::get('/callback/form', [Con\PopupController::class, 'callbackForm'])->name('callback.form');
    Route::post('/callback/save', [Con\PopupController::class, 'callbackSave'])->name('callback.save');
});

Route::prefix('/for-cooks')->name('for-cooks.')->group(function() {
    Route::get('', [Con\ForCooksController::class, 'index'])->name('index');
    Route::get('/become-cook/popup', [Con\ForCooksController::class, 'becomeCookPopup'])->middleware(['auth', 'not-a-cook'])->name('form-settings');
    Route::post('/become-cook/submit', [Con\ForCooksController::class, 'becomeCookForm'])->middleware(['auth', 'not-a-cook'])->name('form-submit');
});

// Все блюда с подразделами
Route::prefix('/menu')->name('menu.')->group(function () {
    Route::get('', [Con\MenuController::class, 'index'])->name('index');
    Route::post('/suggests', [Con\MenuController::class, 'suggests'])->name('dish.suggests');
    Route::get('/detail/{dish}', [Con\MenuController::class, 'detail'])->name('dish.detail');
    Route::get('/{dishCategory}', [Con\MenuController::class, 'index'])->name('dish.category');
});

// Корзина
Route::prefix('/basket')->name('basket.')->group(function () {
    Route::get('', [Con\BasketController::class, 'index'])->middleware('js.router')->name('index');
    Route::post('/add', [Con\BasketController::class, 'add'])->middleware('js.router')->name('add');
    Route::post('/set-count', [Con\BasketController::class, 'setCount'])->middleware('js.router')->name('set-count');
    Route::post('/remove', [Con\BasketController::class, 'remove'])->middleware('js.router')->name('remove');
    Route::post('/clear', [Con\BasketController::class, 'clear'])->middleware('js.router')->name('clear');
});

// Оформление заказа
Route::prefix('/order')->name('order.')->group(function () {
    Route::redirect('', '/basket'); // Нельзя придти на оформление заказа, не указав ID повара
    Route::get('/{cookId}', [Con\OrderController::class, 'index'])->middleware('js.router')->name('index');
    Route::post('/{cookId}/checkout', [Con\OrderController::class, 'checkout'])->middleware('js.router')->name('checkout');
    Route::get('/success/{orderId}', [Con\OrderController::class, 'success'])->name('success');
});

// Все повара
Route::prefix('/cooks')->name('cooks.')->group(function () {
    Route::get('', [Con\CookController::class, 'index'])->name('index');
    Route::get('/{cook}', [Con\CookController::class, 'detail'])->name('detail');
    Route::get('/{cook}/reviews', [Con\CookController::class, 'reviews'])->name('reviews');
    Route::post('/{cook}/review/form', [Con\CookController::class, 'reviewForm']);
    Route::post('/{cook}/review/submit', [Con\CookController::class, 'reviewSubmit'])->name('review-submit');
    Route::get('/pin/{cook}', [Con\CookController::class, 'pin'])->name('pin');
    Route::post('/filter', [Con\CookController::class, 'filter'])->name('filter');
    Route::post('/suggests', [Con\CookController::class, 'suggests'])->name('suggests');
});

// AJAX-запросы на авторизацию/регистрацию
Route::prefix('/auth')->name('account.')->group(function () {
    Route::post('/auth', [Con\AuthController::class, 'auth'])->name('auth');
    Route::post('/register', [Con\AuthController::class, 'register'])->name('register');
    Route::post('/send-code', [Con\AuthController::class, 'sendSmsCode'])->middleware('js.router')->name('send-code');
    Route::post('/reset-send', [Con\AuthController::class, 'resetSend'])->name('reset-send');
    Route::post('/reset-pass', [Con\AuthController::class, 'resetPassword'])->name('reset-password');
    Route::post('/verify-phone', [Con\AuthController::class, 'verifyPhone'])
        ->middleware(['auth'])
        ->name('verify-phone');

    Route::post('/logout', [Con\AuthController::class, 'logout'])->name('logout');
});

// ЛК пользователя
// todo вынести в отдельный роут-файл для ЛК
Route::prefix('/profile')->middleware(['auth'])->name('profile.')->group(function () {
    Route::get('', [Con\ProfileController::class, 'index'])->name('index');

    // todo роуты Dish. Отрефакторить. Сгруппировать. Возможно - вынести в отдельный контроллер
    Route::get('/dishes', [Con\ProfileController::class, 'dishes'])
        ->middleware(['is-cook'])
        ->name('dishes');

    Route::post('/add-dish', [Con\ProfileController::class, 'addDish'])
        ->middleware(['is-cook'])
        ->name('add-dish');

    Route::get('/get-dish/{dish}', [Con\ProfileController::class, 'getDish'])
        ->middleware(['is-cook'])
        ->name('get-dish');

    Route::get('/get-dishes/ajax', [Con\ProfileController::class, 'getDishesAjax'])
        ->middleware(['is-cook'])
        ->name('get-dishes');

    Route::get('/useful', [Con\ProfileController::class, 'useful'])
        ->middleware(['is-cook'])
        ->name('useful');

    // todo Отрефакторить. Сгруппировать.
    Route::get('/chat', [Con\ProfileChatController::class, 'index'])
        ->middleware('js.router')
        ->name('chat-list');

    Route::post('/chat/load', [Con\ProfileChatController::class, 'loadFullData']);
    Route::post('/chat/message', [Con\ProfileChatController::class, 'sendMessage']);
    Route::post('/chat/last-read', [Con\ProfileChatController::class, 'updateLastRead']);

    Route::post('/change-password', [Con\ProfileController::class, 'changePassword'])->name('change-password');
    Route::post('/change-data', [Con\ProfileController::class, 'changeProfileData'])->name('change-profile-data');
    Route::post('/change-cook-data', [Con\ProfileController::class, 'changeCookProfileData'])
        ->middleware(['is-cook'])
        ->name('change-cook-profile-data');

    Route::post('/kitchen/upload', [Con\ProfileController::class, 'kitchenUpload'])
        ->middleware(['is-cook'])
        ->name('kitchen-upload');
    Route::post('/kitchen/remove', [Con\ProfileController::class, 'kitchenRemove'])
        ->middleware(['is-cook'])
        ->name('kitchen-remove');

    Route::post('/document/upload', [Con\ProfileController::class, 'documentUpload'])
        ->middleware(['is-cook'])
        ->name('document-upload');
    Route::post('/document/remove', [Con\ProfileController::class, 'documentRemove'])
        ->middleware(['is-cook'])
        ->name('document-remove');

    // todo Отрефакторить. Подтащить к верхнеё группе роутов про Dish
    Route::post('/dish/remove', [Con\ProfileController::class, 'dishRemove'])
        ->middleware(['is-cook'])
        ->name('dish-remove');

});

// todo а это что такое? нет такого экшна
Route::post('/dish/unsetPicture/{id}', [Con\ProfileController::class, 'attachmentRemove'])
    ->name('deleteAttachment');

// Подтверждение емейла после регистрации
//Route::get('/email/verify', [Con\AuthController::class, 'verifyStub'])->middleware('auth')->name('verification.notice');
Route::get('/email/verify/{id}/{hash}', [Con\AuthController::class, 'verifyEmail'])->middleware(['signed'])->name('verification.verify');



// ЭТОТ РОУТ ДОЛЖЕН БЫТЬ САМЫМ ПОСЛЕДНИМ, Т.К. ПОСЛЕ НЕГО УЖЕ НИЧЕГО НЕ СМАТЧИТСЯ
// Текстовые страницы и 404
Route::fallback([Con\TextPageController::class, 'index']);
