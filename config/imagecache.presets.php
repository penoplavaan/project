<?php

return [
    'admin.preview' => [
        'width'  => 100,
        'height' => 100,
        'method' => 'crop', // crop, resize
    ],
    'header.avatar' => [
        'width'  => 40,
        'height' => 40,
        'method' => 'crop',
    ],
    'for-cooks.review' => [
        'width'  => 392,
        'height' => 680,
        'method' => 'crop',
    ],
    'index.promo' => [
        'width'  => 577,
        'height' => 577,
        'method' => 'crop',
    ],
    'index.promo.cook' => [
        'width'  => 184,
        'height' => 184,
        'method' => 'crop',
    ],
    'index.review' => [
        'width'  => 112,
        'height' => 112,
        'method' => 'crop',
    ],
    'profile.avatar' => [
        'width'  => 176,
        'height' => 176,
        'method' => 'crop',
    ],
    'dish.card' => [
        'width'  => 744,
        'height' => 744,
        'method' => 'crop',
    ],
    'main.category.compilation' => [
        'width'  => 352,
        'height' => 400,
        'method' => 'crop',
    ],
    'cook.attachment' => [
        'width'  => 245,
        'height' => 245,
        'method' => 'crop',
    ],
    'gallery.attachment' => [
        'width'  => 765,
        'height' => 450,
        'method' => 'resize',
    ],
    'gallery.thumb' => [
        'width'  => 148,
        'height' => 148,
        'method' => 'resize',
    ],
    'chat.avatar' => [
        'width'  => 50,
        'height' => 50,
        'method' => 'crop',
    ],
    'chat.dish.preview' => [
        'width'  => 57,
        'height' => 57,
        'method' => 'crop',
    ],
    'detail.dish.gallery' => [
        'width'  => 744,
        'height' => 744,
        'method' => 'crop',
    ],
    'basket.row' => [
        'width'  => 60,
        'height' => 60,
        'method' => 'crop',
    ],
    'order.basket' => [
        'width'  => 56,
        'height' => 56,
        'method' => 'crop',
    ],
];
