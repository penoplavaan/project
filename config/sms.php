<?php

use Illuminate\Support\Facades\Facade;

return [

    'sender' => env('SMS_SENDER', 'fake'),

    'settings' => [
        'fake' => [
            'class'    => \App\Services\SmsSender\Fake::class,
        ],
        'smsc' => [
            'class'    => \App\Services\SmsSender\Smsc::class,
            'login'    => env('SMS_SENDER_SMSC_LOGIN', ''),
            'password' => env('SMS_SENDER_SMSC_PASSWORD', ''),
            'sender'   => env('SMS_SENDER_SMSC_SENDER', ''),
        ]
    ],
];
