COMPOSE = docker-compose
PM_EXEC := $(COMPOSE) exec laravel bash

.PHONY : all help up down recreate restart stop logs install cmd fix phpstan test-coverage

all: help

help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-14s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

init:
	@if [[ -f .env ]]; then echo "File .env already exists. Abort." && exit 1; fi
	cp .env.example .env
	docker compose run --entrypoint='' --rm --no-deps laravel /bin/bash -c "\
		composer install \
		&& php artisan key:generate \
		&& php artisan jwt:secret --force \
	";

ps:
	$(COMPOSE) ps

up: ## Run all services
	$(COMPOSE) up -d

down: ## Stop and remove all services
	$(COMPOSE) down

recreate: down up ## Restart all services

restart: ## Restart all services
	$(COMPOSE) restart

stop: ## Stop all services
	$(COMPOSE) stop

logs: ## Show logs
	$(COMPOSE) logs -f

install: ## Install all dependencies
	$(COMPOSE) composer install

shell: ## Shell
	$(COMPOSE) exec laravel bash

root-shell: ## Root shell
	$(COMPOSE) exec --user=root laravel bash

fix: ## Run PHP CodeStyle fix
	$(COMPOSE) composer run fix

test:
	$(COMPOSE) composer run tests

phpstan: ## Run Static Analysis (PHPStan)
	$(COMPOSE) composer run phpstan

clear-stan: ## Clears PHPStan cache
	$(COMPOSE) composer run clear-stan

test-coverage: ## Run tests with HTML coverage
	$(COMPOSE) php -dpcov.enabled=1 ./vendor/bin/phpunit --coverage-html coverage

