<?php

namespace Database\Seeders;

use App\Models\Cook;
use App\Models\DishCategory;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DishSeeder extends Seeder
{

    public function run()
    {
        /** @var Generator $faker */
        $faker = app()->make(Generator::class);

        $categories = DishCategory::query()->get()->pluck('id')->toArray();
        $activeCookIds = Cook::query()->where('active', 1)->where('verified', 1)->get()->pluck('id')->toArray();

        $preParts = ['вкусный', 'сладкий', 'странный', 'цветной', 'праздничный'];
        $nameParts = ['суп', 'мясо', 'горячее', 'гуляш', 'торт', 'сдоба'];
        $ingrPart = ['с морковкой', 'с мясом', 'с сыром', 'с клёцками', 'с рыбой', 'без нихуя', 'с овощами', 'с красной икрой', 'с картошкой', 'со всем подряд'];
        $byPart = ['по пекински', 'по сибирски', 'по московски', 'по кремлевски', 'по арабски', 'по мексикански'];
        $forPart = ['для студентов', 'для всех', 'к праздничному столу', 'для хорошего настроения', 'для холодной погоды', 'для жаркой погоды'];

        foreach ($activeCookIds as $cookId) {
            $cookDishCount = rand(0, 10);

            for ($i = 0; $i < $cookDishCount; $i++) {

                $parts = [
                    rand(0, 2) ? $preParts[array_rand($preParts)] : '',
                    $nameParts[array_rand($nameParts)],
                    rand(0, 2) ? $ingrPart[array_rand($ingrPart)] : '',
                    rand(0, 2) ? $byPart[array_rand($byPart)] : '',
                    rand(0, 1) ? $forPart[array_rand($forPart)] : '',
                ];
                $parts = array_filter($parts);

                DB::table('dishes')->insert([
                    'active'            => 1,
                    'cook_id'           => $cookId,
                    'category_id'       => $categories[array_rand($categories)],
                    'name'              => ucfirst(implode(' ', $parts)),
                    'price'             => rand(2, 100) * 10,
                    'weight'            => rand(0, 1) ? rand(2, 100) * 10 : '',
                    'quantity'          => rand(0, 1) ? rand(1, 5) : '',
                    'preview_text'      => $faker->text(200),
                    'fats'              => '9999', // У всех тестовых товаров количество жиров будет равно 9999
                ]);
            }
        }
    }
}
