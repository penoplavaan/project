<?php

namespace Database\Seeders;

use App\Models\Cook;
use App\Models\DishCategory;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CookSeeder extends Seeder
{

    public function run()
    {
        /** @var Generator $faker */
        $faker = app()->make(Generator::class);

        $categories = DishCategory::query()->get()->pluck('id')->toArray();

        for ($i = 0; $i < 500; $i++) {
            DB::table('cooks')->insert([
                'user_id'           => 76,
                'speciality_id'     => rand(1, 4),
                'active'            => rand(0, 10) > 0 ? 1 : 0,
                'sort'              => 500,
                'verified'          => rand(0, 10) > 0 ? 1 : 0,
                'passport_verified' => 1,
                'sanitary_verified' => 1,
                'about'             => $faker->text(200),
                'show_on_main'      => 1,
                'address'           => 'Россия, Москва, Красная площадь ' . rand(1, 200),
                'coords'            => '',
            ]);
        }

        $cooks = Cook::query()->where('user_id', 76)->get();

        foreach ($cooks as $cook) {
            if (empty($cook->coords)) {
                $cook->coords = implode(',', [
                    rand(4000, 6500) / 100,
                    rand(3000, 13000) / 100,
                ]);
                $cook->save();

                $randCats = array_rand($categories, rand(2,5));
                foreach ($randCats as $randCat) {
                    DB::table('cook_categories')->insert([
                        'cook_id'     => $cook->id,
                        'category_id' => $categories[$randCat],
                    ]);
                }
            }
        }
    }
}
