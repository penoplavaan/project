<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dishes_to_tags', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('dish_id');
            $table->unsignedBigInteger('tag_id');

            $table->foreign(['dish_id'], 'fk__dish_id')->references(['id'])->on('dishes');
            $table->foreign(['tag_id'], 'fk__tag_id')->references(['id'])->on('tags');

            $table->index('dish_id');
            $table->index('tag_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dishes_to_tags');
    }
};
