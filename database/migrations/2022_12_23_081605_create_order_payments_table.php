<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('order_payments', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('order_id');
            $table->foreign('order_id', 'fk__order_payments__order_id')
                ->references('id')
                ->on('orders');

            $table->unsignedBigInteger('pay_system_id');
            $table->foreign('pay_system_id', 'fk__order_payments__pay_system_id')
                ->references('id')
                ->on('pay_systems');

            $table->integer('sum');
            $table->unsignedTinyInteger('payed')->default('0');
            $table->dateTime('pay_date')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_payments');
    }
};
