<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{

    public function up()
    {
        $chatSection = new SiteSettingsSection([
            'name' => 'Чаты',
            'code' => 'chat',
        ]);
        $chatSection->save();
        $chatSection->refresh();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'chat.send-unread-email',
            'text'       => '5',
            'big_text'   => 'Через сколько минут после отправки сообщения отправлять собеседнику оповещение о непрочитанном сообщении. 0 - отправлять сразу (проверка на кроне 1 раз в минуту, число больше нуля - задержка отправки в количество минут, -1 - не отправлять оповещения на почту (либо деактивировать эту настройку)',
            'section_id' => $chatSection->id,
            'sort'       => 500,
        ]))->save();
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'chat.send-unread-email')->get()->first()->delete();
        SiteSettingsSection::query()->where('code', 'chat')->get()->first()->delete();
    }
};
