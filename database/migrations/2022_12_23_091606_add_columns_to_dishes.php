<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->integer('named_sort')->after('category_id')->default('3');
        });
    }


    public function down()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->dropColumn('named_sort');
        });
    }
};
