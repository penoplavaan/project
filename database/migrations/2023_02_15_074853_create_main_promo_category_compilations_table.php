<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration {

    public function up()
    {
        Schema::create('main_promo_category_compilations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedTinyInteger('active')->default('1');
            $table->integer('sort')->default(500);
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('main_promo_category_compilations');
    }
};
