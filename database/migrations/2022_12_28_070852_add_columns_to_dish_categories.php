<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('dish_categories', function (Blueprint $table) {
            $table->string('h1')->nullable();
            $table->string('menu_title')->nullable();
            $table->string('description')->nullable();
        });
    }
    public function down()
    {
        Schema::table('dish_categories', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('menu_title');
            $table->dropColumn('description');
        });
    }
};
