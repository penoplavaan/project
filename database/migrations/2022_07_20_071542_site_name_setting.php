<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{

    public function up()
    {
        $section = SiteSettingsSection::query()->where('code', 'site')->get()->first();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'site.name',
            'text'       => 'Повар на связи',
            'big_text'   => 'Название сайта',
            'section_id' => $section->id,
            'sort'       => 1,
        ]))->save();
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'site.name')->get()->first()?->delete();
    }
};
