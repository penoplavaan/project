<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('sms_codes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('client_ip', 50)->default('');
            $table->string('phone', 20)->index();
            $table->string('code', 10);
            $table->string('type', 50)->nullable();
            $table->unsignedTinyInteger('confirmed')->default('0');
            $table->string('confirmed_session_id')->default('');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sms_codes');
    }
};
