<?php

use App\Helpers\ViewHelper;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $allUsers = User::query()->select(['id', 'phone'])->get();

        foreach ($allUsers as $user) {
            $phone = ViewHelper::clearPhone($user->phone);
            if ($phone !== $user->phone) {
                $user->phone = $phone;
                $user->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
