<?php

use App\Models\TextBlock;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    public function up()
    {
        (new TextBlock([
                             'code'      => 'for-cooks.cooks-review',
                             'title'     => 'Отзывы участников сервиса',
                             'text1'     => 'Отзывы участников сервиса',
                             'text2'     => '',
                             'big_text'  => '',
                             'sort'      => 1,
                         ]))->save();
    }

    public function down()
    {
        TextBlock::query()->where('code', 'for-cooks.cooks-review')->get()->first()?->delete();
    }
};
