<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\SiteSetting;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $section = \App\Models\SiteSettingsSection::query()->where('code', 'site')->first();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'dish.delivery-default',
            'text'       => '',
            'big_text'   => '',
            'section_id' => $section->id,
            'sort'       => 500,
        ]))->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SiteSetting::query()->where('code', 'dish.delivery-default')->get()->first()->delete();
    }
};
