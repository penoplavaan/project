<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_to_dishes', function (Blueprint $table) {
            $table->renameColumn('price', 'price_one');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_to_dishes', function (Blueprint $table) {
            $table->renameColumn('price_one', 'price');
        });
    }
};
