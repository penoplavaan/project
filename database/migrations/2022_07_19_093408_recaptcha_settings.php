<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        $section = new SiteSettingsSection([
            'name' => 'ReCaptcha',
            'code' => 'recaptcha',
        ]);
        $section->save();
        $section->refresh();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'recaptcha.active',
            'text'       => '1',
            'big_text'   => 'Включено использование ReCaptcha в формах',
            'section_id' => $section->id,
            'sort'       => 100,
        ]))->save();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'recaptcha.site-key',
            'text'       => '6LdA9AEhAAAAAAYhtYjo9q5zpYfYsRh_T2GYaLqo',
            'big_text'   => 'ReCaptcha: ключ',
            'section_id' => $section->id,
            'sort'       => 200,
        ]))->save();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'recaptcha.secret-key',
            'text'       => '6LdA9AEhAAAAAF11uO8zxLUHSMfKtyu0da6QXNCa',
            'big_text'   => 'ReCaptcha: секрет',
            'section_id' => $section->id,
            'sort'       => 300,
        ]))->save();
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'recaptcha.active')->get()->first()?->delete();
        SiteSetting::query()->where('code', 'recaptcha.site-key')->get()->first()?->delete();
        SiteSetting::query()->where('code', 'recaptcha.secret-key')->get()->first()?->delete();
        SiteSettingsSection::query()->where('code', 'recaptcha')->get()->first()?->delete();
    }
};
