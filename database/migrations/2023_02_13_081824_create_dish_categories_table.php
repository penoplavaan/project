<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dishes_to_additional_categories', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('dish_id');
            $table->unsignedBigInteger('category_id');

            $table->foreign(['dish_id'], 'fk__dishes_id')->references(['id'])->on('dishes')->onDelete('cascade');
            $table->foreign(['category_id'], 'fk__category_id')->references(['id'])->on('dish_categories')->onDelete('cascade');

            $table->index(['dish_id', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dishes_to_categories');
    }
};
