<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $section = SiteSettingsSection::query()->where('code', 'popups')->get()->first();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'register.success-no-email',
            'text'       => '',
            'big_text'   => 'Спасибо за регистрацию!',
            'section_id' => $section->id,
            'sort'       => 500,
        ]))->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SiteSetting::query()->where('code', 'register.succes-no-email')->get()->first()->delete();
    }
};
