<?php

use App\Models\SiteSetting;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up()
    {
        $section = \App\Models\SiteSettingsSection::query()->where('code', 'chat')->first();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'chat.send-order-sms',
            'text'       => '1',
            'big_text'   => '<div>Отправлять ли повару СМС о новом заказе.</div><div>1 - отправлять</div><div>0, пустое значение, либо отключить активность - не отправлять</div>',
            'section_id' => $section->id,
            'sort'       => 500,
        ]))->save();
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'verify-phone.success')->get()->first()->delete();
    }
};
