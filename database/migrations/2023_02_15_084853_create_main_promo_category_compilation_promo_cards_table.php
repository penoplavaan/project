<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration {

    public function up()
    {
        Schema::create('main_promo_category_compilation_promo_cards', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedTinyInteger('active')->default('1');

            $table->unsignedBigInteger('compilation_id');
            $table->unsignedBigInteger('promo_card_id');

            $table->foreign(['compilation_id'], 'fk__main_promo_category_compilation_id')->references(['id'])->on('main_promo_category_compilations')->onDelete('cascade');
            $table->foreign(['promo_card_id'], 'fk__promo_card_id')->references(['id'])->on('promo_cards')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('main_promo_category_compilation_promo_cards');
    }
};
