<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('promo_cards', function (Blueprint $table) {
            $table->unsignedTinyInteger('show_on_main_category')->default('1');
        });
    }
    public function down()
    {
        Schema::table('dish_categories', function (Blueprint $table) {
            $table->dropColumn('show_on_main_category');
        });
    }
};
