<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    public function up()
    {
        Schema::create('pay_systems', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('code');
            $table->string('name');
            $table->text('description')->nullable();
        });

        DB::table('pay_systems')->insert([
            'code'        => 'sberbank',
            'name'        => 'Банковской картой на сайте',
            'description' => 'После нажатия кнопки Оформить заказ вы будете перенаправлены на страницу оплаты',
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('pay_systems');
    }
};
