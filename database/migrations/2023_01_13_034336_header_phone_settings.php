<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        $section = new SiteSettingsSection([
            'name' => 'Шапка сайта',
            'code' => 'header',
        ]);
        $section->save();
        $section->refresh();

        $existSetting = SiteSetting::query()->where('code', 'header.phone')->get()->first();

        if ($existSetting) {
            $existSetting->section_id = $section->id;
            $existSetting->save();
        } else {
            (new SiteSetting([
                'active'     => '1',
                'code'       => 'header.phone',
                'text'       => '+71234567890',
                'big_text'   => 'Телефон в шапке',
                'section_id' => $section->id,
                'sort'       => 100,
            ]))->save();
        }
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'header.phone')->get()->first()?->delete();
        SiteSettingsSection::query()->where('code', 'header')->get()->first()?->delete();
    }
};
