<?php

use App\Helpers\ViewHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable(true)->change();
            $table->string('phone', 20);
            $table->string('delivery_address')->nullable();
            $table->date('delivery_date')->nullable();
            $table->string('delivery_time', 20)->nullable();
            $table->text('comment')->nullable();
        });

        // Заполнить новые поля
        $orders = \App\Models\Order::query()->with('user')->get();
        foreach ($orders as $order) {
            $order->phone = $order->user && !empty($order->user->phone) ? ViewHelper::clearPhone($order->user->phone) : '';
            $order->delivery_address = '—';
            $order->delivery_date = $order->created_at;
            $order->delivery_time = '—';
            $order->save();
        }
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable(false)->change();
            $table->dropColumn('phone');
            $table->dropColumn('delivery_address');
            $table->dropColumn('delivery_date');
            $table->dropColumn('delivery_time');
            $table->dropColumn('comment');
        });
    }
};
