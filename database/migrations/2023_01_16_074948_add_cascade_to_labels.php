<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dish_labels', function (Blueprint $table) {
            $table->foreign(['dish_id'], 'fk__d2l_dish_id')->references(['id'])->on('dishes')->onDelete('cascade');
            $table->foreign(['label_id'], 'fk__d2l_label_id')->references(['id'])->on('labels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dish_labels', function (Blueprint $table) {
            $table->dropForeign('fk__d2l_dish_id');
            $table->dropForeign('fk__d2l_label_id');
        });
    }
};
