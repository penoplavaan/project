<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->index('category_id', 'dishes_category_id');
            $table->index('cook_id', 'dishes_cook_id');
            $table->index('active', 'dishes_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->dropIndex('dishes_category_id');
            $table->dropIndex('dishes_cook_id');
            $table->dropIndex('dishes_active');
        });
    }
};
