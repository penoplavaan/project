<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('client_name')->after('payed');
            $table->string('recipient_name')->after('phone')->nullable();
            $table->string('recipient_phone')->after('recipient_name')->nullable();
            $table->renameColumn('phone', 'client_phone');
        });

        // Заполнить новые поля
        $orders = \App\Models\Order::query()->with('user')->get();
        foreach ($orders as $order) {
            $order->client_name = $order->user->getFullName() ?? '';
            $order->save();
        }
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('client_name');
            $table->dropColumn('recipient_name');
            $table->dropColumn('recipient_phone');
            $table->renameColumn('client_phone', 'phone');
        });
    }
};
