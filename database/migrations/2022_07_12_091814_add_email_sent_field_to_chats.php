<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::table('chats', function (Blueprint $table) {
            $table->tinyInteger('from_email_sent')->default(0);
            $table->tinyInteger('to_email_sent')->default(0);
        });
    }

    public function down()
    {
        Schema::table('chats', function (Blueprint $table) {
            $table->dropColumn('from_email_sent');
            $table->dropColumn('to_email_sent');
        });
    }
};
