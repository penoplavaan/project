<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('chat_message_to_dishes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->tinyInteger('count')->default(1);
            $table->integer('price');
            $table->string('name');
            $table->unsignedBigInteger('chat_message_id');
            $table->unsignedBigInteger('dish_id');

            $table->foreign(['chat_message_id'], 'fk__chat_message')->references(['id'])->on('chat_messages');
            $table->foreign(['dish_id'], 'fk__msg_dish')->references(['id'])->on('dishes');
        });
    }

    public function down()
    {
        Schema::dropIfExists('chat_message_to_dishes');
    }
};
