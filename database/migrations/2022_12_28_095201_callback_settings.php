<?php

use Illuminate\Database\Migrations\Migration;
use App\Models\SiteSetting;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $section = \App\Models\SiteSettingsSection::whereCode('popups')->first();

        SiteSetting::create([
            'active'     => '1',
            'code'       => 'callback.popup',
            'text'       => 'Пожалуйста, оставьте свой номер телефона, и мы свяжемся с вами дя уточнения детелей заказа',
            'big_text'   => '',
            'section_id' => $section->id,
            'sort'       => 100,
        ]);


        SiteSetting::create([
            'active'     => '1',
            'code'       => 'callback.success',
            'text'       => 'Отлично!',
            'big_text'   => 'Ваше обращение принято',
            'section_id' => $section->id,
            'sort'       => 100,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SiteSetting::query()->where('code', 'callback.popup')->get()->first()?->delete();
        SiteSetting::query()->where('code', 'callback.success')->get()->first()?->delete();
    }
};
