<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up()
    {
        $chatSection = SiteSettingsSection::where('code', 'site')->first();

        (new SiteSetting(
            [
                'active' => '1',
                'code' => 'site.catalog-empty-result-additional-text',
                'text' => '',
                'big_text' => "<br>
<br>
Позвоните по телефону:
<a class='link' href='tel:8-(922)-396-80-01'>8-(922)-396-80-01</a>. <br>Мы поможем бесплатно подобрать блюдо и изготовителя в Вашем городе.",
                'section_id' => $chatSection->id,
                'sort' => 500,
            ]
        ))->save();
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'site.catalog-empty-result-additional-text')->get()->first()->delete();
    }
};
