<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->tinyInteger('count')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('dish_id');

            $table->foreign(['user_id'], 'fk__user')->references(['id'])->on('users');
            $table->foreign(['dish_id'], 'fk__dish')->references(['id'])->on('dishes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baskets');
    }
};
