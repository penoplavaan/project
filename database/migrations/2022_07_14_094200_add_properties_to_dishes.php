<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->text('storage_conditions')->nullable();
            $table->string('calories')->nullable();
            $table->string('protein')->nullable();
            $table->string('fats')->nullable();
            $table->string('carbohydrates')->nullable();
            $table->string('quantity');
            $table->string('cooking_time')->nullable();
            $table->string('package')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->dropColumn('storage_conditions');
            $table->dropColumn('calories');
            $table->dropColumn('protein');
            $table->dropColumn('fats');
            $table->dropColumn('carbohydrates');
            $table->dropColumn('quantity');
            $table->dropColumn('cooking_time');
            $table->dropColumn('package');
        });
    }
};
