<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up()
    {
        $section = SiteSettingsSection::where('code', 'main')->first();

        (new SiteSetting(
            [
                'active' => '1',
                'code' => 'main.promo-button-background-hover-color',
                'text' => '',
                'big_text' => "",
                'color' => '#06c160',
                'section_id' => $section->id,
                'sort' => 500,
            ]
        ))->save();

        (new SiteSetting(
            [
                'active' => '1',
                'code' => 'main.promo-button-background-color',
                'text' => '',
                'big_text' => "",
                'color' => '#fff',
                'section_id' => $section->id,
                'sort' => 500,
            ]
        ))->save();
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'main.promo-button-background-hover-color')->get()->first()->delete();
        SiteSetting::query()->where('code', 'main.promo-button-backround-color')->get()->first()->delete();
    }
};
