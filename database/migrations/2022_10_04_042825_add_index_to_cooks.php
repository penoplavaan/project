<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::table('cooks', function (Blueprint $table) {
            $table->index(['active', 'verified', 'city_id'], 'active_verified_city_id');
        });
    }

    public function down()
    {
        Schema::table('cooks', function (Blueprint $table) {
            $table->dropIndex('active_verified_city_id');
        });
    }
};
