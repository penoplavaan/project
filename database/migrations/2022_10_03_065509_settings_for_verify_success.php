<?php

use App\Models\SiteSetting;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up()
    {
        $section = \App\Models\SiteSettingsSection::query()->where('code', 'popups')->first();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'verify-phone.success',
            'text'       => '',
            'big_text'   => 'Ваш номер телефона успешно подтвержден!',
            'section_id' => $section->id,
            'sort'       => 500,
        ]))->save();
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'verify-phone.success')->get()->first()->delete();
    }
};
