<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('text_blocks')->insert([
            'active' => 1,
            'code'   => 'seo.footer.dishes.text',
            'title'  => 'Узнать больше',
            'text1'  => '<div><strong>Повар на&nbsp;связи</strong> (текст для страницы списка блюд) &mdash; это&nbsp; сервис поиска изготовителей дает возможность взаимодействия заказчиков и изготовителей напрямую без каких-либо сложностей. Ниже представлены ответы на часто задаваемые вопросы и наши рекомендации для эффективного использования сервиса.</div>',
            'text2'  => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /** @var \App\Models\TextBlock $textBlock */
        $textBlock = \App\Models\TextBlock::query()
            ->where("text_blocks.code", '=', 'seo.footer.dishes.text')
            ->get()
            ->first();

        if ($textBlock) {
            $textBlock->delete();
        }
    }
};
