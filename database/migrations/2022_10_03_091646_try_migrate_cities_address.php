<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $cooks = \App\Models\Cook::query()->get();

        $cityCache = [];
        foreach ($cooks as $cook) {
            if (empty($cook->address)) continue;

            $addressParts = explode(',', $cook->address);
            $cityId = 0;
            foreach ($addressParts as $part) {
                $part = mb_strtolower(trim($part));
                if (isset($cityCache[$part])) {
                    $cityId = $cityCache[$part];
                    break;
                } else {
                    $city = \App\Models\City::query()->where('name', 'like', $part)->first();
                    if ($city) {
                        $cityCache[$part] = $city->id;
                        $cityId = $cityCache[$part];
                        break;
                    }
                }
            }

            if ($cityId) {
                $cook->city_id = $cityId;
                $cook->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
