<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration {

    public function up()
    {
        Schema::table('cooks', function (Blueprint $table) {
            $table->string('delivery')->nullable();
        });
    }

    public function down()
    {
        Schema::table('cooks', function (Blueprint $table) {
            $table->dropColumn('delivery');
        });
    }
};
