<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->string('quantity', 40)->comment('Количество порций')->change();
            $table->string('calories', 20)->nullable()->change();
            $table->string('protein', 20)->nullable()->change();
            $table->string('carbohydrates', 20)->nullable()->comment('Углеводы')->change();
            $table->string('fats', 20)->nullable()->comment('Жиры')->change();
            $table->string('package', 40)->nullable()->comment('Упаковка')->change();
            $table->string('weight', 40)->comment('Вес порции')->change();
            $table->string('cooking_time', 100)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dishes', function (Blueprint $table) {
            $table->string('quantity')->change();
            $table->string('calories')->nullable()->change();
            $table->string('protein')->nullable()->change();
            $table->string('carbohydrates')->nullable()->comment('Углеводы')->change();
            $table->string('fats')->nullable()->comment('Жиры')->change();
            $table->string('package')->nullable()->comment('Упаковка')->change();
            $table->string('weight')->change();
            $table->string('cooking_time')->nullable()->change();
        });
    }
};
