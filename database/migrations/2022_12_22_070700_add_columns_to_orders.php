<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('pay_system_id');
            $table->foreign('pay_system_id', 'fk__orders__pay_system_id')
                ->references('id')
                ->on('pay_systems');

            $table->unsignedBigInteger('delivery_type_id');
            $table->foreign('delivery_type_id', 'fk__orders__delivery_type_id')
                ->references('id')
                ->on('delivery_types');

            $table->unsignedTinyInteger('payed')->default('0');
        });

        // Все существующие заказы помечаем как оплаченные
        DB::table('orders')->update(['payed' => 1, 'pay_system_id' => 1, 'delivery_type_id' => 1]);

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payed');
            $table->dropColumn('pay_system_id');
            $table->dropColumn('delivery_type_id');
        });
        Schema::enableForeignKeyConstraints();
    }
};
