<?php

use App\Models\FeedbackCategory;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    public function up()
    {
        $category = (new \App\Models\FeedbackCategory(
            [
                'name' => 'Оставить отзыв',
                'sort' => '15',
                'active' => '1'
            ]
        ))->save();
    }

    public function down()
    {
        FeedbackCategory::query()->where('name', 'Оставить отзыв')->first()->delete();
    }
};
