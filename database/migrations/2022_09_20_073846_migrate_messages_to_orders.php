<?php

use App\Models\ChatMessage;
use App\Models\Order;
use App\Models\OrderToDishes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $messageDishes = \App\Models\ChatMessageToDish::query()->get();

        $dishGroups = $messageDishes->groupBy('chat_message_id');

        /** @var Collection $group */
        foreach ($dishGroups as $messageId => $group) {
            /** @var \App\Models\ChatMessage $message */
            $message = ChatMessage::whereId($messageId)->first();
            $sum = $group->reduce(fn ($acc, $row) => $acc + $row->count * $row->price, 0);

            /** @var Order $order */
            $order = Order::create([
                'created_at' => $message->created_at,
                'updated_at' => $message->updated_at,
                'message_id' => $message->id,
                'user_id'    => $message->chat->from_user_id,
                'cook_id'    => $message->chat->toUser?->cook?->id ?? 1,
                'sum'        => $sum,
            ]);

            /** @var \App\Models\ChatMessageToDish $dishMessage */
            foreach ($group as $dishMessage) {
                OrderToDishes::create([
                    'created_at' => $dishMessage->created_at,
                    'updated_at' => $dishMessage->updated_at,
                    'count'      => $dishMessage->count,
                    'price'      => $dishMessage->price,
                    'name'       => $dishMessage->name,
                    'order_id'   => $order->id,
                    'dish_id'    => $dishMessage->dish_id,
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('order_to_dishes')->truncate();
        DB::table('orders')->truncate();
        Schema::enableForeignKeyConstraints();
    }
};
