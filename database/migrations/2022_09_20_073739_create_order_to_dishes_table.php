<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_to_dishes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->tinyInteger('count')->default(1);
            $table->integer('price');
            $table->string('name');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('dish_id');

            $table->foreign(['order_id'], 'fk__order')->references(['id'])->on('orders');
            $table->foreign(['dish_id'], 'fk__order_dish')->references(['id'])->on('dishes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_to_dishes');
    }
};
