<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $section = new SiteSettingsSection([
            'name' => 'Отзывы',
            'code' => 'reviews',
        ]);
        $section->save();
        $section->refresh();

        (new SiteSetting([
            'active'     => '1',
            'code'       => 'reviews.success',
            'text'       => 'Текст попапа после успешного оставления отзыва',
            'big_text'   => 'Благодарим за обратную связь. После модерации отзыв будет опубликован на странице повара',
            'section_id' => $section->id,
            'sort'       => 500,
        ]))->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SiteSetting::query()->where('code', 'reviews.success')->get()->first()->delete();
        SiteSettingsSection::query()->where('code', 'reviews')->get()->first()->delete();
    }
};
