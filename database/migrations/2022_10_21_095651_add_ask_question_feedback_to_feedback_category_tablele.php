<?php

use App\Models\FeedbackCategory;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    public function up()
    {
        $category = (new \App\Models\FeedbackCategory(
            [
                'name' => 'Задать вопрос',
                'sort' => '15',
                'active' => '1'
            ]
        ))->save();
    }

    public function down()
    {
        FeedbackCategory::query()->where('name', 'Задать вопрос')->first()->delete();
    }
};

