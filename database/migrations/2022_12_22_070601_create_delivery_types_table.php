<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_types', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('code');
            $table->string('name');
            $table->text('description')->nullable();
        });

        DB::table('delivery_types')->insert([
            'code'        => 'pickup',
            'name'        => 'Самовывоз',
            'description' => '',
        ]);

        DB::table('delivery_types')->insert([
            'code'        => 'delivery',
            'name'        => 'Доставка',
            'description' => 'Повар самостоятельно отправит заказ до вашего адреса. Детали уточняйте в чате с поваром',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_types');
    }
};
