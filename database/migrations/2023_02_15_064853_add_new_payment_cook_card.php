<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration {

    public function up()
    {
        $paysystem = (new \App\Models\PaySystem());
        $paysystem->code = 'cook-card';
        $paysystem->name = 'Перевод на карту изготовителя';
        $paysystem->description = 'Оплата производится на карту изготовителя';
        $paysystem->active = true;
        $paysystem->save();
    }

    public function down()
    {
        $paysystem = new \App\Models\PaySystem();
        $paysystem->query()->where('code', 'cook-card')->delete();
    }
};
