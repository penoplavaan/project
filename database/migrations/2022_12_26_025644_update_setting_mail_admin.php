<?php

use App\Models\SiteSetting;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{

    public function up()
    {
        $setting = SiteSetting::query()->where('code', 'mail.admin')->get()->first();
        if ($setting) {
            $setting->text = 'project@teslaburger.ru, teslakuvar@mail.ru';
            $setting->save();
        }
    }

    public function down()
    {
        $setting = SiteSetting::query()->where('code', 'mail.admin')->get()->first();
        if ($setting) {
            $setting->text = 'project@teslaburger.ru';
            $setting->save();
        }
    }
};
