<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up()
    {
        $paysystemSber = \App\Models\PaySystem::whereCode('sberbank')->first();
        $paysystemSber->active = false;
        $paysystemSber->save();

        $paysystemCookCard = \App\Models\PaySystem::whereCode('cook-card')->first();
        $paysystemCookCard->name = 'Напрямую изготовителю после подтверждения заказа';
        $paysystemCookCard->save();
    }

    public function down()
    {
        $paysystemSber = \App\Models\PaySystem::whereCode('sberbank')->first();
        $paysystemSber->active = true;
        $paysystemSber->save();

        $paysystemCookCard = \App\Models\PaySystem::whereCode('cook-card')->first();
        $paysystemCookCard->name = 'Перевод на карту изготовителя';
        $paysystemCookCard->save();
    }
};
