<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dishes_to_tags', function (Blueprint $table) {
            $table->dropForeign('fk__dish_id');
            $table->dropForeign('fk__tag_id');
            $table->foreign(['dish_id'], 'fk__dish_id')->references(['id'])->on('dishes')->onDelete('cascade');
            $table->foreign(['tag_id'], 'fk__tag_id')->references(['id'])->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dishes_to_tags', function (Blueprint $table) {
            $table->dropForeign('fk__dish_id');
            $table->dropForeign('fk__tag_id');
            $table->foreign(['dish_id'], 'fk__dish_id')->references(['id'])->on('dishes');
            $table->foreign(['tag_id'], 'fk__tag_id')->references(['id'])->on('tags');
        });
    }
};
