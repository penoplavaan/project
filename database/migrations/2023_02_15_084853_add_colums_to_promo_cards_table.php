<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration {

    public function up()
    {
        Schema::table('promo_cards', function (Blueprint $table) {
            $table->integer('sort')->default(500);
            $table->string('link')->nullable()->default('');
        });
    }

    public function down()
    {
        Schema::table('promo_cards', function (Blueprint $table) {
            $table->dropColumn('sort');
            $table->dropColumn('link');
        });
    }
};
