<?php

use App\Models\SiteSetting;
use App\Models\SiteSettingsSection;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $section1 = SiteSettingsSection::query()->where('code', 'order')->get()->first();
        if (!$section1) {
            $section1 = SiteSettingsSection::create([
                'name' => 'Оформление заказа',
                'code' => 'order',
            ]);
        }

        $section2 = SiteSettingsSection::query()->where('code', 'order.payment')->get()->first();
        if (!$section2) {
            $section2 = SiteSettingsSection::create([
                'name'              => 'Оплаты',
                'code'              => 'order.payment',
                'parent_section_id' => $section1->id,
            ]);
        }

        $section3 = SiteSettingsSection::query()->where('code', 'order.payment.sber')->get()->first();
        if (!$section3) {
            $section3 = SiteSettingsSection::create([
                'name'              => 'Сбербанк',
                'code'              => 'order.payment.sber',
                'parent_section_id' => $section2->id,
            ]);
        }

        SiteSetting::create([
            'active'     => '1',
            'code'       => 'order.payment.sber.test',
            'text'       => '0',
            'big_text'   => 'Тестовый режим. 1 - тестовый режим. Любое другое значение, или отелючить настройку - боевей режим',
            'section_id' => $section3->id,
            'sort'       => 100,
        ]);
        SiteSetting::create([
            'active'     => '1',
            'code'       => 'order.payment.sber.userName',
            'text'       => 't7204198351-api',
            'big_text'   => 'Имя пользователя (логин) для подключения к АПИ',
            'section_id' => $section3->id,
            'sort'       => 200,
        ]);
        SiteSetting::create([
            'active'     => '1',
            'code'       => 'order.payment.sber.password',
            'text'       => 'H8naYuA7',
            'big_text'   => 'Пароль для АПИ',
            'section_id' => $section3->id,
            'sort'       => 300,
        ]);
        SiteSetting::create([
            'active'     => '1',
            'code'       => 'order.payment.sber.privateKey',
            'text'       => 'neis07bhh0ctk4e80kj78p3l75',
            'big_text'   => 'Закрытый ключ',
            'section_id' => $section3->id,
            'sort'       => 400,
        ]);
        SiteSetting::create([
            'active'     => '1',
            'code'       => 'order.payment.sber.token',
            'text'       => 'el2ge050fsik0mpetqso973dl1',
            'big_text'   => 'Токен для проверки callback-запросов',
            'section_id' => $section3->id,
            'sort'       => 500,
        ]);
    }

    public function down()
    {
        SiteSetting::query()->where('code', 'order.payment.sber.test')->get()->first()?->delete();
        SiteSetting::query()->where('code', 'order.payment.sber.userName')->get()->first()?->delete();
        SiteSetting::query()->where('code', 'order.payment.sber.password')->get()->first()?->delete();
        SiteSetting::query()->where('code', 'order.payment.sber.privateKey')->get()->first()?->delete();
        SiteSetting::query()->where('code', 'order.payment.sber.token')->get()->first()?->delete();
        SiteSettingsSection::query()->where('code', 'order.payment.sber')->get()->first()->delete();
    }
};
