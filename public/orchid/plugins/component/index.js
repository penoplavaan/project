// Exports the "component" plugin for usage with module loaders
// Usage:
//   CommonJS:
//     require('tinymce/plugins/component')
//   ES2015:
//     import 'tinymce/plugins/component'
require('./plugin.js');
