class TinyMCEPluginComponent {

    componentName = '';
    componentParams = '';
    isNewComponent = false;

    /**
     *
     * @param {TinyMCE.Editor} editor
     * @param name
     */
    constructor(editor, name) {
        this.editor = editor;
        this.name = name;

        this.editorElementClass = 'sx-mce-component';
        this.stateSelector = '.' + this.editorElementClass;
        this.componentListUrl = this.editor.getParam('component_list_url', '');
    }

    register() {
        if (!this.componentListUrl || !this.componentListUrl.length) {
            return;
        }

        this.addButton();
        this.addMenuItem();
    }

    addButton() {
        this.editor.ui.registry.addButton(this.name, {
            text: 'Компонент',
            stateSelector: this.stateSelector,
            onAction: () => this.onAction()
        });
    }

    addMenuItem() {
        this.editor.ui.registry.addMenuItem(this.name, {
            text: 'Компонент',
            context: 'insert',
            onAction: () => this.onAction()
        });
    }

    onAction() {
        const component = this.getSelectedComponent();
        const componentName = this.getComponentName(component);

        this.openComponentPopup(componentName, this.getComponentParams(component), !componentName);
    }

    /**
     *
     * @return {null|HTMLElement}
     */
    getSelectedComponent() {
        const component = this.editor.selection.getNode();

        if (component && component.matches(this.stateSelector)) {
            return component;
        }

        return null;
    }

    /**
     *
     * @param {HTMLElement} component
     * @return {string}
     */
    getComponentName(component) {
        if (!this.isComponent(component)) return '';

        return component.dataset.componentName;
    }

    /**
     *
     * @param {HTMLElement} component
     * @return {{}}
     */
    getComponentParams(component) {
        if (!this.isComponent(component)) return {};

        return JSON.parse(component.dataset.componentParams);
    }

    /**
     *
     * @param {HTMLElement} component
     * @return {boolean}
     */
    isComponent(component) {
        return component && component.dataset && component.dataset.hasOwnProperty('componentParams');
    }

    /**
     *
     * @param {string} componentName
     * @return {string}
     */
    getComponentPopupUrl(componentName) {
        const replace = componentName && componentName.length ? '?componentName=' + encodeURIComponent(componentName) : '';
        return this.componentListUrl.replace('%componentName%', replace);
    }

    /**
     *
     * @param {string} componentName
     * @param {object} currentParams
     * @param {boolean} isNewComponent
     */
    openComponentPopup(componentName, currentParams, isNewComponent) {
        const url = this.getComponentPopupUrl(componentName);

        this.componentName   = componentName;
        this.componentParams = currentParams;
        this.isNewComponent  = isNewComponent;

        this.fetch(url);
    }

    async fetch(url, method = 'GET') {
        try {
            const response = await fetch(url, {
                method: method,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest',
                    'Accept': 'application/json',
                },
            });

            if (response && response.ok) {
                const json = await response.json();

                this.onSuccessFetch(json);
                return;
            }
        } catch (e) {
            console.error(e);
        }

        this.editor.windowManager.alert('Не удалось загрузить список компонентов. Попробуйте позднее.');
    }

    /**
     *
     * @param {object} result
     */
    onSuccessFetch(result) {
        const hasComponents = result.hasOwnProperty('componentList') && result.componentList.length > 0;

        if (!hasComponents) {
            this.editor.windowManager.alert('Список компонентов пуст');
            return;
        }

        const initialData = {
            componentName: this.componentName,
        };

        const componentListItem = {
            name: 'componentName',
            type: 'selectbox',
            label: 'Компонент',
            items: result.componentList,
        };

        if (result.hasOwnProperty('selectedComponent') && result.selectedComponent.length) {
            componentListItem.value = result.selectedComponent;

            if (!this.isNewComponent) {
                componentListItem.disabled = true;
            }
        }

        const dialogBody = {
            type: 'panel',
            items: [componentListItem]
        };

        if (result.hasOwnProperty('selectedComponentParams')) {
            result.selectedComponentParams.forEach((param) => {
                const paramName = 'param.' + param.name;

                if (this.componentParams.hasOwnProperty(param.name)) {
                    if (param.type === 'checkbox') {
                        initialData[paramName] = this.componentParams[param.name] || false;
                    } else {
                        initialData[paramName] = this.componentParams[param.name] || '';
                    }
                }

                param.name = paramName;

                dialogBody.items.push(param);
            })
        }

        const buttons = [
            // кнопка сохранения компонента в редактор
            {
                text: 'Сохранить',
                type: 'submit',
                align: 'end',
                primary: true
            },
            // кнопка отмены редактирования компонента
            {
                text: 'Отмена',
                type: 'cancel',
                align: 'end'
            }
        ];

        this.editor.windowManager.open({
            title: 'Настройка компонента',
            size: 'medium',
            body: dialogBody,
            initialData,
            buttons,
            onChange: (component, componentOptions) => {
                const componentName = componentOptions.name;

                if (componentName === 'componentName') {
                    const componentData = component.getData();

                    this.editor.windowManager.close();

                    this.openComponentPopup(componentData.componentName, this.componentParams, true);
                }
            },
            onSubmit: (event) => {
                const componentData = event.getData();
                let componentTitle = "";

                result.componentList.forEach(item => {
                    if (item.value === componentData.componentName) {
                        componentTitle = item.text;
                    }
                });

                const componentParams = {};

                for (let param in componentData) {
                    if (param.substr(0, 6) !== 'param.') continue;

                    componentParams[param.substr(6)] = componentData[param];
                }

                this.insertOrUpdateComponent(componentData.componentName, componentTitle, componentParams);

                this.editor.windowManager.close();
            }
        });
    }

    insertOrUpdateComponent(name, title, params) {
        const component = this.getSelectedComponent();

        if (name && component) {
            this.replaceComponent(component, name, title, params);
            return;
        }

        if (name) {
            this.insertComponentAtCaret(name, title, params);
            return;
        }

        this.deleteComponent(component);
    }

    replaceComponent(component, name, title, params) {
        if (!component) return;

        this.editor.dom.setHTML(component, title);
        this.editor.dom.setAttrib(component, 'data-component-name', name);
        this.editor.dom.setAttrib(component, 'data-component-params', JSON.stringify(params));
        this.editor.selection.select(component);
    }

    deleteComponent(component) {
        if (!component) return;

        this.editor.dom.remove(component);
        this.editor.focus();
        this.editor.nodeChanged();

        if (this.editor.dom.isEmpty(this.editor.getBody())) {
            this.editor.setContent('');
            this.editor.selection.setCursorLocation();
        }
    }

    insertComponentAtCaret(name, title, params) {
        const element = this.editor.dom.create('section', {
            'class': this.editorElementClass,
            'data-component-name': name,
            'data-component-params': JSON.stringify(params),
        }, title);

        this.editor.dom.setAttrib(element, 'data-mce-id', '__mcenew');
        this.editor.focus();
        this.editor.execCommand('mceInsertContent', false, element.outerHTML + '&nbsp;');

        const insertedElement = this.editor.dom.select('*[data-mce-id="__mcenew"]')[0];
        this.editor.dom.setAttrib(insertedElement, 'data-mce-id', null);
        this.editor.selection.select(insertedElement);
    }
}


tinymce.PluginManager.add('component', function(editor) {
    const plugin = new TinyMCEPluginComponent(editor, 'component');
    plugin.register();
});
