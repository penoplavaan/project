# Описание проекта

## Периодические задачи
Выполнение всех необходимых периодически задач сделано через расписание Laravel. Для его работы добавьте на крон одну задачу

```php artisan schedule:run```

на выполнение каждую минуту

## Чаты
Чаты сделаны через Websocket на PHP. Чтобы они работали - надо на сервере запускать команду
```php artisan websockets:serve```, желательно через supervisor

## Запуск супервизора
1. скопировать реальный конфиг в `/etc/supervisor/conf.d/`, расширение обязательно .conf
2. выполнить `sudo /usr/sbin/service supervisor restart`
3. посмотреть что запустилось - `sudo /usr/sbin/service supervisor status`
4. как будет ненужно - выключить `sudo /usr/sbin/service supervisor stop`

