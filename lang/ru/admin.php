<?php

/*
|--------------------------------------------------------------------------
| Переводы админки Orchid
|--------------------------------------------------------------------------
|
*/

return [

    /* Заголовки страниц */
    'cook-title'                              => 'Список поваров',
    'cook-title-1'                            => 'Повар',
    'text-page-title'                         => 'Текстовые страницы',
    'text-page-title-1'                       => 'Текстовая страница',
    'gallery-title'                           => 'Галереи',
    'text-block-title'                        => 'Текстовые блоки',
    'text-block-title-1'                      => 'Текстовый блок',
    'dish-title'                              => 'Список блюд',
    'dish-title-1'                            => 'Блюдо',
    'cook-speciality-title'                   => 'Специализации поваров',
    'cook-speciality-title-1'                 => 'Специализация повара',
    'dish-category-title'                     => 'Категории меню',
    'dish-category-title-1'                   => 'Категория меню',
    'for-cook-faq-title'                      => 'Поварам: вопросы',
    'for-cook-faq-title-1'                    => 'Вопрос',
    'for-cook-feature-title'                  => 'Поварам: фишки',
    'for-cook-feature-title-1'                => 'Фишка сайта',
    'for-cook-how-work-title'                 => 'Поварам: как работает',
    'for-cook-how-work-title-1'               => 'Пункт "Как работает"',
    'for-cook-review-title'                   => 'Поварам: отзывы',
    'for-cook-review-title-1'                 => 'Отзыв',
    'for-cook-support-title'                  => 'Поварам: поддержка',
    'for-cook-support-title-1'                => 'Пункт "Поддержка"',
    'main-promo-slide-title'                  => 'Главная: Слайды',
    'main-promo-slide-title-1'                => 'Слайд на главной',
    'main-second-promo-title'                 => 'Главная: Слайды',
    'main-second-promo-title-1'               => 'Слайд на главной',
    'main-promo-category-compilation-title'   => 'Категория на главной',
    'main-promo-category-compilation-title-1' => 'Категорию на главной',
    'main-dish-compilation-title'             => 'Главная: Подборки',
    'main-dish-compilation-title-1'           => 'Подборка на главной',
    'main-review-title'                       => 'Главная: Отзывы',
    'main-review-title-1'                     => 'Отзыв на главной',
    'city-title'                              => 'Города',
    'city-title-1'                            => 'Город',
    'gallery-title-1'                         => 'Галерею',
    'attachment-title'                        => 'Вложения',
    'attachment-title-1'                      => 'Вложение',
    'order-title'                             => 'Заказы',
    'order-title-1'                           => 'Заказ',
    'cook-review-title'                       => 'Отзывы на поваров',
    'cook-review-title-1'                     => 'Отзыв на повара',
    'label-title'                             => 'Лейблы',
    'label-title-1'                           => 'Лейбл',
    'promo-card-title'                        => 'Промо карточки',
    'promo-card-title-1'                      => 'Промо карточка',
    'callback-title'                          => 'Обратные звонки',
    'callback-title-1'                        => 'Обратный звонок',

    'feedback-category-title'   => 'Категории обратной связи',
    'feedback-category-title-1' => 'Категория обратной связи',

    'tags-title'                => 'Тэги',
    'tags-title-1'              => 'Тэг',

    'profile-useful-material-title'          => 'Полезные материалы',
    'profile-useful-material-title-1'        => 'Материал',

    /*
    |--------------------------------------------------------------------------
    | Переводы колонок в таблицах
    |--------------------------------------------------------------------------
    |
    */

    'title'                => 'Заголовок',
    'subtitle'             => 'Подзаголовок',
    'name'                 => 'Наименование',
    'fullname'             => 'Полное наименование',
    'original_name'        => 'Исходное наименование',
    'description'          => 'Описание',
    'uploads'              => 'Файлы',
    'upload'               => 'Файл',
    'color'                => 'Цвет',
    'address'              => 'Адрес',
    'emails'               => 'Электронные адреса',
    'email'                => 'Электронный адрес',
    'coordinates'          => 'Координаты',
    'latitude'             => 'Широта',
    'longitude'            => 'Долгота',
    'start_date'           => 'Дата начала',
    'end_date'             => 'Дата окончания',
    'images'               => 'Изображения',
    'image'                => 'Изображение',
    'url'                  => 'Ссылка',
    'text'                 => 'Контент',
    'text2'                => 'Контент, вторая часть',
    'active'               => 'Активен',

    'verified'             => 'Верифицирован',
    'passport_verified'    => 'Паспорт проверен',
    'sanitary_verified'    => 'Санкнижка проверена',

    'created_at'           => 'Создано',
    'updated_at'           => 'Обновлено',
    'video_link'           => 'Ссылка до видео',
    'small_image'          => 'Мобильная картинка',
    'preview_text'         => 'Текст для превью',
    'detail_text'          => 'Подробный текст',
    'date'                 => 'Дата',
    'show_on_main'         => 'Отображать на главной',
    'sort'                 => 'Сортировка',
    'delivery'             => 'Условия доставки',
    'parent'               => 'Родитель',
    'code'                 => 'Код',
    'preview'              => 'Превью',
    'preview_image'        => 'Картинка для карточки',
    'detail_image'         => 'Картинка для детальной',
    'icon'                 => 'Иконка',
    'value'                => 'Значение',
    'comment'              => 'Комментарий',
    'show_on_filter'       => 'Показывать в фильтре',
    'card_position'        => 'Позиция на карточке',
    'properties'           => 'Свойства',
    'property'             => 'Свойство',

    'size'      => 'Размер',
    'full_path' => 'Полный путь',

    'menu-site-create'         => 'Создать',
    'menu-site-delete'         => 'Удалить',
    'menu-site-edit'           => 'Редактировать',
    'menu-site-delete-confirm' => 'Удалить элемент [:e] и все дочерние.',
    'menu-site-title'          => 'Меню сайта',

    'position' => 'Должность',
    /*
    |--------------------------------------------------------------------------
    | Переводы Сообщений
    |--------------------------------------------------------------------------
    |
    */
    'Save and return' => 'Сохранить и добавить',
    'Accept'          => 'Применить',

    'Successful removed!'                             => 'Успешно удалено!',
    'Successful saved!'                               => 'Успешно сохранено!',
    'Are you sure you want to delete this resources?' => 'Вы действительно хотите удалить эти ресурсы?',
    'Activate'                                        => 'Активировать',
    'Deactivate'                                      => 'Деактивировать',


    'delete-restricted' => 'Удаление запрещено',

    /**
     * Специфичные для разделов переводы
     */
    'cook' => [
        'user' => 'Пользователь',
        'speciality' => 'Специализация',
    ],

    'dish' => [
        'name' => 'Наименование',
        'cook' => 'Повар',
        'category' => 'Категория',
        'price' => 'Цена',
        'preview_text' => 'Состав',
    ],

    'main-review' => [
        'name' => 'ФИО',
        'position' => 'Подпись',
        'text' => 'Текст отзыва',
    ],
];
