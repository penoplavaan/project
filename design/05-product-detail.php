<?php
$pageClass = 'product-detail';
$pageType = 'product-detail';
$title = 'Детальная блюда';

$breadcrumbs = [
    'Главная',
    'Все блюда',
    'Жаркое по-деревенски',
];

require('partials/header.php'); ?>

<main class="page__content">
    <?= view('common.breadcrumbs', compact('breadcrumbs')) ?>
    <?= view('common.caption', ['caption' => 'Жаркое по-деревенски']) ?>

    <div class="content-container">
        <?= view('product-detail.main') ?>
        <?= view('product-detail.products') ?>
    </div>
</main>

<?php require('partials/footer.php'); ?>
