<?php
$pageClass = 'profile';
$pageType = 'profile';
$title = 'Профиль. Повар';

$shortFooter = true;

$breadcrumbs = [
    'Главная',
    'Мой профиль',
];

require('partials/header.php');

$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'      => 'file',
            'name'      => 'avatar',
            'label'     => 'Загрузить другое фото',
        ],
        [
            'type'      => 'text',
            'name'      => 'fio',
            'maxlength' => 200,
            'required'  => true,
            'label'     => 'ФИО',
            'value'     => 'Мартынова Елизавета',
        ],
        [
            'type'     => 'text',
            'name'     => 'address',
            'required' => true,
            'label'    => 'Укажите город',
            'value'    => 'Санкт-Петербург, ул. Гороховая, 9а Кв. 12, 2 подъезд, 4 этаж',
        ],
        [
            'type'      => 'textarea',
            'name'      => 'about',
            'maxlength' => 1000,
            'label'     => 'Расскажите о своих вкусовых предпочтениях',
            'value'     => 'Привет! Меня зовут Лиза. Я занимаюсь разработкой игр в небольшой студии в своем городе. Очень люблю музыку, профессионально катаюсь на скейтборде и иногда даю уроки. Я люблю вкусно покушать, но не люблю готовить.',
        ],
        [
            'type'  => 'tel',
            'name'  => 'phone',
            'label' => 'Телефон',
            'value' => '+7 (909) 987-65-43',
        ],
        [
            'type'     => 'email',
            'name'     => 'email',
            'label'    => 'Email',
            'disabled' => true,
            'value'    => 'lizzywizzy123@mail.ru',
        ],
        [
            'type'  => 'submit',
            'name'  => 'submit',
            'label' => 'Сохранить изменения',
        ],
    ],
);
$formData = [
    'fields'     => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form profile-form',
    ],
];

$chefData = CHEF_DATA;

$chefData['documents'][1]['items'] = array_map(function (array $item) {
    $item['editable'] = true;

    return $item;
}, $chefData['documents'][1]['items']);
?>
<main class="page__content">
    <?= view('common.breadcrumbs', compact('breadcrumbs')) ?>

    <div class="content-container">
        <?= ViewHelper::vueTag('vue-profile', [
            'pageData' => array_merge([
                'userData' => [
                    'avatar'       => ViewHelper::getPicture('profile/avatar.jpg'),
                    'fio'          => 'Мартынова Елизавета',
                    'address'      => 'Санкт-Петербург, ул. Гороховая, 9а Кв. 12, 2 подъезд, 4 этаж',
                    'about'        => 'Привет! Меня зовут Лиза. Я занимаюсь разработкой игр в небольшой студии в своем городе. Очень люблю музыку, профессионально катаюсь на скейтборде и иногда даю уроки. Я люблю вкусно покушать, но не люблю готовить.',
                    'registerDate' => '19 января 2022',
                    'phone'        => '+7 (909) 987-65-43',
                    'email'        => 'lizzywizzy123@mail.ru',
                ],
                'formData' => $formData,
                'imageSrc' => ViewHelper::getImage('profile/info-image.png'),
            ], $chefData),
            'nav' => [
                [
                    'name'   => 'Профиль',
                    'link'   => '/06-04-profile.php',
                    'active' => true,
                    'code'   => 'profile',
                ],
                [
                    'name'   => 'Чаты<sup>7</sup>',
                    'link'   => '/06-03-profile.php',
                    'active' => false,
                    'code'   => 'chats',
                ],
                [
                    'name'   => 'Мои блюда<sup>12</sup>',
                    'link'   => '/06-05-profile.php',
                    'active' => false,
                    'code'   => 'chef-products',
                ],
                [
                    'name'   => 'Полезные материалы<sup>20</sup>',
                    'link'   => '/06-06-profile.php',
                    'active' => false,
                    'code'   => 'materials',
                ],
            ]
        ]) ?>
    </div>
</main>
<?php require('partials/footer.php'); ?>
