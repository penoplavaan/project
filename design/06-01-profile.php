<?php
$pageClass = 'profile';
$pageType = 'profile';
$title = 'Профиль';

$shortFooter = true;

$breadcrumbs = [
    'Главная',
    'Мой профиль',
];

require('partials/header.php');

$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'      => 'file',
            'name'      => 'avatar',
            'label'     => 'Загрузить другое фото',
        ],
        [
            'type'      => 'text',
            'name'      => 'fio',
            'maxlength' => 200,
            'required'  => true,
            'label'     => 'ФИО',
            'value'     => 'Мартынова Елизавета',
        ],
        [
            'type'     => 'text',
            'name'     => 'address',
            'required' => true,
            'label'    => 'Укажите город',
            'value'    => 'Санкт-Петербург, ул. Гороховая, 9а Кв. 12, 2 подъезд, 4 этаж',
        ],
        [
            'type'      => 'textarea',
            'name'      => 'about',
            'maxlength' => 1000,
            'label'     => 'Расскажите о своих вкусовых предпочтениях',
            'value'     => 'Привет! Меня зовут Лиза. Я занимаюсь разработкой игр в небольшой студии в своем городе. Очень люблю музыку, профессионально катаюсь на скейтборде и иногда даю уроки. Я люблю вкусно покушать, но не люблю готовить.',
        ],
        [
            'type'  => 'tel',
            'name'  => 'phone',
            'label' => 'Телефон',
            'value' => '+7 (909) 987-65-43',
        ],
        [
            'type'     => 'email',
            'name'     => 'email',
            'label'    => 'Email',
            'disabled' => true,
            'value'    => 'lizzywizzy123@mail.ru',
        ],
        [
            'type'  => 'submit',
            'name'  => 'submit',
            'label' => 'Сохранить изменения',
        ],
    ],
);
$formData = [
    'fields'     => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form profile-form',
    ],
];
?>
<main class="page__content">
    <?= view('common.breadcrumbs', compact('breadcrumbs')) ?>

    <div class="content-container">
        <?= ViewHelper::vueTag('vue-profile', [
            'pageData' => [
                'userData' => [
                    'avatar'       => ViewHelper::getPicture('profile/avatar.jpg'),
                    'fio'          => 'Мартынова Елизавета',
                    'address'      => 'Санкт-Петербург, ул. Гороховая, 9а Кв. 12, 2 подъезд, 4 этаж',
                    'about'        => 'Привет! Меня зовут Лиза. Я занимаюсь разработкой игр в небольшой студии в своем городе. Очень люблю музыку, профессионально катаюсь на скейтборде и иногда даю уроки. Я люблю вкусно покушать, но не люблю готовить.',
                    'registerDate' => '19 января 2022',
                    'phone'        => '+7 (909) 987-65-43',
                    'email'        => 'lizzywizzy123@mail.ru',
                ],
                'formData' => $formData,
                'imageSrc' => ViewHelper::getImage('profile/info-image.png'),
            ],
            'nav' => [
                [
                    'name' => 'Профиль',
                    'link' => '/06-01-profile.php',
                    'active' => true,
                    'code' => 'profile',
                ],
                [
                    'name' => 'Избранное<sup>18</sup>',
                    'link' => '/06-02-profile.php',
                    'active' => false,
                    'code' => 'favorite',
                ],
                [
                    'name' => 'Чаты<sup>7</sup>',
                    'link' => '/06-03-profile.php',
                    'active' => false,
                    'code' => 'chats',
                ],
                [
                    'name' => 'Полезные материалы<sup>20</sup>',
                    'link' => '/06-04-profile.php',
                    'active' => false,
                    'code' => 'materials',
                ],
            ]
        ]) ?>
    </div>
</main>
<?php require('partials/footer.php'); ?>
