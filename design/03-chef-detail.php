<?php
$pageClass = 'chef-detail';
$pageType = 'chef-detail';
$title = 'Повар. Детальная';

$breadcrumbs = [
    'Главная',
    'Все повара',
    'Уразметова Гузель',
];

require('partials/header.php'); ?>
<main class="page__content">
    <?= view('common.breadcrumbs', compact('breadcrumbs')) ?>
    <div class="content-container">

        <div class="chef-detail">
            <div class="chef">
                <?= ViewHelper::vueTag('vue-chef-info', [
                    'chefData' => CHEF_DATA['chefData'],
                ]) ?>
            </div>

            <div class="chef-detail__products">
                <h3 class="h3 chef-detail__header">Меню</h3>
                <?= ViewHelper::vueTag('vue-chef-products', [
                    'products' => array_map(function (array $product) {
                        $product['id'] = rand(1, 1000);

                        return $product;
                    }, [
                        PRODUCTS[2],
                        PRODUCTS[1],
                        PRODUCTS[0],
                        PRODUCTS[3],
                        PRODUCTS[1],
                        PRODUCTS[3],
                    ]),
                    'editable' => false,
                    'onDetail' => true,
                ]) ?>
            </div>

            <div class="chef-detail__kitchen">
                <h4 class="h4 chef-detail__header">Моя кухня</h4>
                <?= ViewHelper::vueTag('vue-chef-kitchen-slider', [
                    'items'    => CHEF_DATA['chefKitchen'],
                ]) ?>
            </div>

            <div class="chef-detail__documents">
                <?= ViewHelper::vueTag('vue-documents-list', [
                    'groups' => [CHEF_DATA['documents'][1]],
                    'lgMargin' => true,
                ]) ?>
            </div>
        </div>

    </div>
</main>
<?php require('partials/footer.php'); ?>
