<?php
$pageClass = 'profile';
$pageType = 'profile';
$title = 'Профиль. Чаты';

$shortFooter = true;

$breadcrumbs = [
    'Главная',
    'Мой профиль',
];

$empty = rand(0, 1) == 0;

require('partials/header.php');
?>
<main class="page__content">
    <?= view('common.breadcrumbs', compact('breadcrumbs')) ?>

    <div class="content-container">
        <?= ViewHelper::vueTag('vue-profile', [
            'pageData' => [
                'chats'         => $empty ? [] : [
                    [
                        'user'      => [
                            'id'     => 1,
                            'avatar' => ['originalSrc' => ViewHelper::getPicture('profile/chat/1.jpg')],
                            'name'   => 'Алексей Петров',
                        ],
                        'date'      => '30 марта 2022 в 19:11',
                        'hasUnread' => false,
                    ],
                    [
                        'user'      => [
                            'id'     => 2,
                            'avatar' => ['originalSrc' => ViewHelper::getPicture('profile/chat/2.jpg')],
                            'name'   => 'Мовсисян Лилит',
                        ],
                        'date'      => '17 марта 2022 в 14:13',
                        'hasUnread' => true,
                    ],
                    [
                        'user'      => [
                            'id'     => 3,
                            'avatar' => ['originalSrc' => ViewHelper::getPicture('profile/chat/3.jpg')],
                            'name'   => 'Уразметова Гузель',
                        ],
                        'date'      => '21 марта 2022 в 08:01',
                        'hasUnread' => true,
                    ],
                    [
                        'user'      => [
                            'id'     => 4,
                            'avatar' => ['originalSrc' => ViewHelper::getPicture('profile/chat/4.jpg')],
                            'name'   => 'Маша Петрова',
                        ],
                        'date'      => '25 марта 2022 в 16:20',
                        'hasUnread' => false,
                    ],
                    [
                        'user'      => [
                            'id'     => 5,
                            'avatar' => ['originalSrc' => ViewHelper::getPicture('profile/chat/5.jpg')],
                            'name'   => 'Ирина Ладыгина',
                        ],
                        'date'      => '25 марта 2022 в 16:20',
                        'hasUnread' => false,
                    ],
                ],
                'messages'      => [
                    [
                        'id'       => 1,
                        'type'     => 'order',
                        'message'  => 'Добрый день!<br /> Хочу сделать заказ на блюдо:',
                        'date'     => '1648616700',
                        'isMy'     => true,
                        'products' => [
                            [
                                'id'      => 1,
                                'picture' => ['originalSrc' => ViewHelper::getPicture('profile/chat/6.jpg')],
                                'name'    => 'Грибной крем-суп',
                                'price'   => '160 ₽',
                                'sum'     => '160 ₽',
                                'count'   => 1,
                                'href'    => 'javascript:void(0)',
                            ],
                        ],
                    ],
                    [
                        'id'      => 2,
                        'type'    => 'message',
                        'message' => 'Здравствуйте, Елизавета! Уточните, пожалуйста, дату и время доставки. Есть ли у вас еще какие-то пожелания к супу?',
                        'date'    => '1648617060',
                        'isMy'    => false,
                    ],
                    [
                        'id'      => 3,
                        'type'    => 'message',
                        'message' => 'Пожеланий нет. Хотелось бы получить заказ сегодня к 20:30.',
                        'date'    => '1648618560',
                        'isMy'    => true,
                    ],
                    [
                        'id'      => 4,
                        'type'    => 'message',
                        'message' => 'Елизавета, ваш заказ принят.',
                        'date'    => '1648618620',
                        'isMy'    => false,
                    ],
                    [
                        'id'      => 5,
                        'type'    => 'message',
                        'message' => 'Спасибо! Начинаю готовить.',
                        'date'    => '1648618680',
                        'isMy'    => false,
                    ],
                    [
                        'id'      => 6,
                        'type'    => 'message',
                        'message' => 'Алексей, заказ уже привезли. Оплатила курьеру. Спасибо вам огромное!',
                        'date'    => '1648622160',
                        'isMy'    => true,
                    ],
                ],
                'userData'      => [
                    'id'     => 99,
                    'avatar' => ['originalSrc' => ViewHelper::getPicture('profile/avatar.jpg')],
                    'name'   => 'Мартынова Елизавета',
                ],
                'orderProducts' => [
                    [
                        'id'      => 1,
                        'product' => [
                            'id'      => 1,
                            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/chat/6.jpg')],
                            'name'    => 'Грибной крем-суп',
                            'price'   => '160 ₽',
                            'sum'     => '160 ₽',
                            'count'   => 1,
                            'href'    => 'javascript:void(0)',
                        ],
                    ],
                    [
                        'id'      => 2,
                        'product' => [
                            'id'      => 2,
                            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/chat/7.jpg')],
                            'name'    => 'Картошка по-домашнему',
                            'price'   => '240 ₽',
                            'sum'     => '240 ₽',
                            'count'   => 1,
                            'href'    => 'javascript:void(0)',
                        ],
                    ],
                    [
                        'id'      => 3,
                        'product' => [
                            'id'      => 3,
                            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/chat/6.jpg')],
                            'name'    => 'Грибной крем-суп',
                            'price'   => '160 ₽',
                            'sum'     => '480 ₽',
                            'count'   => 3,
                            'href'    => 'javascript:void(0)',
                        ],
                    ],
                    [
                        'id'      => 4,
                        'product' => [
                            'id'      => 4,
                            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/chat/7.jpg')],
                            'name'    => 'Картошка по-домашнему',
                            'price'   => '240 ₽',
                            'sum'     => '480 ₽',
                            'count'   => 2,
                            'href'    => 'javascript:void(0)',
                        ],
                    ],
                ],
            ],
            'nav'      => [
                [
                    'name'   => 'Профиль',
                    'link'   => '/06-01-profile.php',
                    'active' => false,
                    'code'   => 'profile',
                ],
                [
                    'name'   => 'Избранное<sup>18</sup>',
                    'link'   => '/06-02-profile.php',
                    'active' => false,
                    'code'   => 'favorite',
                ],
                [
                    'name'   => 'Чаты<sup>7</sup>',
                    'link'   => '/06-03-profile.php',
                    'active' => true,
                    'code'   => 'chats',
                ],
                [
                    'name'   => 'Полезные материалы<sup>20</sup>',
                    'link'   => '/06-04-profile.php',
                    'active' => false,
                    'code'   => 'materials',
                ],
            ]
        ]) ?>
        <? if ($empty): ?>
            <div class="empty-chat">
                <div class="empty-chat__text">Здесь пока<br> нет чатов...</div>
                <div class="empty-chat__image-wrap">
                    <div class="empty-chat__image js-parallax">
                        <div data-depth="0.2">
                            <img src="<?= ViewHelper::getImage('empty-chat/potato.png') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>
</main>
<?php require('partials/footer.php'); ?>
