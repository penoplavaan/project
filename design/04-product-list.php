<?php
$pageClass = 'product-list';
$pageType = 'product-list';
$title = 'Список блюд';

$breadcrumbs = [
    'Главная',
    'Все блюда',
];

require('partials/header.php'); ?>

<main class="page__content">
    <?= view('common.breadcrumbs', compact('breadcrumbs')) ?>
    <?= view('common.caption', ['caption' => 'Все блюда', 'postfix' => '<sup>145</sup>']) ?>
    <?= view('product-list.list') ?>
</main>

<?php require('partials/footer.php'); ?>
