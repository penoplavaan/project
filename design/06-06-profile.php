<?php
$pageClass = 'profile';
$pageType = 'profile';
$title = 'Профиль. Повар. Полезные материалы';

$shortFooter = true;

require('partials/header.php');
?>
<main class="page__content">
    <div class="content-container">
        <?= ViewHelper::vueTag('vue-profile', [
            'pageData' => [
                'title'       => 'Полезные материалы',
                'description' => '<b>Повар на&nbsp;связи</b>&nbsp;&mdash; это сервис поиска изготовителей дает возможность взаимодействия заказчиков и&nbsp;изготовителей напрямую без каких-либо сложностей. Ниже представлены ответы на&nbsp;часто задаваемые вопросы и&nbsp;наши рекомендации для эффективного использования сервиса.',
                'image'       => ViewHelper::getImage('profile/cookie.png'),
                'documents'   => [
                    [
                        'groupName' => 'Фотографии блюд',
                        'items'     => [
                            [
                                'name' => 'Как загружать фото в вашу анкету.pdf',
                                'href' => 'javascript:void(0)',
                            ],
                            [
                                'name' => 'Инструкция по подготовке фотографий.pdf',
                                'href' => 'javascript:void(0)',
                            ],
                            [
                                'name' => 'Как фотографировать еду.pdf',
                                'href' => 'javascript:void(0)',
                            ],
                        ],
                    ],
                    [
                        'groupName' => 'Документы',
                        'items'     => [
                            [
                                'name' => 'Как правильно фотографировать паспорт.pdf',
                                'href' => 'javascript:void(0)',
                            ],
                            [
                                'name' => 'Список документов для подтверждения.pdf',
                                'href' => 'javascript:void(0)',
                            ],
                        ],
                    ],
                    [
                        'groupName' => 'Калорийность',
                        'items'     => [
                            [
                                'name' => 'Как посчитать КБЖУ.pdf',
                                'href' => 'javascript:void(0)',
                            ],
                            [
                                'name' => 'Таблица ккал продуктов питания.pdf',
                                'href' => 'javascript:void(0)',
                            ],
                        ],
                    ],
                ],
            ],
            'nav'      => [
                [
                    'name'   => 'Профиль',
                    'link'   => '/06-04-profile.php',
                    'active' => false,
                    'code'   => 'profile-chef',
                ],
                [
                    'name'   => 'Чаты<sup>7</sup>',
                    'link'   => '/06-03-profile.php',
                    'active' => false,
                    'code'   => 'chats',
                ],
                [
                    'name'   => 'Мои блюда<sup>12</sup>',
                    'link'   => '/06-05-profile.php',
                    'active' => false,
                    'code'   => 'chef-products',
                ],
                [
                    'name'   => 'Полезные материалы<sup>20</sup>',
                    'link'   => '/06-06-profile.php',
                    'active' => true,
                    'code'   => 'chef-materials',
                ],
            ]
        ]) ?>
    </div>
</main>
<?php require('partials/footer.php'); ?>
