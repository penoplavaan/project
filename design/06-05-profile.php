<?php
$pageClass = 'profile';
$pageType = 'profile';
$title = 'Профиль. Повар. Мои блюда';

$shortFooter = true;

require('partials/header.php');

$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'        => 'select',
            'name'        => 'category',
            'required'    => true,
            'label'       => 'Категория',
            'placeholder' => '',
            'value'       => null,
            'options'     => [
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Супы',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Салаты',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Горячее',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Десерты',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Суши',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Торты',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Кейтеринг',
                ],
            ],
        ],
        [
            'type'      => 'text',
            'name'      => 'name',
            'required'  => true,
            'maxlength' => 200,
            'label'     => 'Название блюда',
            'value'     => null,
        ],
        [
            'type'     => 'textarea',
            'name'     => 'description',
            'required' => true,
            'label'    => 'Состав, укажите все ингредиенты',
            'value'    => null,
        ],
        [
            'type'      => 'text',
            'name'      => 'amount',
            'required'  => true,
            'maxlength' => 5,
            'label'     => 'Кол-во порций',
            'value'     => null,
        ],
        [
            'type'      => 'text',
            'name'      => 'weight',
            'required'  => true,
            'maxlength' => 5,
            'label'     => 'Вес блюда, г',
            'value'     => null,
        ],
        [
            'type'      => 'textarea',
            'name'      => 'preparingTime',
            'required'  => true,
            'maxlength' => 1000,
            'label'     => 'Время приготовления',
            'value'     => null,
        ],
        [
            'type'      => 'text',
            'name'      => 'packing',
            'required'  => true,
            'maxlength' => 1000,
            'label'     => 'Упаковка',
            'value'     => null,
        ],
        [
            'type'      => 'text',
            'name'      => 'calories',
            'required'  => true,
            'maxlength' => 5,
            'label'     => 'Калорийность, ккал/100гр',
            'value'     => null,
        ],
        [
            'type'      => 'text',
            'name'      => 'protein',
            'required'  => true,
            'maxlength' => 5,
            'label'     => 'Белки, г',
            'value'     => null,
        ],
        [
            'type'      => 'text',
            'name'      => 'fats',
            'required'  => true,
            'maxlength' => 5,
            'label'     => 'Жиры, г',
            'value'     => null,
        ],
        [
            'type'      => 'text',
            'name'      => 'carbohydrates',
            'required'  => true,
            'maxlength' => 5,
            'label'     => 'Углеводы, г',
            'value'     => null,
        ],
        [
            'type'    => 'multi_checkbox',
            'name'    => 'tags',
            'label'   => 'Теги:',
            'value'   => null,
            'options' => [
                [
                    'id'    => 1,
                    'value' => 'Без глютена',
                ],
                [
                    'id'    => 2,
                    'value' => 'Без сахара',
                ],
                [
                    'id'    => 3,
                    'value' => 'Не содержит орехов',
                ],
            ],
        ],
        [
            'type'      => 'text',
            'name'      => 'price',
            'required'  => true,
            'maxlength' => 5,
            'label'     => 'Цена, ₽',
            'value'     => null,
        ],
        [
            'type'    => 'checkbox',
            'name'    => 'delivery',
            'label'   => 'Доставка',
            'checked' => true,
            'value'   => null,
        ],
        [
            'type'        => 'file',
            'name'        => 'images',
            'multiple'    => true,
            'placeholder' => 'Прикрепить фото',
            'label'       => 'Рекомендуем форматы: jpeg, png, не более 6мб',
            'accept'      => 'image/jpg, image/jpeg, image/png',
            'value'       => null,
        ],
        [
            'type'  => 'submit',
            'name'  => 'submit',
            'label' => 'Добавить',
            'value' => null,
        ],
    ],
);
$formData = [
    'fields'     => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form add-product-form',
    ],
];
?>
<main class="page__content">
    <div class="content-container">
        <?= ViewHelper::vueTag('vue-profile', [
            'pageData' => [
                'products' => array_map(function (array $product) {
                    $product['id'] = rand(1, 1000);

                    return $product;
                }, [
                    PRODUCTS[2],
                    PRODUCTS[1],
                    PRODUCTS[0],
                    PRODUCTS[3],
                    PRODUCTS[1],
                    PRODUCTS[3],
                ]),
                'formData' => $formData,
            ],
            'nav'      => [
                [
                    'name'   => 'Профиль',
                    'link'   => '/06-04-profile.php',
                    'active' => false,
                    'code'   => 'profile-chef',
                ],
                [
                    'name'   => 'Чаты<sup>7</sup>',
                    'link'   => '/06-03-profile.php',
                    'active' => false,
                    'code'   => 'chats',
                ],
                [
                    'name'   => 'Мои блюда<sup>12</sup>',
                    'link'   => '/06-05-profile.php',
                    'active' => true,
                    'code'   => 'chef-products',
                ],
                [
                    'name'   => 'Полезные материалы<sup>20</sup>',
                    'link'   => '/06-06-profile.php',
                    'active' => false,
                    'code'   => 'materials',
                ],
            ]
        ]) ?>
    </div>
</main>
<?php require('partials/footer.php'); ?>
