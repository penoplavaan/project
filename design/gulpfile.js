'use strict';

// Для работы browserSync/liveReload (автоперезагрузка html, css, js) необходимо:
// 1. Установить переменную окружения USE_GULP_BROWSERSYNC/USE_GULP_LIVERELOAD в true.
// 2. Установить локальный домен в переменную окружения process.env.LOCAL_DOMAIN (для BrowserSync).
// 3. раскомментировать все блоки в этом файле, помеченные // #RELOAD для нужного типа перезагрузки
// 4. Запустить задачу serve
const path     = require('path');
const gulp     = require('gulp');
const chokidar = require('chokidar');
const webpack  = require('webpack');
const sxGulpFactory = require('sx-gulpfile-utils');
const gulpMultiProcess = require('gulp-multi-process');

// #RELOAD
// const liveReload  = require('gulp-livereload').create();
// const browserSync = require('browser-sync').create();

const BASE_PATH = './'; // Все исходники - в текущей папке
const options = {
    source: path.resolve(BASE_PATH),
    isMarkup: true, // Удалить этот параметр на сборке.
    // Для Laravel задать output: path.resolve('../public/')
    // #RELOAD
    // liveReload: !!process.env.USE_GULP_LIVERELOAD && liveReload !== undefined ? liveReload : undefined,
    // browserSync: !!process.env.USE_GULP_BROWSERSYNC && browserSync !== 'undefined' ? browserSync : undefined
};

const webpackConfig = require(`${BASE_PATH}webpack.config.js`);
const sibirixGulpUtils = sxGulpFactory.make(options);
const webpackRunner    = sxGulpFactory.makeWebpack(webpack, webpackConfig, options);

gulp.task('sprite', sibirixGulpUtils.spriteCompile({}));
gulp.task('symbol', sibirixGulpUtils.symbolCompile({}));
gulp.task('less', sibirixGulpUtils.lessCompile({}));
// Если надо несколько точек входа для компиляции Less'а - очень желательно делать это через одну задачу через wildcard в указании entry
// gulp.task('less', sibirixGulpUtils.lessCompile({ entry: '/css/style*.less' }));
// т.к. инициализация самого less'а, префиксера, оптимизатора занимает почти 1 секунду
gulp.task('all-styles', gulp.series(gulp.parallel('sprite', 'symbol'), 'less'));

// Webpack
gulp.task('webpack',              (cb) => webpackRunner(cb, false, false));
gulp.task('webpack-prod',         (cb) => webpackRunner(cb, true, false));
gulp.task('webpack-analyze-prod', (cb) => webpackRunner(cb, true, true));

// Следим за изменениями на ФС
gulp.task('watch', function() {
    gulpMultiProcess(['all-styles', 'webpack']);
    chokidar.watch(['images/sprite/**/*'], {cwd: BASE_PATH})
        .on('all', () => gulp.series('sprite'));

    chokidar.watch(['images/symbol/**/*'], {cwd: BASE_PATH})
        .on('all', () => gulp.series('symbol'));

    chokidar.watch(['css/**/*', 'build/**/*.less'], {cwd: BASE_PATH})
        .on('all', () => gulp.series('less'));

    chokidar.watch('js/page/*', {cwd: BASE_PATH})
        .on('add', () => gulp.series('webpack'))
        .on('unlink', () => gulp.series('webpack'));

    chokidar.watch('webpack.config.js', {cwd: BASE_PATH})
        .on('change', () => gulp.series('webpack'));

    // #RELOAD
    // if (!!process.env.USE_GULP_BROWSERSYNC) {
    //     chokidar.watch('build/js/*.js', {cwd: BASE_PATH})
    //         .on('change', browserSync.reload)
    //     chokidar.watch('./**/*.php', {cwd: BASE_PATH})
    //         .on('change', browserSync.reload)
    // }
});

// Сборка всей статики в прод-версию
//gulp.task('build', gulp.parallel(gulp.series(gulp.parallel('sprite', 'symbol'), 'less'), 'webpack-prod'));
gulp.task('build', (cb) => gulpMultiProcess(['all-styles', 'webpack-prod'], cb));

gulp.task('default', gulp.series('watch'));

// #RELOAD
// Инит browser-sync
// gulp.task('browser-sync', function() {
//     browserSync.init({
//         proxy: process.env.LOCAL_DOMAIN,
//     });
// });
// gulp.task('serve', function() {
//     gulp.series('browser-sync')();
//     gulp.series('watch')();
// });
