import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProfileRoot from '~/components/profile/ProfileRoot.vue';
import InitParallax from '~/partials/InitParallax';

/**
 * Список поваров
 */
class ProfilePage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-profile'), ProfileRoot);
        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));
    }
}

new ProfilePage(document.querySelector('body'));
