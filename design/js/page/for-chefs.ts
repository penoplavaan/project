import BasePage from '~/page/base';
import InitParallax from '~/partials/InitParallax';
import initVue from '~/helper/init-vue';
import ServiceFeaturesSlider from '~/components/about-service/ServiceFeaturesSlider.vue';
import ServiceReviewsSlider from '~/components/about-service/ServiceReviewsSlider.vue';
import ServiceSupportItems from '~/components/about-service/ServiceSupportItems.vue';
import RegistrationForm from '~/components/forms/RegistrationForm.vue';
import FaqItem from '~/components/faq/FaqItem.vue';

/**
 * Поварам
 */
class ForChefsPage extends BasePage {

    init(): void {
        super.init();

        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));

        initVue(this.el?.querySelectorAll('.vue-service-features-slider'), ServiceFeaturesSlider);
        initVue(this.el?.querySelectorAll('.vue-service-reviews-slider'), ServiceReviewsSlider);
        initVue(this.el?.querySelectorAll('.vue-service-support-items'), ServiceSupportItems);
        initVue(this.el?.querySelectorAll('.vue-registration-form'), RegistrationForm);
        initVue(this.el?.querySelectorAll('.vue-faq-item'), FaqItem);
    }
}

new ForChefsPage(document.querySelector('body'));
