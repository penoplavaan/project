import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ChefInfo from '~/components/chef/ChefInfo.vue';
import DocumentsList from '~/components/documents/DocumentsList.vue';
import ChefProducts from '~/components/chef/ChefProducts.vue';
import ChefKitchenSlider from '~/components/chef/ChefKitchenSlider.vue';

/**
 * Детальная повара
 */
class ChefDetailPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-chef-info'), ChefInfo);
        initVue(this.el?.querySelectorAll('.vue-chef-products'), ChefProducts);
        initVue(this.el?.querySelectorAll('.vue-chef-kitchen-slider'), ChefKitchenSlider);
        initVue(this.el?.querySelectorAll('.vue-documents-list'), DocumentsList);
    }
}

new ChefDetailPage(document.querySelector('body'));
