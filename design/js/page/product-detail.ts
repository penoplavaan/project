import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProductPictureSlider from '~/components/product/ProductPictureSlider.vue';
import CountForm from '~/components/forms/CountForm.vue';
import ChefCard from '~/components/chef/ChefCard.vue';
import ProductsSlider from '~/components/product/ProductsSlider.vue';

/**
 * Детальная блюда
 */
class ProductDetailPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-product-picture-slider'), ProductPictureSlider);
        initVue(this.el?.querySelectorAll('.vue-count-form'), CountForm);
        initVue(this.el?.querySelectorAll('.vue-chef-card'), ChefCard);
        initVue(this.el?.querySelectorAll('.vue-products-slider'), ProductsSlider);
    }
}

new ProductDetailPage(document.querySelector('body'));
