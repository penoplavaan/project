import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import PromoSlider from '~/components/main/PromoSlider.vue';
import ReviewsSlider from '~/components/reviews/ReviewsSlider.vue';
import ChefsSlider from '~/components/chef/ChefsSlider.vue';
import ProductsSlider from '~/components/product/ProductsSlider.vue';
import InitParallax from '~/partials/InitParallax';

/**
 * Главная страница
 */
class MainPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-promo-slider'), PromoSlider);
        initVue(this.el?.querySelectorAll('.vue-products-slider'), ProductsSlider);
        initVue(this.el?.querySelectorAll('.vue-reviews-slider'), ReviewsSlider);
        initVue(this.el?.querySelectorAll('.vue-chefs-slider'), ChefsSlider);

        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));
    }
}

new MainPage(document.querySelector('body'));
