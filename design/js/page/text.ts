import BasePage from '~/page/base';

/**
 * Типовая текстовая
 */
class TextPage extends BasePage {

    init(): void {
        super.init();
    }
}

new TextPage(document.querySelector('body'));
