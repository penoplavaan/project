import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ChefsMap from '~/components/chef/ChefsMap.vue';

/**
 * Список поваров
 */
class ChefListPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-chef-list'), ChefsMap);
    }
}

new ChefListPage(document.querySelector('body'));
