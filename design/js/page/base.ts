import initVue from '~/helper/init-vue';
import eventBus from '~/helper/event-bus';
import ResizeWatcher from '~/partials/ResizeWatcher';
import PopupController from '~/components/PopupController.vue';
import Header from '~/partials/Header';
import Footer from '~/partials/Footer';
import ScrollClassToggle from '~/partials/ScrollClassToggle';
import ScrollUp from '~/components/ScrollUp.vue';
import ConfirmLocation from '~/components/header/ConfirmLocation.vue';
import VTooltip from 'v-tooltip';
import Vue from 'vue';
import '~/directives';
import TextContent from '~/partials/TextContent';
import Detector from '~/helper/detector';

/**
 * Базовый контроллер страницы
 */
abstract class BasePage {
    readonly el: HTMLBodyElement | null = null;

    constructor(el: HTMLBodyElement | null) {
        if (el === null) {
            throw Error('Empty el parameter is not allowed');
        }

        Vue.use(VTooltip);

        this.el = el;
        this.init();
    }

    init(): void {
        new ResizeWatcher();

        // Init popups
        this.initPopupBtns();
        initVue(this.el?.querySelector('.vue-popup-controller'), PopupController);
        initVue(this.el?.querySelector('.vue-confirm-location'), ConfirmLocation);

        // Init layout
        new Header(this.el?.querySelector('.js-header') as HTMLElement);
        new Footer(this.el?.querySelector('.js-footer') as HTMLElement);
        initVue(this.el?.querySelector('.vue-scroll-up'), ScrollUp);

        // Init scroll animation
        new ScrollClassToggle(this.el as HTMLElement);

        // Init text content helper
        this.el?.querySelectorAll('.text-content').forEach(el => new TextContent(el as HTMLElement));

        document.documentElement.style.setProperty('--scrollbar-width', `${Detector.scrollbarWidth}px`);
    }

    /**
     * Вызов попапов.
     * Умеет находить кнопку вызова попапа даже если она отрендерилась во Vue
     */
    initPopupBtns() {
        window.addEventListener('click', (ev) => {
            const el = ev.target as HTMLElement;
            const vuePopup = el.closest('.vue-popup') as HTMLElement;

            if (vuePopup) {
                ev.preventDefault();

                let props = {};

                if (vuePopup.dataset.popup) {
                    props = JSON.parse(vuePopup.dataset.popup || '{}');
                }

                if (vuePopup.dataset.close) {
                    eventBus.$emit('close-all-popup');
                }

                eventBus.$emit('open-popup', {
                    type: vuePopup.dataset.type,
                    componentProps: props,
                });
            }
        });
    }
}

export default BasePage;
