/**
 * initResizeObserver следит за ресайзом ДОКУМЕНТА
 * это хэлпер для функций, которые должны отрабатывать при изменении размеров и позиций блоков вследствие:
 * - ресайзе окна
 * - переключении Mobile/Desktop в режиме эмуляции
 * - инициализации компонентов, переключении табов и т.п.
 */
import {debounce} from 'debounce';

export function initResizeObserver(callback) {
    if (!window) return;

    if (!('ResizeObserver' in window)) {
        window?.addEventListener('resize', callback);

        return;
    }

    const debouncedCallback = debounce(callback, 100);

    const ro = new ResizeObserver(entries => {
        debouncedCallback();
    });

    ro.observe(document.body);
}
