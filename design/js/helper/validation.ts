import {email, phone, required, requiredIfEmpty} from 'sx-vue-form-validator';
import TObjectCollection from '~/types/TObjectCollection';

type TConvertedValidation = {
    rule: Function,
    message?: string,
};

const stringLength = (min: number, max: number) => {
    return (val: string) => {
        let error: string = '';

        if (!val) {
            return true;
        }

        if (val.length > max) {
            error = `Максимальная длина строки - ${max}`;
        } else if (val.length < min) {
            error = `Минимальная длина строки - ${min}`;
        }

        return error ? error : true;
    };
};

const booleanRequired = (val: boolean) => {
    return val ? true : 'Заполните поле';
};

const validatorMap: TObjectCollection<(validationData: any) => TConvertedValidation> = {
    required: () => {
        return {
            rule: required,
        };
    },
    phone: () => {
        return {
            rule: phone,
        };
    },
    email: () => {
        return {
            rule: email,
        };
    },
    stringLength: (validationData: any) => {
        return {
            rule: stringLength(validationData.min, validationData.max),
        };
    },
    requiredIfEmpty: (validationData: any) => {
        return {
            rule: requiredIfEmpty(validationData.field),
            message: 'Заполните поле',
        };
    },
    booleanRequired: () => {
        return {
            rule: booleanRequired,
        };
    },
};

export const convertValidator = (validatorName: string, validatorData: any): TConvertedValidation => {
    if (validatorMap.hasOwnProperty(validatorName)) {
        return validatorMap[validatorName](validatorData);
    }

    return {rule: () => true};
};
