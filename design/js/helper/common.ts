export function clearPhone(phone: string): string {
    let cleaned = (phone).replace(/\D/g, '');
    let len = cleaned.length;

    if (len == 10) return '+7' + cleaned;

    if (len == 11) return cleaned.replace(/^[78]/, '+7');

    return cleaned;
}

export function wordForm(num: number, word: string[]): string {
    const num100 = num % 100;
    const num10 = num % 10;

    if (
        (num100 > 10 && num100 < 20) // 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
        || (num >= 5) //5, 6, 7, 8, 9
        || (num === 0) // 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100
    ) {
        return word[2]; // штук
    } else if (num10 === 1) {
        return word[0]; // штука
    } else  {
        // 2, 3, 4
        return word[1]; // штуки
    }
}

export function getEntryVisibility(entry: IntersectionObserverEntry): { visible: boolean, below: boolean, above: boolean, showed: boolean } {
    const rectY = entry.boundingClientRect.y || 0;
    const rootY = entry.rootBounds ? entry.rootBounds.y : 0;

    const visible = entry.isIntersecting;
    const below = rectY > rootY;
    const above = rectY < rootY;
    const showed = visible || above;

    return {
        visible: visible,
        below: below,
        above: above,
        showed: showed,
    };
}

export function randString(length: number = 8): string {
    const chars = 'abdehkmnpswxzABDEFGHKMNPQRSTWXZ123456789';
    let str = '';

    for (let i = 0; i < length; i++) {
        let pos = Math.floor(Math.random() * chars.length);

        str += chars.substring(pos,pos+1);
    }

    return str;
}

export function scrollTo(target: Element|HTMLElement, offset: number = 0) {
    const headerHeight = document.querySelector('header')?.getBoundingClientRect().height;
    const headerCompensate = headerHeight ? headerHeight : 0;

    window?.scrollTo({
        top: target.getBoundingClientRect().top + window.scrollY - headerCompensate - offset,
        behavior: 'smooth',
    });
}

export function clearHTMLEntites(text: string): string {
    const tmpDiv = document.createElement('div');

    tmpDiv.innerHTML = text;

    return tmpDiv.textContent || tmpDiv.innerText || '';
}
