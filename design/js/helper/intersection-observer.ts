export function initIntersectionObserver(el, callback, unobserve = true) {
    if (!el || !window || !('IntersectionObserver' in window)) {
        callback();
    } else {
        const io = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    callback();

                    if (unobserve) io.observe(el);
                }
            });
        });

        io.observe(el);
    }
}
