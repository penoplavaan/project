import { BREAKPOINTS } from '~/helper/const';

export function checkWebpSupport(): boolean {
    // Если SSR
    if (typeof window === 'undefined') {
        return false;
    }

    let webpSupported;
    const element = document.createElement('canvas');

    if (element.getContext && element.getContext('2d')) {
        webpSupported = element.toDataURL('image/webp').indexOf('data:image/webp') == 0;
    } else {
        webpSupported = false;
    }

    return webpSupported;
}

const hasWebpSupport = checkWebpSupport();
const scrollbarWidth = Math.round(window.innerWidth - document.documentElement.clientWidth);

const Detector = {
    get isXs(): boolean  {
        return typeof document !== 'undefined' ? document.documentElement.clientWidth < BREAKPOINTS.xs : false;
    },

    get isSm(): boolean  {
        return typeof document !== 'undefined' ? document.documentElement.clientWidth < BREAKPOINTS.sm : false;
    },

    get isMd(): boolean  {
        return typeof document !== 'undefined' ? document.documentElement.clientWidth < BREAKPOINTS.md : false;
    },

    get isLg(): boolean  {
        return typeof document !== 'undefined' ? document.documentElement.clientWidth < BREAKPOINTS.lg : false;
    },

    get isXl(): boolean  {
        return typeof document !== 'undefined' ? document.documentElement.clientWidth < BREAKPOINTS.xl : false;
    },

    get userAgent(): string {
        return navigator !== undefined ? navigator.userAgent : '';
    },

    get vendor(): string {
        return (navigator !== undefined ? navigator?.vendor : '') || '';
    },

    get getWindowHeight(): number {
        return window.innerHeight;
    },

    get getOrientation(): string {
        return window.innerWidth > window.innerHeight ? 'horizontal' : 'vertical';
    },

    get hasWebpSupport(): boolean {
        return hasWebpSupport;
    },

    get scrollbarWidth(): number {
        return scrollbarWidth;
    },

    get isSafari(): boolean {
        const userAgent: string = this.userAgent.toLowerCase();
        const vendor: string = this.vendor.toLowerCase();

        return (
            userAgent.includes('safari') &&
            !userAgent.includes('crios') &&
            !userAgent.includes('chrome') &&
            vendor.includes('apple')
        );
    },
};

export default Detector;
