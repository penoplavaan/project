export type TCatalogCategory = {
    url: string,
    name: string,
    countText: string,
}
