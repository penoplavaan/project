import TFile from '~/types/TFile';

type TDocumentsGroup = {
    groupName: string,
    items: TFile[],
}

export default TDocumentsGroup;
