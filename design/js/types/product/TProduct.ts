import {TBackendImage, TPicture} from '~/types/images';
import {TChef} from '~/types/chef/TChef';

export type TProduct = TPicture & {
    id: number,
    promoPicture?: TBackendImage,
    name: string,
    price: number,
    chef: TChef,
    url: string,
    size?: string,
    previewText?: string,
}
