import {TPicture} from '~/types/images';

export type TSocial = TPicture & {
    url: string,
}
