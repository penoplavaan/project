type TFile = {
    name: string,
    href: string,
    editable?: boolean,
}

export default TFile;
