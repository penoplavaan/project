export type TFaqItem = {
    question: string,
    answer: string,
};
