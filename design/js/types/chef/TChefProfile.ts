import {TBackendImage} from '~/types/images';
import TChefLabel from '~/types/chef/TChefLabel';
import TUserData from '~/types/profile/TUserData';

type TCategory = {
    id: number,
    name: string,
}

type TChefProfile = TUserData & {
    avatar: TBackendImage,
    name: string,
    specialization: string,
    categories: TCategory[],
    labels: TChefLabel[],
    about: string,
    address: string,
    experience: string,
    registerDate: string,
}

export default TChefProfile;
