import {TBackendImage} from '~/types/images';

type TChefKitchen = {
    id: number,
    picture: TBackendImage,
}

export default TChefKitchen;
