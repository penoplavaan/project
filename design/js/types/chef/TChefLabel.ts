import {TBackendImage} from '~/types/images';

type TChefLabel = {
    color?: string,
    icon: TBackendImage,
    name: string,
}

export default TChefLabel;
