import {TPicture} from '~/types/images';
import {TLink} from '~/types/TLink';
import TMapMarker from '~/types/map/TMapMarker';
import TChefLabel from '~/types/chef/TChefLabel';

export type TChef = TPicture & TMapMarker & {
    name: string,
    url: string,
    position: string,
    distance?: number,
    tags: TLink[],
    labels: TChefLabel[],
}
