type TUserData = {
    avatar: string,
    fio: string,
    address: string,
    about: string,
    registerDate: string,
    phone: string,
    email: string,
}

export default TUserData;
