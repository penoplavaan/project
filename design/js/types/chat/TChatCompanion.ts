import TChatUser from '~/types/chat/TChatUser';

type TChatCompanion = {
    user: TChatUser,
    date: string,
    hasUnread: boolean,
}

export default TChatCompanion;
