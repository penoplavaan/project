import {TBackendImage} from '~/types/images';

type TChatUser = {
    id: number,
    avatar: TBackendImage,
    name: string,
}

export default TChatUser;
