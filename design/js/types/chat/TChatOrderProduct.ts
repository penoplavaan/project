import TChatProduct from '~/types/chat/TChatProduct';

type TChatOrderProduct = {
    id: number,
    product: TChatProduct,
}

export default TChatOrderProduct;
