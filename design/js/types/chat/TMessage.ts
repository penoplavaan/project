import TChatProduct from '~/types/chat/TChatProduct';

export type TMessage = {
    id: number,
    type: 'message' | 'order',
    message: string,
    date: number, //timestamp
    isMy: boolean,
    images?: string[],
}

export type TOrderMessage = TMessage & {
    products: TChatProduct[],
}
