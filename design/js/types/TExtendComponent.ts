import Vue from 'vue';

// используется при наследовании компонентов, чтобы ts не ругался на отсутствие свойств/методов
// пример: Class extends Mixins<TExtend<ParentClass>>(ParentClass)
type TExtend<T> = {} & (T extends Vue ? T['$data'] : never) & T;

export default TExtend;