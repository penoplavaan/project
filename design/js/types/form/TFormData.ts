import TFormAttributes from '~/types/form/TFormAttributes';
import TFormField from '~/types/form/TFormField';
import TObjectCollection from '~/types/TObjectCollection';

type TFormData = {
    attributes: TFormAttributes,
    fields: TObjectCollection<TFormField>,
    validation: TObjectCollection<TObjectCollection<any>>,
    privacy: string,
}

export default TFormData;
