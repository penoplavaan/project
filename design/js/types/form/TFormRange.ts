export type TFormRange = {
    min: number,
    max: number,
    interval?: number,
}
