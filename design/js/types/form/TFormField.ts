import TObjectCollection from '~/types/TObjectCollection';

type TFormField = TObjectCollection & {
    type: string,
    required?: boolean,
    disabled?: boolean,
    label?: string,
    placeholder?: string,
    value?: any,
    options?: any[],
    pattern?: string,
}

export default TFormField;
