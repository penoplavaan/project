type TObjectCollection<T = any> = {
    [key: string]: T,
}

export default TObjectCollection;