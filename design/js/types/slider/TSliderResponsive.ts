import TSliderParams from '~/types/slider/TSliderParams';

type TSliderResponsive = {
    breakpoint: number,
    settings: Omit<TSliderParams, 'responsive'> | 'unslick',
}

export default TSliderResponsive;
