type TTabNav = {
    name: string,
    link: string,
    active: boolean,
}

export default TTabNav;
