import TObjectCollection from '~/types/TObjectCollection';

type TMapMarker = {
    id: number,
    clusterName?: string,
    coords: number[],
    options?: TObjectCollection,
}

export default TMapMarker;