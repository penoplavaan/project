export type TResizeWatcher = {
    isXs: boolean,
    isSm: boolean,
    isMd: boolean,
    isLg: boolean,
    isXl: boolean,
}
