import {TPicture} from '~/types/images';

export type TReview = TPicture & {
    name: string,
    position?: string,
    previewText: string,
    detailText?: string,
}
