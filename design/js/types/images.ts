export type TWebpImage = {
    srcset?: string,
    src: string,
};

export type TImageSizes = {
    width: number;
    height: number;
};

export type TBackendImage = {
    originalSrc: string,
    size?: TImageSizes,
    mime: string,
    webp?: TWebpImage,
    srcset?: string,
}

export type TPicture = {
    picture: TBackendImage,
    pictureAlt?: string,
    pictureTitle: string,
}
