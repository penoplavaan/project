import {TPicture} from '~/types/images';

export type TServiceFeature = TPicture & {
    name: string,
    text?: string,
}
