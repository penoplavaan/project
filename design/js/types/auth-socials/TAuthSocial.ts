import {TPicture} from '~/types/images';

type TAuthSocial = TPicture;

export default TAuthSocial;
