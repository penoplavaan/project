import Vue from 'vue';

type TOptions = {
    prevent?: boolean,
}

type TRegisterElement = {
    el: HTMLElement,
    callback: Function,
    options: TOptions,
}

const elements: TRegisterElement[] = [];

// регистрация отслеживаемых элементов
function registerElement(el: HTMLElement, callback: Function, options: TOptions) {
    elements.push({el, callback, options});
}

// при отвязывании директивы больше не отслеживаем элемент
function unregisterElement(el: HTMLElement) {
    for (const index in elements) {
        if (elements[index].el === el) {
            elements.splice(Number(index), 1);
            break;
        }
    }
}

// при клике находим все отслеживаемые элементы, которые не находятся внутри элемента на который кликнули, вызываем их callback'и
function clickHandler(ev: MouseEvent) {
    for (const item of elements) {
        if (!item.el.contains(ev.target as HTMLElement)) {
            item.callback.call(null, ev);

            if (item.options.prevent) {
                ev.preventDefault();
            }
        }
    }
}

window?.addEventListener('click', clickHandler);

Vue.directive('click-outside', {
    bind: (el, binding) => {
        let options: TOptions = {};

        if (Object.keys(binding.modifiers).length) {
            options = {
                prevent: binding.modifiers.hasOwnProperty('prevent'),
            }
        }

        registerElement(el, binding.value as Function, options);
    },
    unbind: el => {
        unregisterElement(el);
    },
});