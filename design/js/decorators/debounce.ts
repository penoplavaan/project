import {createDecorator} from 'vue-class-component';
import {debounce} from 'debounce';

type TOptions = {
    time?: number,
}

export default function Debounce(options: TOptions) {
    return createDecorator((opts, handler) => {
        if (!opts.methods) {
            throw new Error('This decorator must be used on a vue component method.');
        }

        const originalFn = opts.methods[handler];

        opts.methods[handler] = debounce(originalFn, options.time ? options.time : undefined);
    });
}
