// Декларации модулей добавлять в этот файл
// declare module 'inputmask';
declare module 'vue-slick-carousel';
declare module 'debounce';
declare module 'vue-slide-up-down';
declare module 'vue-yandex-maps';
declare module 'parallax-js';
declare module 'vue-dnd-zone';

declare const APP: {
    sessid: string,
    router?: Array<string>,
};
