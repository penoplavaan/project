import { VuexModule, Module, Mutation, Action, MutationAction } from 'vuex-module-decorators'

@Module({ namespaced: true })
class ExampleStore extends VuexModule {

    // initial state
    public exampleValue: number = 0;

    // Мутации могут быть ТОЛЬКО синхронными
    @Mutation
    public setNewValue(newValue: number): void {
        this.exampleValue = newValue;
    }

    @Action
    public increment(): void {
        this.context.commit('setNewValue', (this.exampleValue + 1))
    }

    @Action
    public decrement(): void {
        this.context.commit('setNewValue', (this.exampleValue - 1))
    }

    // Пример использование асинхронной мутации
    @MutationAction({mutate: ['exampleValue']})
    async reset() {
        await new Promise((resolve, reject) => {
            setTimeout(resolve, 1500);
        });

        return ({exampleValue: 0});
    }

}
export default ExampleStore
