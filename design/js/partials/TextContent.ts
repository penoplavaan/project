import initVue from '~/helper/init-vue';
import Gallery from '~/components/Gallery.vue';

class TextContent {
    el: HTMLElement;

    constructor(el: HTMLElement) {
        this.el = el;

        this.init();
    }

    init() {
        this.el?.querySelectorAll('table').forEach(el => this.wrapTable(el));

        initVue(this.el?.querySelector('.vue-gallery'), Gallery);
    }

    wrapTable(table: HTMLElement) {
        let wrap: HTMLElement = document.createElement('div');

        wrap.classList.add('table-wrap', 'custom-scrollbar');
        table?.parentElement?.insertBefore(wrap, table);
        wrap.appendChild(table);
    }
}

export default TextContent;
