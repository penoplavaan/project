import initVue from '~/helper/init-vue';
import Socials from '~/components/Socials.vue';

class Footer {
    el: HTMLElement;

    constructor(el: HTMLElement) {
        this.el = el;

        this.init();
    }

    init() {
        initVue(this.el?.querySelectorAll('.vue-socials'), Socials);
    }
}

export default Footer;
