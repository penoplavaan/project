// @ts-ignore
import {getEntryVisibility} from '~/helper/common';
import eventBus from '~/helper/event-bus';

class ScrollClassToggle {
    el: HTMLElement;
    io: any;
    selector: string = '.js-scroll-class';

    constructor(el: HTMLElement) {
        this.el = el;

        this.init();
    }

    init() {
        this.io = new IntersectionObserver((entries) => {
            entries.forEach(entry => {
                const visibility = getEntryVisibility(entry);

                const target = entry.target as HTMLElement;
                const classPrefix = target.dataset.blockName ? target.dataset.blockName + '--' : '';

                //target.classList.toggle(classPrefix + 'visible', visibility.visible);
                //target.classList.toggle(classPrefix + 'below', visibility.below);
                //target.classList.toggle(classPrefix + 'above', visibility.above);
                target.classList.toggle(classPrefix + 'showed', visibility.showed);
            });
        }, {});

        this.el.querySelectorAll(this.selector).forEach((el) => {
            this.io.observe(el);
        });

        eventBus.$on('scroll-class-toggle-add', this.add.bind(this));
    }

    add(el) {
        if (!el) return;

        if (NodeList.prototype.isPrototypeOf(el as object)) {
            Array.prototype.forEach.call(el, item => this.io.observe(item));
        } else {
            this.io.observe(el);
        }
    }
}

export default ScrollClassToggle;
