import Parallax from 'parallax-js';

class InitParallax {
    el: HTMLElement;

    constructor(el: HTMLElement) {
        this.el = el;

        this.init();
    }

    init() {
        new Parallax(this.el);
    }
}

export default InitParallax;
