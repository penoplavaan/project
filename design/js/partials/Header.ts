import initVue from '~/helper/init-vue';
import MenuBtn from '~/components/header/MenuBtn.vue';
import Menu from '~/components/header/Menu.vue';
import {initResizeObserver} from '~/helper/resize-observer';
import eventBus from '~/helper/event-bus';
import LocationBtn from '~/components/header/LocationBtn.vue';

class Header {
    el: HTMLElement;
    wrapper: HTMLElement;
    fixedAnchor: HTMLElement;
    fixedBreakpoint: number = 0;

    constructor(el: HTMLElement) {
        this.el = el;
        this.wrapper = this.el.parentElement || document.body;
        this.fixedAnchor = this.wrapper.querySelector('.js-header-fixed-anchor') || this.wrapper;

        this.init();
    }

    init() {
        initVue(this.el?.querySelectorAll('.vue-location-btn'), LocationBtn);

        initVue(this.el?.querySelectorAll('.vue-menu-btn'), MenuBtn);
        initVue(this.el?.querySelectorAll('.vue-menu'), Menu);

        initResizeObserver(this.onResize.bind(this));
        this.onResize();

        window?.addEventListener('scroll', this.onScroll.bind(this));

        // fix подменю в шапке не закрывалось на iPad по тапу мимо
        document.addEventListener('touchstart', () => {},false);

        eventBus.$on('menu-toggle', this.onMenuToggle.bind(this));
    }

    onResize() {
        this.fixedBreakpoint  =  this.fixedAnchor.getBoundingClientRect().top + window.scrollY;

        this.onScroll();
    }

    onScroll() {
        this.el.classList.toggle('header--fixed', window?.scrollY > this.fixedBreakpoint);
    }

    onMenuToggle(state) {
        this.el.classList.toggle('header--menu-open', state);
    }
}

export default Header;
