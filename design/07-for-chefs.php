<?php
$pageClass = 'for-chefs';
$pageType = 'for-chefs';
$title = 'Поварам';

require('partials/header.php'); ?>
<main class="page__content">
    <div class="content-container">
        <?= view('for-chefs.promo') ?>
        <?= view('for-chefs.how-work') ?>
        <?= view('for-chefs.service-features') ?>
        <?= view('for-chefs.service-reviews') ?>
        <?= view('for-chefs.support') ?>
        <?= view('for-chefs.registration') ?>
        <?= view('for-chefs.faq') ?>
    </div>
</main>
<?php require('partials/footer.php'); ?>
