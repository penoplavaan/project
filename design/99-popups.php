<?php
$pageClass = 'text';
$pageType = 'text';
$title = 'Попапы';

$confirmLocation = true;

require('partials/header.php'); ?>
<main class="page__content">
    <div class="content-container">
        <div style="display: flex; flex-wrap: wrap; grid-gap: 20px;">
            <?= view('popups.locations') ?>
            <?= view('popups.feedback') ?>
            <?= view('popups.registration') ?>
            <?= view('popups.auth') ?>
            <?= view('popups.remember-password') ?>
            <?= view('popups.message') ?>
            <?= view('popups.chef') ?>
        </div>
    </div>
</main>
<?php require('partials/footer.php'); ?>
