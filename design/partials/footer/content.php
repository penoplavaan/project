<?
/** @var boolean $isShort */
?>

<footer class="footer js-footer <?= $isShort ? 'footer--short' : '' ?>">
    <div class="content-container footer__grid">
        <? if(!$isShort) { ?>
            <div class="footer__text-wrap">
                <div class="text-content footer__text">
                    <strong>Повар на&nbsp;связи</strong>&nbsp;&mdash; это сервис поиска изготовителей дает возможность взаимодействия
                    заказчиков и&nbsp;изготовителей напрямую без каких-либо сложностей. Ниже представлены ответы на&nbsp;часто
                    задаваемые вопросы и&nbsp;наши рекомендации для эффективного использования сервиса.
                </div>
                <a href="javascript:void(0);" class="btn btn--clear footer__btn">
                    <span class="btn__text">Узнать больше</span>
                </a>
            </div>
        <? } ?>

        <div class="footer__app app-block">
            <div class="app-block__buttons">
                <a href="javascript:void(0);" class="link app-block__btn"><?= ViewHelper::getSymbol('app-store'); ?></a>
                <a href="javascript:void(0);" class="link app-block__btn"><?= ViewHelper::getSymbol('google-play'); ?></a>
            </div>
            <div class="app-block__text">
                А&nbsp;еще&nbsp;&mdash; <strong>Повар на&nbsp;связи</strong> в&nbsp;твоем телефоне
            </div>

            <div class="footer__socials">
                <?= ViewHelper::vueTag('vue-socials', ['items' => SOCIALS, 'inFooter' => true]); ?>
            </div>
        </div>

        <div class="footer__copy">
            &copy;<?= date('Y') ?> <strong>Повар&nbsp;на&nbsp;связи</strong>
        </div>

        <div class="footer__terms">
            <a href="javascript:void(0);" class="link link--uline">Условия использования сервиса</a>
        </div>

        <div class="footer__dev">
            <a href="https://www.sibirix.ru" target="_blank" rel="nofollow" class="link link--with-icon">
                <span class="link__text">Разработка сайта — студия «Сибирикс»</span>
                <span class="link__icon footer__dev-icon-wrap">
                    <img src="<?= ViewHelper::getImage('dev.svg') ?>" alt="Sibirix">
                </span>
            </a>
        </div>
    </div>
</footer>
<?= view('footer.foot-nav'); ?>
