<?
$navItems = [
    [
        'icon' => '1.svg',
        'name' => 'Все блюда',
        'href' => 'javascript:void(0)',
    ],
    [
        'icon' => '2.svg',
        'name' => 'Профиль',
        'href' => 'javascript:void(0)',
    ],
    [
        'icon' => '3.svg',
        'name' => 'Чаты',
        'href' => 'javascript:void(0)',
    ],
]
?>

<nav class="foot-nav page__foot-nav">
    <div class="foot-nav__items">
        <? foreach ($navItems as $navItem): ?>
            <a href="<?= $navItem['href'] ?>" class="foot-nav__item link">
                <span class="foot-nav__icon">
                    <?= file_get_contents(P_DR . DIRECTORY_SEPARATOR . P_PICTURES . 'foot-nav' . DIRECTORY_SEPARATOR . $navItem['icon']) ?>
                </span>
                <span class="foot-nav__name"><?= $navItem['name'] ?></span>
            </a>
        <? endforeach ?>
    </div>
</nav>
