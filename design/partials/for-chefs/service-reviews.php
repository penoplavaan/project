<? $items = [
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('service-reviews/1.jpg')],
        'name' => 'Алексей Петров',
        'position' => 'Профессиональный повар',
        'previewText' => 'Я&nbsp;много лет готовлю выпечку дома. Благодаря сервису повар на&nbsp;связи я&nbsp;нашел новых клиентов и&nbsp;усовершенствовал рецепты',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('service-reviews/2.jpg')],
        'name' => 'Софья Ермакова',
        'position' => 'Повар-кондитер',
        'previewText' => 'Открыли для себя новое место для заказа фермерских продуктов. ',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('service-reviews/3.jpg')],
        'name' => 'Иван Андреев',
        'position' => 'Повар',
        'previewText' => 'После ухода на&nbsp;удаленку вопрос о&nbsp;моей карьере стал очень остро.  :)',
    ],
]; ?>

<section class="service-reviews page__section scroll-class js-scroll-class">
    <div class="h1 service-reviews__caption">Отзывы участников сервиса</div>
    <div class="service-reviews__slider">
        <?= ViewHelper::vueTag('vue-service-reviews-slider', [
            'items' => $items,
        ]) ?>
    </div>
</section>
