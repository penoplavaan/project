<section class="main-promo scroll-class js-scroll-class page__section">
    <div class="main-promo__grid">
        <div class="main-promo__caption">
            <div class="main-promo__label">Привет!</div>
            <div class="h0 main-promo__title">
                ПОВА-А-АР<br/>
                КАМО-О-ОН!
            </div>
        </div>
        <div class="main-promo__right-col main-promo__rocket promo-rocket scroll-class js-scroll-class js-parallax">
            <div class="promo-rocket__circle" data-depth="0.1"></div>
            <div class="promo-rocket__rocket" data-depth="0.3"></div>
            <div class="promo-rocket__logo" data-depth="0.2"></div>
        </div>
        <div class="main-promo__text-wrap">
            <div class="h3 main-promo__subtitle">
                Приглашаем
                к&nbsp;сотрудничеству
            </div>
            <div class="main-promo__text">
                &mdash;&nbsp;мы&nbsp;объединяем домашних и&nbsp;профессиональных поваров с&nbsp;людьми, готовыми заказывать блюда напрямую
            </div>
            <a href="javascript:void(0);" class="btn main-promo__btn">
                <span class="btn__text">Регистрация в&nbsp;сервисе</span>
            </a>
            <div class="main-promo__cta-comment">
                Регистрация займет всего 3&nbsp;минуты
            </div>
        </div>
    </div>
</section>
