<?
$answer = '<p><strong>Чтобы начать готовить на&nbsp;заказ, нужны лишь:</strong></p><ul><li>духовой шкаф и/или плита;</li><li>посуда для приготовления;</li><li>немного инвентаря (миксер, ножи, доски, скалки).</li><li>одноразовая посуда для упаковки.</li></ul>';

$items = [
    [
        'question' => 'О&nbsp;сервисе',
        'answer' => $answer,
    ],
    [
        'question' => 'Для чего нужен сервис?',
        'answer' => $answer,
    ],
    [
        'question' => 'Как создать задание?',
        'answer' => $answer,
    ],
    [
        'question' => 'Как выбрать изготовителя?',
        'answer' => $answer,
    ],
    [
        'question' => 'Как договориться о&nbsp;сделке?',
        'answer' => $answer,
    ],
    [
        'question' => 'Как стать изготовителем?',
        'answer' => $answer,
    ],
    [
        'question' => 'Как зарабатывать на&nbsp;сервисе?',
        'answer' => $answer,
    ],
    [
        'question' => 'О&nbsp;том, что нужно для&nbsp;старта',
        'answer' => $answer,
    ],
    [
        'question' => 'О&nbsp;том, как устанавливать цены',
        'answer' => $answer,
    ],
    [
        'question' => 'Как подобрать упаковку?',
        'answer' => $answer,
    ],
];
?>

<section class="faq-section page__section">
    <div class="faq-section__grid">
        <div class="faq-section__list">
            <div class="h1 faq-section__caption">ВОПРОСЫ<br/>и&nbsp;ОТВЕТЫ</div>
            <div class="faq-section__items">
                <? foreach ($items as $item) { ?>
                    <?= ViewHelper::vueTag('vue-faq-item', ['item' => $item, 'cssClass' => 'faq-section__item']) ?>
                <? } ?>
            </div>
        </div>
        <div class="faq-section__ask ask-block scroll-class js-scroll-class">
            <div class="ask-block__macaron js-parallax">
                <div data-depth="0.1" class="ask-block__macaron-1"></div>
                <div data-depth="0.2" class="ask-block__macaron-2"></div>
            </div>
            <div class="ask-block__content-wrap">
                <div class="ask-block__content">
                    <div class="ask-block__text">
                        <strong>&mdash;&nbsp;Не&nbsp;нашли ответа на&nbsp;свой вопрос?</strong><br/>
                        Задайте его нам срочно!
                    </div>
                    <a href="javascript:void(0);" class="btn ask-block__btn">Задать вопрос</a>
                </div>
            </div>
        </div>
    </div>
</section>
