<? $items = [
    [
        'name' => 'Контент',
        'text' => 'Поможем подготовить фотоконтент&nbsp;&mdash; в&nbsp;нашей команде работают опытные фотографы, которые разработалии гайд по&nbsp;подготовке фотографии блюд',
    ],
    [
        'name' => 'ПРОЦЕСС',
        'text' => 'Занимайтесь любимым делом, а&nbsp;мы&nbsp;берем на&nbsp;себя вопрос с&nbsp;платежами и&nbsp;логистикой, чтобы вы&nbsp;могли сосредоточиться на&nbsp;приготовлении блюд',
    ],
    [
        'name' => 'состав',
        'text' => 'Помощь в&nbsp;ценообразовании и&nbsp;рассчете КЖБУ&nbsp;&mdash; поможем вам составить привлекательное меню и&nbsp;предоставим советы по&nbsp;ценообразованию ваших блюд, а&nbsp;также предоставим формулу для рассчета КБЖУ для блюд',
    ],
    [
        'name' => 'на связи',
        'text' => 'Служба поддержки 24/7&nbsp;&mdash; задайте вопрос в&nbsp;любой момент, наша служба поддержки быстро поможет',
    ],
]; ?>

<section class="service-support page__section scroll-class js-scroll-class">
    <div class="service-support__back js-parallax">
        <div data-depth="-0.2" class="service-support__circle"></div>
        <div data-depth="0.3" class="service-support__pizza"></div>
    </div>

    <div class="h1 service-support__caption">поддержка участников</div>

    <div class="service-support__items">
        <?= ViewHelper::vueTag('vue-service-support-items', [
            'items' => $items,
        ]) ?>
    </div>
</section>
