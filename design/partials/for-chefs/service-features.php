<? $items = [
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('service-features/1.svg')],
        'name' => 'Верификация по&nbsp;паспорту',
        'text' => 'Мы&nbsp;просим всех специалистов пройти проверку документов. Тех, кто это сделал, легко найти по&nbsp;отметке &laquo;Паспорт проверен&raquo; в&nbsp;анкете',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('service-features/2.svg')],
        'name' => 'Достоверные<br/>отзывы',
        'text' => 'Публикуем только проверенные отзывы.<br/>С&nbsp;помощью отзывов клиенты могут составить впечатление о&nbsp;специалисте',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('service-features/3.svg')],
        'name' => 'Доверие и&nbsp;безопасность',
        'text' => 'Мы&nbsp;блокируем специалистов и&nbsp;клиентов, которые нарушают правила сервиса',
    ],
]; ?>

<section class="service-features page__section scroll-class js-scroll-class">
    <div class="service-features__cupcake-wrap js-parallax">
        <div class="service-features__cupcake" data-depth=".3"></div>
    </div>

    <div class="h1 service-features__caption">Ключевые фишки&nbsp;сервиса</div>

    <?= ViewHelper::vueTag('vue-service-features-slider', [
        'items' => $items,
    ]) ?>
</section>
