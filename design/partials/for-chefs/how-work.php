<? $items = [
    [
        'name' => 'Регистрируйтесь в&nbsp;сервисе',
        'text' => 'Мы&nbsp;заботимся о&nbsp;наших пользователях, поэтому каждого повара просим пройти проверку документов. Прикрепите паспорт в&nbsp;личном кабинете, мы&nbsp;оперативно рассмотрим вашу заявку',
    ],
    [
        'name' => 'Создавайте свои блюда',
        'text' => 'Вы&nbsp;лучше всех готовите уху? Или у&nbsp;вас лучшие мясные пироги в&nbsp;городе? Создайте блюда в&nbsp;личном кабинете, добавьте описание, прикрепите красочные фотографии и&nbsp;опубликуйте блюдо',
    ],
    [
        'name' => 'Откликайтесь на&nbsp;заказы',
        'text' => 'Ищите заказы пользователей в&nbsp;разделе &laquo;Все заказы&raquo; и&nbsp;оставляйте заявки на&nbsp;блюда. Вас обязательно выберут',
    ],
]; ?>

<section class="how-work page__section scroll-class js-scroll-class">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">как работает сервис?</div>
        <div class="section-caption__text">
            &mdash;&nbsp;Максимально просто и&nbsp;прозрачно, честное слово!
        </div>
    </div>

    <div class="how-work__list">
        <? foreach ($items as $index => $item) { ?>
            <div class="how-work-item how-work__item scroll-class js-scroll-class">
                <div class="how-work-item__name"><?= $item['name'] ?></div>
                <div class="how-work-item__text"><?= $item['text'] ?></div>
                <div class="how-work-item__num"><span></span><span></span></div>
            </div>
        <? } ?>

        <div class="how-work__btn-wrap">
            <a href="javascript:void(0);" class="btn how-work__btn">Регистрация в&nbsp;сервисе</a>
        </div>
    </div>


    <div class="how-work__sushi js-parallax">
        <div class="how-work__sushi-layer" data-depth="-0.1"></div>
        <div class="how-work__sushi-layer" data-depth="-0.2"></div>
    </div>
</section>
