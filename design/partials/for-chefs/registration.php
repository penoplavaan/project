<?php
$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'     => 'email',
            'name'     => 'email',
            'required' => true,
            'label'    => 'Email',
            'value'    => null,
        ],
        [
            'type'     => 'tel',
            'name'     => 'phone',
            'required' => true,
            'label'    => 'Телефон',
            'value'    => null,
        ],
        [
            'type'     => 'password',
            'name'     => 'password',
            'required' => true,
            'label'    => 'Пароль',
            'value'    => null,
        ],
        [
            'type'     => 'password',
            'name'     => 'repeatPassword',
            'required' => true,
            'label'    => 'Повторите пароль',
            'value'    => null,
        ],
        [
            'type'     => 'submit',
            'name'     => 'submit',
            'label'    => 'Зарегистрироваться',
            'value'    => null,
        ],
    ]
);

$formData = [
    'fields' => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form form--white registration-form',
    ],
    'privacy' => 'Нажимая на&nbsp;кнопку &laquo;Отправить&raquo;, я&nbsp;даю согласие на&nbsp;обработку моих персональных данных в&nbsp;соответствии с&nbsp;<a href="javascript:void(0)">политикой информационной безопасности</a>. Мы&nbsp;не&nbsp;используем данные и&nbsp;не&nbsp;присылаем рассылки',
]
?>

<section class="registration-section page__section scroll-class js-scroll-class">
    <div class="registration-section__content">
        <div class="registration-section__back js-parallax">
            <div data-depth=".1" class="registration-section__donut"></div>
            <div data-depth="-.1" class="registration-section__tacos"></div>
        </div>
        <div class="h1 registration-section__caption">Регистрация в&nbsp;сервисе</div>
        <div class="registration-section__form">
            <?= ViewHelper::vueTag('vue-registration-form', [
                'formData' => $formData,
                'submitcolor' => 'clear-white',
            ]) ?>
        </div>
    </div>
</section>
