<?
/* @var string $pageClass */
/* @var string $title */

require_once 'helper.php';
require_once 'data/include.php';

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="cmsmagazine" content="2c2951bb57cffc1481be768a629d3a6e" />
    <meta name="format-detection" content="telephone=no">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta name="description" content="<?= htmlspecialchars($title) ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?
    // Автоматический прелоад шрифтов
    $fontsPath = 'css/fonts';
    if ($dirh = opendir(P_LOCAL_PATH . $fontsPath)) {
        while ($file = readdir($dirh)) {
            if (in_array($file, ['.', '..'])) continue;
            if (!preg_match('/.*\.woff2$/i', $file)) continue;

            ?><link rel="preload" href="<?= $fontsPath ?>/<?= $file ?>" as="font" crossorigin="anonymous"><?
        }
    }
    ?>
    <title><?= $title ?></title>

    <meta name="msapplication-TileColor" content="#023e84">
    <meta name="msapplication-config" content="<?= P_IMAGES ?>favicons/browserconfig.xml">
    <meta name="theme-color" content="#023e84">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= P_IMAGES ?>favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="256x256" href="<?= P_IMAGES ?>favicons/android-chrome-256x256.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= P_IMAGES ?>favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= P_IMAGES ?>favicons/favicon-16x16.png">
    <link rel="manifest" href="<?= P_IMAGES ?>favicons/site.webmanifest">
    <link rel="mask-icon" href="<?= P_IMAGES ?>favicons/safari-pinned-tab.svg" color="#023e84">
    <meta name="theme-color" content="#06c160">

    <link rel="stylesheet" href="build/css/style.css?v=<?= file_exists('build/css/style.css') ? filemtime('build/css/style.css') : '0' ?>">
</head>
<body class="page <?= $pageClass ?>-page">

<div class="page__wrapper">
    <?= view('header.content', ['confirmLocation' => $confirmLocation ?? false]) ?>
    <div class="page__content-wrap">
