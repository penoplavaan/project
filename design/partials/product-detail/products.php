<? $sliderPlaceholder = view('placeholder.products-slider') ?>

<div class="products-slider-section page__section">
    <h3 class="h3 products-slider-section__caption">другие блюда этого повара</h3>
    <?= ViewHelper::vueTag('vue-products-slider', [
        'items'    => PRODUCTS,
        'btn'      => ['name' => 'Все блюда', 'url' => 'javascript:void(0);'],
        'onDetail' => true,
    ], 'div', $sliderPlaceholder); ?>
</div>
