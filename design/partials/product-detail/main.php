<div class="product-main page__section">
    <div class="product-main__grid">
        <?= view('product-detail.main.gallery') ?>
        <?= view('product-detail.main.buy') ?>
        <?= view('product-detail.main.about') ?>
        <?= view('product-detail.main.socials') ?>
        <?= view('product-detail.main.chef') ?>
    </div>
</div>
