<div class="product-main__gallery">
    <?= ViewHelper::vueTag('vue-product-picture-slider', ['items' => [
        ['originalSrc' => ViewHelper::getPicture('product-gallery/1.jpg')],
        ['originalSrc' => ViewHelper::getPicture('product-gallery/2.jpg')],
        ['originalSrc' => ViewHelper::getPicture('product-gallery/1.jpg')],
        ['originalSrc' => ViewHelper::getPicture('product-gallery/2.jpg')],
        ['originalSrc' => ViewHelper::getPicture('product-gallery/1.jpg')],
        ['originalSrc' => ViewHelper::getPicture('product-gallery/2.jpg')],
    ]]) ?>
</div>
