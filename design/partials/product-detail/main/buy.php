<div class="product-buy product-main__block product-main__buy visible-mobile">
    <span class="price product-buy__price">150 &#8381;</span>
    <div class="product-buy__count"><?= ViewHelper::vueTag('vue-count-form') ?></div>
    <div class="product-buy__btn-wrap">
        <button class="btn btn--clear"><span class="btn__text">Заказать блюдо</span></button>
    </div>
</div>
