<div class="product-main__about product-main__block product-about">
    <div class="product-about__block">
        <div class="product-about__title"><div class="label">Состав</div></div>
        Жаркое по-деревенски&nbsp;&mdash; простое в&nbsp;приготовлении, очень вкусное и&nbsp;сытное блюдо для семейного обеда или ужина. Много рецептов жаркого я&nbsp;перепробовала, но&nbsp;этот рецепт самый лучший, блюдо понравилось всем
    </div>

    <div class="product-about__block">
        <div class="product-about__title"><div class="label">Условия хранения</div></div>
        Желательно употребить сразу. Хранить в холодильнике при -5° С
    </div>

    <div class="product-about__block">
        <div class="product-about__title"><div class="label label--light-green visible-mobile">Описание</div></div>
        <div class="product-about__params">
            <div class="product-about__params-item">
                <div class="product-about__title">Пищевая ценность блюда на 100&nbsp;г</div>
                <div class="product-params">
                    <div class="product-params__item">
                        <div class="product-params__name">Калорийность</div>
                        <div class="product-params__value">250&nbsp;Ккал</div>
                    </div>
                    <div class="product-params__item">
                        <div class="product-params__name">Белки</div>
                        <div class="product-params__value">3.5&nbsp;г</div>
                    </div>
                    <div class="product-params__item">
                        <div class="product-params__name">Жиры</div>
                        <div class="product-params__value">11&nbsp;г</div>
                    </div>
                    <div class="product-params__item">
                        <div class="product-params__name">Углеводы</div>
                        <div class="product-params__value">5.5&nbsp;г</div>
                    </div>
                </div>
            </div>
            <div class="product-about__params-item">
                <div class="product-about__title">Инфо</div>
                <div class="product-params">
                    <div class="product-params__item">
                        <div class="product-params__name">Вес порции</div>
                        <div class="product-params__value">260 г</div>
                    </div>
                    <div class="product-params__item">
                        <div class="product-params__name">Расстояние</div>
                        <div class="product-params__value">
                            <div class="product-chef-location">
                                <?= ViewHelper::getSymbol('location', 'symbol product-chef-location__symbol') ?>
                                2.2&nbsp;км от&nbsp;вас
                            </div>
                        </div>
                    </div>
                    <div class="product-params__item">
                        <div class="product-params__name">Доставка</div>
                        <div class="product-params__value">Самовывоз</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-about__block product-about__tags">
        <div class="label label--light-green">Теги:</div>
        <? // Если нужно будет отключить hover, есть btn--static  ?>
        <a href="javascript:void(0);" class="btn btn--sm btn--tag"><span class="btn__text">Без глютена</span></a>
        <a href="javascript:void(0);" class="btn btn--sm btn--tag"><span class="btn__text">Без сахара</span></a>
        <a href="javascript:void(0);" class="btn btn--sm btn--tag"><span class="btn__text">Не содержит орехов</span></a>
    </div>

    <hr class="product-about__hr hidden-mobile" />

    <div class="product-buy hidden-mobile">
        <span class="price product-buy__price">150 &#8381;</span>
        <div class="product-buy__count"><?= ViewHelper::vueTag('vue-count-form') ?></div>
        <button class="btn btn--clear"><span class="btn__text">Заказать блюдо</span></button>
    </div>
</div>
