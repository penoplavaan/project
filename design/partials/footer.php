<?
/* @var string $pageType */
/* @var string $shortFooter */

$shortFooter = $shortFooter ?? false;
?>

        <?= view('sidebar') ?>
    </div>

    <?= view('footer.content', ['isShort' => $shortFooter]) ?>
</div>

<div class="vue-popup-controller"></div>

<?

// JS'ки подключаются так, чтобы к ним добавлялось время последней модификации
$jsFileList = ['commons.chunk'];

if ($pageType) {
    $jsFileList[] = $pageType . ".bundle";
}

foreach ($jsFileList as $jsFilename):
    $jsFilePath = "build/js/$jsFilename.js";
    if (!file_exists($jsFilePath)) continue;
    ?><script defer src="<?= $jsFilePath . '?' . filemtime($jsFilePath)?>"></script><?
endforeach;

?>
</body>
</html>

