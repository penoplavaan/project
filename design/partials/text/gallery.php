<?php
$images = [
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/2.jpg')],
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/3.jpg')],
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/4.jpg')],
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/5.jpg')],
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/2.jpg')],
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/3.jpg')],
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/4.jpg')],
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('text/gallery/1.jpg')],
        'thumb' => ['originalSrc' => ViewHelper::getPicture('text/gallery/5.jpg')],
    ],
];

echo ViewHelper::vueTag('vue-gallery', ['items' => $images]);
