<?
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT  & ~E_NOTICE); // todo УБРАТЬ НА СБОРКЕ

/** @noinspection PhpUnused */
define('P_DR', realpath(__DIR__ . '/../'));
define("P_LOCAL", "/");
define("P_LOCAL_PATH", P_DR . P_LOCAL);
define('P_IMAGES',   'images/');
define('P_PICTURES', 'pictures/');
define('P_PARTIALS', 'partials/');
define('P_BUILD',    'build/');
define('SYMBOLS_SVG', P_BUILD . 'symbols.svg');
define('P_BLANK_IMG',  P_IMAGES . 'blank.gif');

/**
 * @deprecated
 * @use ViewHelper::getSymbol
 */
function getSymbol(string $id = '', string $class = '', string $useClass = ''): string {
    return ViewHelper::getSymbol($id, $class, $useClass);
}

/**
 * @deprecated
 * @use ViewHelper::getPicture
 */
function getPicture(string $path): string {
    return ViewHelper::getPicture($path);
}

/**
 * @deprecated
 * @use ViewHelper::getImage
 */
function getImage(string $path): string {
    return ViewHelper::getPicture($path);
}

/**
 * @deprecated
 * @use view() и через echo либо <?= view()
 */
function render(string $template, array $params = []): void {
    echo view($template, $params);
}

/**
 * Рендер партиала с параметрами
 * @param string $template - можно писать путь с точками, как в Ларавеле.
 * @param array $params
 * @return string
 */
function view(string $template, array $params = []): string {
    if (is_array($params)) {
        extract($params);
    }

    $path = P_DR . DIRECTORY_SEPARATOR . P_PARTIALS . str_replace('.', DIRECTORY_SEPARATOR, $template) . '.php';

    ob_start();
    include $path;
    return ob_get_clean();
}

/**
 * Набор хелперов для работы с вёрсткой, в точности такой же, как и в чистой битриксовой сборке
 */
class ViewHelper {
    /**
     * @param string $id Название файла в папке /images/symbol/ (id символа в /images/svg-symbols.svg)
     * @param string $class
     * @param string $useClass
     *
     * @return string
     */
    public static function getSymbol(string $id = '', string $class = '', string $useClass = ''): string {
        return '<svg class="symbol-' . $id . ' ' . $class . '">' .
            '<use class="symbol__use ' . $useClass . '" xlink:href="' . SYMBOLS_SVG . '#' . $id . '"></use>' .
            '</svg>';
    }

    /**
     * @param string $path
     * @return string
     */
    public static function getPicture(string $path): string {
        $fullPath = P_LOCAL_PATH . P_PICTURES . $path;
        $time = file_exists($fullPath) ? filemtime($fullPath) : 0;

        return P_PICTURES . $path . "?" . $time;
    }

    /**
     * @param string $path
     * @return string
     */
    public static function getImage(string $path): string {
        $fullPath = P_LOCAL_PATH . P_IMAGES . $path;
        $time = file_exists($fullPath) ? filemtime($fullPath) : 0;

        return P_IMAGES . $path . "?" . $time;
    }

    /**
     * @param string $tag
     * @param array $attrs
     * @param string $content
     * @return string
     */
    public static function tag(string $tag, array $attrs = [], string $content = ''): string {
        $autoClose = ['img', 'input']; // ... more types
        $close = (array_search($tag, $autoClose) !== false);
        return '<' . $tag . ' ' . static::attrs($attrs) . (empty($content) && $close ? ' />' : '>' . $content . '</' . $tag . '>');
    }

    /**
     * @param array $params
     * @param bool $escape
     * @return string
     */
    public static function attrs(array $params, bool $escape = true): string {
        $attrString = [];

        foreach ($params as $key => $value) {
            if ($value === false || $value === null) continue;

            if ($value === true) {
                $attrString[] = $key;
            } else {
                if (is_array($value)) $value = implode(' ', $value);
                $val = $escape ? htmlspecialchars((string)$value) : $value;
                $attrString[] = $key . '="' . $val . '"';
            }
        }

        return implode(' ', $attrString);
    }

    /**
     * json_encode с доп. флагами, чтобы результат можно было безопасно вывести в data-аттрибут в HTML
     * @param array $data
     * @param int $flags
     * @return false|string
     */
    public static function jsonEncode(array $data, int $flags = 0) {
        return json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | $flags);
    }


    /**
     * Вывести тег для рендера Vue-компонента на фронте
     * CSS-класс надо указывать полностью, с префиксом "vue-"
     * это сделано для того, чтобы по коду проекта было проще найти все использования такого класса.
     * @param string $class
     * @param array $data
     * @param string $tag
     * @param string $content
     * @return string
     */
    public static function vueTag(string $class = 'vue-', array $data = [], string $tag = 'div', string $content = ''): string {
        return '<' . $tag . ' class="' . $class . '" data-vue=\'' . static::jsonEncode($data) . '\'>' . $content . '</' . $tag . '>';
    }

    public static function prepareFormFields(array $formFields): array {
        $result = [];

        foreach ($formFields as $formField) {
            if (!isset($formField['name'])) {
                continue;
            }

            $result[$formField['name']] = $formField;
        }

        return $result;
    }

    public static function getValidatorsForForm(array $formFields): array {
        $validators = [];

        foreach ($formFields as $formField) {
            $fieldValidators = [];
            $fieldName = $formField['name'] ?? null;

            if (is_null($fieldName)) {
                continue;
            }

            $required = isset($formField['required']) && $formField['required'];

            if ($required) {
                $fieldValidators['required'] = [];
            }

            if (isset($formField['minlength']) || isset($formField['maxlength'])) {
                $fieldValidators['stringLength'] = [
                    'min' => $formField['minlength'] ?? 0,
                    'max' => $formField['maxlength'] ?? PHP_INT_MAX,
                ];
            }

            if ($formField['type'] == 'tel') {
                $fieldValidators['phone'] = [];
            } else if ($formField['type'] == 'email') {
                $fieldValidators['email'] = [];
            }

//            if (!$fieldValidators) {
//                continue;
//            }

            $validators[$fieldName] = $fieldValidators;
        }

        return $validators;
    }
}
