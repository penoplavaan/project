<?php
$caption = $caption ?? '';
$postfix = $postfix ?? '';
?>
<div class="content-container">
    <h1 class="h1 page__page-caption">
        <?= $caption ?>
        <?= $postfix ?>
    </h1>
</div>
