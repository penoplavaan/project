<?
$breadcrumbs = $breadcrumbs ?? [];
$count = count($breadcrumbs);
?>

<nav class="breadcrumbs page__breadcrumbs">
    <div class="content-container">
        <div class="breadcrumbs__list custom-scrollbar custom-scrollbar--hidden">
            <? foreach ($breadcrumbs as $index => $item): ?>
                <? if ($index < $count - 1): ?>
                    <a href="javascript:void(0)" class="breadcrumbs__item link link--uline link--with-icon">
                        <span class="link__text"><?= $item ?></span>
                        <?= ViewHelper::getSymbol('breadcrumb-arrow', 'breadcrumbs__icon') ?>
                    </a>
                <? else: ?>
                    <span class="breadcrumbs__item breadcrumbs__item--disabled">
                        <?= $item ?>
                    </span>
                <? endif ?>
            <? endforeach ?>
        </div>
    </div>
</nav>
