<?
$currentLocation = 'Санкт-Петербург';

$nav = [
    [
        'name' => 'Все блюда',
        'url' => 'javascript:void(0);',
        'items' => [
            ['name' => 'Супы','url' => 'javascript:void(0);'],
            ['name' => 'Салаты','url' => 'javascript:void(0);'],
            ['name' => 'Горячее','url' => 'javascript:void(0);'],
            ['name' => 'Десерты','url' => 'javascript:void(0);'],
            ['name' => 'Суши','url' => 'javascript:void(0);'],
            ['name' => 'Торты','url' => 'javascript:void(0);'],
            ['name' => 'Кейтеринг','url' => 'javascript:void(0);'],
        ],
    ],
    [
        'name' => 'Все повара',
        'url' => 'javascript:void(0);',
    ],
    [
        'name' => 'Поварам',
        'url' => 'javascript:void(0);',
        //'current' => true,
    ],
];
?>

<div class="header-wrapper">
    <div class="header-wrapper__fixed-anchor js-header-fixed-anchor"></div>

    <header class="header js-header">
        <div class="content-container header__grid">

            <div class="header__location">
                <?= ViewHelper::vueTag('vue-location-btn', [
                    'current' => $currentLocation
                ]) ?>
            </div>

            <?= view('header.nav', ['items' => $nav]) ?>

            <div class="header__user">
                <a href="javascript:void(0);" class="link link--with-icon link--dashed">
                    <span class="link__icon"><?= ViewHelper::getSymbol('sign-in', 'symbol') ?></span>
                    <span class="link__text header__user-text">Авторизация</span>
                </a>
            </div>

            <div class="header__logo">
                <a href="javascript:void(0);" class="logo">
                    <span class="logo__img"><?= ViewHelper::getSymbol('logo', 'symbol') ?></span>
                    <span class="logo__text"><?= ViewHelper::getSymbol('logo-text', 'symbol') ?></span>
                </a>

                <? if ($confirmLocation) {
                    echo ViewHelper::vueTag('vue-confirm-location', ['current' => 'Санкт-Петербург']);
                } ?>
            </div>

            <div class="header__buttons">
                <a href="javascript:void(0);" class="btn btn--grey header__btn">
                    <span class="btn__text">Стать поваром</span>
                </a>
                <?/* <a href="javascript:void(0);" class="btn header__btn">
                    <span class="btn__text">Создать заказ</span>
                </a> */?>
            </div>

            <div class="header__menu-btn">
                <?= ViewHelper::vueTag('vue-menu-btn') ?>
            </div>
        </div>

        <?= ViewHelper::vueTag('vue-menu', [
            'currentLocation' => $currentLocation,
            'items' => $nav,
            'phone' => '8 (495) 545 31 88',
            'socials' => SOCIALS,
        ]) ?>
    </header>
</div>
