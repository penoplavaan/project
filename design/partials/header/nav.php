<?
/** @var array $items */

$items = $items ?? [];
?>

<nav class="header__nav header-nav">
    <ul class="header-nav__ul">
        <? foreach ($items as $level1) { ?>
            <li class="header-nav__li">
                <a href="<?= $level1['url'] ?>"
                   class="link link--hover-uline link--with-icon header-nav__link <?= isset($level1['current']) && $level1['current'] ? 'link--current' : '' ?>">
                    <span class="link__text"><?= $level1['name'] ?></span>

                    <? if (isset($level1['items']) && count($level1['items']) > 0) { ?>
                        <span class="link__icon">
                            <?= ViewHelper::getSymbol('spoiler', 'symbol header-nav__arrow') ?>
                        </span>
                    <? } ?>
                </a>

                <? if (isset($level1['items']) && count($level1['items']) > 0) { ?>
                    <div class="header-nav__sub dropdown">
                        <ul class="dropdown__ul">
                            <? foreach ($level1['items'] as $level2) { ?>
                                <li class="dropdown__li">
                                    <a href="<?= $level2['url'] ?>"
                                       class="link dropdown__link <?= isset($level2['current']) && $level2['current'] ? 'link--current' : '' ?>">
                                        <?= $level2['name'] ?>
                                    </a>
                                </li>
                            <? } ?>
                        </ul>
                    </div>
                <? } ?>
            </li>
        <? } ?>
    </ul>
</nav>
