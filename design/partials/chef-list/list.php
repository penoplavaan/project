<?= ViewHelper::vueTag('vue-chef-list', [
    'items'  => array_map(function (array $item) {
        $item['clusterName'] = 'chef';

        return $item;
    }, CHEFS),
    'filter' => CHEF_FILTER,
]) ?>
