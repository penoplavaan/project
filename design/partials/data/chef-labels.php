<?
$labels = [
    [
        'color' => '#06C160',
        'icon' => ['originalSrc' => ViewHelper::getPicture('chef-labels/1.svg')],
        'name' => 'Есть санкнижка',
    ],
    [
        'color' => '#F5DF4D',
        'icon' => ['originalSrc' => ViewHelper::getPicture('chef-labels/2.svg')],
        'name' => 'Паспорт подтвержден',
    ],
];
define('CHEF_LABELS', $labels);
