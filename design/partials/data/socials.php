<?
$socials = [
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('socials/whatsapp.svg')],
        'url' => 'javascript:void(0);',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('socials/telegram.svg')],
        'url' => 'javascript:void(0);',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('socials/vk.svg')],
        'url' => 'javascript:void(0);',
    ],
];

define('SOCIALS', $socials);
