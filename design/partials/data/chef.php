<?php

$chefData = [
    'chefData' => [
        'avatar'         => ['originalSrc' => ViewHelper::getPicture('profile/chef-avatar.jpg')],
        'name'           => 'Уразметова Гузель',
        'specialization' => 'Домашний повар',
        'categories'     => [
            [
                'id' => 1,
                'name' => 'Торты',
            ],
            [
                'id' => 2,
                'name' => 'Горячее',
            ],
            [
                'id' => 3,
                'name' => 'Выпечка',
            ],
        ],
        'labels'         => CHEF_LABELS,
        'about'          => 'Больше всего мне нравится готовить мясные блюда: стейки, конечно же. Предлагаю вам самим убедиться, что мои угощения сделаны с душой и любовью.',
        'address'        => 'Санкт-Петербург, ул. Маяковского, 1/96',
        'experience'     => '5 лет',
        'registerDate' => '19.01.2022',
        'phone'        => '+7 (909) 987-65-43',
        'email'        => 'lizzywizzy123@mail.ru',
    ],
    'chefKitchen' => [
        [
            'id' =>  rand(1, 1000),
            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/kitchen/1.jpg')],
        ],
        [
            'id' =>  rand(1, 1000),
            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/kitchen/2.jpg')],
        ],
        [
            'id' =>  rand(1, 1000),
            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/kitchen/3.jpg')],
        ],
        [
            'id' =>  rand(1, 1000),
            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/kitchen/4.jpg')],
        ],
        [
            'id' =>  rand(1, 1000),
            'picture' => ['originalSrc' => ViewHelper::getPicture('profile/kitchen/1.jpg')],
        ],
    ],
    'documents' => [
        [
            'groupName' => 'Документы',
            'items' => [
                [
                    'name' => 'Паспорт.pdf',
                    'href' => 'javascript:void(0)',
                ],
                [
                    'name' => 'Санкнижка 2022.pdf',
                    'href' => 'javascript:void(0)',
                ],
            ],
        ],
        [
            'groupName' => 'Дипломы и&nbsp;сертификаты',
            'items' => [
                [
                    'name' => 'Диплом о высшем образовании.pdf',
                    'href' => 'javascript:void(0)',
                ],
                [
                    'name' => 'Сертификат курса 29.03.2022.pdf',
                    'href' => 'javascript:void(0)',
                ],
                [
                    'name' => 'Сертификат курса 29.03.2022.pdf',
                    'href' => 'javascript:void(0)',
                ],
                [
                    'name' => 'Сертификат курса 29.03.2022.pdf',
                    'href' => 'javascript:void(0)',
                ],
                [
                    'name' => 'Диплом о высшем образовании.pdf',
                    'href' => 'javascript:void(0)',
                ],
            ],
        ],
    ],
];

define('CHEF_DATA', $chefData);

?>
