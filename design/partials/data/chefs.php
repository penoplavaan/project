<?
$chefs = [
    [
        'id' => rand(1, 1000),
        'name' => 'Алексей Петров',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/1.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 4.2,
        'position' => 'Профессиональный повар',
        'coords' => [55.798113436949734, 37.56441477734375],
        'labels' => CHEF_LABELS,
        'tags' => [
            ['name' => 'Салаты', 'url' => 'javascript:void(0);'],
            ['name' => 'Суши', 'url' => 'javascript:void(0);'],
            ['name' => 'Горячее', 'url' => 'javascript:void(0);'],
        ],
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Мовсисян Лилит',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/3.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 0.4,
        'position' => 'Профессиональный повар',
        'coords' => [55.83522775444078, 38.38701609570313],
        'labels' => CHEF_LABELS,
        'tags' => [
            ['name' => 'Торты', 'url' => 'javascript:void(0);'],
            ['name' => 'Десерты', 'url' => 'javascript:void(0);'],
        ],
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Уразметова Гузель',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/2.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 3.4,
        'position' => 'Профессиональный повар',
        'coords' => [55.55353217794332, 37.86026924271116],
        'labels' => CHEF_LABELS,
        'tags' => [
            ['name' => 'Горячее', 'url' => 'javascript:void(0);'],
            ['name' => 'Кейтеринг', 'url' => 'javascript:void(0);'],
        ],
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Маша Петрова',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/4.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 0.8,
        'position' => 'Домашний повар',
        'coords' => [55.49042627997218, 37.24640815872677],
        'labels' => CHEF_LABELS,
        'tags' => [
            ['name' => 'Горячее', 'url' => 'javascript:void(0);'],
            ['name' => 'Салаты', 'url' => 'javascript:void(0);'],
            ['name' => 'Десерты', 'url' => 'javascript:void(0);'],
            ['name' => 'Супы', 'url' => 'javascript:void(0);'],
        ],
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Алексей Петров',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/1.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 4.2,
        'position' => 'Профессиональный повар',
        'coords' => [55.6072089089449, 37.19422310013303],
        'labels' => CHEF_LABELS,
        'tags' => [
            ['name' => 'Салаты', 'url' => 'javascript:void(0);'],
            ['name' => 'Суши', 'url' => 'javascript:void(0);'],
            ['name' => 'Горячее', 'url' => 'javascript:void(0);'],
        ],
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Мовсисян Лилит',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/3.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 0.4,
        'position' => 'Профессиональный повар',
        'coords' => [55.748437698318554, 37.4620148481799],
        'tags' => [
            ['name' => 'Торты', 'url' => 'javascript:void(0);'],
            ['name' => 'Десерты', 'url' => 'javascript:void(0);'],
        ],
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Уразметова Гузель',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/2.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 3.4,
        'position' => 'Профессиональный повар',
        'coords' => [55.803419378026064, 37.73727068872043],
        'labels' => CHEF_LABELS,
        'tags' => [
            ['name' => 'Горячее', 'url' => 'javascript:void(0);'],
            ['name' => 'Кейтеринг', 'url' => 'javascript:void(0);'],
        ],
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Маша Петрова',
        'picture' => ['originalSrc' => ViewHelper::getPicture('chefs/4.jpg')],
        'url' => 'javascript: void(0);',
        'distance' => 0.8,
        'position' => 'Домашний повар',
        'coords' => [55.87605828889723, 37.64388689965793],
        'labels' => CHEF_LABELS,
        'tags' => [
            ['name' => 'Горячее', 'url' => 'javascript:void(0);'],
            ['name' => 'Салаты', 'url' => 'javascript:void(0);'],
            ['name' => 'Десерты', 'url' => 'javascript:void(0);'],
            ['name' => 'Супы', 'url' => 'javascript:void(0);'],
        ],
    ],
];
define('CHEFS', $chefs);
