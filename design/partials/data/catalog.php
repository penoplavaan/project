<?
$catalogCategories = [
    [
        'name' => 'Супы',
        'count' => 238,
        'countText' => '238 поваров',
    ],
    [
        'name' => 'Салаты',
        'count' => 87,
        'countText' => '87 поваров',
    ],
    [
        'name' => 'Горячее',
        'count' => 245,
        'countText' => '245 поваров',
    ],
    [
        'name' => 'Десерты',
        'count' => 15,
        'countText' => '15 кондитеров',
    ],
    [
        'name' => 'Суши',
        'count' => 79,
        'countText' => '79 поваров',
    ],
    [
        'name' => 'Торты',
        'count' => 48,
        'countText' => '48 кондитеров',
    ],
    [
        'name' => 'Кейтеринг',
        'count' => 13,
        'countText' => '13 предложений',
    ],
];
define('CATALOG_CATEGORIES', $catalogCategories);
