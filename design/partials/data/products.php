<?
$products = [
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/1.jpg')],
        'name' => 'Суп с курицей и карри',
        'price' => 250,
        'chef' => CHEFS[0],
        'url' => 'javascript:void(0);',
        'size' => '1 порция / 200 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/2.jpg')],
        'name' => 'Томатный <nobr>суп-пюре</nobr>',
        'price' => 420,
        'chef' => CHEFS[1],
        'url' => 'javascript:void(0);',
        'size' => '2 порции / 400 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/3.jpg')],
        'name' => 'Тыквеный суп со&nbsp;сливками',
        'price' => 150,
        'chef' => CHEFS[2],
        'url' => 'javascript:void(0);',
        'size' => '1 порция / 200 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/4.jpg')],
        'name' => 'Банановый десерт',
        'price' => 400,
        'chef' => CHEFS[0],
        'url' => 'javascript:void(0);',
        'size' => '1 порция / 200 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/5.jpg')],
        'name' => 'Овсяный десерт с орехами ',
        'price' => 310,
        'chef' => CHEFS[1],
        'url' => 'javascript:void(0);',
        'size' => '2 порции / 400 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/6.jpg')],
        'name' => 'Десерт &laquo;Картошка&raquo;',
        'price' => 150,
        'chef' => CHEFS[2],
        'url' => 'javascript:void(0);',
        'size' => '1 порция / 200 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/7.jpg')],
        'name' => 'Руккола + Брокколи',
        'price' => 299,
        'chef' => CHEFS[0],
        'url' => 'javascript:void(0);',
        'size' => '400 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/8.jpg')],
        'name' => 'Помидоры домашние ',
        'price' => 160,
        'chef' => CHEFS[1],
        'url' => 'javascript:void(0);',
        'size' => '1 кг',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
    [
        'picture' => ['originalSrc' => ViewHelper::getPicture('main-catalog/9.jpg')],
        'name' => 'Свежий салат',
        'price' => 80,
        'chef' => CHEFS[2],
        'url' => 'javascript:void(0);',
        'size' => '250 г',
        'previewText' => 'Очень простое в&nbsp;приготовлении, но&nbsp;очень ароматное и&nbsp;вкусное блюдо для семейного обеда или ужина',
    ],
];
define('PRODUCTS', $products);

$mainPromoProducts = [
    [
        'promoPicture' => ['originalSrc' => ViewHelper::getPicture('main-promo/1.png')],
        'name' => 'Лазанья традиционная',
        'price' => 420,
        'chef' => CHEFS[0],
        'url' => 'javascript:void(0);',
    ],
    [
        'promoPicture' => ['originalSrc' => ViewHelper::getPicture('main-promo/2.png')],
        'name' => 'Спагетти с&nbsp;фаршем',
        'price' => 420,
        'chef' => CHEFS[1],
        'url' => 'javascript:void(0);',
    ],
    [
        'promoPicture' => ['originalSrc' => ViewHelper::getPicture('main-promo/3.png')],
        'name' => 'Мясо по-кремлевски',
        'price' => 420,
        'chef' => CHEFS[2],
        'url' => 'javascript:void(0);',
    ],
];
define('MAIN_PROMO_PRODUCTS', $mainPromoProducts);

$mainCatalogProducts = [
    [
        'name' => 'Блюда от&nbsp;поваров',
        'items' => [
            PRODUCTS[0],
            PRODUCTS[1],
            PRODUCTS[2],
        ],
    ],
    [
        'name' => 'ДЕСЕРТЫ ОТ&nbsp;КОНДИТЕРОВ',
        'items' => [
            PRODUCTS[3],
            PRODUCTS[4],
            PRODUCTS[5],
        ],
    ],
    [
        'name' => 'Продукты от&nbsp;фермеров',
        'items' => [
            PRODUCTS[6],
            PRODUCTS[7],
            PRODUCTS[8],
        ],
    ],
];
define('MAIN_CATALOG_PRODUCTS', $mainCatalogProducts);

$productList = [
    PRODUCTS[1],
    PRODUCTS[0],
    PRODUCTS[2],
    PRODUCTS[2],
    PRODUCTS[1],
    PRODUCTS[0],
    PRODUCTS[0],
    PRODUCTS[2],
    PRODUCTS[1],
];
define('PRODUCT_LIST', $productList);
