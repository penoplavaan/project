<?
$filter = [
    'category' => [
        'type'        => 'checkbox',
        'label'       => 'Все категории',
        'collapsable' => false,
        'items'       => [
            [
                'type'  => 'checkbox',
                'value' => 2,
                'label' => 'Супы<sup>8</sup>',
                'name'  => 'category',
            ],
            [
                'type'  => 'checkbox',
                'value' => 3,
                'label' => 'Салаты<sup>12</sup>',
                'name'  => 'category',
            ],
            [
                'type'  => 'checkbox',
                'value' => 4,
                'label' => 'Горячее<sup>10</sup>',
                'name'  => 'category',
            ],
            [
                'type'  => 'checkbox',
                'value' => 5,
                'label' => 'Десерты<sup>5</sup>',
                'name'  => 'category',
            ],
            [
                'type'  => 'checkbox',
                'value' => 6,
                'label' => 'Суши<sup>2</sup>',
                'name'  => 'category',
            ],
            [
                'type'  => 'checkbox',
                'value' => 7,
                'label' => 'Торты<sup>4</sup>',
                'name'  => 'category',
            ],
            [
                'type'  => 'checkbox',
                'value' => 8,
                'label' => 'Кейтеринг<sup>3</sup>',
                'name'  => 'category',
            ],
        ],
    ],
    'deliveryType' => [
        'type'        => 'radio',
        'label'       => 'Тип доставки',
        'collapsable' => true,
        'items'       => [
            [
                'type'  => 'radio',
                'value' => 1,
                'label' => 'Самовывоз',
                'name'  => 'delivery',
            ],
            [
                'type'  => 'radio',
                'value' => 2,
                'label' => 'Доставка',
                'name'  => 'delivery',
            ],
        ],
    ],
    'isBanket' => [
        'type' => 'checkbox',
        'label'       => null,
        'collapsable' => false,
        'items'       => [
            [
                'type'  => 'checkbox',
                'value' => 1,
                'label' => 'Обслуживание банкетов/кейтеринг',
                'name'  => 'service',
            ],
        ],
    ],
    'price' => [
        'type'        => 'range',
        'label'       => 'Цена, &#8381;',
        'collapsable' => true,
        'range' => [
            'min'   => 0,
            'max'   => 150,
        ]
    ],
    'taste' => [
        'type'        => 'checkbox',
        'label'       => 'Вкусовые предпочтения',
        'collapsable' => true,
        'items'       => [
            [
                'type'  => 'checkbox',
                'value' => 1,
                'label' => 'Без глютена',
                'name'  => 'taste',
            ],
            [
                'type'  => 'checkbox',
                'value' => 2,
                'label' => 'Без сахара',
                'name'  => 'taste',
            ],
            [
                'type'  => 'checkbox',
                'value' => 3,
                'label' => 'Не содержит орехов',
                'name'  => 'taste',
            ],
        ],
    ],
];

define('CHEF_FILTER', [
    $filter['category'],
    $filter['deliveryType'],
    $filter['isBanket']
]);

define('PRODUCT_FILTER', [
    $filter['category'],
    $filter['price'],
    $filter['deliveryType'],
    $filter['taste'],
    $filter['isBanket']
]);
