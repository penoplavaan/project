<section class="page__section">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">отзывы</div>
        <div class="section-caption__text">&mdash;&nbsp;Мы&nbsp;публикуем только проверенные отзывы</div>
    </div>

    <div class="about-reviews js-scroll-class scroll-class">
        <div class="about-reviews__text">
            Наши пользователи оставили <a href="javascript:void(0);">206&nbsp;отзывов</a> и&nbsp;поставили
            более <a href="javascript:void(0);">1&nbsp;860</a> оценок различным блюдам
        </div>

        <div class="about-reviews__hand-container">
            <div class="about-reviews__hand"></div>
        </div>

        <a href="javascript:void(0);" class="btn btn--white about-reviews__btn">
            <span class="btn__text">Оставить отзыв</span>
        </a>
    </div>

    <?= ViewHelper::vueTag('vue-reviews-slider', [
        'items' => REVIEWS,
        'btn' => ['name' => 'Все отзывы', 'url' => 'javascript:void(0);'],
        'useScrollClass' => true,
    ]) ?>
</section>
