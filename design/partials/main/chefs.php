<section class="page__section">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">наши повара</div>
        <div class="section-caption__text">&mdash;&nbsp;Наши герои! Рейтинг основан на&nbsp;отзывах клиентов</div>
    </div>

    <?= ViewHelper::vueTag('vue-chefs-slider', [
        'items' => CHEFS,
        'btn' => ['name' => 'Все повара', 'url' => 'javascript:void(0);'],
        'useScrollClass' => true,
    ]) ?>
</section>
