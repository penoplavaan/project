<? $sliderPlaceholder = view('placeholder.products-slider') ?>

<section class="main-catalog page__section">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">Омномном!</div>
        <div class="section-caption__text hidden-mobile">
            &mdash;&nbsp;Вы&nbsp;только посмотрите что у&nbsp;нас есть поесть
        </div>
        <? // на мобилках выводится текст из промика ?>
        <div class="section-caption__text visible-mobile">
            &mdash;&nbsp;мы&nbsp;объединяем домашних
            и&nbsp;профессиональных поваров
            с&nbsp;людьми, готовыми заказывать
            блюда&nbsp;напрямую
        </div>
    </div>

    <?= view('catalog.nav') ?>

    <? foreach (MAIN_CATALOG_PRODUCTS as $i => $section) {?>
        <div class="main-catalog__section">
            <div class="h1 main-catalog__title"><?= $section['name'] ?></div>

            <?= ViewHelper::vueTag('vue-products-slider', [
                'items' => $section['items'],
                'useScrollClass' => true,
            ], 'div', $sliderPlaceholder); ?>
        </div>

        <? if ($i === 0) { ?>
            <div class="main-catalog__section visible-mobile">
                <div class="h1 main-catalog__title">специальные предложения</div>
                <div class="main-catalog__slider">
                    <?= ViewHelper::vueTag('vue-promo-slider', [
                        'items' => MAIN_PROMO_PRODUCTS,
                    ]) ?>
                </div>
            </div>
        <? } ?>
    <? } ?>

    <div class="main-catalog__more">
        <a href="javascript:void(0);" class="btn btn--clear">
            <span class="btn__text">Показать еще</span>
        </a>
    </div>
</section>
