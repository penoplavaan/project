<section class="main-promo scroll-class js-scroll-class hidden-mobile">
    <div class="main-promo__grid">
        <div class="main-promo__caption">
            <div class="main-promo__label">Привет!</div>
            <div class="h0 main-promo__title">
                У НАС ЕСТЬ<br/>
                ЧТО ПОЕСТЬ!
            </div>
        </div>
        <div class="main-promo__text-wrap">
            <div class="main-promo__text">
                &mdash;&nbsp;мы&nbsp;объединяем домашних
                и&nbsp;профессиональных поваров
                с&nbsp;людьми, готовыми заказывать
                блюда&nbsp;напрямую
            </div>
            <a href="javascript:void(0);" class="btn main-promo__btn">
                <span class="btn__text">Выбрать блюда</span>
            </a>
            <div class="main-promo__cta">
                Вы&nbsp;повар?
                &mdash;&nbsp;<a href="javascript:void(0);" class="link link--hover-uline">давайте с&nbsp;нами!</a>
            </div>
            <div class="main-promo__cta-comment">
                Регистрация займет всего 3&nbsp;минуты
            </div>
        </div>
        <div class="main-promo__right-col main-promo__slider">
            <?= ViewHelper::vueTag('vue-promo-slider', [
                'items' => MAIN_PROMO_PRODUCTS,
                'useScrollClass' => true,
            ]) ?>
        </div>
    </div>
</section>
