<section class="join-section page__section">
    <div class="join-section__text-wrap">
        <div class="page__section-caption section-caption">
            <div class="h1 section-caption__h1">Присоединяйтесь!</div>
            <div class="section-caption__text">&mdash;&nbsp;Максимально просто и&nbsp;прозрачно, честное слово!</div>
        </div>
        <div class="text-content join-section__text">
            Вы&nbsp;повар? &mdash;&nbsp;регистируйтесь в&nbsp;сервисе, создавайте свои замечательные блюда, откликайтесь на&nbsp;заказы. Профит!
        </div>
        <div class="join-section__btn-wrap">
            <a href="javascript:void(0);" class="btn">
                <span class="btn__text">Зарегистрироваться</span>
            </a>
        </div>
    </div>
    <div class="join-section__picture-wrap">
        <div class="join-section__picture js-parallax">
            <div class="join-section__woman" data-depth="0.15"></div>
            <div class="join-section__logo" data-depth="0.3"></div>
        </div>
    </div>
</section>
