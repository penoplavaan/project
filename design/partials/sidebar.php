<div class="page__sidebar">
    <?= ViewHelper::vueTag('vue-scroll-up') ?>

    <a href="javascript:void(0);" class="btn btn--feedback">
        <span class="btn__icon"><?= ViewHelper::getSymbol('feedback', 'symbol') ?></span>
        <span class="btn__text">Обратная связь</span>
    </a>
</div>
