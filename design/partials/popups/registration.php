<?php
$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'     => 'email',
            'name'     => 'email',
            'required' => true,
            'label'    => 'Email',
            'value'    => null,
        ],
        [
            'type'     => 'tel',
            'name'     => 'phone',
            'required' => true,
            'label'    => 'Телефон',
            'value'    => null,
        ],
        [
            'type'     => 'password',
            'name'     => 'password',
            'required' => true,
            'label'    => 'Пароль',
            'value'    => null,
        ],
        [
            'type'     => 'password',
            'name'     => 'repeatPassword',
            'required' => true,
            'label'    => 'Повторите пароль',
            'value'    => null,
        ],
        [
            'type'     => 'submit',
            'name'     => 'submit',
            'label'    => 'Зарегистрироваться',
            'value'    => null,
        ],
    ],
);
$formData = [
    'fields' => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form registration-form',
    ],
    'privacy' => 'Нажимая на&nbsp;кнопку &laquo;Отправить&raquo;, я&nbsp;даю согласие на&nbsp;обработку моих персональных данных в&nbsp;соответствии с&nbsp;<a href="javascript:void(0)">политикой информационной безопасности</a>. Мы&nbsp;не&nbsp;используем данные и&nbsp;не&nbsp;присылаем рассылки',
]
?>

<a href="javascript:void(0)" class="btn vue-popup" data-type="registration"
   data-popup='<?= ViewHelper::jsonEncode(['formData' => $formData]) ?>'>
    <span class="btn__text">Регистрация</span>
</a>
