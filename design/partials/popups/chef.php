<?php
$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'      => 'text',
            'name'      => 'name',
            'maxlength' => 200,
            'required'  => true,
            'label'     => 'ФИО',
            'value'     => null,
        ],
        [
            'type'      => 'tel',
            'name'      => 'phone',
            'maxlength' => 200,
            'required'  => true,
            'label'     => 'Телефон',
            'value'     => null,
        ],
        [
            'type'      => 'textarea',
            'name'      => 'about',
            'maxlength' => 200,
            'required'  => true,
            'label'     => 'Какие блюда готовите?',
            'value'     => null,
        ],
        [
            'type'    => 'multi_checkbox',
            'name'    => 'categories',
            'value'   => null,
            'options' => [
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Кейтеринг',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Выпечка',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Фермерские продукты',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Супы',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Суши',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Горячее',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Торты',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Другое',
                ],
            ],
        ],
        [
            'type'      => 'text',
            'name'      => 'address',
            'maxlength' => 200,
            'label'     => 'Укажите адрес кухни',
            'value'     => null,
        ],
        [
            'type'        => 'file',
            'name'        => 'kitchenImages',
            'multiple'    => true,
            'placeholder' => 'Прикрепите фото кухни',
            'label'       => '',
            'value'       => null,
        ],
        [
            'type'        => 'select',
            'name'        => 'specialization',
            'required'    => true,
            'placeholder' => '',
            'label'       => 'Специализация',
            'value'       => null,
            'options'     => [
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Повар',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Кондитер',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Фермер',
                ],
                [
                    'id'    => rand(1, 1000),
                    'value' => 'Суши-мастер',
                ],
            ],
        ],
        [
            'type'      => 'text',
            'name'      => 'experience',
            'maxlength' => 200,
            'label'     => 'Сколько лет в деле?',
            'value'     => null,
        ],
        [
            'type'        => 'file',
            'name'        => 'passport',
            'placeholder' => 'Прикрепите фото лица с паспортом',
            'label'       => '',
            'value'       => null,
        ],
        [
            'type'        => 'file',
            'name'        => 'sanitaryBook',
            'multiple'    => true,
            'placeholder' => 'Прикрепите фото санитарной книжки',
            'label'       => '',
            'value'       => null,
        ],
        [
            'type'        => 'file',
            'name'        => 'certificates',
            'multiple'    => true,
            'placeholder' => 'Прикрепите фото сертификата или диплома',
            'label'       => '',
            'value'       => null,
        ],
        [
            'type'        => 'file',
            'name'        => 'avatar',
            'placeholder' => 'Рекомендуем форматы: jpeg, png, не более 6мб',
            'label'       => 'Загрузить другое фото',
            'value'       => null,
        ],
        [
            'type'  => 'submit',
            'name'  => 'submit',
            'label' => 'Отправить заявку',
            'value' => null,
        ],
    ],
);
$formData = [
    'fields'     => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form become-chef-form',
    ],
]
?>

<a href="javascript:void(0)" class="btn vue-popup" data-type="become-chef"
   data-popup='<?= ViewHelper::jsonEncode(['formData' => $formData]) ?>'>
    <span class="btn__text">Стать поваром</span>
</a>
