<?php
$locations = [
    [
        'id' => rand(1, 1000),
        'name' => 'Москва',
        'bold' => true,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Санкт-Петербург',
        'bold' => true,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Новосибирск',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Екатеринбург',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Казань',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Нижний Новгород',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Челябинск',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Омск',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Самара',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Ростов-на-Дону',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Уфа',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Красноярск',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Пермь',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Воронеж',
        'bold' => false,
    ],
    [
        'id' => rand(1, 1000),
        'name' => 'Волгоград',
        'bold' => false,
    ],
];
?>

<a href="javascript:void(0)" class="btn vue-popup" data-type="locations" data-popup='<?= ViewHelper::jsonEncode(['items' => $locations]) ?>'>
    <span class="btn__text">Выбор местоположения</span>
</a>
