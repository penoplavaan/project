<?php
$formFields = ViewHelper::prepareFormFields(
    [
        'name' => [
            'type'      => 'text',
            'name'      => 'name',
            'maxlength' => 200,
            'required'  => true,
            'label'     => 'Имя',
            'value'     => null,
        ],
        [
            'type'     => 'tel',
            'name'     => 'phone',
            'required' => true,
            'label'    => 'Телефон',
            'value'    => null,
        ],
        [
            'type'     => 'email',
            'name'     => 'email',
            'required' => true,
            'label'    => 'Email',
            'value'    => null,
        ],
        [
            'type'     => 'select',
            'name'     => 'theme',
            'required' => true,
            'label'    => 'Тема вопроса',
            'placeholder' => '',
            'value'    => null,
            'options'  => [
                [
                    'id' => rand(1, 1000),
                    'value' => 'Общие вопросы',
                ],
                [
                    'id' => rand(1, 1000),
                    'value' => 'Личный кабинет',
                ],
                [
                    'id' => rand(1, 1000),
                    'value' => 'Для поваров',
                ],
                [
                    'id' => rand(1, 1000),
                    'value' => 'Техподдержка',
                ],
            ],
        ],
        [
            'type'     => 'textarea',
            'name'     => 'question',
            'required' => true,
            'maxlength' => 1000,
            'label'    => 'Ваш вопрос',
            'value'    => null,
        ],
        [
            'type'     => 'submit',
            'name'     => 'submit',
            'label'    => 'Отправить',
            'value'    => null,
        ],
    ],
);
$formData = [
    'fields' => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form',
    ],
    'privacy' => 'Нажимая на&nbsp;кнопку &laquo;Отправить&raquo;, я&nbsp;даю согласие на&nbsp;обработку моих персональных данных в&nbsp;соответствии с&nbsp;<a href="javascript:void(0)">политикой информационной безопасности</a>. Мы&nbsp;не&nbsp;используем данные и&nbsp;не&nbsp;присылаем рассылки',
]
?>

<a href="javascript:void(0)" class="btn vue-popup" data-type="feedback"
   data-popup='<?= ViewHelper::jsonEncode(['formData' => $formData]) ?>'>
    <span class="btn__text">Обратная связь</span>
</a>
