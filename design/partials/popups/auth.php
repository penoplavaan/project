<?php
$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'     => 'email',
            'name'     => 'email',
            'required' => true,
            'label'    => 'Email',
            'value'    => null,
        ],
        [
            'type'     => 'password',
            'name'     => 'password',
            'required' => true,
            'label'    => 'Пароль',
            'value'    => null,
        ],
        [
            'type'  => 'submit',
            'name'  => 'submit',
            'label' => 'Войти',
            'value' => null,
        ],
    ],
);
$formData = [
    'fields'     => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form auth-form',
    ],
    'privacy'    => 'Нажимая на&nbsp;кнопку &laquo;Отправить&raquo;, я&nbsp;даю согласие на&nbsp;обработку моих персональных данных в&nbsp;соответствии с&nbsp;<a href="javascript:void(0)">политикой информационной безопасности</a>. Мы&nbsp;не&nbsp;используем данные и&nbsp;не&nbsp;присылаем рассылки',
];

$socials = [
    [
        'pictureTitle' => 'ВКонтакте',
        'pictureAlt'   => 'ВКонтакте',
        'picture'      => ['originalSrc' => ViewHelper::getPicture('auth-socials/1.svg')],
    ],
    [
        'pictureTitle' => 'Telegram',
        'pictureAlt'   => 'Telegram',
        'picture'      => ['originalSrc' => ViewHelper::getPicture('auth-socials/2.svg')],
    ],
    [
        'pictureTitle' => 'WhatsApp',
        'pictureAlt'   => 'WhatsApp',
        'picture'      => ['originalSrc' => ViewHelper::getPicture('auth-socials/3.svg')],
    ],
]
?>

<a href="javascript:void(0)" class="btn vue-popup" data-type="auth"
   data-popup='<?= ViewHelper::jsonEncode(['formData' => $formData, 'socials' => $socials]) ?>'>
    <span class="btn__text">Авторизация</span>
</a>
