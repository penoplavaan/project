<?php
$formFields = ViewHelper::prepareFormFields(
    [
        [
            'type'     => 'email',
            'name'     => 'email',
            'required' => true,
            'label'    => 'Email',
            'value'    => null,
        ],
        [
            'type'  => 'submit',
            'name'  => 'submit',
            'label' => 'Отправить',
            'value' => null,
        ],
    ],
);
$formData = [
    'fields'     => $formFields,
    'validation' => ViewHelper::getValidatorsForForm($formFields),
    'attributes' => [
        'class' => 'form remember-password-form',
    ],
];
?>

<a href="javascript:void(0)" class="btn vue-popup" data-type="remember-password"
   data-popup='<?= ViewHelper::jsonEncode(['formData' => $formData]) ?>'>
    <span class="btn__text">Напомнить пароль</span>
</a>
