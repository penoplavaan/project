<? /* дублируется в CatalogNav.vue из-за того, что пришлось использовать в компоненте */ ?>

<div class="catalog-nav">
    <div class="catalog-nav__scroll-wrap custom-scrollbar custom-scrollbar--hidden">
        <div class="catalog-nav__items">
            <? foreach (CATALOG_CATEGORIES as $category) { ?>
                <a href="javascript:void(0);" class="catalog-category catalog-nav__item">
                    <div class="catalog-category__name"><?= $category['name'] ?></div>
                    <div class="catalog-category__count"><?= $category['countText'] ?></div>
                </a>
            <? } ?>
        </div>
    </div>
</div>
