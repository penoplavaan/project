<?php
$pageClass = 'chef-list';
$pageType = 'chef-list';
$title = 'Список поваров';

$breadcrumbs = [
    'Главная',
    'Все повара',
];

require('partials/header.php'); ?>
<main class="page__content">
    <?= view('common.breadcrumbs', compact('breadcrumbs')) ?>
    <?= view('common.caption', ['caption' => 'Все повара', 'postfix' => '<sup>145</sup>']) ?>
    <?= view('chef-list.list') ?>
</main>
<?php require('partials/footer.php'); ?>
