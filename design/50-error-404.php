<?php
$pageClass = 'error-404';
$pageType = 'error-404';
$title = 'Ошибка 404';
$shortFooter = true;

require('partials/header.php'); ?>
<main class="page__content">
    <div class="content-container error-404 scroll-class js-scroll-class">
        <div class="error-404__micro-header">— Упс.. тут ошибочка</div>

        <div class="error-404__banner js-parallax">
            <div class="error-404__plate">
                <img class="error-404__svg-text" src="<?= ViewHelper::getImage('error-404.svg')?>" alt="">
            </div>
            <div class="error-404__image-wrap" data-depth="0.2">
                <img class="error-404__image" src="<?= ViewHelper::getPicture('error-404/cake.png') ?>" alt="">
            </div>
        </div>

        <div class="error-404__content">
            <h1 class="h0 error-404__header">ПИЧАЛЬКА...</h1>
            <div class="error-404__description text-content">
                Нет такой странички, зато есть куча вкусняшек для тебя ;)
            </div>
            <div class="error-404__out-button">
                <a href="javascript:void(0)" class="btn"><span class="btn__text">Каталог блюд</span></a>
            </div>
            <div class="error-404__go-main">
                Или давай на <a href="javascript:void(0)" class="link link--uline"><span class="link__text">главную</span></a>
            </div>
        </div>
    </div>
</main>
<?php require('partials/footer.php'); ?>
