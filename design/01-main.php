<?php
$pageClass = 'main';
$pageType = 'main';
$title = 'Главная';

require('partials/header.php'); ?>
<main class="page__content">
    <div class="content-container">
        <?= view('main.promo') ?>
        <?= view('main.catalog') ?>
        <?= view('main.reviews') ?>
        <?= view('main.chefs') ?>
        <?= view('main.join') ?>
    </div>
</main>
<?php require('partials/footer.php'); ?>
