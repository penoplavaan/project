import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import {TCook, TCookReference} from '~/types/cook/TCook';
import {TAjaxResponse} from '~/types/TAjaxResponse';
import {getJson} from '~/helper/ajax';

@Module({ namespaced: true })
class CooksStore extends VuexModule {

    public detailData: TCookReference = {};

    @Mutation
    public mSetData(payload: TCook[]): void {
        const newData = {...this.detailData};

        for (let i = 0; i < payload.length; i++) {
            newData[payload[i].id] = payload[i];
        }

        this.detailData = newData;
    }

    @Action
    public async loadData(cookId: number) {
        const response = await getJson<TAjaxResponse>('/cooks/pin/' + cookId);

        if (response.success) {
            this.context.commit('mSetData', response.data as TCook[]);
        }
    }

    @Action
    public async setCookData(cooks: TCook[]) {
        if (cooks && cooks.length) {
            this.context.commit('mSetData', cooks);
        }
    }
}

export default CooksStore;
