import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import {TBasketByCook} from '~/types/basket/TBasketByCook';
import {postJson} from '~/helper/ajax';
import route from '~/helper/route';
import {TAjaxResponse} from '~/types/TAjaxResponse';
import {TBasketInfo} from '~/types/basket/TBasketInfo';

@Module({ namespaced: true })
class BasketStore extends VuexModule {
    basketsByCook: TBasketByCook[] = [];

    count:number = APP.basket.count || 0;

    @Mutation
    updateItems(items: TBasketByCook[]) {
        this.basketsByCook = items;
    }

    @Mutation
    updateInfo(info: TBasketInfo) {
        this.count = info.count;
    }

    @Action
    async quantity({dishId, count}) {
        const url = route('basket.set-count');
        await this.context.dispatch('post', {url, data: { dishId, count }});
    }

    @Action
    async remove(cookId) {
        const url = route('basket.clear');
        await this.context.dispatch('post', {url, data: { cookId }});
    }

    @Action
    async removeDish(dishId) {
        const url = route('basket.remove');
        await this.context.dispatch('post', {url, data: { dishId }});
    }

    @Action
    async post({url, data}):Promise<TAjaxResponse> {
        const result = await postJson<TAjaxResponse>(url, data);

        if (result.data?.items) {
            this.context.commit('updateItems', result.data.items);
        }

        if (result.data?.info) {
            this.context.commit('updateInfo', result.data.info);
        }

        return result;
    }
}
export default BasketStore;
