import Vue from 'vue';
import Vuex from 'vuex';
import CooksStore from '~/store/modules/cooks/CooksStore';
import BasketStore from '~/store/modules/BasketStore';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        CooksStore,
        BasketStore,
    },
});
