import {TBackendImage} from '~/types/images';

type TChatProduct = {
    id: number,
    name: string,
    picture: TBackendImage,
    price: string,
    count: number,
    href: string,
}

export default TChatProduct;
