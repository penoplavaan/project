import {TMessage} from '~/types/chat/TMessage';
import TChatOrderProduct from '~/types/chat/TChatOrderProduct';

type TChat = {
    fromUserId: number;
    toUserId: number;
    lastReadId: number|null,
    messages: TMessage[];
    order: TChatOrderProduct[];
}

export default TChat;
