import TChatProduct from '~/types/chat/TChatProduct';
import {TBackendImage} from '~/types/images';

export type TMessage = {
    id: number,
    chatUserId: number,
    message: string,
    date: number, //timestamp
    userId: number,
    products?: TChatProduct[],
    images?: TBackendImage[],
}
