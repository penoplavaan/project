import TChatUser from '~/types/chat/TChatUser';

type TChatUserRef = {[key: number]: TChatUser}

export default TChatUserRef;
