import {TBackendImage} from '~/types/images';

type TChatUser = {
    id: number,
    avatar: TBackendImage,
    name: string,
    address?: string,
    url?: string,
}

export default TChatUser;
