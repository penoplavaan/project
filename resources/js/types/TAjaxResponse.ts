import TObjectCollection from '~/types/TObjectCollection';

export type TAjaxResponse = {
    success: boolean;
    data?: TObjectCollection;
    reload?: number;
    redirect?: string;
    message?: {
        title: string;
        text: string;
    } | string;
};
