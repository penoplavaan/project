import TFile from '~/types/TFile';

type TDocumentsGroup = {
    groupName: string,
    items: TFile[],
    code?: string,
    max?: number,
}

export default TDocumentsGroup;
