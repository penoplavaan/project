export type TPictureObj = {
    id: number,
    src: string,
}

export type TDish = {
    id: 0,
    category_id?: string | null,
    name?: string,
    preview_text?: string,
    storage_conditions?: string,
    quantity?: string,
    weight?: string,
    cooking_time?: string,
    package?: string,
    calories?: string,
    protein?: string,
    fats?: string,
    carbohydrates?: string,
    tags?: string[],
    price?: string,
    existingPictures?: TPictureObj[],
}

export default TDish;
