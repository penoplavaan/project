import {TLink} from '~/types/TLink';

export type TNavItem = TLink & {
    items?: TNavItem[],
    current?: boolean,
}
