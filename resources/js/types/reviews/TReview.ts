import {TBackendImage} from '~/types/images';

export type TReview = {
    name: string,
    position: string,
    previewText: string,
    picture: TBackendImage,
}
