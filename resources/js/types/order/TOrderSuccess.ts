
type TOrderSuccess = {
    id: number;
    cookId: number;
    payed: boolean;
    hasChat: boolean
}

export default TOrderSuccess;
