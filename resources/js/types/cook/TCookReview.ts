import {TBackendImage} from '~/types/images';

type TCookReview = {
    id: number,
    user: {
        name: string,
        avatar: TBackendImage,
    }
    date: string,
    rating: number,
    text: string,
}

export default TCookReview;
