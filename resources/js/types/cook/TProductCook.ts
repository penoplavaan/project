import {TBackendImage} from '~/types/images';

export type TProductCook = {
    name: string,
    url: string,
    distance?: number,
    picture: TBackendImage,
    rating: string,
    reviewsCount: number,
}

export type TPromoCook = TProductCook & {
    position: string,
}

