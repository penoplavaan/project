import {TBackendImage} from '~/types/images';

type TCookKitchen = {
    id: number,
    picture: TBackendImage,
}

export default TCookKitchen;
