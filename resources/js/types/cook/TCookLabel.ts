import {TBackendImage} from '~/types/images';

type TCookLabel = {
    color?: string,
    icon: TBackendImage,
    name: string,
}

export default TCookLabel;
