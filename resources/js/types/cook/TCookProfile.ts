import {TBackendImage} from '~/types/images';
import TCookLabel from '~/types/cook/TCookLabel';
import TUserData from '~/types/profile/TUserData';

type TCategory = {
    id: number,
    name: string,
    link: string
}

type TCookProfile = TUserData & {
    avatar: TBackendImage,
    name: string,
    speciality: string,
    specialityId: string,
    categories: TCategory[],
    categoriesIds: string[],
    labels: TCookLabel[],
    about: string,
    city_id: number,
    address: string,
    experience: string,
    experienceRaw: string,
    registerDate: string,
    rating: string,
    reviewsCount: number,
}

export default TCookProfile;
