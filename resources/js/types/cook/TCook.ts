import {TBackendImage} from '~/types/images';
import {TLink} from '~/types/TLink';
import TCookLabel from '~/types/cook/TCookLabel';

export type TCook = {
    id: number,
    name: string,
    url: string,
    labels: TCookLabel[],
    position: string,
    distance?: number,
    picture: TBackendImage,
    tags: TLink[],
    address: string,
    rating: string,
    reviewsCount: number,
    phone?: number,
}

export type TCookReference = {[key: number]: TCook};
