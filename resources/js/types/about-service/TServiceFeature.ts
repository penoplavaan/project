export type TServiceFeature = {
    name: string,
    text: string,
    image: string | false,
}
