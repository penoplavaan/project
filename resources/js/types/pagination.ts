export type TBackendPagination = {
    count: number; // Всего страниц
    size: number;  // Элементов на 1 странице
    current: number; // Текущая страница
    totalItemsCount: number; // Всего элементов
}
