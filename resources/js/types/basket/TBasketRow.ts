import {TBackendImage} from '~/types/images';

export type TBasketRow = {
    id: number,
    picture: TBackendImage,
    name: string,
    price: number,
    url: string,
    weight: string,
    count: number,
}

export type TBasketElement = {
    dishId: number,
    count: number,
}
