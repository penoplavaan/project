import {TBasketRow} from '~/types/basket/TBasketRow';
import {TBackendImage} from '~/types/images';

export type TBasketByCook = {
    cook: {
        id: number,
        name: string,
        speciality: string,
        picture: TBackendImage,
        url: string,
    }
    rows: TBasketRow[],
}
