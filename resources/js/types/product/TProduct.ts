import {TBackendImage} from '~/types/images';
import {TProductCook, TPromoCook} from '~/types/cook/TProductCook';
import {TProductLabel} from '~/types/product/TProductLabel';

export type TProduct = {
    id: number,
    name: string,
    price: number,
    url: string,
    weight?: string,
    quantity?: string,
    previewText?: string,
    picture: TBackendImage,
    cook?: TProductCook,
    labels: TProductLabel[],
}

export type TPromoProduct = {
    id: number,
    name: string,
    price: number,
    url: string,
    size?: string,
    previewText?: string,
    picture: TBackendImage,
    cook?: TPromoCook,
}

export type TSecondPromoProduct = {
    id: number,
    url: string,
    price: number,
    name: string,
    picture: TBackendImage,
    cook?: TPromoCook,
}
