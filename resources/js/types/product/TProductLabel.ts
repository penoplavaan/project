
export type TProductLabel = {
    id: number,
    name: string
    color: string
    background: string
}
