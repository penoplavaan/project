import TObjectCollection from '~/types/TObjectCollection';

type TMapMarker = {
    id: number,
    name: string,
    map: number[],
    options?: TObjectCollection,
}

export default TMapMarker;
