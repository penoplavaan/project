export type TWebpImage = {
    srcset?: string,
    src: string,
};

export type TImageSizes = {
    width: number;
    height: number;
};

export type TBackendImage = {
    src: string,
    original?: string,
    webp?: string,
    png?: string,
    jpg?: string,
    size?: TImageSizes,
    relativeUrl?: string,
}

export type TPicture = {
    picture: TBackendImage,
    pictureAlt?: string,
    pictureTitle: string,
}
