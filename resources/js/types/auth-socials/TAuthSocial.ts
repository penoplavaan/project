type TAuthSocial = {
    image: string,
    url: string
};

export default TAuthSocial;
