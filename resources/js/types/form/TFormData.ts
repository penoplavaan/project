import TFormAttributes from '~/types/form/TFormAttributes';
import TFormField from '~/types/form/TFormField';
import TObjectCollection from '~/types/TObjectCollection';
import TSetting from '~/types/TSetting';

export type TFormData = {
    attributes: TFormAttributes,
    fields: TObjectCollection<TFormField>,
    validation: TObjectCollection<TObjectCollection<any>>,
    validationType?: string,
    privacy: string,
}

export type TAllAuthFormData = {
    authCode?: TFormData;
    authPass?: TFormData;
    register?: TFormData;
    resetSend?: TFormData;
    reset?: TFormData;
    becomeCook?: TFormData;
    profileDetail?: TFormData;
    cookFormSettings: TSetting[];
}

export default TFormData;
