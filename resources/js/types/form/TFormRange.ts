export type TFormRange = {
    min: number,
    max: number,
    curMin: number,
    curMax: number,
    interval?: number,
}
