import TObjectCollection from '~/types/TObjectCollection';

type TFormField = TObjectCollection & {
    type: string,
    name: string,
    label?: string,
    required?: boolean,
    disabled?: boolean,
    multiple?: boolean,
    checked?: boolean,
    placeholder?: string,
    options?: any[] | {[key: string]: any},
    attrs?: any[] | {[key: string]: any},
    value?: any,
    pattern?: string,
}

export default TFormField;
