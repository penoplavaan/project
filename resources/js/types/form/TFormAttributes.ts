import TObjectCollection from '~/types/TObjectCollection';

type TFormAttributes = {
    name?: string,
    action?: string,
    method?: string,
} & TObjectCollection;

export default TFormAttributes;
