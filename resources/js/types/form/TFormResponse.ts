import TObjectCollection from '~/types/TObjectCollection';
import {TAjaxResponse} from '~/types/TAjaxResponse';

export type TFormResponse = TAjaxResponse & {
    errors?: TObjectCollection<TObjectCollection<string>>,
    message?: {
        title: string,
        text: string,
    },
};
