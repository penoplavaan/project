type TFile = {
    id: number,
    name: string,
    href: string,
    editable?: boolean,
}

export default TFile;
