import {TBackendImage} from '~/types/images';

export type TPromoCard = {
    id: number,
    picture: TBackendImage,
    name: string,
    link?: string,
    price?: string,
    previewText?: string,
    buttonText?: string,
}
