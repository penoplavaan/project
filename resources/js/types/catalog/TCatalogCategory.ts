export type TCatalogCategory = {
    id: number,
    url: string,
    title: string,
    total: string,
}
