import TFormData from '~/types/form/TFormData';

type TUserData = {
    avatar: string,
    fio: string,
    address: string,
    about: string,
    registerDate: string,
    phone: string,
    phoneVerified: boolean,
    email: string,
    emailVerified: boolean,
    verifyPhoneForm: TFormData | null,
}

export default TUserData;
