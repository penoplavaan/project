import TFormField from '~/types/form/TFormField';
import {TFormRange} from '~/types/form/TFormRange';

type TFilter = {
    type: 'checkbox' | 'radio' | 'range' | 'toggle',
    label?: string,
    name: string,
    collapsable: boolean,
    items?: TFormField[],
    range?: TFormRange,
}

export default TFilter;
