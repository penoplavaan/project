import {TBackendImage} from '~/types/images';

export type TSetting = {
    id: number,
    code: string,
    text?: string,
    big_text?: string,
    attachment: TBackendImage[]
}

export default TSetting;
