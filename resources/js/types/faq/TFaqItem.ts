export type TFaqItem = {
    title: string,
    text: string,
};
