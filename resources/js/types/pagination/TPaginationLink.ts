export type TPaginationLink = {
    url?: string;
    label: string;
    active: boolean;
}
