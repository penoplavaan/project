import {TPaginationLink} from '~/types/pagination/TPaginationLink';

export type TPaginationData = {
    first_page_url: string;
    last_page_url: string;
    prev_page_url: string;
    next_page_url: string;
    path: string;

    from: number; // record number (page #2, records from 2 to 4)
    to: number; // record number
    per_page: number; // records per page
    current_page: number;
    last_page: number;
    total: number; // records

    links: Array<TPaginationLink>;
}
