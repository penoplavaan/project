import Vue, {Component} from 'vue';
import store from '~/store';

function initVueComponent(vueEl: HTMLElement | null | undefined, component: Component): void {
    if (!vueEl) return;

    const props = JSON.parse(vueEl.dataset.vue || '{}');

    new Vue({
        store,
        el: vueEl,
        name: vueEl.classList.toString(), // чтобы хоть как то различать компоненты в vue devtools
        render: h => h(component, {props}),
    });
}

export default function initVue(vueEl: NodeList | Element | null | undefined, component: Component): void {
    if (NodeList.prototype.isPrototypeOf(vueEl as object)) {
        Array.prototype.forEach.call(vueEl, el => initVueComponent(el as HTMLElement, component));
    } else {
        initVueComponent(vueEl as HTMLElement, component);
    }
}
