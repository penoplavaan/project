import {BodyScrollOptions, clearAllBodyScrollLocks, disableBodyScroll} from 'body-scroll-lock';
import Detector from '~/helper/detector';

document.documentElement.style.setProperty('--scroll-lock-compensation', '0');

export function lockScroll(lock: boolean, targetElement, options?: BodyScrollOptions) {
    const sw = Detector.scrollbarWidth;

    if (lock) {
        disableBodyScroll(targetElement, options);
        //document.body.style.setProperty('top', -window.scrollY + 'px');
        document.documentElement.style.setProperty('--scroll-lock-compensation', `${sw}px`);
    } else {
        clearAllBodyScrollLocks();
        //document.body.style.removeProperty('top');
        document.documentElement.style.setProperty('--scroll-lock-compensation', '0');
    }
}
