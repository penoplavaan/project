export const setCookie = (name: string, value: string, days: number, path: string = '/'): void => {
    const date = new Date();

    date.setTime(date.getTime() + 1000 * 60 * 60 * 24 * days);
    document.cookie = name + '=' + value + ';path=' + path + ';expires=' + date.toUTCString();
};
