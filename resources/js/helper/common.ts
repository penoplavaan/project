import eventBus from '~/helper/event-bus';
import {getJson} from '~/helper/ajax';
import {TAjaxResponse} from '~/types/TAjaxResponse';

export function clearPhone(phone: string): string {
    let cleaned = (phone).replace(/\D/g, '');
    let len = cleaned.length;

    if (len == 10) return '+7' + cleaned;

    if (len == 11) return cleaned.replace(/^[78]/, '+7');

    return cleaned;
}

export function wordForm(num: number, word: string[]): string {
    const num100 = num % 100;
    const num10 = num % 10;

    if (
        (num100 > 10 && num100 < 20) // 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
        || (num >= 5) //5, 6, 7, 8, 9
        || (num === 0) // 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100
    ) {
        return word[2]; // штук
    } else if (num10 === 1) {
        return word[0]; // штука
    } else  {
        // 2, 3, 4
        return word[1]; // штуки
    }
}

export function getEntryVisibility(entry: IntersectionObserverEntry): { visible: boolean, below: boolean, above: boolean, showed: boolean } {
    const rectY = entry.boundingClientRect.y || 0;
    const rootY = entry.rootBounds ? entry.rootBounds.y : 0;

    const visible = entry.isIntersecting;
    const below = rectY > rootY;
    const above = rectY < rootY;
    const showed = visible || above;

    return {
        visible: visible,
        below: below,
        above: above,
        showed: showed,
    };
}

export function randString(length: number = 8): string {
    const chars = 'abdehkmnpswxzABDEFGHKMNPQRSTWXZ123456789';
    let str = '';

    for (let i = 0; i < length; i++) {
        let pos = Math.floor(Math.random() * chars.length);

        str += chars.substring(pos,pos+1);
    }

    return str;
}

export function scrollTo(target: Element|HTMLElement, offset: number = 0) {
    const header = document.querySelector('header');

    const headerHeight = document.querySelector('header')?.getBoundingClientRect().height;
    let headerCompensate = headerHeight ? headerHeight : 0;

    if (header) {
        const rawHeight = getComputedStyle(header).getPropertyValue('--header-fixed-height');
        headerCompensate = parseInt(rawHeight);
    }

    window?.scrollTo({
        top: target.getBoundingClientRect().top + window.scrollY - headerCompensate - offset,
        behavior: 'smooth',
    });
}

export function clearHTMLEntites(text: string): string {
    const tmpDiv = document.createElement('div');

    tmpDiv.innerHTML = text;

    return tmpDiv.textContent || tmpDiv.innerText || '';
}

export function showMessagePopup(title, message, callback?, buttonText?): void {
    eventBus.$emit('open-popup', {
        type: 'message',
        componentProps: {
            title,
            message,
            callback,
            buttonText,
        },
    });
}

export function showConfirmPopup({ title, message, callback, btnOk, btnCancel }): void {
    eventBus.$emit('open-popup', {
        type: 'confirm',
        componentProps: {
            title,
            message,
            callback,
            btnOk,
            btnCancel,
        },
    });
}

export async function getPopupData(url: string) {
    const response = await getJson<TAjaxResponse>(url);
    if (response && response.success) {
        return response
    }
}

export function getParam(name: string): string | null {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    return urlParams.get(name);
}

export function dateTimeFromTimestamp(timestamp: number): string {
    const date = new Date();

    date.setTime(timestamp * 1000);

    const dayMonth = new Intl.DateTimeFormat('ru-RU', {
        day: 'numeric',
        month: 'long',
    }).format(date);

    const year = date.getFullYear();

    const time = new Intl.DateTimeFormat('ru-RU', {
        hour: '2-digit',
        minute: '2-digit',
    }).format(date);

    return dayMonth + ' ' + year + ' в ' + time;
}

export const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
};
