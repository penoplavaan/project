// Чтобы не ругался на Vue.prototype.*, якобы они не используются
// noinspection JSUnusedGlobalSymbols

// import helper, {IHelper} from '~/helper/common';
import EventBus from '~/helper/event-bus';
import Vue from 'vue';

declare module 'vue/types/vue' {
    // eslint-disable-next-line no-shadow
    interface Vue {
        // $helper: IHelper
        $eventBus: Vue
    }
}

export const VueHelperPlugin = {
    install: (/*vue: typeof Vue, options: any*/) => {
        // Vue.prototype.$helper = helper;
        Vue.prototype.$eventBus = EventBus;
    },
};
