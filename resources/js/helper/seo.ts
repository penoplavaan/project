
export function sendYmGoal(goalName: string) {
    const metricaId = APP.ymId;
    console.log(`ym(${metricaId}, 'reachGoal', '${goalName}')`);

    if (typeof window['ym'] !== 'undefined') {
        window['ym'](metricaId, 'reachGoal', goalName);
    }
}
