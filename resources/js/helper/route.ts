/**
 * Построить роут
 */
function route(routeName: string, params = {}): string {
    if (!APP.routes || !APP.routes[routeName]) return '/';

    let url = APP.routes[routeName];

    for (let paramName in params) {
        if (!params.hasOwnProperty(paramName)) continue;

        const expr = new RegExp('(.*)(\{'+paramName+'[\?]{0,1}\})(.*)');

        url = url.replace(expr, '$1' + params[paramName] + '$3');
    }

    return url;
}

export default route;
