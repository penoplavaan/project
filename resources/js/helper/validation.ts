import {email, phone, required, requiredIfEmpty} from 'sx-vue-form-validator';
import TObjectCollection from '~/types/TObjectCollection';

type TConvertedValidation = {
    rule: Function,
    message?: string,
};

function isNumeric(value) {
    return /^\d+$/.test(value);
}

const stringLength = (min: number, max: number) => {
    return (val: string) => {
        let error: string = '';

        if (!val) {
            return true;
        }

        if (max && val.length > max) {
            error = `Максимальная длина строки - ${max}`;
        } else if (min && val.length < min) {
            error = `Минимальная длина строки - ${min}`;
        }

        return error ? error : true;
    };
};

const between = (min: number, max: number) => {
    return (val: string) => {
        let error: string = '';

        if (!isNumeric(val)) {
            return 'Введите число';
        }

        // Если нам пришло не число
        let parsed = parseInt(val);
        if (isNaN(parsed)) {
            return 'Введите целое число';
        }

        if (max && parsed > max) {
            error = `Максимальное значение - ${max}`;
        } else if (min && parsed < min) {
            error = `Минимальное значение - ${min}`;
        }

        return error ? error : true;
    };
};

const booleanRequired = (val: boolean) => {
    return val ? true : 'Заполните поле';
};

const validatorMap: TObjectCollection<(validationData: any) => TConvertedValidation> = {
    required: () => {
        return {
            rule: required,
        };
    },
    phone: () => {
        return {
            rule: phone,
        };
    },
    email: () => {
        return {
            rule: email,
        };
    },
    stringLength: (validationData: any) => {
        return {
            rule: stringLength(validationData.min, validationData.max),
        };
    },
    requiredIfEmpty: (validationData: any) => {
        return {
            rule: requiredIfEmpty(validationData.field),
            message: 'Заполните поле',
        };
    },
    booleanRequired: () => {
        return {
            rule: booleanRequired,
        };
    },
    between: (validationData: any) => {
        return {
            rule: between(validationData.min, validationData.max),
        };
    },
};

const laravelValidatorMap: TObjectCollection<(validationData: any) => TConvertedValidation> = {
    required: () => {
        return {
            rule: required,
        };
    },
    phone: () => {
        return {
            rule: phone,
        };
    },
    email: () => {
        return {
            rule: email,
        };
    },
    min: (minLength: string) => {
        return {
            rule: stringLength(parseInt(minLength), 0),
        };
    },
    max: (maxLength: string) => {
        return {
            rule: stringLength(0, parseInt(maxLength)),
        };
    },
};

export const convertValidator = (validatorName: string, validatorData: any): TConvertedValidation => {
    if (validatorMap.hasOwnProperty(validatorName)) {
        return validatorMap[validatorName](validatorData);
    }

    return {rule: () => true};
};

export const convertValidatorLaravel = (index: string, validatorData: any): TConvertedValidation => {
    const parts = validatorData.split(':');

    if (laravelValidatorMap.hasOwnProperty(parts[0])) {
        return laravelValidatorMap[parts[0]](parts[1]);
    }

    return {rule: () => true};
};
