export const BREAKPOINTS = {
    xs: 480,
    sm: 768,
    md: 1024,
    lg: 1240,
    xl: 1440,
};

export const SYMBOLS_SVG: string = '/build/symbols.svg';
export const P_IMAGES: string = '/images/';
