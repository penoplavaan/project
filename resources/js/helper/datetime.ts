import {format, parse, toDate} from 'date-format-parse';

// Для отправки формы на сервер - подготовить дату в этих форматах
export const DATE_FORMAT_FORM_INPUT = 'DD.MM.YYYY';

export function parseServerDate(date: Date | string | any): Date|null {
    if (typeof date === 'string') {
        return parse(date, DATE_FORMAT_FORM_INPUT) || new Date(date);
    } else if (date instanceof Date) {
        return date;
    } else if (date != null) {
        return new Date(date);
    } else {
        return null;
    }
}

export function prepareServerDate(date: Date | string | any): string | any {
    if (!(date instanceof Date) && typeof date !== 'string') return date;

    date = toDate(date);

    return format(date, DATE_FORMAT_FORM_INPUT);
}

export function formatDate(date: string | Date, withYear: boolean = true) {
    if (typeof date === 'string') date = new Date(date);

    let params: Intl.DateTimeFormatOptions = {
        day: 'numeric',
        month: 'long',
    };

    if (withYear) {
        params.year = 'numeric';
    }

    return date.toLocaleDateString('ru-RU', params);
}

export function formatDateToServer(date: string | Date) {
    if (typeof date === 'string') {
        const parsedDate = parseServerDate(date);

        if (parsedDate === null) {
            return (new Date).toISOString().split('T')[0];
        }

        date = parsedDate;
    }

    return date.toLocaleDateString('fr-CA', {year: 'numeric', month: '2-digit' ,day: '2-digit', });
}

export function resetTime(date: Date): Date {
    const clone = new Date(date);

    clone.setUTCHours(0, 0, 0, 0);

    return clone;
}

/**
 * Сравнение двух дат; часть со временем отбрасывается, если dateOnly = true
 *
 * @param {Date} a
 * @param {Date} b
 * @param {boolean} dateOnly
 * @return {number} int [-1, 0, 1]
 */
export function dateCompare(a: Date, b: Date, dateOnly: boolean = true): number {
    const cloneA = (dateOnly ? resetTime(a) : new Date(a)).valueOf();
    const cloneB = (dateOnly ? resetTime(b) : new Date(b)).valueOf();

    if (cloneA === cloneB) return 0;

    return cloneA < cloneB ? -1 :  1;
}

export function formatTime(date: string | Date) {
    if (typeof date === 'string') date = new Date(date);

    return date.toLocaleTimeString('ru-RU', {
        hour: 'numeric',
        minute: 'numeric',
    });
}
