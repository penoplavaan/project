// Декларации модулей добавлять в этот файл
// declare module 'inputmask';
declare module 'vue-slick-carousel';
declare module 'debounce';
declare module 'vue-slide-up-down';
declare module 'parallax-js';
declare module 'vue-dnd-zone';
declare module 'scrollbooster';
declare module 'vue2-datepicker';

declare const APP: {
    isAuth: boolean,
    isCook: boolean,
    ymId:   number,
    routes: {[key: string]: string},
    basket:  import('~/types/basket/TBasketInfo').TBasketInfo,
    yandexKey?: string,
    userData?: {
        id: number,
        name: string,
        avatar: string,
    },
    becomeCookForm?: import('~/types/form/TFormData').TFormData;
    authForms: import('~/types/form/TFormData').TAllAuthFormData;
    changePasswordForm?: import('~/types/form/TFormData').TFormData;
    settings: {[key: string]: import('~/types/TSetting').TSetting[]};
    chatParams?: {[key: string]: string};
};
