import BasePage from '~/page/base';
import InitParallax from '~/partials/InitParallax';
import initVue from '~/helper/init-vue';
import ServiceFeaturesSlider from '~/components/about-service/ServiceFeaturesSlider.vue';
import ServiceReviewsSlider from '~/components/about-service/ServiceReviewsSlider.vue';
import ServiceSupportItems from '~/components/about-service/ServiceSupportItems.vue';
import RegistrationForm from '~/components/forms/RegistrationForm.vue';
import FaqItems from '~/components/faq/FaqItems.vue';
import eventBus from '~/helper/event-bus';
import TFormData from '~/types/form/TFormData';
import {getPopupData} from '~/helper/common';

/**
 * Поварам
 */
class ForCooksPage extends BasePage {

    init(): void {
        super.init();

        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));

        initVue(this.el?.querySelectorAll('.vue-service-features-slider'), ServiceFeaturesSlider);
        initVue(this.el?.querySelectorAll('.vue-service-reviews-slider'), ServiceReviewsSlider);
        initVue(this.el?.querySelectorAll('.vue-service-support-items'), ServiceSupportItems);
        initVue(this.el?.querySelectorAll('.vue-registration-form'), RegistrationForm);
        initVue(this.el?.querySelectorAll('.vue-faq-items'), FaqItems);

        this.initRegisterButtons();
        this.initFeedbackButton();
    }

    initRegisterButtons() {
        const buttons = this.el?.querySelectorAll('.js-register-btn');
        let formType = 'registration';

        if (!buttons) return;

        if (APP.isAuth) {
            formType = 'become-cook';
        }

        if (APP.isCook) {
            // скрываем кнопки
            buttons.forEach(node =>  node.remove());
            this.el?.querySelector('.js-registration-block')?.remove();

        } else {
            buttons.forEach(node => {
                node.addEventListener('click', () => {
                    eventBus.$emit('open-popup', {
                        type: formType,
                        componentProps: {
                            isForCook: true,
                        },
                    });
                });
            });
        }
    }

    initFeedbackButton() {
        let popupData: TFormData | null = null;
        getPopupData('/popup/feedback/Задать вопрос').then(responce => {
            if (typeof responce !== 'undefined') {
                popupData = responce.data as TFormData;
            }
        });

        this.el?.querySelector('.js-ask-btn')?.addEventListener('click', () => {
            if (popupData) {
                eventBus.$emit('open-popup', {
                    type: 'feedback',
                    componentProps: {
                        formData: popupData,
                    },
                });
            }
        }, false);
    }
}

new ForCooksPage(document.querySelector('body'));
