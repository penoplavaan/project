import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProfileNavigation from '~/components/profile/ProfileNavigation.vue';
import InitParallax from '~/partials/InitParallax';

/**
 * Профиль пользователя - Полезные материалы
 */
class ProfileUsefulMaterialsPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-profile-tabs'), ProfileNavigation);

        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));
    }
}

new ProfileUsefulMaterialsPage(document.querySelector('body'));
