import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProductList from '~/components/product/ProductList.vue';

/**
 * Список блюд
 */
class ProductListPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-product-list'), ProductList);
    }
}

new ProductListPage(document.querySelector('body'));
