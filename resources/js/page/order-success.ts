import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import OrderSuccess from '~/components/order/OrderSuccess.vue';
import InitParallax from '~/partials/InitParallax';

/**
 * Успшниое оформление заказа
 */
class OrderSuccessPage extends BasePage {
    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-order-success'), OrderSuccess);

        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));
    }
}

new OrderSuccessPage(document.querySelector('body'));
