import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProfileCookProducts from '~/components/profile/ProfileCookProducts.vue';
import ProfileNavigation from '~/components/profile/ProfileNavigation.vue';

/**
 * Профиль пользователя - страница блюд
 */
class ProfileDishesPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-profile-tabs'), ProfileNavigation);
        initVue(this.el?.querySelectorAll('.vue-profile-dishes'), ProfileCookProducts);
    }
}

new ProfileDishesPage(document.querySelector('body'));
