import BasePage from '~/page/base';
import InitParallax from '~/partials/InitParallax';

/**
 * 404 страница
 */
class Error404Page extends BasePage {

    init(): void {
        super.init();

        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));
    }
}

new Error404Page(document.querySelector('body'));
