import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProfileForm from '~/components/profile/ProfileForm.vue';
import InitParallax from '~/partials/InitParallax';

/**
 * Профиль пользователя - главная страница
 */
class ProfilePage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-profile'), ProfileForm);
        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));
    }
}

new ProfilePage(document.querySelector('body'));
