import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import CookInfo from '~/components/cook/CookInfo.vue';
import DocumentsList from '~/components/documents/DocumentsList.vue';
import CookProducts from '~/components/cook/CookProducts.vue';
import CookKitchenSlider from '~/components/cook/CookKitchenSlider.vue';
import InitReviews from '~/partials/InitReviews';

/**
 * Детальная повара
 */
class CookDetailPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-cook-info'), CookInfo);
        initVue(this.el?.querySelectorAll('.vue-cook-products'), CookProducts);
        initVue(this.el?.querySelectorAll('.vue-cook-kitchen-slider'), CookKitchenSlider);
        initVue(this.el?.querySelectorAll('.vue-documents-list'), DocumentsList);

        new InitReviews(this.el);
    }
}

new CookDetailPage(document.querySelector('body'));
