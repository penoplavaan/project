import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import OrderForm from '~/components/forms/OrderForm.vue';

/**
 * Оформление заказа
 */
class OrderPage extends BasePage {
    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-order'), OrderForm);
    }
}

new OrderPage(document.querySelector('body'));
