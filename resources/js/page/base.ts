import initVue from '~/helper/init-vue';
import eventBus from '~/helper/event-bus';
import ResizeWatcher from '~/partials/ResizeWatcher';
import PopupController from '~/components/PopupController.vue';
import Header from '~/partials/Header';
import Footer from '~/partials/Footer';
import ScrollClassToggle from '~/partials/ScrollClassToggle';
import Sidebar from '~/components/Sidebar.vue';
import ConfirmLocation from '~/components/header/ConfirmLocation.vue';
import VTooltip from 'v-tooltip';
import Vue from 'vue';
import '~/directives';
import TextContent from '~/partials/TextContent';
import Detector from '~/helper/detector';
import {VueHelperPlugin} from '~/helper/vueHelperPlugin';
import CookiePanel from '~/components/CookiePanel.vue';
import ConfirmLocationMobile from '~/components/header/ConfirmLocationMobile.vue';

/**
 * Базовый контроллер страницы
 */
abstract class BasePage {
    readonly el: HTMLBodyElement | null = null;

    constructor(el: HTMLBodyElement | null) {
        if (el === null) {
            throw Error('Empty el parameter is not allowed');
        }

        Vue.use(VTooltip);

        this.el = el;
        this.init();
    }

    init(): void {
        Vue.use(VueHelperPlugin);

        new ResizeWatcher();

        // Init popups
        this.initPopupBtns();
        initVue(this.el?.querySelector('.vue-popup-controller'), PopupController);
        this.el?.querySelectorAll('.vue-confirm-location').forEach(el => initVue(el, ConfirmLocation));
        this.el?.querySelectorAll('.vue-confirm-location-mobile').forEach(el => initVue(el, ConfirmLocationMobile));

        // Init layout
        new Header(this.el?.querySelector('.js-header') as HTMLElement);
        new Footer(this.el?.querySelector('.js-footer') as HTMLElement);
        initVue(this.el?.querySelector('.vue-sidebar'), Sidebar);

        // Init scroll animation
        new ScrollClassToggle(this.el as HTMLElement);

        // Init text content helper
        this.el?.querySelectorAll('.text-content').forEach(el => new TextContent(el as HTMLElement));

        initVue(this.el?.querySelector('.vue-cookie-panel'), CookiePanel);
        document.documentElement.style.setProperty('--scrollbar-width', `${Detector.scrollbarWidth}px`);

        this.initAutoShowPopups();
    }

    /**
     * Вызов попапов.
     * Умеет находить кнопку вызова попапа даже если она отрендерилась во Vue
     */
    initPopupBtns() {
        window.addEventListener('click', (ev) => {
            const el = ev.target as HTMLElement;
            const vuePopup = el.closest('.vue-popup') as HTMLElement;

            if (vuePopup) {
                ev.preventDefault();

                let props = {};

                if (vuePopup.dataset.popup) {
                    props = JSON.parse(vuePopup.dataset.popup || '{}');
                }

                if (vuePopup.dataset.close) {
                    eventBus.$emit('close-all-popup');
                }

                eventBus.$emit('open-popup', {
                    type: vuePopup.dataset.type,
                    componentProps: props,
                });
            }
        });
    }

    initAutoShowPopups() {
        if (window.location.hash === '#become-cook' && APP.isAuth && !APP.isCook) {
            eventBus.$emit('open-popup', {
                type: 'become-cook',
                componentProps: {},
            });
        }
    }
}

export default BasePage;
