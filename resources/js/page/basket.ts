import BasePage from '~/page/base';
import BasketList from '~/components/basket/BasketList.vue';
import initVue from '~/helper/init-vue';

/**
 * Корзина
 */
class BasketPage extends BasePage {
    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-basket-list'), BasketList);
    }
}

new BasketPage(document.querySelector('body'));
