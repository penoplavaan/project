import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import SecondPromoSlider from '~/components/main/SecondPromoSlider.vue';
import ReviewsSlider from '~/components/reviews/ReviewsSlider.vue';
import CooksSlider from '~/components/cook/CooksSlider.vue';
import InitParallax from '~/partials/InitParallax';
import eventBus from '~/helper/event-bus';
import {getParam, getPopupData} from '~/helper/common';
import MainCompilations from '~/components/main/MainCompilations.vue';
import CategoryNav from '~/components/main/CatalogNav.vue';
import TFormData from '~/types/form/TFormData';
import MainPromoCompilations from '~/components/main/MainPromoCompilations.vue';

/**
 * Главная страница
 */
class MainPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-promo-slider'), SecondPromoSlider);
        initVue(this.el?.querySelectorAll('.vue-reviews-slider'), ReviewsSlider);
        initVue(this.el?.querySelectorAll('.vue-cooks-slider'), CooksSlider);
        initVue(this.el?.querySelectorAll('.vue-compilations'), MainCompilations);
        initVue(this.el?.querySelectorAll('.vue-promocards-compilations'), MainPromoCompilations);
        initVue(this.el?.querySelectorAll('.vue-nav'), CategoryNav);

        this.el?.querySelectorAll('.js-parallax').forEach((el) => new InitParallax(el as HTMLElement));

        this.el?.querySelector('.js-btn-become-cook-main')?.addEventListener('click', () => {
            eventBus.$emit('open-popup', {
                type: 'become-cook',
                componentProps: {},
            });
        }, false);

        this.el?.querySelector('.js-leave-review')?.addEventListener('click', () => {
            getPopupData("/popup/feedback/Оставить отзыв").then(responce => {
                if (typeof responce !== 'undefined') {
                    const popupData = responce.data as TFormData;

                    eventBus.$emit('open-popup', {
                        type: 'feedback',
                        componentProps: {
                            formData: popupData
                        },
                    });
                }
            });
        }, false);

        this.checkPopups();
    }

    checkPopups() {
        // todo Понять что из этого выкинуть после прихода Ивана
        const pathEmail = '?email-verified';
        const pathReset = '/reset-password/';
        const emailConfirmed = '?become-cook';

        if (window.location.search === pathEmail) {
            history.replaceState({}, '', '/');
            eventBus.$emit('open-popup', {
                type: 'auth',
                componentProps: {},
            });

        } else if (window.location.pathname.startsWith(pathReset)) {
            const token = window.location.pathname.substring(pathReset.length);
            const email = getParam('email');

            history.replaceState({}, '', '/');

            eventBus.$emit('open-popup', {
                type: 'reset-password',
                componentProps: {
                    token,
                    email,
                },
            });
        } else if (window.location.search === emailConfirmed) {
            history.replaceState({}, '', '/');
            eventBus.$emit('open-popup', {
                type: 'become-cook',
                componentProps: {},
            }, false);
        }
    }
}

new MainPage(document.querySelector('body'));
