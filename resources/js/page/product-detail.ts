import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProductPictureSlider from '~/components/product/ProductPictureSlider.vue';
import CountForm from '~/components/forms/CountForm.vue';
import CookCard from '~/components/cook/CookCard.vue';
import ProductsSlider from '~/components/product/ProductsSlider.vue';
import BuyBlock from '~/components/product/BuyBlock.vue';
import StaticSpoiler from '~/partials/StaticSpoiler';
import InitReviews from '~/partials/InitReviews';

/**
 * Детальная блюда
 */
class ProductDetailPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-product-picture-slider'), ProductPictureSlider);
        initVue(this.el?.querySelectorAll('.vue-count-form'), CountForm);
        initVue(this.el?.querySelectorAll('.vue-cook-card'), CookCard);
        initVue(this.el?.querySelectorAll('.vue-products-slider'), ProductsSlider);
        initVue(this.el?.querySelectorAll('.vue-buy-block'), BuyBlock);

        new InitReviews(this.el);

        this.el?.querySelectorAll('.js-static-spoiler').forEach(el => new StaticSpoiler(el as HTMLElement));
    }
}

new ProductDetailPage(document.querySelector('body'));
