import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import ProfileNavigation from '~/components/profile/ProfileNavigation.vue';
import ProfileChat from '~/components/profile/ProfileChat.vue';

/**
 * Профиль пользователя - Чаты
 */
class ProfileChatPage extends BasePage {

    init(): void {
        super.init();
        initVue(this.el?.querySelectorAll('.vue-profile-tabs'), ProfileNavigation);
        initVue(this.el?.querySelectorAll('.vue-profile-chat'), ProfileChat);
    }
}

new ProfileChatPage(document.querySelector('body'));
