import BasePage from '~/page/base';
import initVue from '~/helper/init-vue';
import CooksMap from '~/components/cook/CooksMap.vue';

/**
 * Список поваров
 */
class CookListPage extends BasePage {

    init(): void {
        super.init();

        initVue(this.el?.querySelectorAll('.vue-cook-list'), CooksMap);
    }
}

new CookListPage(document.querySelector('body'));
