import Vue from 'vue';

type TOptions = {
    once?: boolean,
    in?: boolean,
}

type TRegisterElement = {
    callback: Function,
    options: TOptions,
}

const map = new WeakMap<HTMLElement, TRegisterElement>();

const io = globalThis.IntersectionObserver !== undefined ? new IntersectionObserver((entries) => {
    entries.forEach(entry => {
        const item = map.get(entry.target as HTMLElement) as TRegisterElement;

        if (!item) {
            return;
        }

        if (!item.options.in || (item.options.in && entry.isIntersecting)) {
            item.callback(entry);
        }

        if (item.options.once && entry.isIntersecting) {
            io?.unobserve(entry.target);
            map.delete(entry.target as HTMLElement);
        }
    });
}) : undefined;

function registerElement(el: HTMLElement, callback: Function, options: TOptions) {
    if (!io) return;

    map.set(el, {
        callback,
        options,
    });
    io.observe(el);
}

function unregisterElement(el: HTMLElement) {
    if (!io) return;

    io.unobserve(el);
    map.delete(el);
}

Vue.directive('intersect', {
    bind: (el: HTMLElement, binding) => {
        let options: TOptions = {};

        if (Object.keys(binding.modifiers).length) {
            options = {
                once: binding.modifiers.hasOwnProperty('once'),
                in: binding.modifiers.hasOwnProperty('in'),
            }
        }

        registerElement(el, binding.value as Function, options);
    },
    unbind: (el: HTMLElement) => {
        unregisterElement(el);
    },
});
