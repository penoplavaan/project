import initVue from '~/helper/init-vue';
import Socials from '~/components/Socials.vue';
import Developer from '~/partials/Developer';

class Footer {
    el: HTMLElement;

    constructor(el: HTMLElement) {
        this.el = el;

        this.el?.querySelectorAll('.js-developer').forEach((node) => new Developer(node as HTMLElement));

        this.init();
    }

    init() {
        initVue(this.el?.querySelectorAll('.vue-socials'), Socials);
    }
}

export default Footer;
