import {getRandomInt} from '~/helper/common';

class Developer {
    el: HTMLElement;
    gifEl: HTMLImageElement;

    constructor(el: HTMLElement) {
        this.el = el;
        this.gifEl = this.el.querySelector('.js-dev-gif') as HTMLImageElement;

        this.el.addEventListener('mouseenter', this.onMouseEnter.bind(this));
    }

    onMouseEnter() {
        if (this.el.classList.contains('developer--animate')) return;

        this.el.classList.add('developer--animate');
        let oldSrc = '';

        if (this.gifEl) {
            oldSrc = this.gifEl.src;
            this.gifEl.src = this.gifEl.src + '?' + getRandomInt(1, 99999);
        }

        setTimeout(() => {
            if (this.gifEl) {
                this.gifEl.src = oldSrc;
            }

            this.el.classList.remove('developer--animate');
        }, 6000);
    }
}

export default Developer;
