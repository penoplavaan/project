import {IClassConstructor, layout} from 'yandex-maps';

/**
 * Фабрика меток для yandex карты
 */
export default class PlacemarkLayoutFactory {
    protected layout!: IClassConstructor<layout.templateBased.Base>;

    protected getTemplate() {
        return `
               <svg class="map-placemark" width="31" height="35" viewBox="0 0 31 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M15.4773 32.9903C17.7675 31.8577 29.158 25.6911 29.158 15.079C29.158 7.30339 22.8546 1 15.079 1C7.30339 1 1 7.30339 1 15.079C1 25.6911 12.3906 31.8577 14.6808 32.9903C14.9361 33.1166 15.2219 33.1166 15.4773 32.9903ZM15.0807 21.1128C18.4131 21.1128 21.1146 18.4114 21.1146 15.079C21.1146 11.7466 18.4131 9.0451 15.0807 9.0451C11.7483 9.0451 9.04688 11.7466 9.04688 15.079C9.04688 18.4114 11.7483 21.1128 15.0807 21.1128Z" fill="#06C160"/>
                    <path d="M15.4773 32.9903L15.9206 33.8867H15.9206L15.4773 32.9903ZM14.6808 32.9903L14.2375 33.8867H14.2375L14.6808 32.9903ZM28.158 15.079C28.158 20.0039 25.517 23.9552 22.4838 26.8682C19.4545 29.7775 16.1361 31.5489 15.034 32.0939L15.9206 33.8867C17.1087 33.2991 20.6306 31.4209 23.8691 28.3107C27.1038 25.2043 30.158 20.7662 30.158 15.079H28.158ZM15.079 2C22.3024 2 28.158 7.85567 28.158 15.079H30.158C30.158 6.7511 23.4069 0 15.079 0V2ZM2 15.079C2 7.85567 7.85567 2 15.079 2V0C6.7511 0 0 6.7511 0 15.079H2ZM15.1241 32.0939C14.022 31.5489 10.7035 29.7775 7.67423 26.8682C4.64104 23.9552 2 20.0039 2 15.079H0C0 20.7662 3.05424 25.2043 6.28888 28.3107C9.52741 31.4209 13.0494 33.2991 14.2375 33.8867L15.1241 32.0939ZM15.034 32.0939C15.0407 32.0906 15.0568 32.085 15.079 32.085C15.1012 32.085 15.1173 32.0906 15.1241 32.0939L14.2375 33.8867C14.7722 34.1511 15.3858 34.1511 15.9206 33.8867L15.034 32.0939ZM20.1146 15.079C20.1146 17.8591 17.8609 20.1128 15.0807 20.1128V22.1128C18.9654 22.1128 22.1146 18.9637 22.1146 15.079H20.1146ZM15.0807 10.0451C17.8609 10.0451 20.1146 12.2988 20.1146 15.079H22.1146C22.1146 11.1943 18.9654 8.0451 15.0807 8.0451V10.0451ZM10.0469 15.079C10.0469 12.2988 12.3006 10.0451 15.0807 10.0451V8.0451C11.196 8.0451 8.04688 11.1943 8.04688 15.079H10.0469ZM15.0807 20.1128C12.3006 20.1128 10.0469 17.8591 10.0469 15.079H8.04688C8.04688 18.9637 11.196 22.1128 15.0807 22.1128V20.1128Z" fill="#06C160"/>
                </svg>
            `.trim();
    }

    protected getOverrides() {
        const self = this;

        return {
            build: function() {
                return self.build.bind(this, self).apply(this, Array.prototype.slice.call(arguments) as []);
            },
        };
    }

    protected build(self) {
        self.layout.superclass.build.call(this);

        const me: ymaps.layout.templateBased.Base|any = this;

        if (typeof me.getData !== 'function') return;

        let element = me.getElement() as HTMLElement;

        if (!element) return;

        const data: any = me.getData();

        if (!data || !data.geoObject) return;

        const isActive: boolean = !!data.geoObject.options.get('active');
        const [width, height] = [31, 35];
        const [dx, dy] = [-15, -18];
        const shape = {
            type: 'Rectangle',
            coordinates: [
                [dx, dy],
                [width + dx, height + dy],
            ],
        };

        if (data.geoObject.setBounds) {
            data.geoObject.setBounds(shape.coordinates);
        }

        data.options.set('shape', shape);
        data.geoObject.options.set('hideIconOnBalloonOpen', false);
        element.style.position = 'absolute';
        element.style.top = dy + 'px';
        element.style.left = dx + 'px';
        element.classList.toggle('map-placemark--active', isActive);

        data.geoObject.options.events.add('change', () => {
            const active: boolean = !!data.geoObject.options.get('active');

            element?.classList.toggle('map-placemark--active', active);
        });
    }

    create(ymaps) {
        this.layout = this.layout || ymaps.templateLayoutFactory.createClass(this.getTemplate(), this.getOverrides());

        return this.layout;
    }
}
