// @ts-ignore
import {IClassConstructor, layout} from 'yandex-maps';


/**
 * Фабрика baloon'ов для yandex карты
 */
export default class BalloonLayoutFactory {
    layout!: IClassConstructor<layout.templateBased.Base>;
    arrowSize: number = 0;

    private getTemplate() {
        return `<div class="map-balloon">
                    $[[options.contentLayout]]
                    <button class="map-balloon__close cross-btn js-balloon-close"></button>
                </div>`;
    }

    private getOverrides() {
        const self = this;

        return {
            build: function() {
                return self.build.bind(this, self).apply(this, Array.prototype.slice.call(arguments) as []);
            },
            getShape: function() {
                return self.getShape.bind(this, self).apply(this, Array.prototype.slice.call(arguments) as []);
            },
        };
    }

    private build(self) {
        self.layout.superclass.build.call(this);

        // ymaps.layout.templateBased.Base
        const me: any = this;

        if (typeof me.getData !== 'function') return;

        let element = me.getElement() as HTMLElement;

        if (!element) return;

        let balloon = element.querySelector('.map-balloon') as HTMLElement;

        element.addEventListener('click', (ev) => {
            if ((ev.target as HTMLElement).closest('.js-balloon-close')) {
                me.events.fire('userclose');
            }
        });

        self.element = element;
        self.balloon = balloon;
    }

    getShape(self) {
        // @ts-ignore
        const {ymaps} = window;

        if (!ymaps || !self.balloon) {
            return self.layout.superclass.getShape.call(this);
        }

        let position = {
            top: self.element.offsetTop,
            left: self.element.offsetLeft,
        };

        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
            [
                position.left + self.balloon.offsetWidth / 2,
                position.top - self.balloon.offsetHeight,
            ],
            [
                position.left - self.balloon.offsetWidth / 2,
                position.top + self.balloon.offsetHeight,
            ],
        ]));
    }

    create(ymaps) {
        this.layout = this.layout || ymaps.templateLayoutFactory.createClass(this.getTemplate(), this.getOverrides());

        return this.layout;
    }
}
