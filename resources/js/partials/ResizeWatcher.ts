import {debounce} from 'debounce';
import Detector from '~/helper/detector';
import eventBus from '~/helper/event-bus';

/** ResizeWatcher следит за ресайзом ОКНА
 * устанавливает значения, зависящих от размера viewport'a:
 * - CSS переменные
 * - вызывает событие 'resize-watcher', которое слушают компоненты (чтобы не вызывать в каждом компоненте вычисления в Detector)
 * отличается от initResizeObserver тем, что ResizeWatcher не нужно дёргать при каждом изменении размера документа (инит компонента, переключение таба и т.д.) + initResizeObserver не учитывает изменение vh при сворачивании / разворачивании браузерной панельки на мобилках
 **/

class ResizeWatcher {
    constructor() {
        const debouncedCallback = debounce(this.onResize, 100);

        window.addEventListener('resize', debouncedCallback);
        window.addEventListener('load', debouncedCallback);
    }

    onResize() {
        let vh = window.innerHeight * 0.01;

        document.documentElement.style.setProperty('--vh', `${vh}px`); // fix vh криво работают в CSS на мобилках

        eventBus.$emit('resize-watcher', {
            isXs: Detector.isXs,
            isSm: Detector.isSm,
            isMd: Detector.isMd,
            isLg: Detector.isLg,
            isXl: Detector.isXl,
        });
    }
}

export default ResizeWatcher;
