class StaticSpoiler {
    el: HTMLElement;
    content: HTMLElement;
    inner: HTMLElement;
    btn: HTMLElement;
    private isOpen: boolean = false;
    hideText:string;
    showText:string;

    constructor(el: HTMLElement) {
        this.el = el;
        this.content = el.querySelector('.js-static-spoiler-content') as HTMLElement;
        this.btn = el.querySelector('.js-static-spoiler-btn') as HTMLElement;
        this.inner = el.querySelector('.js-static-spoiler-inner') as HTMLElement;
        this.hideText = this.el.dataset.hide as string;
        this.showText = this.el.dataset.show as string;

        this.init();
    }

    init() {
        this.btn.addEventListener('click', this.toggle.bind(this));
    }

    toggle() {
        if (this.isOpen) {
            this.close();
        }  else  {
            this.open();
        }
    }

    open() {
        this.isOpen = true;

        this.content.style.height = this.inner.clientHeight + 'px';
        this.btn.innerText = this.hideText;

    }

    close() {
        this.isOpen = false;
        this.content.style.height = '';
        this.btn.innerText = this.showText;
    }
}

export default StaticSpoiler;
