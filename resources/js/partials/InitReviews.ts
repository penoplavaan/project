import initVue from '~/helper/init-vue';
import CookReviews from '~/components/cook/CookReviews.vue';
import eventBus from '~/helper/event-bus';

class InitReviews {
    el: HTMLBodyElement | null;

    constructor(el: HTMLBodyElement|null) {
        this.el = el;

        this.init();
    }

    init() {
        initVue(this.el?.querySelectorAll('.vue-cook-reviews'), CookReviews);

        const btn = this.el!.querySelector('.js-open-review-form') as HTMLButtonElement;
        if (btn) {
            btn.addEventListener('click', () => {
                eventBus.$emit('open-popup', {
                    type: 'cook-review',
                    componentProps: {
                        cookId: parseInt(btn.dataset.cookId || '0', 10),
                    },
                });
            });
        }
    }
}

export default InitReviews;
