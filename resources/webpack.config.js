const path = require('path');
const fs   = require('fs');

module.exports = (isProd, isAnalyze, isMarkup) => {
    const webpack         = require('webpack');
    const vueLoaderPlugin = require('vue-loader/lib/plugin');
    let analyzePlugin = [];

    if (isAnalyze) {
        const bundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
        analyzePlugin = [new bundleAnalyzerPlugin({})];
    }

    let moduleConf = {
        context: __dirname + '/js/page',
        entry: {},
        output: {
            path:          path.join(__dirname, '../public/build/js'),
            publicPath:    '/build/js/',
            filename:      '[name].bundle.js?[chunkhash]',
            chunkFilename: '[name].chunk.js?[chunkhash]',
            pathinfo:      false,
        },

        watch: !isProd,
        mode: isProd ? 'production' : 'development',
        devtool: false,

        cache: {
            type: 'filesystem',
        },

        stats: {
            colors: true,
        },

        module: {
            unknownContextCritical: false,
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                },
                {
                    test: /\.js|\.ts$/,
                    loader: 'esbuild-loader',
                    options: {
                        loader: 'ts',
                        minify: isProd,
                        legalComments: 'none',
                        target: 'es2015',
                    },
                },
                {
                    test: /\.less$/,
                    use: [
                        'vue-style-loader',
                        'css-loader',
                        'less-loader',
                    ],
                },
            ],
        },

        optimization: {
            runtimeChunk: false,
            splitChunks: {
                cacheGroups: {
                    default: {
                        name: 'commons',
                        filename: '[name].chunk.js?[chunkhash]',
                        chunks: 'initial',
                        minChunks: 2,
                        priority: -20,
                        reuseExistingChunk: true,
                    },
                },
            },
        },

        performance: {
            hints: false,
            maxEntrypointSize: 1024 * 1024,
            maxAssetSize: 1024 * 1024,
        },

        resolve: {
            extensions: ['.ts', '.vue', '.js'],
            alias: {
                '~': path.resolve(__dirname) + '/js',
                '@': path.resolve(__dirname) + '/js',
            },
        },

        plugins: [
            ...analyzePlugin,

            new webpack.NoEmitOnErrorsPlugin(),

            new vueLoaderPlugin(),
        ],
    };

    /**
     * add entry
     */

    fs.readdirSync(__dirname + '/js/page').map(file => {
        let partFile = file.split('.')[0];

        if (partFile !== 'base') {
            moduleConf.entry[partFile] = `./${partFile}`;
        }
    });

    return moduleConf;
};
