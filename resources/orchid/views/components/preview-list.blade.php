@php
/** @var array $attachments */
if (!count($attachments)) return '';
@endphp

<div class="preview-list">
    @foreach($attachments as $attachment)
        @include('orchid::components.preview', ['attachment' => $attachment])
    @endforeach
</div>
