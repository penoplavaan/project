@php
/** @var \App\Models\Attachment $attachment */
/** @var string $src */

$attachment = $attachment ?? null;
$src ??= '';

if ($attachment) {
    if (!$attachment->isImage()) return '';

    $src = $src ?: $attachment->getRelativeUrlAttribute();
}

if (!$src) return '';

@endphp

<div class="preview-item">
    <img src="{{ $src }}" alt="" style="max-width: 100px; max-height: 100px">
</div>
