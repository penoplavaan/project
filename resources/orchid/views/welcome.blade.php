<div class="bg-white rounded-top shadow-sm mb-3">
    <div class="row g-0">
        <div class="col col-lg-7 mt-6 p-4">
            <h2 class="mt-2 text-dark fw-light">Административная панель сайта Повар-на-связи</h2>
        </div>
        <div class="d-none d-lg-block col align-self-center text-end text-muted p-4">
            <img src="/images/favicons/android-chrome-192x192.png" width="96" height="96" alt="" />
        </div>
    </div>

    <div class="row m-0 p-md-4 p-3 border-top rounded-bottom">
        <div class="col-md-12 my-2">
            <h3>Статистика:</h3>
        </div>

        <div class="col-md-4 my-2">
            <p>Количество пользователей:</p>
            <ul>
                <li>Всего: <b>{{ $count['users']['total'] }}</b></li>
                <li>С подтверждённым email: <b>{{ $count['users']['email'] }}</b></li>
                <li>С подтверждённым телефоном: <b>{{ $count['users']['phone'] }}</b></li>
            </ul>
        </div>

        <div class="col-md-4 my-2">
            <p>Количество поваров:</p>
            <ul>
                <li>Всего заявок: <b>{{ $count['cooks']['total'] }}</b></li>
                <li>Верифицированных: <b>{{ $count['cooks']['verified'] }}</b></li>
            </ul>
        </div>

        <div class="col-md-4 my-2">
            <p>Количество блюд у поваров:</p>
            <ul>
                <li>Всего: <b>{{ $count['dishes']['total'] }}</b></li>
            </ul>
        </div>

        <hr>

        <div class="col-md-12 my-2">
            <h3>Заказы:</h3>
        </div>

        <div class="col-md-3">
            <p>За сутки:</p>
            <ul>
                <li>Количество: <b>{{ $count['orders']['day']['count'] }}</b></li>
                <li>На сумму: <b>{{ \App\Helpers\ViewHelper::price($count['orders']['day']['sum']) }} ₽</b></li>
            </ul>
        </div>
        <div class="col-md-3">
            <p>За неделю:</p>
            <ul>
                <li>Количество: <b>{{ $count['orders']['week']['count'] }}</b></li>
                <li>На сумму: <b>{{ \App\Helpers\ViewHelper::price($count['orders']['week']['sum']) }} ₽</b></li>
            </ul>
        </div>
        <div class="col-md-3">
            <p>За месяц:</p>
            <ul>
                <li>Количество: <b>{{ $count['orders']['month']['count'] }}</b></li>
                <li>На сумму: <b>{{ \App\Helpers\ViewHelper::price($count['orders']['month']['sum']) }} ₽</b></li>
            </ul>
        </div>
        <div class="col-md-3">
            <p>Всего:</p>
            <ul>
                <li>Количество: <b>{{ $count['orders']['all']['count'] }}</b></li>
                <li>На сумму: <b>{{ \App\Helpers\ViewHelper::price($count['orders']['all']['sum']) }} ₽</b></li>
            </ul>
        </div>
    </div>

</div>

