@php
    /** @var \App\Models\User $user */
@endphp
@if (isset($user))
    <div class="user-block">
        <div class="user-block__avatar">
            <img src="{!! $user->getAvatar() ? $user->getAvatar()->resize('admin.preview')->getUrl() : \App\Helpers\ViewHelper::getEmptyAvatar($user->isCook()) !!}" alt="">
        </div>
        <h3 class="user-block__title">Пользователь: {{ $user->getFullName() }}</h3>
        <h4>{{ $user->email }}</h4>
        [<a href="{!! route('platform.systems.users.edit', ['user' => $user->id]) !!}">Редактировать</a>]<br>
        [<a href="{!! route('platform.resource.list', ['resource' => 'dish-resources']) !!}?filter[cook_id]={{ $user->cook->id }}">Перейти к списку блюд</a>]<br>
    </div>
@endif
