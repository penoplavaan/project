@component($typeForm, get_defined_vars())
    <textarea {!! $attributes !!} data-controller="tinymce">
        @php
        // оригинальное значение без преобразований лежит именно в этом массива
        // в $value html сущности преобразованы
        @endphp
        {!! $attributes['value'] !!}
    </textarea>
@endcomponent
