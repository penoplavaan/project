@php
    /** @var \App\Models\Cook $cook */
@endphp
@if (isset($cook))
    <div class="user-block">
        <h3 class="user-block__title">Повар</h3>
        <h4>{{ $cook->user->getFullName() }}</h4>
        [<a href="{!! route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $cook->id]) !!}">Редактировать</a>]
    </div>
@endif
