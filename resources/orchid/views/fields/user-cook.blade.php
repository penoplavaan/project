@php
    /** @var \App\Models\Cook $cook */
@endphp
@if (isset($cook))
    <div class="user-block">
        <a href="{!! route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $cook->id]) !!}">[Перейти к профилю повара]</a>
    </div>
@else
    <div class="user-block">
        Пользователь не подавал заявку на становление поваром
    </div>
@endif

