@php
    /** @var \App\Models\Order $order */
@endphp
@if (isset($order))
    <div class="order-block">
        <h6>Заказ №{{ $order->id }}</h6>
        <p>
            Дата создания заказа: {{ $order->created_at }}
            <br>
            Заказ оплачен: <b>{{ $order->payed ? 'ДА' : 'нет' }}</b>
            <br>
            Покупатель:
            @if ($order->user_id)
                <a class="order-block__link" target="_blank" href="{{ route('platform.systems.users.edit', ['user' => $order->user_id]) }}">{{ $order->user->getFullName() }} [{{ $order->user_id }}]</a>
            @else
                <i>Неавторизованный</i>
            @endif
            <br>
            Повар:
            <a class="order-block__link" target="_blank" href="{{ route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $order->cook_id]) }}">{{ $order->cook?->user?->getFullName() }} [{{ $order->cook_id }}]</a>
            {{--
            {{ $order->cook?->user?->getFullName() }}
            [<a class="order-block__link" target="_blank" href="{{ route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $order->cook_id]) }}">Повар: {{ $order->cook_id }}</a>]
            [<a class="order-block__link" target="_blank" href="{{ route('platform.systems.users.edit', ['user' => $order->cook->user_id]) }}">Пользователь: {{ $order->cook->user_id }}</a>]
            --}}
            <br>
            Адрес повара: {{ $order->cook->address }}
        </p>

        <hr>
        <h6>Список блюд: </h6>

        <table class="order-block__table">
            <tr>
                <th>#</th>
                <th>Блюдо</th>
                <th>Цена за 1шт</th>
                <th>Количество</th>
                <th>Сумма</th>
            </tr>
            @foreach ($order->orderToDishes as $ind => $pivot)
                <tr>
                    <td>{{ $ind+1 }}</td>
                    @if ($pivot->dish)
                        <td><a class="order-block__link" target="_blank" href="{{ route('platform.resource.edit', ['resource' => 'dish-resources', 'id' => $pivot->dish->id]) }}">{{ $pivot->name }}</a></td>
                    @else
                        <td>{{ $pivot->name }}</td>
                    @endif
                    <td style="text-align: right">{{ \App\Helpers\ViewHelper::price($pivot->price_one) }} ₽</td>
                    <td>{{ $pivot->count }}</td>
                    <td style="text-align: right">{{ \App\Helpers\ViewHelper::price($pivot->count * $pivot->price_one) }} ₽</td>
                </tr>
            @endforeach
            <tr>
                <th colspan="4" style="text-align: right">Общая сумма</th>
                <th style="text-align: right">{{ \App\Helpers\ViewHelper::price($order->sum) }} ₽</th>
            </tr>
        </table>
    </div>
@endif
