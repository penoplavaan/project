<div class="row">
    <div class="col-xl-4 col-md-12">
        {!! $columnLeft !!}
    </div>
    <div class="col-xl-8 col-md-12">
        {!! $columnRight !!}
    </div>
</div>
