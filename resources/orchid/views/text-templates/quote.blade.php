<blockquote>
    Для заказчиков&nbsp;&mdash; это возможность быстро найти изготовителя для своего задания и&nbsp;сэкономить, заказывая напрямую.
    Для&nbsp;изготовителей&nbsp;&mdash; источник дополнительного заработка благодаря своему хобби, а&nbsp;также отличная возможность наработать базу клиентов и&nbsp;сделать это основным источником дохода.
    <footer>
        <div class="quote-author">
            <div class="quote-author__picture">
                <img src="/images/text/2.jpg" alt="">
            </div>
            <div class="quote-author__info">
                <cite class="quote-author__name">Алексей Петров</cite>
                <div class="quote-author__position">Профессиональный повар</div>
            </div>
        </div>
    </footer>
</blockquote>
