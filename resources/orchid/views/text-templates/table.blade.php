<table>
    <tr>
        <th>Как выбрать изготовителя?</th>
        <th>Как подобрать упаковку?</th>
    </tr>
    <tr>
        <td>
            Выбрать изготовителя очень просто&nbsp;&mdash; главное, чтобы он&nbsp;отвечал Вашим критериям, об&nbsp;остальном позаботились&nbsp;мы.
        </td>
        <td>
            Упаковка для готовых блюд&nbsp;&mdash;
            это отдельная история. Иногда совсем не&nbsp;хочется отдавать блюда
            в&nbsp;контейнерах, потому что это сказывается либо на&nbsp;себестоимости, либо на&nbsp;цене готового блюда,
            что тоже не&nbsp;очень-то радует.
        </td>
    </tr>
    <tr>
        <td>
            Регистрируясь на&nbsp;сервисе, изготовители проходят проверку администрацией сайта&nbsp;&mdash; загружают медицинскую книжку при ее&nbsp;наличии, а&nbsp;также показывают фотографию своей кухни.
        </td>
        <td>
            Вы&nbsp;можете найти подходящую упаковку в&nbsp;магазинах по&nbsp;запросу &laquo;пищевая упаковка&raquo; в&nbsp;2ГИС, Яндекс.картах и&nbsp;Google Maps.
            Также упаковку можно найти
            в&nbsp;гипермаркетах, например Лента, Ашан или Мetro.
        </td>
    </tr>
</table>
