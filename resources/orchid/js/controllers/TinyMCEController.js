import TinyMCE from "tinymce";
import 'tinymce/icons/default';
import 'tinymce/themes/silver';
import 'tinymce/models/dom';
import 'tinymce/plugins/code';
import 'tinymce/plugins/link';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/table';
import 'tinymce/plugins/template';
import 'tinymce/plugins/image';
import 'tinymce/skins/ui/oxide/skin.min.css';

export default class extends window.Controller {

    async connect() {

        const editor = await TinyMCE.init({
            target: this.element,
            plugins: 'code link lists table component template image',
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | bullist numlist | image link table | code | template component',
            skin: false,
            forced_root_block : 'div',
            content_css: '/build/css/style-editor.css?' + new Date().getTime(),
            body_class: 'text-content',
            language_url: '/langs/ru.js',
            language: 'ru_RU',
            base_url: '/orchid',
            relative_urls: false,

            external_plugins: {
                component: '/orchid/plugins/component/plugin.js?' + new Date().getTime(),
            },

            // параметры для плагина компонентов
            component_list_url: '/admin/text/component%componentName%',

            // Параметры для плагина шаблонов
            templates: '/admin/text/templates',
            height: this.element.dataset.height || '480',
        });
    }

    disconnect() {
        TinyMCE.activeEditor.destroy();
    }
}
