const path = require('path');

module.exports = (isProd) => {
    const webpack = require('webpack');

    return {
        context: __dirname,
        entry: __dirname + '/orchid/js/app.js',
        output: {
            path: path.join(__dirname, '../public/orchid/js'),
            publicPath: '/orchid/js/',
            filename: 'app.bundle.js?[chunkhash]',
            pathinfo: false,
        },

        watch: !isProd,
        mode: isProd ? 'production' : 'development',
        devtool: false,

        cache: {
            type: 'filesystem',
            cacheDirectory: path.resolve(__dirname, 'node_modules', '.cache', 'webpack-admin'),
        },

        stats: {
            colors: true,
        },

        module: {
            unknownContextCritical: false,
            rules: [
                {
                    test: /\.js|\.ts$/,
                    use: {
                        loader: 'esbuild-loader',
                        options: {
                            loader: 'ts',
                            minify: isProd,
                            legalComments: 'none',
                            target: 'es2015',
                        },
                    },
                },
                {
                    test: /(\.less|\.css)$/,
                    use: [
                        'vue-style-loader',
                        'css-loader',
                        'less-loader',
                    ],
                },
            ],
        },

        optimization: {
            runtimeChunk: false,
        },

        performance: {
            hints: false,
            maxEntrypointSize: 1024 * 1024,
            maxAssetSize: 1024 * 1024,
        },

        resolve: {
            extensions: ['.ts', '.js'],
            alias: {
                '~': path.resolve(__dirname) + '/js',
                '@': path.resolve(__dirname) + '/js',
            },
        },

        plugins: [
            new webpack.NoEmitOnErrorsPlugin(),
        ],
    };
};
