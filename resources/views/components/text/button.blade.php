@php
/** @var string $caption */
/** @var string $url */
@endphp

<a href="{{ $url }}" class="btn">
    <span class="btn__back">{{ $caption }}</span>
</a>
