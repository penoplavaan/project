@php
/** @var \Illuminate\Support\Collection|string[] $slideList */
if ($slideList->isEmpty()) return '';
@endphp
{!! vueTag('vue-gallery', ['items' =>  $slideList, 'cssClass' => 'text-slider']) !!}
