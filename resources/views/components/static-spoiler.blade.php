<div class="static-spoiler js-static-spoiler" data-show="{{$showText}}" data-hide="{{$hideText}}">
    <div class="static-spoiler__content js-static-spoiler-content">
        <div class="static-spoiler__inner js-static-spoiler-inner">
            {{ $slot }}
        </div>
    </div>
    <a href="javascript:void(0)"
       class="js-static-spoiler-btn static-spoiler__toggle link link--dashed">{{$showText}}</a>
</div>
