@php
use \App\Models\TextPage;

/** @var TextPage $textPage */

$pageClass = 'text';
@endphp

@extends('layout.text')

@section('content')
    <h1 class="h1 page__h1">{{ $textPage->title }}</h1>

    {!! $textPage->getText() !!}
@endsection

