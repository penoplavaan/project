@php
    /* @var bool                           $isCook */
    /* @var \Illuminate\Support\Collection $slides */
@endphp

<section class="main-promo scroll-class js-scroll-class hidden-mobile">
    <div class="main-promo__grid">
        <div class="main-promo__caption">
            <div class="main-promo__label">{!! textBlock('main.promo')->title !!}</div>
            <div class="h0 main-promo__title">{!! textBlock('main.promo')->text1 !!}</div>
        </div>
        <div class="main-promo__text-wrap">
            <div class="main-promo__text">{!! textBlock('main.promo')->text2 !!}</div>
            <a class="btn main-promo__btn" href="/menu">
                <span class="btn__text">Выбрать блюда</span>
            </a>
            @if(!$isCook)
                <div class="main-promo__cta">
                    Вы&nbsp;повар?
                    &mdash;&nbsp;<a href="{!! setting('main.promo-link')->text !!}" class="link link--hover-uline">давайте с&nbsp;нами!</a>
                </div>
                <div class="main-promo__cta-comment">
                    Регистрация займет всего 3&nbsp;минуты
                </div>
            @endif
        </div>
        <div class="main-promo__right-col main-promo__slider">
            {!! vueTag('vue-promo-slider', [
                'items'          => $slides,
                'useScrollClass' => true,
            ]) !!}
        </div>
    </div>
</section>
