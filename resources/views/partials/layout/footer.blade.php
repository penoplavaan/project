@php
    $isShort = false; // todo

    $seoService = app()->make(\App\Services\SeoService::class);
    $footerSeoTextBlock = textBlock($seoService->getFooterTextCode());
@endphp

<footer class="footer js-footer {{ $isShort ? 'footer--short' : '' }}">
    <div class="content-container footer__grid">
        @if (!$isShort && $footerSeoTextBlock)
            <div class="footer__text-wrap">
                <div class="text-content footer__text">
                    {!! $footerSeoTextBlock->text1 !!}
                </div>
                <a href="{!! setting('footer.more.link')->text !!}" class="btn btn--clear footer__btn">
                    <span class="btn__text">{!! $footerSeoTextBlock->title !!}</span>
                </a>
            </div>
        @endif
        @if(isset(setting('footer.apple.link')->text) || isset(setting('footer.google.link')->text))
            <div class="footer__app app-block">
                <div class="app-block__buttons">
                    @if(isset(setting('footer.apple.link')->text))
                        <a href="{!! setting('footer.apple.link')->text !!}"
                           class="link app-block__btn">{!! getSymbol('app-store') !!}</a>
                    @endif
                    @if(isset(setting('footer.google.link')->text))
                        <a href="{!! setting('footer.google.link')->text !!}"
                           class="link app-block__btn">{!! getSymbol('google-play') !!}</a>
                    @endif
                </div>
                <div class="app-block__text">
                    {!! textBlock('footer.text.socials')->text1 !!}
                </div>
        @endif

                <div class="footer__socials">
                {!! vueTag('vue-socials', ['items' => $footerSocials, 'inFooter' => true, 'color' => 'dark']) !!}
            </div>
        </div>

        <div class="footer__copy">
            <a class="footer__tel" href="tel:{!! setting('footer.phone')->text !!}">{!! setting('footer.phone')->text !!}</a>
            <div class="footer__label">  &copy;{{ date('Y') }} <strong>Повар&nbsp;на&nbsp;связи</strong></div>
        </div>

        <div class="footer__terms">
            <a href="mailto:{!! setting('footer.email')->text !!}" class="footer__email link link--uline" >{!! setting('footer.email')->text !!}</a>
            @foreach ($footerTermsMenu as $menuItem)
                <a href="{{ $menuItem->url }}" class="link link--uline">{{ $menuItem->title }}</a>
            @endforeach
        </div>

        <div class="footer__dev">
            <a href="https://www.sibirix.ru" target="_blank" rel="nofollow" class="link link--with-icon developer js-developer">
                <span class="link__text">Разработка сайта — студия «Сибирикс»</span>
                <span class="link__icon footer__dev-icon-wrap">
                    <img src="{{ getImage('dev.svg') }}" data-gif="{{ getImage('pizzaslon.gif') }}" alt="Sibirix" class="developer__dev-svg">
                    <img src="{{ getImage('pizzaslon.gif') }}" data-gif="{{ getImage('pizzaslon.gif') }}" alt="Sibirix" class="developer__dev-gif js-dev-gif">
                </span>
            </a>
        </div>
    </div>
</footer>

<nav class="foot-nav page__foot-nav">
    <div class="foot-nav__items">
        @foreach ($footerMobileMenu as $menuItem)
            <a href="{{ $menuItem->url }}" class="foot-nav__item link">
                <span class="foot-nav__icon">
                    {!! getSymbol($menuItem->code) !!}
                </span>
                <span class="foot-nav__name">{{ $menuItem->title }}</span>
            </a>
        @endforeach
    </div>
</nav>

