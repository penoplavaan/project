@php
    /** @var \App\Models\MenuItem[] $headerTopMenu */
@endphp
<nav class="header__nav header-nav">
    <ul class="header-nav__ul">
        @foreach ($headerTopMenu as $menuItem)
            <li class="header-nav__li">
                <a href="{!! $menuItem->url !!}"
                   class="link link--hover-uline link--with-icon header-nav__link {!! isset($level1['current']) && $level1['current'] ? 'link--current' : '' !!}">
                    <span class="link__text">{{ $menuItem->title }}</span>

                    @if ($menuItem->getManualSubmenu())
                        <span class="link__icon">
                            {!! getSymbol('spoiler', 'symbol header-nav__arrow') !!}
                        </span>
                    @endif
                </a>

                @if ($menuItem->getManualSubmenu())
                    <div class="header-nav__sub dropdown">
                        <ul class="dropdown__ul">
                            @foreach ($menuItem->getManualSubmenu() as $menuSubItem)
                                <li class="dropdown__li">
                                    <a href="{{ $menuSubItem['url'] }}"
                                       class="link dropdown__link {{ $menuSubItem['current'] ? 'link--current' : '' }}">
                                        {{ $menuSubItem['name'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </li>
        @endforeach
    </ul>

    <div class="header-nav__phone">
        @if (setting('header.phone') && !empty(setting('header.phone')->text))
            <a class="header-nav__tel" href="tel: {{ clearPhone(setting('header.phone')->text) }}">
                {{ setting('header.phone')->text }}
            </a>
        @endif
    </div>
</nav>
