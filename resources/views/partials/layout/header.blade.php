@php
    /** @var \App\Models\MenuItem $headerTopMenu */
    /** @var int $headerCityDetection */
    /** @var \App\Models\City $headerCity */

    $currentLocation = $headerCity->name;
    $confirmedLocation = $headerCityDetection === \App\Services\Geo\Detector::DETECT_COOKIE;
@endphp
<div class="header-wrapper">
    <div class="header-wrapper__fixed-anchor js-header-fixed-anchor"></div>

    <header class="header js-header {{$pageClass == 'main' ? ' header--transparent' : ''}}">
        <div class="content-container header__grid">

            <div class="header__location">
                {!! vueTag('vue-location-btn', ['current' => $currentLocation]) !!}
            </div>

            {{ view('partials.layout.header-nav') }}

            <div class="header__user">
                {!! vueTag('vue-auth-block', ['phone' => clearPhone(setting('header.phone')->text)]) !!}
            </div>

            <div class="header__logo">
                <a href="/" class="logo">
                    <span class="logo__img">{!! getSymbol('logo', 'symbol') !!}</span>
                    <span class="logo__text">{!! getSymbol('logo-text', 'symbol') !!}</span>
                </a>

                @if (!$confirmedLocation)
                    {!! vueTag('vue-confirm-location', ['current' => $currentLocation]) !!}
                @endif
            </div>

            <div class="header__buttons">
                @if (!$isCook)
                    <button class="btn btn--grey header__btn js-btn-become-cook">
                        <span class="btn__text">Стать поваром</span>
                    </button>
                @endif
                {{-- <a href="javascript:void(0);" class="btn header__btn">
                    <span class="btn__text">Создать заказ</span>
                </a>--}}
            </div>

            <div class="header__menu-btn">
                {!! vueTag('vue-menu-btn') !!}
            </div>
        </div>
        {!! vueTag('vue-menu', [
            'currentLocation' => $currentLocation,
            'items' => \App\Http\Resources\TopMenuItemsResource::collection($headerTopMenu),
            'phone' => setting('header.phone')->text,
            'socials' => $footerSocials,
        ])  !!}
        @if (!$confirmedLocation)
            {!! vueTag('vue-confirm-location-mobile', ['current' => $currentLocation]) !!}
        @endif

    </header>
</div>
