@php
    $caption = $caption ?? '';
    $postfix = $postfix ?? '';
@endphp
<h1 class="h1 page__page-caption">
    {{ $caption }}
    {{ $postfix }}
</h1>
