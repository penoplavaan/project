<div class="reviews page__section">
    <h4 class="h4 cook-detail__header cook-detail__header--with-btn">
        Отзывы покупателей
        @if ($reviewsCount > 0)
            <span class="cook-detail__header-count">
                {{ $reviewsCount . ' отзыв' . \App\Helpers\ViewHelper::getWordForm($reviewsCount, ['', 'а', 'ов'])}}
            </span>
        @endif

        @if ($canAddReview)
            <button class="btn cook-detail__header-btn js-open-review-form" data-cook-id="{{ $cook->id ?? $cook['id'] }}">
                Оставить свой отзыв
            </button>
        @endif
    </h4>

    @if ($reviews && $reviews->count() > 0)
        {!! vueTag('vue-cook-reviews', [
            'cookId'     => $cook->id ?? $cook['id'],
            'reviews'    => $reviews,
            'totalPages' => $totalPages,
        ]) !!}
    @else
        Здесь будут опубликованы отзывы покупателей
    @endif
</div>
