@php
/** @var array $breadcrumbs */
$count = count($breadcrumbs);

if ($count <= 1) return '';

$class = $class ?? '';
@endphp

<nav class="breadcrumbs page__breadcrumbs">
    <div class="content-container">
        <div class="breadcrumbs__list custom-scrollbar custom-scrollbar--hidden">
            @foreach ($breadcrumbs as $index => $breadcrumb)
                @if ($index < $count - 1)
                    <a href="{{ $breadcrumb['link'] }}" class="breadcrumbs__item link link--uline link--with-icon">
                        <span class="link__text">{{ $breadcrumb['name'] }}</span>
                        {!! getSymbol('breadcrumb-arrow', 'breadcrumbs__icon') !!}
                    </a>
                @else
                    <span class="breadcrumbs__item breadcrumbs__item--disabled">
                        {{ $breadcrumb['name'] }}
                    </span>
                @endif
            @endforeach
        </div>
    </div>
</nav>
