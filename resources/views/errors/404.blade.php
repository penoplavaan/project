@php
    $pageClass = 'not-found';
    $pageType = 'error-404';
@endphp

@extends('layout.base')

@section('structure')
    <div class="content-container error-404 scroll-class js-scroll-class">
        <div class="error-404__micro-header">{!! textBlock('error.404')->title !!}</div>

        <div class="error-404__banner js-parallax">
            <div class="error-404__plate">
                <img class="error-404__svg-text" src="{!! getImage('error-404.svg') !!}" alt="">
            </div>
            <div class="error-404__image-wrap" data-depth="0.2">
                <img class="error-404__image" src="{!! getImage('error-404-cake.png') !!}" alt="">
            </div>
        </div>

        <div class="error-404__content">
            <h1 class="h0 error-404__header">{!! textBlock('error.404')->text1 !!}</h1>
            <div class="error-404__description text-content">
                {!! textBlock('error.404')->text2 !!}
            </div>
            <div class="error-404__out-button">
                <a href="{{ setting('404.button')->getText() }}" class="btn"><span class="btn__text">{!! setting('404.button')->getBigText() !!}</span></a>
            </div>
            <div class="error-404__go-main">
                {!! setting('404.text')->getBigText() !!}
            </div>
        </div>
    </div>
@endsection
