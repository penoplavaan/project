<div class="product-main page__section">
    <div class="product-main__grid product-detail">
        @include('menu.main.gallery')
        @include('menu.main.buy')
        @include('menu.main.about')
        @include('menu.main.socials')
        @include('menu.main.cook')
    </div>
</div>
