@php
/** @var array $dishes */
/** @var array $pageData */
/** @var array $filterData */
/** @var array $filterValues */

$pageClass = 'product-list';

@endphp

@extends('layout.inner')

@section('outer-content')
    <section class="main-promo scroll-class js-scroll-class page__section">
        <div class="content-container">
            <h1 class="h1 page__page-caption">
                {{ $h1 ?: 'Все блюда' }}<sup class="js-total-count">{{ $pageData['total'] }}</sup>
            </h1>
        </div>

        {!! vueTag('vue-product-list', [
            'initialDishes'             => $dishes,
            'initialPageData'           => $pageData,
            'filterParams'              => $filterData,
            'initialFilter'             => $filterValues,
            'initialPromoCards'         => $promoCards,
            'navItems'                  => [],
            'additionalEmptyResultText' => setting('site.catalog-empty-result-additional-text')->getBigText(),
        ]) !!}
    </section>
@endsection
