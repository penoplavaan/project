<div class="product-main__about product-main__block product-about">
    @if(!empty($dish['previewText']))
        <div class="product-about__block">
            <div class="product-about__title">
                <div>Состав</div>
            </div>
            {{ $dish['previewText'] }}
        </div>
    @endif


    <div class="product-about__block">
        <div class="product-about__title">
            <div>Условия доставки</div>
        </div>
        @if(!empty($dish['delivery']))
            {{ $dish['delivery'] }}
        @elseif(!empty($dish['cook']['delivery']))
            {{ $dish['cook']['delivery'] }}
        @else
            {!! setting('dish.delivery-default')->getBigText() !!}
        @endif
    </div>

    <x-static-spoiler>
        @if(!empty($dish['storageConditions']))
            <div class="product-about__block">
                <div class="product-about__title">
                    <div>Условия хранения</div>
                </div>
                {{ $dish['storageConditions'] }}
            </div>
        @endif
        <div class="product-about__block">
            <div class="product-about__params">
                @if(!empty($dish['weight']) || !empty($dish['distance']) || !empty($dish['quantity']) || !empty($dish['cookingTime']) || !empty($dish['package']))
                    <div class="product-about__params-item">
                        <div class="product-about__title">Инфо</div>
                        <div class="product-params">
                            @if(!empty($dish['weight']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Вес</div>
                                    <div class="product-params__value">{{ $dish['weight'] }}</div>
                                </div>
                            @endif
                            @if(!empty($dish['distance']))
                                <div class="product-params__item todo">
                                    <div class="product-params__name">Расстояние</div>
                                    <div class="product-params__value">
                                        <div class="product-cook-location">
                                            {!! getSymbol('location', 'symbol product-cook-location__symbol')  !!}
                                            {{ $dish['distance'] }}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(!empty($dish['quantity']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Количество порций</div>
                                    <div class="product-params__value">
                                        {{ $dish['quantity'] }}
                                    </div>
                                </div>
                            @endif
                            @if(!empty($dish['cookingTime']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Время приготовления</div>
                                    <div class="product-params__value">
                                        {{ $dish['cookingTime'] }}
                                    </div>
                                </div>
                            @endif
                            @if(!empty($dish['package']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Упаковка</div>
                                    <div class="product-params__value">
                                        {{ $dish['package'] }}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
                @if(!empty($dish['calories']) || !empty($dish['protein']) || !empty($dish['fats']) || !empty($dish['carbohydrates']))
                    <div class="product-about__params-item">
                        <div class="product-about__title">Пищевая ценность на 100&nbsp;г</div>
                        <div class="product-params">
                            @if(!empty($dish['calories']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Калорийность</div>
                                    <div class="product-params__value">{{ $dish['calories'] }}</div>
                                </div>
                            @endif
                            @if(!empty($dish['protein']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Белки</div>
                                    <div class="product-params__value">{{ $dish['protein'] }}</div>
                                </div>
                            @endif
                            @if(!empty($dish['fats']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Жиры</div>
                                    <div class="product-params__value">{{ $dish['fats'] }}</div>
                                </div>
                            @endif
                            @if(!empty($dish['carbohydrates']))
                                <div class="product-params__item">
                                    <div class="product-params__name">Углеводы</div>
                                    <div class="product-params__value">{{ $dish['carbohydrates'] }}</div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="product-about__block product-about__tags">
            @if(!empty($dish['tags']))
                <div>Теги:</div>
                @foreach($dish['tags'] as $tag)
                    <a href="{{ route('menu.index') . '?tags[0]=' . $tag['id'] }}"
                       class="btn btn--sm btn--tag">
                        <span class="btn__text">{{ $tag['name'] }}</span>
                    </a>
                @endforeach
            @endif
        </div>
    </x-static-spoiler>

    <div class="product-main__buy hidden-mobile">
        {!! vueTag('vue-buy-block', [
            'item' => $dish,
        ]) !!}
    </div>
</div>
