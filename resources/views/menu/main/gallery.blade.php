<div class="product-main__gallery">
    {!! vueTag('vue-product-picture-slider', [
        'items' => $dish['gallery'],
        'labels' => $dish['labels'],
    ]) !!}
</div>
