<div class="product-main__buy visible-mobile">
    {!! vueTag('vue-buy-block', [
        'item' => $dish,
    ]) !!}
</div>
