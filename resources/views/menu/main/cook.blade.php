<div class="product-main__cook">
    {!! vueTag('vue-cook-card', [
        'cook'          => $dish['cook'],
        'onDetail'      => true,
        'withBigRating' => true,
        'phone'         =>  setting('header.phone')->getText(),
    ]) !!}
</div>
