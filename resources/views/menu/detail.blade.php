@php
    $pageClass = 'product-detail';
@endphp

@extends('layout.inner')

@section('content')
    @include('partials.caption')
    @include('menu.main')
    @include('partials.reviews', [
       'cook'       => $dish['cook'],
       'reviews'    => $reviews,
       'totalPages' => $totalPages
       ])

    @if (!$products->isEmpty())
        @include('menu.products')
    @endif
@endsection
