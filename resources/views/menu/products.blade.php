<div class="products-slider-section page__section">
    <h3 class="h3 products-slider-section__caption">другие блюда этого повара</h3>
    {!! vueTag('vue-products-slider', [
        'items' => $products,
        'btn'   => ['name' => 'Все блюда', 'url' => route('cooks.detail', ['cook' => $dish['cook']['id']]) . '#menu'],
        'onDetail' => true,
    ]) !!}
</div>

