@php
    /** @var \App\Models\ForCookHowWork[]|\Illuminate\Support\Collection $howWorks */
    /** @var \App\Models\ForCookFeature[]|\Illuminate\Support\Collection $features */
    /** @var \App\Models\ForCookReview[]|\Illuminate\Support\Collection $reviews */
    /** @var \App\Models\ForCookSupport[]|\Illuminate\Support\Collection $supports */
    /** @var \App\Models\ForCookFaq[]|\Illuminate\Support\Collection $faqs */

    $pageClass = 'for-cooks';
@endphp

@extends('layout.inner')

@section('content')
<section class="main-promo scroll-class js-scroll-class page__section">
    <div class="main-promo__grid">
        <div class="main-promo__caption">
            <div class="main-promo__label">{{ textBlock('for-cooks.top1')->title }}</div>
            <div class="h0 main-promo__title">
                {!! textBlock('for-cooks.top1')->text1 !!}
            </div>
        </div>
        <div class="main-promo__right-col main-promo__rocket promo-rocket scroll-class js-scroll-class js-parallax">
            <div class="promo-rocket__circle" data-depth="0.1"></div>
            <div class="promo-rocket__rocket" data-depth="0.3"></div>
            <div class="promo-rocket__logo" data-depth="0.2"></div>
        </div>

        <div class="main-promo__text-wrap">
            <div class="h3 main-promo__subtitle">
                {!! textBlock('for-cooks.top2')->title !!}
            </div>
            <div class="main-promo__text">
                {!! textBlock('for-cooks.top2')->text1 !!}
            </div>
            <button class="btn main-promo__btn js-register-btn">
                <span class="btn__text">Регистрация в&nbsp;сервисе</span>
            </button>
            <div class="main-promo__cta-comment">
                {!! textBlock('for-cooks.top2')->text2 !!}
            </div>
        </div>
    </div>
    @if(!$howWorks->isEmpty())
        {{ view('for-cooks.how-work', ['howWorks' => $howWorks]) }}
    @endif
    @if(!$features->isEmpty())
        {{ view('for-cooks.features', ['features' => $features]) }}
    @endif
    @if(!$reviews->isEmpty())
        {{ view('for-cooks.reviews',  ['reviews' => $reviews]) }}
    @endif
    @if(!$supports->isEmpty())
        {{ view('for-cooks.support',  ['supports' => $supports]) }}
    @endif
    @if(!empty($formData))
        {{ view('for-cooks.registration', ['formData' => $formData]) }}
    @endif
    @if(!$faqs->isEmpty())
        {{ view('for-cooks.faq', ['faqs' => $faqs]) }}
    @endif
</section>
@endsection
