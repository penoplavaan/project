@php
    /** @var array $formData */
@endphp

<section class="registration-section page__section scroll-class js-scroll-class js-registration-block">
    <div class="registration-section__content">
        <div class="registration-section__back js-parallax">
            <div data-depth=".1" class="registration-section__donut"></div>
            <div data-depth="-.1" class="registration-section__tacos"></div>
        </div>
        <div class="h1 registration-section__caption">Регистрация в&nbsp;сервисе</div>
        <div class="registration-section__form">
            {!! vueTag('vue-registration-form', [
                'formData'         => $formData,
                'submitcolor'      => 'clear-white',
                'additionalStyles' => 'form--white'
            ]) !!}
        </div>
    </div>
</section>
