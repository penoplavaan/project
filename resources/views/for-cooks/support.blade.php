@php
    /** @var \App\Models\ForCookSupport[]|\Illuminate\Support\Collection $supports */
@endphp

<section class="service-support page__section scroll-class js-scroll-class">
    <div class="service-support__back js-parallax">
        <div data-depth="-0.2" class="service-support__circle"></div>
        <div data-depth="0.3" class="service-support__pizza"></div>
    </div>

    <div class="h1 service-support__caption">поддержка участников</div>

    <div class="service-support__items">
        {!! vueTag('vue-service-support-items', ['items' => \App\Http\Resources\ForCookSupportResource::collection($supports->slice(0, 4))]) !!}
    </div>
</section>
