@php
    /** @var \App\Models\ForCookHowWork[]|\Illuminate\Support\Collection $howWorks */
@endphp

<section class="how-work page__section scroll-class js-scroll-class">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">как работает сервис?</div>
        <div class="section-caption__text">
            &mdash;&nbsp;Максимально просто и&nbsp;прозрачно, честное слово!
        </div>
    </div>

    <div class="how-work__list">
        @foreach ($howWorks as $index => $item)
            <div class="how-work-item how-work__item scroll-class js-scroll-class">
                <div class="how-work-item__name">{!! $item->title !!}</div>
                <div class="how-work-item__text">{!! $item->text !!}</div>
                <div class="how-work-item__num"><span></span><span></span></div>
            </div>
        @endforeach

        <div class="how-work__btn-wrap">
            <button class="btn how-work__btn js-register-btn">Регистрация в&nbsp;сервисе</button>
        </div>
    </div>

    <div class="how-work__sushi js-parallax">
        <div class="how-work__sushi-layer" data-depth="-0.1"></div>
        <div class="how-work__sushi-layer" data-depth="-0.2"></div>
    </div>
</section>
