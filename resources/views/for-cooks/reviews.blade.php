@php
    /** @var \App\Models\ForCookReview[]|\Illuminate\Support\Collection $reviews */
@endphp

<section class="service-reviews page__section scroll-class js-scroll-class">
    <div class="h1 service-reviews__caption">{!! textBlock('for-cooks.cooks-review')->text1 !!}</div>
    <div class="service-reviews__slider">
        {!! vueTag('vue-service-reviews-slider', ['items' => \App\Http\Resources\ForCookReviewResource::collection($reviews)]) !!}
    </div>
</section>
