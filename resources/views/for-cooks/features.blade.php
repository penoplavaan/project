@php
    /** @var \App\Models\ForCookFeature[]|\Illuminate\Support\Collection $features */
@endphp


<section class="service-features page__section scroll-class js-scroll-class">
    <div class="service-features__cupcake-wrap js-parallax">
        <div class="service-features__cupcake" data-depth=".3"></div>
    </div>

    <div class="h1 service-features__caption">Ключевые фишки&nbsp;сервиса</div>

    {!! vueTag('vue-service-features-slider', ['items' => \App\Http\Resources\ForCookFeatureResource::collection($features)]) !!}
</section>
