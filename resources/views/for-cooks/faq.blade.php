@php
    /** @var \App\Models\ForCookFaq[]|\Illuminate\Support\Collection $faqs */
@endphp

<section class="faq-section page__section">
    <div class="faq-section__grid">
        <div class="faq-section__list">
            <div class="h1 faq-section__caption">ВОПРОСЫ<br/>и&nbsp;ОТВЕТЫ</div>

            {!! vueTag('vue-faq-items', ['items' => \App\Http\Resources\ForCookFaqResource::collection($faqs), 'cssClass' => 'faq-section__item']) !!}
        </div>
        <div class="faq-section__ask ask-block scroll-class js-scroll-class">
            <div class="ask-block__macaron js-parallax">
                <div data-depth="0.1" class="ask-block__macaron-1"></div>
                <div data-depth="0.2" class="ask-block__macaron-2"></div>
            </div>
            <div class="ask-block__content-wrap">
                <div class="ask-block__content">
                    <div class="ask-block__text">
                        <strong>{!! textBlock('for-cooks.faq')->title !!}</strong><br/>
                        {!! textBlock('for-cooks.faq')->text1 !!}
                    </div>
                    <button class="btn ask-block__btn js-ask-btn">Задать вопрос</button>
                </div>
            </div>
        </div>
    </div>
</section>
