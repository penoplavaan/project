@extends('layout.base')

@section('structure')
    @include('partials.breadcrumbs')

    <div class="content-container">
        @yield('content')
    </div>

    @yield('outer-content')
@endsection
