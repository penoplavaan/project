@php
    $pageClass = 'text';
@endphp

@extends('layout.base')

@section('structure')
    @include('partials.breadcrumbs')

    <div class="content-container text-page__content-container">
        <div class="text-content text-content--text-page page__section">
            @yield('content')
        </div>
    </div>
@endsection
