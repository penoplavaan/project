@php
/* @var string $pageClass */
/* @var string $title */
/* @var string $stylePath */

use App\Helpers\BaseHelper;
use App\Helpers\ViewHelper;

/** @var \App\Services\SeoService $seoService */
$seoService = app()->make(\App\Services\SeoService::class);
$title = $seoService->getMeta('title') ?: ($title ?? '');
$description = BaseHelper::truncateString($seoService->getMeta('description') ?: '', 200);
$title = !empty($title) ? $title . ' — Повар на связи' : 'Повар на связи';

$pageClass = $pageClass ?? 'main';
$pageType = $pageType ?? $pageClass;
$ogTags = $seoService->getMeta('og') ?: [];

@endphp
<!doctype html>
<html lang="ru"{!! $seoService->getMeta('og:prefix') !!}>
<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    <meta name="description" content="{{ $description }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="cmsmagazine" content="2c2951bb57cffc1481be768a629d3a6e"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! ViewHelper::pagePreloadFonts() !!}
    <link rel="apple-touch-icon" sizes="180x180" href="{{ P_IMAGES }}favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ P_IMAGES }}favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ P_IMAGES }}favicons/favicon-16x16.png">
    <link rel="manifest" href="{{ P_IMAGES }}favicons/site.webmanifest">
    <link rel="mask-icon" href="{{ P_IMAGES }}favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#f1f6fb">
    <meta name="theme-color" content="#ffffff">

    @if(\App\Helpers\BaseHelper::isProd())
        @include('layout.metrics')
    @endif

    @foreach ($ogTags as $type => $value)
        <meta property="{!! $type !!}" content="{{ $value }}" />
    @endforeach
    <link rel="stylesheet" href="{!! $stylePath !!}">
    {!! BaseHelper::jsApp() !!}
    {!! setting('seo.head')->getBigText() !!}
</head>
<body class="{{ $pageClass }}-page page">
@if(\App\Helpers\BaseHelper::isProd())
    @include('layout.seo-top')
@endif

<div class="page__wrapper js-page-wrapper">
    {!! vueTag('vue-sidebar') !!}
    @include('partials.layout.header')

    <div class="page__content-wrap">
        <main class="page__content">
            @yield('structure')
        </main>
    </div>

    <x-cookie />
    @include('partials.layout.footer')
</div>

<div class="vue-popup-controller"></div>

{!! ViewHelper::pageJsList($pageType) !!}
{!! setting('seo.body.end')->getBigText() !!}
</body>
</html>
