@php
    /** @var array<\App\Dto\Basket\BasketByCook> $basket */

    $pageClass = 'basket';
@endphp

@extends('layout.inner')

@section('content')
    <h1 class="h1 page__page-caption">
        Ваш заказ
    </h1>

    {!! vueTag('vue-basket-list', ['items' => App\Http\Resources\BasketByCookResource::collection($basket)]) !!}

@endsection
