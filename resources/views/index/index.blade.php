@php
    $pageClass = 'main';
@endphp

@extends('layout.base')

@section('structure')
    <div class="content-container">
        @include('partials.promoblock')
        @include('index.compilations')
        @include('index.reviews')
        @include('index.cooks')
        @include('index.join')
    </div>
@endsection
