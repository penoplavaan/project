@php
    $pageClass = 'main';

    /** @var array $compilations */
    /** @var \Illuminate\Support\Collection $slides */
    /** @var int $compilationCount */
@endphp

<section class="main-catalog page__section">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">{!! setting('main.section.caption')->text !!}</div>
        <div class="section-caption__text hidden-mobile">
            {!! setting('main.section.caption')->big_text !!}
        </div>
        {{-- на мобилках выводится текст из промика --}}
        <div class="section-caption__text visible-mobile">{!! textBlock('main.promo')->text2 !!}</div>
    </div>

{{--    @include('index.catalog-nav')--}}
    @if(!empty($promoCardsCompilations))
        {!! vueTag('vue-promocards-compilations', [
            'compilations'   => $promoCardsCompilations,
            'cardColor'      => setting('main.promo-button-background-color')->getColor(),
            'cardHoverColor' => setting('main.promo-button-background-hover-color')->getColor(),
        ]); !!}
    @endif

    {!! vueTag('vue-compilations', [
        'compilations' => array_slice($compilations, 0, 3),
        'slides' => $slides,
        'showMore' => $compilationCount > 3,
    ]); !!}
</section>
