@php
    /* @var array $textBlocks */
@endphp

<section class="page__section">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">{!! textBlock('main.cooks')->title !!}</div>
        <div class="section-caption__text">{!! textBlock('main.cooks')->text1 !!}</div>
    </div>

    {!! vueTag('vue-cooks-slider', [
        'items' => $mainSliderCooks,
        'btn' => ['name' => 'Все повара', 'url' => route('cooks.index')],
        'useScrollClass' => true,
    ]) !!}
</section>
