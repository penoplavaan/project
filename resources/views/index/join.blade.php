@php
    /* @var array $isCook */
    /* @var array $isAutorized */
@endphp

<section class="join-section page__section">
    <div class="join-section__text-wrap">
        <div class="page__section-caption section-caption">
            <div class="h1 section-caption__h1">{!! textBlock('main.join')->title !!}</div>
            <div class="section-caption__text">{!! textBlock('main.join')->text1!!}</div>
        </div>
        <div class="text-content join-section__text">
            {!! textBlock('main.join')->text2!!}
        </div>
        @if(!$isCook)
            @if($isAutorized)
                <div class="join-section__btn-wrap">
                    <button class="btn js-btn-become-cook-main">
                        <span class="btn__text">Стать поваром</span>
                    </button>
                </div>
            @else
                <div class="join-section__btn-wrap">
                    <button class="btn vue-popup" data-type="registration">
                        <span class="btn__text">Зарегистрироваться</span>
                    </button>
                </div>
            @endif
        @endif
    </div>
    <div class="join-section__picture-wrap">
        <div class="join-section__picture js-parallax">
            <div class="join-section__woman" data-depth="0.15"></div>
            <div class="join-section__logo" data-depth="0.3"></div>
        </div>
    </div>
</section>
