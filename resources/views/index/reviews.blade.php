@php
    /* @var array $reviews */
@endphp

<section class="page__section">
    <div class="page__section-caption section-caption">
        <div class="h1 section-caption__h1">{!! textBlock('main.reviews')->title !!}</div>
        <div class="section-caption__text">{!! textBlock('main.reviews')->text1 !!}</div>
    </div>

    <div class="about-reviews js-scroll-class scroll-class">
        <div class="about-reviews__text">{!! textBlock('main.reviews')->text2 !!}</div>

        <div class="about-reviews__hand-container">
            <div class="about-reviews__hand"></div>
        </div>

        <button class="about-reviews__btn btn btn--white js-leave-review">Оставить отзыв</button>
    </div>

    {!! vueTag('vue-reviews-slider', [
        'items' => $reviews,
        // 'btn' => ['name' => 'Все отзывы', 'url' => 'javascript:void(0);'], // todo кнопка скрыта, т.к. пока нет страницы всех отзывов
        'useScrollClass' => true,
    ]) !!}
</section>

