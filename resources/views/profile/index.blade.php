@php
    /** @var array $profileNavigation */

    /** @var array $userProfileData */
    /** @var array $formData */
    /** @var array $cookData */
    /** @var array $сookFormData */
    /** @var array $cookKitchen */
    /** @var array $documents */

    $pageClass = 'profile';
@endphp

@extends('layout.inner')

@section('content')
    {!! vueTag('vue-profile', [
        'navigation'   => $profileNavigation,

        'userData'     => $userProfileData,
        'formData'     => $formData,
        'cookData'     => $cookData ?? null,
        'cookFormData' => $сookFormData ?? null,
        'cookKitchen'  => $cookKitchen ?? null,
        'documents'    => $documents ?? null,

        'imageSrc'     => '/images/profile/info-image.png',
    ]) !!}
@endsection
