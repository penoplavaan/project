@php
    /** @var \App\Models\ProfileUsefulMaterial[] $materials */
    /** @var array $profileNavigation */
    /** @var array $chats */
    /** @var array $users */

    $pageClass = 'profile';
    $pageType = 'profile-chat';
@endphp

@extends('layout.inner')

@section('content')
    <div class="profile-grid page__section">
        {!! vueTag('vue-profile-tabs', [
            'navigation' => $profileNavigation
        ]) !!}

        @if ($chats)
            {!! vueTag('vue-profile-chat', [
                'initChats' => $chats,
                'initUsers' => $users,
            ]) !!}
        @else
            <div class="empty-chat">
                <div class="empty-chat__text-wrapper">
                    <div class="empty-chat__text">Здесь пока<br> нет чатов...</div>
                    <div class="empty-chat__caption">
                        Здесь появятся чаты с&nbsp;поварами, когда вы&nbsp;сделаете заказ. Детали заказа вы&nbsp;можете
                        уточнить в&nbsp;чате. Чтобы сделать заказ, нажмите кнопку &laquo;Заказать&raquo; на&nbsp;блюде
                    </div>
                </div>

                <div class="empty-chat__image-wrap">
                    <div class="empty-chat__image js-parallax">
                        <div data-depth="0.2">
                            <img src="<?= \App\Helpers\ViewHelper::getImage('empty-chat/potato.png') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection
