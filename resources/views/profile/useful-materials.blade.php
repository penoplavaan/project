@php
    /** @var \App\Models\ProfileUsefulMaterial[] $materials */
    /** @var array $profileNavigation */

    $pageClass = 'profile';
    $pageType = 'profile-useful-materials';
@endphp

@extends('layout.inner')

@section('content')
    <div class="profile-grid page__section">
        {!! vueTag('vue-profile-tabs', [
            'navigation' => $profileNavigation
        ]) !!}

        <div class="profile-cook-materials">
            <div class="profile-cook-materials__grid">
                <div>
                    <h1 class="h1 profile-cook-materials__header">Полезные материалы</h1>
                    @foreach ($materials as $index => $material)
                        <div class="profile-cook-materials__group @if ($index === 0) profile-cook-materials__group--first @endif">
                            @if ($material->title)
                                <h4 @if ($material->anchor) id="{!! $material->anchor !!}" @endif
                                class="h4 profile-cook-materials__subtitle">{!! $material->title !!}</h4>
                            @endif
                            @if ($material->text)
                                <div class="profile-cook-materials__description @if ($index === 0) text-content @endif">{!! $material->text !!}</div>
                            @endif

                            @if ($material->attachment && $material->attachment->count())
                                <div class="documents-list documents-list--sm-title documents-list--lg-margin">
                                    <div>
                                        <div class="documents-list__items">
                                            @foreach ($material->attachment as $attachment)
                                                <a href="{!! $attachment->relative_url !!}"
                                                   class="document link link--with-icon"
                                                   download="{!! $attachment->original_name !!}"
                                                >
                                                    {!! getSymbol('document', 'document__icon') !!}
                                                    <span class="link__text">{!! $attachment->original_name !!}</span>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
