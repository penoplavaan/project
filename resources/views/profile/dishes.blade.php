@php
    /** @var array $formData */
    /** @var array $dishes */
    /** @var boolean $preOpenAddDish */

    $pageClass = 'profile';
    $pageType = 'profile-dishes';
@endphp

@extends('layout.inner')
@section('content')
    {!! vueTag('vue-profile-tabs', [
        'navigation' => $profileNavigation
    ]) !!}

    {!! vueTag('vue-profile-dishes', [
        'products'   => $dishes,
        'formData'   => $formData,
        'preOpenAdd' => $preOpenAddDish,
    ]) !!}
@endsection
