@php
    /** @var \App\Models\Cook $cook */
    /** @var array $cookData */
    /** @var array $dishes */
    /** @var array $kitchenPhotos */
    /** @var array $certificates */
    /** @var \App\Http\Resources\CookReviewResource[]|\Illuminate\Support\Collection $reviews */
    /** @var int $totalPages */
    /** @var array $canAddReview */

    $pageClass = 'cook-detail';
@endphp

@extends('layout.inner')



@section('content')
    <div class="cook-detail">
        <div class="chef">
            {!! vueTag('vue-cook-info', ['cookData' => $cookData]) !!}
        </div>

        @if ($dishes)
            <div id="menu" class="hidden"></div>
            <div class="cook-detail__products">
                <h3 class="h3 cook-detail__header">Меню</h3>
                {!! vueTag('vue-cook-products', [
                    'products' => $dishes,
                    'editable' => false,
                    'onDetail' => true,
                ]) !!}
            </div>
        @endif

        @include('partials.reviews', [
            'cook'       => $cook,
            'reviews'    => $reviews,
            'totalPages' => $totalPages,
            ])

        @if ($kitchenPhotos)
            <div class="cook-detail__kitchen">
                <h4 class="h4 cook-detail__header">Моя кухня</h4>
                {!! vueTag('vue-cook-kitchen-slider', [
                    'items' => $kitchenPhotos,
                ]) !!}
            </div>
        @endif

        @if ($certificates && count($certificates))
            <div class="cook-detail__documents">
                {!! vueTag('vue-documents-list', [
                    'groups' => [
                        [
                            'groupName' => 'Дипломы и&nbsp;сертификаты',
                            'items' => $certificates
                        ]
                    ],
                    'blank' => true,
                    'lgMargin' => true,
                ]) !!}
            </div>
        @endif
    </div>
@endsection
