@php
    /** @var array $cookCoords */
    /** @var array $filterData */
    /** @var string $suggest */

    $pageClass = 'cook-list';
@endphp

@extends('layout.base')

@section('structure')
    @include('partials.breadcrumbs')

    <div class="content-container">
        <h1 class="h1 page__page-caption">
            Все повара<sup class="js-total-count">{{ count($cookCoords) }}</sup>
        </h1>
    </div>

    {!! vueTag('vue-cook-list', [
        'items'  => $cookCoords,
        'filter' => $filterData,
        'suggestText' => $suggest,
    ]) !!}

@endsection
