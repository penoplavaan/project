@php
    /** @var array $formData */
    /** @var array $basket */
    /** @var string $cookCity */
    $pageClass = 'order-success';
@endphp

@extends('layout.inner')

@section('content')
    <h1 class="h1 page__page-caption">
        БИНГО!
    </h1>

    {!! vueTag('vue-order-success', ['order' => $order, 'basket' => $basket]) !!}

@endsection
