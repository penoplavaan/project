@php
    /** @var array $formData */
    /** @var array $basket */
    /** @var string $cookCity */

    $pageClass = 'order';
@endphp

@extends('layout.inner')

@section('content')
    <h1 class="h1 page__page-caption">
        оформить заказ
    </h1>

    {!! vueTag('vue-order', [
        'formData' => $formData,
        'basket'   => $basket,
        'cookCity' => $cookCity,
        'cookAddress' => $cookAddress,
    ]) !!}
@endsection
