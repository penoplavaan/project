@component('mail::message')

@if (! empty($greeting))
# {{ $greeting }}
@endif

## Здравствуйте!

На сайте появилась новая заявка на обратный звонок.

@component('mail::button', ['url' => route('platform.resource.edit', ['resource' => 'callback-resources', 'id' => $callbackId]), 'color' => 'primary'])
Перейти
@endcomponent


@lang('mail.regards'),<br>
{{ config('app.ruName') }}

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang('mail.fallback-text') <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
