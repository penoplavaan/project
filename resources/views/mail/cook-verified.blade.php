@component('mail::message')

@if (! empty($greeting))
# {{ $greeting }}
@endif

## Здравствуйте!

Ваша учетная запись подтверждена, теперь вы можете добавлять блюда и принимать заказы от пользователей.
Перейдите в личный кабинет для добавления блюд.

@component('mail::button', ['url' => route('profile.index'), 'color' => 'primary'])
Перейти в личный кабинет
@endcomponent


@lang('mail.regards'),<br>
{{ config('app.ruName') }}

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang('mail.fallback-text') <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
