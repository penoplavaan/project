@extends('mail.base-html-layout')

@section('header')
    {!! $header !!}
@endsection

@section('subheader')
    {!! $subheader !!}
@endsection

@section('content')

@foreach ($paragraph as $line)
    <p style="font-family: Verdana,Arial,sans-serif;font-size: 16px;line-height: 24px;font-weight: 400;color: #111;margin-bottom: 20px;">{!! $line !!}</p>
@endforeach

@if ($list)
    <ol>
    @foreach ($list as $line)
        <li style="font-family: Verdana,Arial,sans-serif;font-size: 16px;line-height: 21px;font-weight: 400;color: #111;margin-bottom: 7px;">{!! $line !!}</li>
    @endforeach
    </ol>
@endif

<br><a href="{{ $url }}" class="button" style="padding: 8px 20px 8px 20px; background: #06C160; color: #fff; border-radius: 22px; font-size: 16px; line-height: 20px; font-weight: 700;">Перейти к заказу в админке сайта</a>

@endsection
