@php
    /** @var array $footerSocials */
    /** @var string $mailDomain */
@endphp
<!doctype html>
<html lang="ru"
      style="min-height: 100vh; font-family: sans-serif; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; height: 100%; line-height: 1.15;">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Повар На Связи</title>
    <style>
        a {
            color: #5dbbf9;
            text-decoration: none;
        }

        ol, ul {
            margin-bottom: 20px;
        }

        li {
            font-size: 16px;
            line-height: 24px;
            font-weight: 400;
            margin-bottom: 10px;
        }

        @media only screen and (max-device-width: 800px) {
            p {
                font-size: 22px;
                line-height: 30px;
            }

            li {
                font-size: 22px;
                line-height: 30px;
            }
        }
    </style>
</head>
<body style="margin: 0; padding: 0 0 30px 0; font: 400 16px/1.5 Verdana,Arial,sans-serif; line-height: 24px; background-color: #e5e5e5; letter-spacing: normal; color: #111; vertical-align: middle;">
<table style="border-spacing: 0; width: 100%; background-color: #e5e5e5; " width="100%">
    <tr>
        <td>
            <table style="border-spacing: 0; width: 100%; padding: 0;" width="100%">
                <tr>
                    <td>
                        <table style="border-spacing: 0; width: 740px; padding: 0 0 0 0; margin: 0 auto;" width="740">
                            <tr style="height: 100px; background: #06C160">
                                <td style="height: 100px; background: #06C160; padding: 0 36px 0 36px;">
                                    <table>
                                        <tr>
                                            <td style="padding-top: 11px;">
                                                <a href="{{ route('home') }}" target="_blank">
                                                    <img src="{{ $mailDomain  }}/images/email/logo.png" alt="">
                                                </a>
                                            </td>
                                            <td style="width:100%">&nbsp;</td>
                                            @foreach ($footerSocials as $social)
                                                <td style="padding-top: 19px">
                                                    <a href="{{ $social['url'] }}" target="_blank" style="margin-left: 80px;">
                                                        <img src="{{ $mailDomain . '/images/email/' . $social['code'] }}.png" alt="">
                                                    </a>
                                                </td>
                                            @endforeach
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="background: #fff; padding: 60px 36px 50px 36px;">
                                    <table style="width: 100%" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <h1 style="font-size: 40px; line-height: 48px; font-weight: 700; font-family: Verdana,Arial,sans-serif; color: #111; margin-top: 0; margin-bottom: 40px;">
                                                    @yield('header')
                                                </h1>
                                                <h2 style="font-size: 28px; line-height: 34px; font-weight: 700; font-family: Verdana,Arial,sans-serif; color: #111; margin-top: 0; margin-bottom: 20px;">
                                                    @yield('subheader')
                                                </h2>

                                                @yield('content')
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="background: #E6F9EF; padding: 40px 36px 50px 36px;">
                                    <table style="width: 100%" cellpadding="0" cellspacing="0" width="100%" border="0">
                                        @if(isset(setting('footer.apple.link')->text) || isset(setting('footer.google.link')->text))
                                            <tr>
                                                <td style="padding-bottom: 20px; height: 54px;">
                                                    @if(isset(setting('footer.apple.link')->text))
                                                        <a href="{!! setting('footer.apple.link')->text !!}">
                                                            <img src="{{ $mailDomain . '/images/email/appstore.png' }}" alt="AppStore" width="177" height="54">
                                                        </a>
                                                        &nbsp;&nbsp;&nbsp;
                                                    @endif
                                                    @if(isset(setting('footer.google.link')->text))
                                                        <a href="{!! setting('footer.google.link')->text !!}">
                                                            <img src="{{ $mailDomain . '/images/email/googleplay.png' }}" alt="Google Play" width="177" height="54">
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-bottom: 50px; font-family: Verdana,Arial,sans-serif; font-size: 18px; line-height: 27px;">
                                                    {!! textBlock('footer.text.socials')->text1 !!}
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td>
                                                <table style="width: 100%" cellpadding="0" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="font-family: Verdana,Arial,sans-serif; font-size: 14px; line-height: 21px;">©<?= date('Y') ?> <b>Повар на связи</b></td>
                                                        <td style="text-align: right; color: #06C160; font-family: Verdana,Arial,sans-serif; font-size: 14px; line-height: 21px;font-weight: 700;">
                                                            <a href="{{ route('home') }}" target="_blank" style="color: #06C160">
                                                                {!! $mailDomain !!}
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
