@component('mail::message')

@if (! empty($greeting))
# {{ $greeting }}
@endif

## Здравствуйте!

На сайте появилась новая заявка на обратную связь.

@component('mail::button', ['url' => route('platform.resource.edit', ['resource' => 'feedback-resources', 'id' => $feedbackId]), 'color' => 'primary'])
Перейти
@endcomponent


@lang('mail.regards'),<br>
{{ config('app.ruName') }}

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang('mail.fallback-text') <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
