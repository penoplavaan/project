@component('mail::message')

@if (! empty($greeting))
# {{ $greeting }}
@endif

## Здравствуйте!

На cайте появилась новая заявка на регистрацию повара.

@component('mail::button', ['url' => route('platform.resource.edit', ['resource' => 'cook-resources', 'id' => $cookId]), 'color' => 'primary'])
Перейти
@endcomponent


@lang('mail.regards'),<br>
{{ config('app.ruName') }}

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang('mail.fallback-text') <span class="break-all">[{{ $displayableActionUrl }}]({{ $actionUrl }})</span>
@endslot
@endisset
@endcomponent
