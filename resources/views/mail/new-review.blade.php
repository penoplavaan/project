@extends('mail.base-html-layout')

@section('header')
    {!! $header !!}
@endsection

@section('content')

@foreach ($paragraph as $line)
    <p style="font-family: Verdana,Arial,sans-serif;font-size: 16px;line-height: 24px;font-weight: 400;color: #111;margin-bottom: 20px;">{!! $line !!}</p>
@endforeach

@if ($text)
    <p>
    {{ $text }}
    </p>
@endif

<br><a href="{{ $url }}" class="button" style="padding: 8px 20px 8px 20px; background: #06C160; color: #fff; border-radius: 22px; font-size: 16px; line-height: 20px; font-weight: 700;">Перейти к отзыву в админке сайта</a>

@endsection
