'use strict';

const path     = require('path');
const gulp     = require('gulp');
const chokidar = require('chokidar');
const webpack  = require('webpack');
const sxGulpFactory = require('sx-gulpfile-utils');
const gulpMultiProcess = require('gulp-multi-process');

const BASE_PATH = './';
const options = {
    source: path.resolve(BASE_PATH),
    output: path.resolve('../public/'),
};

const webpackConfig = require(`${BASE_PATH}webpack.config.js`);
const sibirixGulpUtils = sxGulpFactory.make(options);
const webpackRunner    = sxGulpFactory.makeWebpack(webpack, webpackConfig, options);

const imageOptions = {
    output: path.resolve(BASE_PATH),
    spriteUrl: '/build/',
    spriteOutput: '/../public/build/',
    lessOutput: '/build/',
};

gulp.task('sprite', sibirixGulpUtils.spriteCompile(imageOptions));
gulp.task('symbol', sibirixGulpUtils.symbolCompile(Object.assign({}, imageOptions)));
gulp.task('less', sibirixGulpUtils.lessCompile({
    entry: '/css/style*.less',
    output: '/build/css/',
}));

gulp.task('all-styles', gulp.series(gulp.parallel('sprite', 'symbol'), 'less'));

// Webpack
gulp.task('webpack',              (cb) => webpackRunner(cb, false, false));
gulp.task('webpack-prod',         (cb) => webpackRunner(cb, true, false));
gulp.task('webpack-analyze-prod', (cb) => webpackRunner(cb, true, true));

// сборка статики для админки
const adminOptions = {
    source: path.resolve(BASE_PATH + '/orchid/'),
    // Для Laravel задать output: path.resolve('../public/')
    output: path.resolve('../public/orchid/'),
};
const webpackAdminConfig = require(`${BASE_PATH}webpack-admin.config.js`);
const webpackAdminRunner = sxGulpFactory.makeWebpack(webpack, webpackAdminConfig, adminOptions);
gulp.task('webpack-admin', (cb) => webpackAdminRunner(cb, false, false));
gulp.task('webpack-admin-prod',  (cb) => webpackAdminRunner(cb, true, false));

// Следим за изменениями на ФС
gulp.task('watch', function() {
    gulpMultiProcess(['all-styles', 'webpack', 'webpack-admin']);
    chokidar.watch(['images/sprite/**/*'], {ignoreInitial: true, cwd: BASE_PATH})
        .on('all', gulp.series('sprite'));

    chokidar.watch(['images/symbol/**/*'], {ignoreInitial: true, cwd: BASE_PATH})
        .on('all', gulp.series('symbol'));

    chokidar.watch(['css/**/*', 'build/**/*.less'], {ignoreInitial: true, cwd: BASE_PATH})
        .on('all', gulp.series('less'));

    chokidar.watch('js/page/*', {ignoreInitial: true, cwd: BASE_PATH})
        .on('add', gulp.series('webpack'))
        .on('unlink', gulp.series('webpack'));

    chokidar.watch('webpack.config.js', {ignoreInitial: true, cwd: BASE_PATH})
        .on('change', gulp.series('webpack'));
});

// Сборка всей статики в прод-версию
gulp.task('build', (cb) => gulpMultiProcess(['all-styles', 'webpack-prod', 'webpack-admin-prod'], cb));

gulp.task('default', gulp.series('watch'));

